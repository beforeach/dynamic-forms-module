/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionBuilder;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.BaseField;
import com.foreach.across.modules.dynamicforms.domain.definition.types.*;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldExpressionParser;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidatorFactory;
import com.foreach.across.modules.dynamicforms.domain.document.validation.validators.RequiredFieldValidatorBuilder;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyController;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.convert.TypeDescriptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.Field.builder;
import static infrastructure.DynamicFormsAssertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author Arne Vandamme
 * @since 0.0.2
 */
public class TestBuildDocumentDefinitions
{
	private DynamicDocumentDefinitionBuilder builder;

	@Before
	public void configureBuilder() {
		DynamicDocumentFieldValidatorFactory fieldValidatorFactory = new DynamicDocumentFieldValidatorFactory();
		fieldValidatorFactory.setValidatorBuilders( Collections.singletonList( new RequiredFieldValidatorBuilder() ) );

		DynamicTypeDefinitionFactory typeDefinitionFactory = new DynamicTypeDefinitionFactory();
		typeDefinitionFactory.setTypeDefinitionBuilders(
				Arrays.asList( new SimpleDynamicTypeDefinitionBuilder(), new FieldsetDynamicTypeDefinitionBuilder( new DynamicDataFieldSetTemplateRegistry() ) )
		);

		builder = new DynamicDocumentDefinitionBuilder( typeDefinitionFactory, fieldValidatorFactory, mock( DynamicDocumentFieldExpressionParser.class ) );
	}

	@Test
	public void fieldsetCollection() {
		val rawDefinition = RawDocumentDefinition
				.builder()
				.name( "my-doc" )
				.field( builder().id( "name" ).type( "string[]" ).build() )
				.field(
						builder()
								.id( "address" )
								.type( "fieldset[]" )
								.member(
										builder().field( builder().id( "street" ).type( "string" ).build() ).build()
								)
								.build() )
				.build();

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "data:document" ) ) )
				.hasFields( 3 )
				.withNextField(
						field -> field.hasId( "name" )
						              .hasCanonicalId( "name" )
						              .hasTypeName( "string[]" )
						              .hasTypeDescriptor( TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( String.class ) ) )
						              .hasNoInitializerExpression()
						              .isACollection( true )
						              .isAMember( false )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:name" );
						              } )
						              .hasFields( 0 )
				)
				.withNextField(
						field -> field.hasId( "address" )
						              .hasCanonicalId( "address" )
						              .hasTypeName( "fieldset[]" )
						              .hasTypeDescriptor( TypeDescriptor.collection( ArrayList.class, DynamicDataFieldSet.TYPE_DESCRIPTOR ) )
						              .hasNoInitializerExpression().isACollection( true )
						              .isAMember( false )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:address" );
						              } )
						              .hasFields( 0 )
				)
				.withNextField(
						field -> field.hasId( "address[]" )
						              .hasCanonicalId( "address[]" )
						              .hasTypeName( "fieldset" )
						              .hasTypeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
						              .hasNoInitializerExpression()
						              .isACollection( false )
						              .isAMember( true )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:address[]" );
						              } )
						              .satisfies( f -> assertThat( f.asRawDefinition().isSynthetic() ).isTrue() )
						              .hasFields( 1 )
						              .withNextField(
								              street -> street.hasId( "street" )
								                              .hasCanonicalId( "address[].street" )
								                              .hasTypeName( "string" )
								                              .hasTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
								                              .withPropertyDescriptor( d -> {
									                              assertThat( d.getName() ).isEqualTo( "field:address[].street" );
								                              } )
								                              .hasNoInitializerExpression()
								                              .isACollection( false )
								                              .isAMember( false )
						              )
				);
	}

	@Test
	public void nestedFieldsetCollection() {
		val rawDefinition = RawDocumentDefinition
				.builder()
				.name( "my-doc" )
				.field(
						builder()
								.id( "books" )
								.type( "fieldset[]" )
								.member(
										builder()
												.field( builder().id( "title" ).type( "string" ).build() )
												.field(
														builder()
																.id( "authors" )
																.type( "fieldset[]" )
																.member(
																		builder().field( builder().id( "name" ).type( "string" ).build() ).build()
																)
																.build()
												)
												.build()
								)
								.build()
				)
				.build();

		verifyNestedBooksCollection( rawDefinition );
	}

	@Test
	public void nestedFieldsetCollectionAsSharedTypes() {
		val rawDefinition = RawDocumentDefinition
				.builder()
				.name( "my-doc" )
				.type(
						BaseField.builder()
						         .id( "author" )
						         .baseType( "fieldset" )
						         .field( builder().id( "name" ).type( "string" ).build() )
						         .build()
				)
				.type(
						BaseField.builder()
						         .id( "book" )
						         .baseType( "fieldset" )
						         .field( builder().id( "title" ).type( "string" ).build() )
						         .field( builder().id( "authors" ).type( "author[]" ).build() )
						         .build()
				)
				.field(
						builder()
								.id( "books" )
								.type( "book[]" )
								.build()
				)
				.build();

		verifyNestedBooksCollection( rawDefinition );
	}

	@Test
	public void customCollectionType() {
		val rawDefinition = RawDocumentDefinition
				.builder()
				.name( "my-doc" )
				.type(
						BaseField.builder()
						         .id( "author" )
						         .baseType( "fieldset" )
						         .field( builder().id( "name" ).type( "string" ).build() )
						         .build()
				)
				.type(
						BaseField.builder()
						         .id( "book" )
						         .baseType( "fieldset" )
						         .field( builder().id( "title" ).type( "string" ).build() )
						         .field( builder().id( "authors" ).type( "author[]" ).build() )
						         .build()
				)
				.type(
						BaseField.builder()
						         .id( "listOfBooks" )
						         .baseType( "book[]" )
						         .build()
				)
				.field(
						builder()
								.id( "books" )
								.type( "listOfBooks" )
								.build()
				)
				.build();

		verifyNestedBooksCollection( rawDefinition );
	}

	private void verifyNestedBooksCollection( RawDocumentDefinition rawDefinition ) {
		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "data:document" ) ) )
				.hasFields( 2 )
				.withNextField(
						field -> field.hasId( "books" )
						              .hasCanonicalId( "books" )
						              .hasTypeName( "fieldset[]" )
						              .hasTypeDescriptor( TypeDescriptor.collection( ArrayList.class, DynamicDataFieldSet.TYPE_DESCRIPTOR ) )
						              .hasNoInitializerExpression().isACollection( true )
						              .isAMember( false )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:books" );
							              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
							              assertThat( d.isNestedProperty() ).isFalse();
						              } )
						              .hasFields( 0 )
				)
				.withNextField(
						field -> field.hasId( "books[]" )
						              .hasCanonicalId( "books[]" )
						              .hasTypeName( "fieldset" )
						              .hasTypeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
						              .hasNoInitializerExpression()
						              .isACollection( false )
						              .isAMember( true )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:books[]" );
							              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
							              assertThat( d.isNestedProperty() ).isTrue();
						              } )
						              .hasFields( 3 )
						              .withNextField(
								              title -> title
										              .hasId( "title" )
										              .hasCanonicalId( "books[].title" )
										              .hasTypeName( "string" )
										              .hasTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
										              .withPropertyDescriptor( d -> {
											              assertThat( d.getName() ).isEqualTo( "field:books[].title" );
											              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
											              assertThat( d.isNestedProperty() ).isTrue();
										              } )
										              .hasNoInitializerExpression()
										              .isACollection( false )
										              .isAMember( false )
						              )
						              .withNextField(
								              authors -> authors
										              .hasId( "authors" )
										              .hasCanonicalId( "books[].authors" )
										              .hasTypeName( "fieldset[]" )
										              .hasTypeDescriptor( TypeDescriptor.collection( ArrayList.class, DynamicDataFieldSet.TYPE_DESCRIPTOR ) )
										              .hasNoInitializerExpression()
										              .isACollection( true )
										              .isAMember( false )
										              .withPropertyDescriptor( d -> {
											              assertThat( d.getName() ).isEqualTo( "field:books[].authors" );
											              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
											              assertThat( d.isNestedProperty() ).isTrue();
										              } )
										              .hasFields( 0 )
						              )
						              .withNextField(
								              author -> author
										              .hasId( "authors[]" )
										              .hasCanonicalId( "books[].authors[]" )
										              .hasTypeName( "fieldset" )
										              .hasTypeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
										              .hasNoInitializerExpression()
										              .isACollection( false )
										              .isAMember( true )
										              .withPropertyDescriptor( d -> {
											              assertThat( d.getName() ).isEqualTo( "field:books[].authors[]" );
											              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
											              assertThat( d.isNestedProperty() ).isTrue();
										              } )
										              .hasFields( 1 )
										              .withNextField( name -> name.hasId( "name" )
										                                          .hasCanonicalId( "books[].authors[].name" )
										                                          .hasTypeName( "string" )
										                                          .hasTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
										                                          .withPropertyDescriptor( d -> {
											                                          assertThat( d.getName() )
													                                          .isEqualTo( "field:books[].authors[].name" );
											                                          assertThat( d.getController().getOrder() )
													                                          .isEqualTo( EntityPropertyController.BEFORE_ENTITY );
											                                          assertThat( d.isNestedProperty() ).isTrue();
										                                          } )
										                                          .hasNoInitializerExpression()
										                                          .isACollection( false )
										                                          .isAMember( false )
										              )
						              )
				);
	}

	@Test
	public void listOfListType() {
		val rawDefinition = RawDocumentDefinition
				.builder()
				.name( "my-doc" )
				.type(
						BaseField.builder()
						         .id( "book" )
						         .baseType( "fieldset" )
						         .field( builder().id( "title" ).type( "string" ).build() )
						         .build()
				)
				.type(
						BaseField.builder()
						         .id( "listOfBooks" )
						         .baseType( "book[]" )
						         .build()
				)
				.field(
						builder()
								.id( "books" )
								.type( "listOfBooks[]" )
								.build()
				)
				.build();

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "data:document" ) ) )
				.hasFields( 3 )
				.withNextField(
						field -> field.hasId( "books" )
						              .hasCanonicalId( "books" )
						              .hasTypeName( "fieldset[][]" )
						              .hasTypeDescriptor(
								              TypeDescriptor.collection(
										              ArrayList.class,
										              TypeDescriptor.collection( ArrayList.class, DynamicDataFieldSet.TYPE_DESCRIPTOR )
								              )
						              )
						              .hasNoInitializerExpression()
						              .isACollection( true )
						              .isAMember( false )
						              .hasMemberFieldDefinition( "books[]" )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:books" );
							              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
							              assertThat( d.isNestedProperty() ).isFalse();
						              } )
						              .hasFields( 0 )
				)
				.withNextField(
						field -> field.hasId( "books[]" )
						              .hasCanonicalId( "books[]" )
						              .hasTypeName( "fieldset[]" )
						              .hasTypeDescriptor( TypeDescriptor.collection( ArrayList.class, DynamicDataFieldSet.TYPE_DESCRIPTOR ) )
						              .hasNoInitializerExpression()
						              .isACollection( true )
						              .isAMember( true )
						              .hasMemberFieldDefinition( "books[][]" )
						              .hasCollectionFieldDefinition( "books" )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:books[]" );
							              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
							              assertThat( d.isNestedProperty() ).isTrue();
							              assertThat( d.getParentDescriptor().getName() ).isEqualTo( "field:books" );
						              } )
				)
				.withNextField(
						field -> field.hasId( "books[][]" )
						              .hasCanonicalId( "books[][]" )
						              .hasTypeName( "fieldset" )
						              .hasTypeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
						              .hasNoInitializerExpression()
						              .isACollection( false )
						              .isAMember( true )
						              .hasCollectionFieldDefinition( "books[]" )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:books[][]" );
							              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
							              assertThat( d.isNestedProperty() ).isTrue();
							              assertThat( d.getParentDescriptor().getName() ).isEqualTo( "field:books[]" );
						              } )
						              .hasFields( 1 )
						              .withNextField(
								              title -> title
										              .hasId( "title" )
										              .hasCanonicalId( "books[][].title" )
										              .hasTypeName( "string" )
										              .hasTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
										              .withPropertyDescriptor( d -> {
											              assertThat( d.getName() ).isEqualTo( "field:books[][].title" );
											              assertThat( d.getController().getOrder() ).isEqualTo( EntityPropertyController.BEFORE_ENTITY );
											              assertThat( d.isNestedProperty() ).isTrue();
											              assertThat( d.getParentDescriptor().getName() ).isEqualTo( "field:books[][]" );
										              } )
										              .hasNoInitializerExpression()
										              .isACollection( false )
										              .isAMember( false )
						              )
				);
	}
}
