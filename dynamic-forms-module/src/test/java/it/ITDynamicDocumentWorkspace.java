/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSetRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSet;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentRepository;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataMapper;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.BuildDynamicDocumentFormulaEvaluationContext;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;

import static java.time.Month.JANUARY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@ContextConfiguration(classes = ITDynamicDocumentWorkspace.CustomEvaluationContextConfiguration.class)
public class ITDynamicDocumentWorkspace extends AbstractFormsApplicationIT
{
	private static DynamicDefinitionDataSet dataSet;

	@Autowired
	private DynamicDefinitionDataSetRepository dataSetRepository;

	@Autowired
	private DynamicDefinitionRepository definitionRepository;

	@Autowired
	private DynamicDefinitionVersionRepository definitionVersionRepository;

	@Autowired
	private DynamicDocumentRepository documentRepository;

	@Autowired
	private DynamicDocumentService documentService;

	@Before
	public void createDataSet() {
		if ( dataSet == null ) {
			dataSet = new DynamicDefinitionDataSet();
			dataSet.setName( "Workspace Test DataSet" );
			dataSet.setKey( "test-ws" );
			dataSetRepository.save( dataSet );
		}
	}

	@Test
	public void workspaceTest() {
		val definition = createDocumentDefinition( "workspace-v1" );

		val workspace = documentService.createDocumentWorkspace( definition );
		assertThatWorkspaceIsLoadedWithBlankDocument( definition, workspace );

		val initialVersion = createInitialDocument( workspace );
		val updatedVersion = addDocumentVersionWithSameDefinitionVersion( workspace );
		assertThat( updatedVersion ).isNotEqualTo( initialVersion );

		val secondUpdate = updateSameVersionWithChanges( workspace );
		assertThat( secondUpdate ).isEqualTo( updatedVersion );

		val thirdUpdate = updateUsingImportFields( workspace );
		assertThat( thirdUpdate ).isNotNull();

		val manualCopy = createCopyManually( workspace );
		assertThat( manualCopy ).isNotNull();
		assertThat( manualCopy.getDocument() ).isNotEqualTo( thirdUpdate.getDocument() );

		exportDocuments( workspace, manualCopy );

		val definitionVersion = addDocumentDefinitionVersion( definition, "workspace-v2" );
		migrateExistingVersionToNewDefinition( workspace, definitionVersion );
	}

	@Test
	public void workspaceDocumentSwitching() {
		val defOne = createDocumentDefinition( "workspace-v1" );
		val defTwo = createDocumentDefinition( "workspace-v1" );

		val workspaceOne = documentService.createDocumentWorkspace( defOne );
		val workspaceTwo = documentService.createDocumentWorkspace( defTwo );
		val initialVersion = createInitialDocument( workspaceOne );
		val otherVersion = createInitialDocument( workspaceTwo );

		workspaceOne.loadNewDocument();
		assertThatWorkspaceIsLoadedWithBlankDocument( defOne, workspaceOne );

		workspaceOne.loadNewDocument( defTwo );
		assertThatWorkspaceIsLoadedWithBlankDocument( defTwo, workspaceOne );

		workspaceOne.loadDocument( initialVersion.getDocument() );
		assertThatWorkspaceIsLoadedWithDocument( initialVersion, workspaceOne );

		workspaceTwo.loadDocument( initialVersion.getDocument() );
		assertThatWorkspaceIsLoadedWithDocument( initialVersion, workspaceTwo );

		workspaceOne.loadDocument( otherVersion );
		assertThatWorkspaceIsLoadedWithDocument( otherVersion, workspaceOne );
	}

	@Test
	public void workspaceCanBeUsedOnDataBinder() {
		val definition = createDocumentDefinition( "workspace-v1" );
		val document = documentService.createDocumentWorkspace( definition );
		DataBinder dataBinder = new DataBinder( document );

		Map<String, Object> values = new HashMap<>();
		values.put( "binderMap[group-name]", "Bound Group Name" );
		values.put( "binderMap[user-count]", "555" );
		dataBinder.bind( new MutablePropertyValues( values ) );

		assertThat( dataBinder.getBindingResult().hasErrors() ).isFalse();
		assertThat( document.<String>getFieldValue( "group-name" ) ).isEqualTo( "Bound Group Name" );
		assertThat( document.<Long>getFieldValue( "user-count" ) ).isEqualTo( 555L );

		dataBinder = new DataBinder( document );
		values.clear();
		values.put( "binderMap[bad.field]", "Bound Group Name" );
		values.put( "binderMap[user-count]", "abc" );
		dataBinder.bind( new MutablePropertyValues( values ) );

		assertThat( dataBinder.getBindingResult().getErrorCount() ).isEqualTo( 2 );
		assertThat( dataBinder.getBindingResult().hasFieldErrors( "binderMap[bad.field]" ) ).isTrue();
		assertThat( dataBinder.getBindingResult().hasFieldErrors( "binderMap[user-count]" ) ).isTrue();

		document.setDataBinderTarget( "data" );
		dataBinder = new DataBinder( new Holder( document ) );
		values.clear();
		values.put( "data.binderMap[bad.field]", "Bound Group Name" );
		values.put( "data.binderMap[user-count]", "999" );
		dataBinder.bind( new MutablePropertyValues( values ) );

		assertThat( dataBinder.getBindingResult().getErrorCount() ).isEqualTo( 1 );
		assertThat( dataBinder.getBindingResult().hasFieldErrors( "data.binderMap[bad.field]" ) ).isTrue();
		assertThat( document.<String>getFieldValue( "group-name" ) ).isEqualTo( "Bound Group Name" );
		assertThat( document.<Long>getFieldValue( "user-count" ) ).isEqualTo( 999L );
	}

	@Test
	public void workspaceSaveIsTransactional() {
		val definition = createDocumentDefinition( "workspace-v1" );
		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.getDocumentVersion().setDocument( null );

		assertThatExceptionOfType( Throwable.class )
				.isThrownBy( workspace::save );

		assertThat( workspace.getDocument().isNew() ).isFalse();
		assertThat( documentRepository.findById( workspace.getDocument().getId() ).isPresent() ).isFalse();
	}

	@Test
	public void workspaceValidation() {
		val definition = createDocumentDefinition( "workspace-v1" );
		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setFieldValue( "group-name", null );

		Errors errors = workspace.validate();
		assertThat( errors.hasFieldErrors() ).isTrue();
		assertThat( errors.getFieldErrorCount() ).isEqualTo( 1 );
		assertThat( errors.getFieldError( "group-name" ) )
				.isNotNull()
				.matches( e -> e.getField().equals( "group-name" ) )
				.matches( e -> !e.isBindingFailure() )
				.matches( e -> e.getCode().equals( "required" ) );
	}

	@Test
	public void nestedDocumentsAndCalculatedFields() {
		val definition = createDocumentDefinition( "calculated-fields" );
		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		assertThat( workspace.<Long>getFieldValue( "previous" ) ).isNull();
		assertThat( workspace.<Long>getFieldValue( "total" ) ).isNull();
		assertThat( workspace.<Long>getFieldValue( "aggregatedTotal" ) ).isNull();
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.x" ) ).isNull();
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.level2.y" ) ).isNull();

		workspace.setFieldValue( "x", 5L );
		workspace.setFieldValue( "y", 5L );

		// by default calculated fields are not updated immediately
		assertThat( workspace.<Long>getFieldValue( "total" ) ).isNull();
		assertThat( workspace.<Long>getFieldValue( "aggregatedTotal" ) ).isNull();
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.x" ) ).isNull();
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.level2.y" ) ).isNull();

		val one = workspace.save();

		// before saving fields have been calculated
		assertThat( workspace.<Long>getFieldValue( "total" ) ).isEqualTo( 10L );
		assertThat( workspace.<Long>getFieldValue( "aggregatedTotal" ) ).isEqualTo( 10L );
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.x" ) ).isEqualTo( 15L );
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.level2.y" ) ).isEqualTo( 20L );

		workspace.createNewVersion( true );
		workspace.setFieldValue( "previous", one );
		workspace.setFieldValue( "x", 10L );
		assertThat( workspace.<Long>getFieldValue( "total" ) ).isEqualTo( 10L );
		assertThat( workspace.<Long>getFieldValue( "previous.x" ) ).isEqualTo( 5L );
		assertThat( workspace.<Long>getFieldValue( "previous.y" ) ).isEqualTo( 5L );
		assertThat( workspace.<Long>getFieldValue( "previous.total" ) ).isEqualTo( 10L );
		assertThat( workspace.<Long>getFieldValue( "previous.aggregatedTotal" ) ).isEqualTo( 10L );
		assertThat( workspace.<Long>getFieldValue( "previous.calculatedFieldset.x" ) ).isEqualTo( 15L );
		assertThat( workspace.<Long>getFieldValue( "previous.calculatedFieldset.level2.y" ) ).isEqualTo( 20L );

		workspace.setCalculatedFieldsEnabled( true );
		assertThat( workspace.<Long>getFieldValue( "total" ) ).isEqualTo( 15L );
		assertThat( workspace.<Long>getFieldValue( "aggregatedTotal" ) ).isEqualTo( 25L );
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.x" ) ).isEqualTo( 25L );
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.level2.y" ) ).isEqualTo( 35L );

		val two = workspace.save();
		workspace.createNewVersion( true );
		workspace.setFieldValue( "previous", two );
		workspace.setFieldValue( "y", 2L );
		val three = workspace.save();

		val reloaded = documentService.createDocumentWorkspace( three );
		assertThat( reloaded.<Long>getFieldValue( "x" ) ).isEqualTo( 10L );
		assertThat( reloaded.<Long>getFieldValue( "y" ) ).isEqualTo( 2L );
		assertThat( reloaded.<Long>getFieldValue( "total" ) ).isEqualTo( 12L );
		assertThat( reloaded.<Long>getFieldValue( "aggregatedTotal" ) ).isEqualTo( 37L );
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.x" ) ).isEqualTo( 22L );
		assertThat( workspace.<Long>getFieldValue( "calculatedFieldset.level2.y" ) ).isEqualTo( 32L );
		assertThat( reloaded.<DynamicDocumentVersion>getFieldValue( "previous" ) ).isEqualTo( two );
		assertThat( reloaded.<Long>getFieldValue( "previous.x" ) ).isEqualTo( 10L );
		assertThat( reloaded.<Long>getFieldValue( "previous.y" ) ).isEqualTo( 5L );
		assertThat( reloaded.<Long>getFieldValue( "previous.total" ) ).isEqualTo( 15L );
		assertThat( reloaded.<Long>getFieldValue( "previous.aggregatedTotal" ) ).isEqualTo( 25L );
		assertThat( workspace.<Long>getFieldValue( "previous.calculatedFieldset.x" ) ).isEqualTo( 25L );
		assertThat( workspace.<Long>getFieldValue( "previous.calculatedFieldset.level2.y" ) ).isEqualTo( 35L );
		assertThat( reloaded.<DynamicDocumentVersion>getFieldValue( "previous.previous" ) ).isEqualTo( one );
		assertThat( reloaded.<Long>getFieldValue( "previous.previous.x" ) ).isEqualTo( 5L );
		assertThat( reloaded.<Long>getFieldValue( "previous.previous.y" ) ).isEqualTo( 5L );
		assertThat( reloaded.<Long>getFieldValue( "previous.previous.total" ) ).isEqualTo( 10L );
		assertThat( reloaded.<Long>getFieldValue( "previous.previous.aggregatedTotal" ) ).isEqualTo( 10L );
		assertThat( workspace.<Long>getFieldValue( "previous.previous.calculatedFieldset.x" ) ).isEqualTo( 15L );
		assertThat( workspace.<Long>getFieldValue( "previous.previous.calculatedFieldset.level2.y" ) ).isEqualTo( 20L );
		assertThat( reloaded.<DynamicDocumentVersion>getFieldValue( "previous.previous.previous" ) ).isNull();
	}

	@Test
	@SuppressWarnings("unchecked")
	public void calculatedFieldsAndCollections() {
		val definition = createDocumentDefinition( "calculated-collections" );
		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		assertThat( workspace.<Long>getFieldValue( "x" ) ).isNull();
		assertThat( workspace.<List>getFieldValue( "items" ) ).isNull();

		workspace.setFieldValue( "x", 5L );

		val one = workspace.save();
		assertThat( workspace.<Long>getFieldValue( "x" ) ).isEqualTo( 5L );
		assertThat( workspace.<List>getFieldValue( "items" ) ).isNull();

		workspace.createNewVersion( true );

		workspace.setFieldValue( "items", Arrays.asList( Collections.singletonMap( "y", 5L ), Collections.singletonMap( "y", 10L ) ) );
		assertThat( workspace.<Long>getFieldValue( "x" ) ).isEqualTo( 5L );

		List<Map> items = workspace.getFieldValue( "items" );
		assertThat( items ).hasSize( 2 );

		assertThat( items.get( 0 ) )
				.hasSize( 4 )
				.containsEntry( "y", 5L )
				.containsEntry( "extra", Collections.singletonMap( "z", 10L ) )
				.containsEntry( "x-y-z", null )
				.containsEntry( "totals", Collections.singletonMap( "sum", null ) );

		assertThat( items.get( 1 ) )
				.hasSize( 4 )
				.containsEntry( "y", 10L )
				.containsEntry( "extra", Collections.singletonMap( "z", 10L ) )
				.containsEntry( "x-y-z", null )
				.containsEntry( "totals", Collections.singletonMap( "sum", null ) );

		workspace.save();

		items = workspace.getFieldValue( "items" );
		assertThat( items ).hasSize( 2 );
		assertThat( items.get( 0 ) )
				.hasSize( 4 )
				.containsEntry( "y", 5L )
				.containsEntry( "extra", Collections.singletonMap( "z", 10L ) )
				.containsEntry( "x-y-z", 20L )
				.containsEntry( "totals", Collections.singletonMap( "sum", 30L ) );

		assertThat( items.get( 1 ) )
				.hasSize( 4 )
				.containsEntry( "y", 10L )
				.containsEntry( "extra", Collections.singletonMap( "z", 10L ) )
				.containsEntry( "x-y-z", 25L )
				.containsEntry( "totals", Collections.singletonMap( "sum", 35L ) );
	}

	@Test
	public void workspaceSupportsNestedFieldsets() {
		MathContext mc = new MathContext( 3, RoundingMode.HALF_UP );
		val definition = createDocumentDefinition( "doc-nested-fieldsets" );
		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		assertThat( workspace.<String>getFieldValue( "simple-string" ) ).isNull();
		assertThat( workspace.<String>getFieldValue( "top-level-fieldset.simple-text" ) ).isNull();
		assertThat( workspace.<Long>getFieldValue( "top-level-fieldset.inner-fieldset.simple-number" ) ).isNull();
		assertThat( workspace.<BigDecimal>getFieldValue( "top-level-fieldset.inner-fieldset.inner-inner-fieldset.simple-currency" ) ).isNull();

		workspace.setFieldValue( "simple-string", "simple string" );
		workspace.setFieldValue( "top-level-fieldset.simple-text", "simple text" );
		workspace.setFieldValue( "top-level-fieldset.inner-fieldset.simple-number", 99L );
		workspace.setFieldValue( "top-level-fieldset.inner-fieldset.inner-inner-fieldset.simple-currency", new BigDecimal( 3.14, mc ) );

		assertThat( workspace.<String>getFieldValue( "simple-string" ) ).isEqualTo( "simple string" );
		assertThat( workspace.<String>getFieldValue( "top-level-fieldset.simple-text" ) ).isEqualTo( "simple text" );
		assertThat( workspace.<Long>getFieldValue( "top-level-fieldset.inner-fieldset.simple-number" ) ).isEqualTo( 99L );
		assertThat( workspace.<BigDecimal>getFieldValue( "top-level-fieldset.inner-fieldset.inner-inner-fieldset.simple-currency" ) )
				.isEqualTo( new BigDecimal( 3.14, mc ) );

		val one = workspace.save();

		val reloaded = documentService.createDocumentWorkspace( one );
		assertThat( reloaded.<String>getFieldValue( "simple-string" ) ).isEqualTo( "simple string" );
		assertThat( reloaded.<String>getFieldValue( "top-level-fieldset.simple-text" ) ).isEqualTo( "simple text" );
		assertThat( reloaded.<Long>getFieldValue( "top-level-fieldset.inner-fieldset.simple-number" ) ).isEqualTo( 99L );
		assertThat( reloaded.<BigDecimal>getFieldValue( "top-level-fieldset.inner-fieldset.inner-inner-fieldset.simple-currency" ) )
				.isEqualTo( new BigDecimal( 3.14, mc ) );
	}

	@Test
	public void nestedFieldsetCollections() {
		val definition = createDocumentDefinition( "doc-nested-fieldset-collections" );
		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		List<Map> books = new ArrayList<>();

		DynamicDataFieldSet author = new DynamicDataFieldSet();
		author.put( "name", "John Doe" );

		val book = new HashMap<>();
		book.put( "title", "Some book" );
		book.put( "author", author );
		books.add( book );

		assertThat( workspace.setFieldValue( "books", books ) ).isNull();

		assertThat( workspace.<Object>getFieldValue( "books" ) ).isEqualTo( books );
	}

	@Test
	public void calculatedFieldWithCustomContext() {
		val definition = createDocumentDefinition( "calculated-custom-context", "calculated-custom-context", dataSet );
		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );
		workspace.updateCalculatedFields();

		assertThat( workspace.<String>getFieldValue( "className" ) ).isEqualTo( DynamicDocumentService.class.getName() );
	}

	@Test
	public void workspaceDefaultValuesSimpleTest() {
		val definition = createDocumentDefinition( "default-values-simple" );

		val workspace = documentService.createDocumentWorkspace( definition );
		assertThat( workspace.getDocumentName() ).isNull();
		assertThat( workspace.getVersionLabel() ).isEqualTo( "1" );
		assertThat( workspace.getDefinitionVersion() ).isEqualTo( definition.getLatestVersion() );
		assertThat( workspace.<Long>getFieldValue( "user-count" ) ).isEqualTo( 13L );
		assertThat( workspace.<String>getFieldValue( "user-name" ) ).isEqualTo( "guido" );
		assertThat( workspace.<Object>getFieldValue( "gender" ) ).isEqualTo( new ArrayList<String>()
		{{
			add( "woman" );
		}} );
		assertThat( workspace.<Long>getFieldValue( "telephone" ) ).isEqualTo( 123456789L );
		assertThat( workspace.<Object>getFieldValue( "address-and-city.city" ) ).isEqualTo( "city" );
		assertThat( workspace.<Object>getFieldValue( "address-and-city.address.number" ) ).isEqualTo( 13L );
		assertThat( workspace.<Object>getFieldValue( "address-and-city.address.street" ) ).isEqualTo( "street" );
		assertThat( workspace.<String>getFieldValue( "bio" ) ).isEqualTo( "my bio" );
		assertThat( workspace.<LocalDate>getFieldValue( "founded" ).getYear() ).isEqualTo( 1970 );
		assertThat( workspace.<LocalDate>getFieldValue( "founded" ).getMonth() ).isEqualTo( JANUARY );
		assertThat( workspace.<LocalDate>getFieldValue( "founded" ).getDayOfMonth() ).isEqualTo( 2 );
		assertThat( workspace.<BigDecimal>getFieldValue( "weight" ) ).isEqualTo( BigDecimal.valueOf( 67.7 ) );
		assertThat( workspace.<DynamicDefinition>getFieldValue( "definition" ) ).isNotNull();
	}

	@Test
	public void workspaceDefaultValuesforTypeTest() {
		val definition = createDocumentDefinition( "default-values-with-types" );

		val workspace = documentService.createDocumentWorkspace( definition );
		assertThat( workspace.getDocumentName() ).isNull();
		assertThat( workspace.getVersionLabel() ).isEqualTo( "1" );
		assertThat( workspace.getDefinitionVersion() ).isEqualTo( definition.getLatestVersion() );
		assertThat( workspace.<Object>getFieldValue( "company.ceo" ) ).isEqualTo( "John" );
		assertThat( workspace.<Boolean>getFieldValue( "company.operational" ) ).isEqualTo( true );
		assertThat( workspace.<Long>getFieldValue( "age" ) ).isEqualTo( 50L );
		assertThat( workspace.<String>getFieldValue( "company.name" ) ).isEqualTo( "contentName" );
	}

	@Test
	public void workspaceDefaultValuesforNestedTypeTest() {
		val definition = createDocumentDefinition( "default-values-with-types" );

		val workspace = documentService.createDocumentWorkspace( definition );
		assertThat( workspace.<Object>getFieldValue( "company.fullname.firstname" ) ).isEqualTo( "Guido" );
		assertThat( workspace.<Object>getFieldValue( "company.fullname.lastname" ) ).isEqualTo( "Pellemans" );
		assertThat( workspace.<Object>getFieldValue( "company.category" ) ).isEqualTo( "Y" );
	}

	@Test
	public void workspaceDefaultValuesforCalculatedFieldsTest() {
		val definition = createDocumentDefinition( "default-values-with-types" );

		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		assertThat( workspace.<Object>getFieldValue( "profit-per-item-with-default" ) ).isEqualTo( 9L );
		assertThat( workspace.<Object>getFieldValue( "profit-per-item-without-formula" ) ).isEqualTo( 4L );
		assertThat( workspace.<Object>getFieldValue( "profit-per-person" ) ).isEqualTo( 17L );
	}

	@Test
	public void workspaceFormulaEqualsDefaultFormula() {
		val definition = createDocumentDefinition( "default-values-with-types" );

		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		// not yet implemented --> formula = defaultFormula
		assertThat( workspace.<Object>getFieldValue( "profit-per-item-with-formula" ) ).isEqualTo( 3L );
	}

	@Test
	public void overrideDefaultValue() {
		val definition = createDocumentDefinition( "default-values-with-types" );

		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		assertThat( workspace.<Object>getFieldValue( "age" ) ).isEqualTo( 50L );
		assertThat( workspace.<Object>getFieldValue( "company.category" ) ).isEqualTo( "Y" );

		workspace.setFieldValue( "age", 18L );
		workspace.setFieldValue( "company.category", "second-category" );
		workspace.save();

		assertThat( workspace.<Object>getFieldValue( "age" ) ).isEqualTo( 18L );
		assertThat( workspace.<Object>getFieldValue( "company.category" ) ).isEqualTo( "second-category" );
	}

	@Test
	public void removeDefaultValueFromExistingDocument() {
		val definition = createDocumentDefinition( "default-values-new-doc-v1" );

		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		assertThat( workspace.<Object>getFieldValue( "buy-value" ) ).isEqualTo( 6L );
		assertThat( workspace.<Object>getFieldValue( "company.category" ) ).isEqualTo( "Y" );

		// Remove the default value from the defenition and create a new version of it
		val definitionVersion = addDocumentDefinitionVersion( definition, "default-values-new-doc-v2" );
		val report = workspace.changeDefinitionVersion( definitionVersion );
		assertThat( report.hasErrors() ).isFalse();
		workspace.setAllowVersionUpdates( true );
		val version = workspace.save();
		workspace.setAllowVersionUpdates( false );
		assertThat( version.getDefinitionVersion() ).isEqualTo( definitionVersion );

		assertThat( workspace.<Object>getFieldValue( "buy-value" ) ).isEqualTo( 6L );
		assertThat( workspace.<Object>getFieldValue( "company.category" ) ).isEqualTo( "Y" );

		val newWorkspace = documentService.createDocumentWorkspace( definitionVersion );
		newWorkspace.setDocumentName( UUID.randomUUID().toString() );

		assertThat( newWorkspace.<Object>getFieldValue( "buy-value" ) ).isEqualTo( null );
		assertThat( newWorkspace.<Object>getFieldValue( "company.category" ) ).isEqualTo( null );
	}

	@Test
	public void resetDefaultValueInContent() {
		val definition = createDocumentDefinition( "default-values-reset" );

		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		HashMap<String, Object> ceoValue = new HashMap<>();
		ceoValue.put( "ceo", null );

		assertThat( workspace.<Object>getFieldValue( "company" ) ).isEqualTo( ceoValue );
		assertThat( workspace.<Object>getFieldValue( "name" ) ).isEqualTo( null );
		assertThat( workspace.<Object>getFieldValue( "age" ) ).isEqualTo( null );
		assertThat( workspace.<Object>getFieldValue( "category" ) ).isEqualTo( null );
	}

	@Test
	public void defaultFormulaFieldDependencies() {
		val definition = createDocumentDefinition( "default-values-formula-dependence" );

		val workspace = documentService.createDocumentWorkspace( definition );
		workspace.setDocumentName( UUID.randomUUID().toString() );

		assertThat( workspace.<Object>getFieldValue( "a" ) ).isEqualTo( 10L );
		assertThat( workspace.<Object>getFieldValue( "b" ) ).isEqualTo( 10L );
		assertThat( workspace.<Object>getFieldValue( "c" ) ).isEqualTo( "My name" );
	}

	private void migrateExistingVersionToNewDefinition( DynamicDocumentWorkspace document, DynamicDefinitionVersion definitionVersion ) {
		val report = document.changeDefinitionVersion( definitionVersion );
		assertThat( report.hasErrors() ).isTrue();
		assertThat( report.hasError( "group-name" ) ).isTrue();
		assertThat( report.hasError( "user-count" ) ).isFalse();

		assertThat( document.setFieldValue( "user-name", "Some name" ) ).isNull();

		document.setAllowVersionUpdates( true );
		val version = document.save();
		document.setAllowVersionUpdates( false );
		assertThat( version.getDefinitionVersion() ).isEqualTo( definitionVersion );
	}

	private DynamicDefinitionVersion addDocumentDefinitionVersion( DynamicDefinition definition, String docName ) {
		val rawDefinition = readRawDocumentDefinition( docName );

		val defVersion = DynamicDefinitionVersion
				.builder()
				.version( "2.0" )
				.definition( definition )
				.published( true )
				.definitionContent( toJson( rawDefinition ) ).build();
		definitionVersionRepository.save( defVersion );

		val updated = definition.toDto();
		updated.setLatestVersion( defVersion );
		definitionRepository.save( updated );

		definition.setLatestVersion( defVersion );

		return defVersion;
	}

	private void exportDocuments( DynamicDocumentWorkspace document, DynamicDocumentVersion versionCopy ) {
		String json = document.exportFields( DynamicDocumentDataMapper.JSON );
		assertThat( json )
				.isNotEmpty()
				.isEqualTo( versionCopy.getDocumentContent() );
	}

	private DynamicDocumentVersion createCopyManually( DynamicDocumentWorkspace document ) {
		val copy = documentService.createDocumentWorkspace( document.getDefinitionVersion() );
		assertThat( copy.importFieldsFromVersion( document.getDocumentVersion() ).hasErrors() ).isFalse();

		assertThat( copy.<String>getFieldValue( "group-name" ) ).isEqualTo( "importedGroupName" );
		assertThat( copy.<Long>getFieldValue( "user-count" ) ).isEqualTo( 333L );

		copy.setDocumentName( "My Document Copy" );

		return copy.save();
	}

	private DynamicDocumentVersion updateUsingImportFields( DynamicDocumentWorkspace document ) {
		Map<String, Object> data = new HashMap<>();
		data.put( "user-count", "333" );
		data.put( "group-name", "importedGroupName" );
		data.put( "not-existing", "illegal-value" );

		val report = document.importFields( data );
		assertThat( report.hasErrors() ).isTrue();
		assertThat( report.getError( "not-existing" ) ).isNotNull();

		document.createNewVersion( true );
		return document.save();
	}

	private DynamicDocumentVersion updateSameVersionWithChanges( DynamicDocumentWorkspace document ) {
		document.setFieldValue( "user-count", 123L );

		assertThatExceptionOfType( IllegalStateException.class )
				.isThrownBy( document::save );

		document.setAllowVersionUpdates( true );
		val updated = document.save();
		assertThat( updated ).isNotNull();
		document.setAllowVersionUpdates( false );

		val otherDocument = documentService.createDocumentWorkspace( updated );
		assertThat( otherDocument.<Long>getFieldValue( "user-count" ) ).isEqualTo( 123L );

		return updated;
	}

	private DynamicDocumentVersion addDocumentVersionWithSameDefinitionVersion( DynamicDocumentWorkspace document ) {
		assertThat( document.createNewVersion( true ).hasErrors() ).isFalse();
		assertThat( document.getVersionLabel() ).isEqualTo( "2" );
		document.setVersionLabel( "3" );
		document.setFieldValue( "user-count", 1L );
		document.deleteFieldValue( "group-name" );
		val updated = document.save();
		assertThat( updated ).isNotNull();
		assertThat( updated.getVersion() ).isEqualTo( "3" );

		val reloaded = documentService.createDocumentWorkspace( updated );
		assertThat( reloaded.<Long>getFieldValue( "user-count" ) ).isEqualTo( 1L );
		assertThat( reloaded.<String>getFieldValue( "group-name" ) ).isNull();
		assertThat( reloaded.findFieldValue( "group-name" ) ).isNull();

		return updated;
	}

	private DynamicDocumentVersion createInitialDocument( DynamicDocumentWorkspace document ) {
		document.setDocumentName( "My Workspace Test Document" );
		document.setFieldValue( "group-name", "initialGroupName" );
		document.setFieldValue( "user-count", 0L );
		val version = document.save();
		assertThat( version ).isNotNull();
		assertThat( version.isNew() ).isFalse();
		assertThat( version.getVersion() )
				.isEqualTo( "1" )
				.isEqualTo( document.getVersionLabel() );
		assertThat( document.getDocument() ).isEqualTo( version.getDocument() );
		assertThat( document.getDocumentVersion() ).isEqualTo( version );
		assertThat( version.getDocumentContent() ).isNotEmpty();

		assertThat( document.<String>getFieldValue( "group-name" ) ).isEqualTo( "initialGroupName" );
		assertThat( document.<Long>getFieldValue( "user-count" ) ).isEqualTo( 0L );
		assertThat( document.<String, String>getFieldValue( "user-count", String.class ) ).isEqualTo( "0" );
		assertThat( document.<Integer>getFieldValue( "user-count", TypeDescriptor.valueOf( Integer.class ) ) ).isEqualTo( 0 );

		assertThat( document.getDocumentName() ).isEqualTo( "My Workspace Test Document" );
		assertThat( document.getDocument().getName() ).isEqualTo( document.getDocumentName() );

		val reloaded = documentService.createDocumentWorkspace( version );
		assertThat( reloaded.getDocument() ).isEqualTo( version.getDocument() );
		assertThat( reloaded.getDocumentVersion() ).isEqualTo( version );

		assertThat( reloaded.<String>getFieldValue( "group-name" ) ).isEqualTo( "initialGroupName" );
		assertThat( reloaded.<Long>getFieldValue( "user-count" ) ).isEqualTo( 0L );
		assertThat( reloaded.<String, String>getFieldValue( "user-count", String.class ) ).isEqualTo( "0" );
		assertThat( reloaded.<Integer>getFieldValue( "user-count", TypeDescriptor.valueOf( Integer.class ) ) ).isEqualTo( 0 );

		assertThat( reloaded.getDocumentName() ).isEqualTo( "My Workspace Test Document" );
		assertThat( reloaded.getDocument().getName() ).isEqualTo( document.getDocumentName() );

		return version;
	}

	private void assertThatWorkspaceIsLoadedWithBlankDocument( DynamicDefinition definition, DynamicDocumentWorkspace document ) {
		assertThat( document.getDocumentName() ).isNull();
		assertThat( document.getVersionLabel() ).isEqualTo( "1" );
		assertThat( document.getDefinitionVersion() ).isEqualTo( definition.getLatestVersion() );
		assertThat( document.<Object>getFieldValue( "group-name" ) ).isNull();
		assertThat( document.<Long>getFieldValue( "user-count" ) ).isNull();
	}

	private void assertThatWorkspaceIsLoadedWithDocument( DynamicDocumentVersion documentVersion, DynamicDocumentWorkspace document ) {
		assertThat( document.getVersionLabel() ).isEqualTo( documentVersion.getVersion() );
		assertThat( document.getDocumentVersion() ).isEqualTo( documentVersion );
		assertThat( document.getDefinitionVersion() ).isEqualTo( documentVersion.getDefinitionVersion() );
		assertThat( document.<Object>getFieldValue( "group-name" ) ).isEqualTo( "initialGroupName" );
		assertThat( document.<Long>getFieldValue( "user-count" ) ).isEqualTo( 0L );
	}

	private DynamicDefinition createDocumentDefinition( String docName ) {
		return createDocumentDefinition( docName, UUID.randomUUID().toString(), dataSet );
	}

	@RequiredArgsConstructor
	private static class Holder
	{
		@Getter
		private final DynamicDocumentWorkspace data;
	}

	@Configuration
	@RequiredArgsConstructor
	static class CustomEvaluationContextConfiguration
	{
		private final BeanFactory beanFactory;

		@EventListener(condition = "#document.definitionId == 'test-ws:calculated-custom-context'")
		@Order(Ordered.HIGHEST_PRECEDENCE)
		public void registerCustomEvaluationContext( BuildDynamicDocumentFormulaEvaluationContext document ) {
			StandardEvaluationContext ec = BuildDynamicDocumentFormulaEvaluationContext.createDefaultEvaluationContext();
			ec.setBeanResolver( new BeanFactoryResolver( beanFactory ) );
			document.setEvaluationContext( ec );
		}
	}
}
