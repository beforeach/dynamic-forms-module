/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSetRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.entity.query.EntityQuery;
import com.foreach.across.modules.entity.query.EntityQueryFacade;
import com.foreach.across.modules.entity.query.EntityQueryFacadeResolver;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class ITDynamicDocumentWorkspaceQuery extends AbstractFormsApplicationIT
{
	private static DynamicDefinitionDataSet dsOne, dsTwo;
	private static DynamicDefinition defOneDsOne, defTwoDsOne, defOneDsTwo;
	private static DynamicDocument docOneDefOneDsOne, docTwoDefOneDsOne, docOneDefTwoDsOne, docOneDefOneDsTwo;

	@Autowired
	private DynamicDefinitionDataSetRepository dataSetRepository;

	@Autowired
	private DynamicDocumentService documentService;

	@Autowired
	private EntityQueryFacadeResolver entityQueryFacadeResolver;

	private EntityQueryFacade<DynamicDocumentWorkspace> entityQueryFacade;

	@Before
	public void createTestData() {
		if ( dsOne == null ) {
			dsOne = new DynamicDefinitionDataSet();
			dsOne.setName( "Workspace Query DataSet 1" );
			dsOne.setKey( "ds-1" );
			dataSetRepository.save( dsOne );

			dsTwo = new DynamicDefinitionDataSet();
			dsTwo.setName( "Workspace Query DataSet 2" );
			dsTwo.setKey( "ds-2" );
			dataSetRepository.save( dsTwo );

			defOneDsOne = createDocumentDefinition( "workspace-v1", "def-1-ds-1", dsOne );
			defTwoDsOne = createDocumentDefinition( "workspace-v2", "def-2-ds-1", dsOne );
			defOneDsTwo = createDocumentDefinition( "workspace-v1", "def-1-ds-2", dsTwo );

			DynamicDocumentWorkspace workspace = documentService.createDocumentWorkspace( defOneDsOne );
			workspace.setDocumentName( "doc-1-def-1-ds-1" );
			workspace.setFieldValue( "group-name", "Users" );
			workspace.setFieldValue( "user-count", 10L );
			docOneDefOneDsOne = workspace.save().getDocument();

			workspace = documentService.createDocumentWorkspace( defOneDsOne );
			workspace.setDocumentName( "doc-2-def-1-ds-1" );
			workspace.setFieldValue( "group-name", "Administrators" );
			workspace.setFieldValue( "user-count", 2L );
			docTwoDefOneDsOne = workspace.save().getDocument();

			workspace = documentService.createDocumentWorkspace( defTwoDsOne );
			workspace.setDocumentName( "doc-1-def-2-ds-1" );
			workspace.setFieldValue( "user-name", "John Doe" );
			workspace.setFieldValue( "user-count", 0L );
			docOneDefTwoDsOne = workspace.save().getDocument();

			workspace = documentService.createDocumentWorkspace( defOneDsTwo );
			workspace.setDocumentName( "doc-1-def-1-ds-2" );
			workspace.setFieldValue( "group-name", "Users" );
			workspace.setFieldValue( "user-count", 33L );
			docOneDefOneDsTwo = workspace.save().getDocument();
		}

		entityQueryFacade = entityQueryFacadeResolver.forEntityType( DynamicDocumentWorkspace.class );
	}

	@Test
	public void selectAllWorkspaces() {
		assertThat( entityQueryFacade.findAll( EntityQuery.all() )
		                             .stream()
		                             .map( DynamicDocumentWorkspace::getDocument )
		                             .collect( Collectors.toList() ) )
				.contains( docOneDefOneDsOne, docTwoDefOneDsOne, docOneDefTwoDsOne, docOneDefOneDsTwo );
	}

	@Test
	public void selectWorkspacesByName() {
		assertThat( find( "document.name like 'doc-1%'" ) )
				.contains( docOneDefOneDsOne, docOneDefOneDsTwo, docOneDefTwoDsOne )
				.doesNotContain( docTwoDefOneDsOne );
	}

	@Test
	public void workspacesByType() {
		assertThat( find( "document.type = " + defOneDsOne.getId() ) )
				.contains( docOneDefOneDsOne, docTwoDefOneDsOne )
				.doesNotContain( docOneDefTwoDsOne, docOneDefOneDsTwo );

		assertThat( find( "document.type = '" + defOneDsOne.getFullKey() + "'" ) )
				.contains( docOneDefOneDsOne, docTwoDefOneDsOne )
				.doesNotContain( docOneDefTwoDsOne, docOneDefOneDsTwo );
	}

	private List<DynamicDocument> find( String eql ) {
		return entityQueryFacade.findAll( eql )
		                        .stream()
		                        .map( DynamicDocumentWorkspace::getDocument )
		                        .collect( Collectors.toList() );
	}
}
