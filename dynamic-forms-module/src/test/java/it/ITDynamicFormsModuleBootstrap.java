/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it;

import com.foreach.across.core.context.registry.AcrossContextBeanRegistry;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.dynamicforms.DynamicFormsModule;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSet;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataMapper;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataMapperFactory;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.test.AcrossTestContext;
import lombok.val;
import org.junit.Test;
import org.springframework.core.convert.ConversionService;

import static com.foreach.across.test.support.AcrossTestBuilders.web;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class ITDynamicFormsModuleBootstrap
{
	@Test
	public void adminWebModule() {
		try (AcrossTestContext context = web()
				.modules( AdminWebModule.NAME, EntityModule.NAME, DynamicFormsModule.NAME, AcrossHibernateJpaModule.NAME )
				.build()) {
			defaultMappersShouldBeRegistered( context );
			defaultConvertersShouldBeRegistered( context );
		}
	}

	@Test
	public void entityModuleOnly() {

	}

	private void defaultConvertersShouldBeRegistered( AcrossTestContext context ) {
		val conversionService = context.getBeanOfType( ConversionService.class );
		assertThat( conversionService.canConvert( DynamicDataFieldSet.class, String.class ) ).isTrue();
	}

	private void defaultMappersShouldBeRegistered( AcrossContextBeanRegistry beanRegistry ) {
		val mapperFactory = beanRegistry.getBeanOfType( DynamicDocumentDataMapperFactory.class );
		assertThat( mapperFactory.createMapper( DynamicDocumentDataMapper.JSON ) ).isNotNull();
	}
}
