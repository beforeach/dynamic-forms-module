/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSetRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.*;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.view.RawViewDefinition;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.util.StringUtils;

import java.util.UUID;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class ITDynamicDefinitionService extends AbstractFormsApplicationIT
{
	@Autowired
	private DynamicDefinitionService dynamicDefinitionService;

	@Autowired
	private DynamicDefinitionDataSetRepository dataSetRepository;

	@Autowired
	private DynamicDefinitionRepository definitionRepository;

	@Autowired
	private DynamicDefinitionVersionRepository definitionVersionRepository;

	@Autowired
	private DynamicDefinitionTypeConfigurationRegistry typeConfigurationRegistry;

	@Test
	public void typeMustBeRegistered() {
		DynamicDefinitionId id = DynamicDefinitionId.fromString( "dataset:illegal-type:value" );
		assertThat( id ).isNotNull();

		assertThatExceptionOfType( IllegalArgumentException.class )
				.isThrownBy( () -> dynamicDefinitionService.findDefinition( id ) )
				.withMessage( "Unknown definition type: illegal-type" );
		assertThatExceptionOfType( IllegalArgumentException.class )
				.isThrownBy( () -> dynamicDefinitionService.findDefinitionVersion( id.asDefinitionVersionId( "123" ) ) )
				.withMessage( "Unknown definition type: illegal-type" );
	}

	@Test
	public void definitionExists() {
		DynamicDefinitionDataSet dataSet = new DynamicDefinitionDataSet();
		String uuidDataSet = UUID.randomUUID().toString();
		dataSet.setName( randomAlphanumeric( 50 ) );
		dataSet.setKey( uuidDataSet );
		dataSetRepository.save( dataSet );

		DynamicDefinitionType type = typeConfigurationRegistry.register( randomAlphanumeric( 50 ), RawDocumentDefinition.class, null )
		                                                      .getType();

		DynamicDefinition definition = new DynamicDefinition();
		definition.setName( StringUtils.randomAlphanumeric( 50 ) );
		definition.setDataSet( dataSet );
		definition.setType( type );
		String definitionUUID = UUID.randomUUID().toString();
		definition.setKey( definitionUUID );
		definitionRepository.save( definition );

		assertThat( dynamicDefinitionService.findDefinition( definition.getDefinitionId() ) )
				.isEqualTo( definition );
	}

	@Test
	public void keyShouldBeUniqueWithinParent() {
		DynamicDefinitionDataSet dataSet = dataSetRepository.save( DynamicDefinitionDataSet.builder().name( randomAlphanumeric( 50 ) )
		                                                                                   .key( UUID.randomUUID().toString() ).build() );

		DynamicDefinitionType viewType = typeConfigurationRegistry.register( randomAlphanumeric( 50 ), RawViewDefinition.class, null )
		                                                          .getType();
		DynamicDefinitionType documentType = typeConfigurationRegistry.register( randomAlphanumeric( 50 ), RawDocumentDefinition.class, null )
		                                                              .getType();

		DynamicDefinition project = definitionRepository.save( DynamicDefinition.builder().name( "Project" ).key( "project" ).name( "project" )
		                                                                        .dataSet( dataSet ).type( documentType ).build() );
		DynamicDefinition customer = definitionRepository.save( DynamicDefinition.builder().name( "Customer" ).key( "customer" ).name( "customer" )
		                                                                         .dataSet( dataSet ).type( documentType ).build() );

		assertThat( definitionRepository.save( DynamicDefinition.builder().name( "listView" ).key( "listView" ).dataSet( dataSet ).type( viewType )
		                                                        .parent( project )
		                                                        .build() ) ).isNotNull();
		assertThat( definitionRepository.save( DynamicDefinition.builder().name( "listView" ).key( "listView" ).dataSet( dataSet ).type( viewType )
		                                                        .parent( customer )
		                                                        .build() ) ).isNotNull();
	}

	@Test
	public void definitionVersionExists() {
		DynamicDefinitionDataSet dataSet = new DynamicDefinitionDataSet();
		String uuidDataSet = UUID.randomUUID().toString();
		dataSet.setName( randomAlphanumeric( 50 ) );
		dataSet.setKey( uuidDataSet );
		dataSetRepository.save( dataSet );

		DynamicDefinitionType type = typeConfigurationRegistry.register( randomAlphanumeric( 50 ), RawDocumentDefinition.class, null )
		                                                      .getType();

		DynamicDefinition definition = new DynamicDefinition();
		definition.setName( StringUtils.randomAlphanumeric( 50 ) );
		definition.setDataSet( dataSet );
		definition.setType( type );
		String definitionUUID = UUID.randomUUID().toString();
		definition.setKey( definitionUUID );
		definitionRepository.save( definition );

		DynamicDefinitionVersion version = DynamicDefinitionVersion.builder()
		                                                           .definition( definition )
		                                                           .definitionContent( "{}" )
		                                                           .version( "draft" )
		                                                           .published( false )
		                                                           .build();
		definitionVersionRepository.save( version );

		assertThat( version ).isNotNull().extracting( "definition" ).contains( definition );

		assertThat( dynamicDefinitionService.findDefinitionVersion( version.getDefinitionVersionId() ) )
				.isEqualTo( version );
		assertThat( dynamicDefinitionService.findDefinition( version.getDefinitionVersionId() ) )
				.isEqualTo( definition );
	}
}
