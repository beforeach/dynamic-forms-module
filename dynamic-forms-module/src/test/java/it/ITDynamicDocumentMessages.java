/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSetRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDocumentDefinitionService;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class ITDynamicDocumentMessages extends AbstractFormsApplicationIT
{
	private static DynamicDefinitionDataSet dataSet;
	private static DynamicDefinitionVersion definitionVersion;
	private static DynamicDocumentDefinition documentDefinition;

	@Autowired
	private DynamicDefinitionDataSetRepository dataSetRepository;

	@Autowired
	private DynamicDocumentDefinitionService documentDefinitionService;

	private MessageSource messageSource;

	@Before
	public void createDataSet() {
		if ( dataSet == null ) {
			dataSet = new DynamicDefinitionDataSet();
			dataSet.setName( "Messages Test DataSet" );
			dataSet.setKey( "test-messages" );
			dataSetRepository.save( dataSet );

			val definition = createDocumentDefinition( "doc-messages", "messages", dataSet );
			definitionVersion = definition.getLatestVersion();
			documentDefinition = documentDefinitionService.loadDocumentDefinition( definitionVersion );
		}

		messageSource = documentDefinition.createMessageSource( null );
	}

	@Test
	public void messageCodeNotFound() {
		assertThat( resolve( "dummy" ) ).isNull();
	}

	@Test
	public void regularFieldMessages() {
		assertThat( resolve( "properties.field:name[help]" ) ).isEqualTo( "Original help text" );
		assertThat( resolve( "properties.field:name[help]", Locale.FRENCH ) ).isEqualTo( "Original help text" );
		assertThat( resolve( "properties.field:name[help]", Locale.forLanguageTag( "nl" ) ) ).isEqualTo( "Dutch help text" );
		assertThat( resolve( "properties.field:name[help]", Locale.forLanguageTag( "nl-BE" ) ) ).isEqualTo( "Flemish help text" );

		assertThat( resolve( "properties.field:name[tooltip]" ) ).isNull();
		assertThat( resolve( "properties.field:name[tooltip]", Locale.forLanguageTag( "nl" ) ) ).isNull();
		assertThat( resolve( "properties.field:name[tooltip]", Locale.FRENCH ) ).isEqualTo( "French only tooltip" );
		assertThat( resolve( "properties.field:name[tooltip]", Locale.CANADA_FRENCH ) ).isEqualTo( "French only tooltip" );
	}

	@Test
	public void translatedFieldLabel() {
		assertThat( resolve( "properties.field:name" ) ).isEqualTo( "My Name" );
		assertThat( resolve( "properties.field:name", Locale.forLanguageTag( "nl-BE" ) ) ).isEqualTo( "Mijn naam" );
	}

	@Test
	public void fieldValidationMessages() {
		assertThat( resolve( "validation.required.properties[field:name]" ) ).isEqualTo( "A field value is required" );
		assertThat( resolve( "validation.required.properties[field:name]", Locale.forLanguageTag( "nl-BE" ) ) ).isEqualTo( "Dutch required" );
	}

	@Test
	public void documentLevelMessages() {
		assertThat( resolve( "name.singular" ) ).isEqualTo( "My document" );
		assertThat( resolve( "name.plural" ) ).isEqualTo( "My documents" );
		assertThat( resolve( "name.singular", Locale.forLanguageTag( "nl-BE" ) ) ).isEqualTo( "mijn document" );
		assertThat( resolve( "name.plural", Locale.forLanguageTag( "nl-BE" ) ) ).isEqualTo( "mijn documenten" );
		assertThat( resolve( "name.singular", Locale.FRENCH ) ).isEqualTo( "Mon document" );
		assertThat( resolve( "name.plural", Locale.FRENCH ) ).isEqualTo( "My documents" );
		assertThat( resolve( "french-only", Locale.FRENCH ) ).isEqualTo( "Bonjour" );
		assertThat( resolve( "french-only", Locale.forLanguageTag( "nl-BE" ) ) ).isNull();
		assertThat( resolve( "french-only" ) ).isNull();
	}

	private String resolve( String messageCode ) {
		return resolve( messageCode, Locale.ENGLISH );
	}

	private String resolve( String messageCode, Locale locale ) {
		String actualMessageCode = "DynamicDocument[" + definitionVersion.getDefinition().getDefinitionId() + "]." + messageCode;
		return messageSource.getMessage( actualMessageCode, new Object[0], null, locale );
	}
}
