/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.dynamicforms.DynamicFormsModule;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.definition.*;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.test.AcrossTestConfiguration;
import com.foreach.across.test.AcrossWebAppConfiguration;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.yaml.snakeyaml.Yaml;

/**
 * Bootstraps a simple application with DynamicFormsModule.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@RunWith(SpringJUnit4ClassRunner.class)
@AcrossWebAppConfiguration(classes = { AbstractFormsApplicationIT.Config.class })
public abstract class AbstractFormsApplicationIT
{
	private DynamicDefinitionType documentType;

	@Autowired
	private RawDefinitionService rawDefinitionService;

	@Autowired
	protected DynamicDefinitionRepository definitionRepository;

	@Autowired
	protected DynamicDefinitionVersionRepository definitionVersionRepository;

	@Autowired
	protected DynamicDefinitionTypeRepository definitionTypeRepository;

	@SneakyThrows
	String toJson( RawDocumentDefinition def ) {
		return rawDefinitionService.writeDefinition( def );
	}

	@Before
	public void before() {
		documentType = definitionTypeRepository.findByName( DynamicDefinitionTypes.DOCUMENT ).orElse( null );
	}

	@SneakyThrows
	RawDocumentDefinition readRawDocumentDefinition( String docName ) {
		Yaml yaml = new Yaml();
		Object raw = yaml.load( new ClassPathResource( "yaml/definitions/" + docName + ".yml" ).getInputStream() );

		ObjectMapper objectMapper = new ObjectMapper();
		String convertedJson = objectMapper.writeValueAsString( raw );
		return rawDefinitionService.readRawDefinition( convertedJson, RawDocumentDefinition.class );
	}

	DynamicDefinition createDocumentDefinition( String docName, String definitionName, DynamicDefinitionDataSet dataSet ) {
		val rawDefinition = readRawDocumentDefinition( docName );

		val def = DynamicDefinition.builder().name( definitionName ).key( definitionName ).type( documentType ).dataSet( dataSet ).build();
		definitionRepository.save( def );

		val defVersion = DynamicDefinitionVersion
				.builder()
				.version( "1.0" )
				.definition( def )
				.published( true )
				.definitionContent( toJson( rawDefinition ) ).build();
		definitionVersionRepository.save( defVersion );

		val updated = def.toDto();
		updated.setLatestVersion( defVersion );
		definitionRepository.save( updated );

		return updated;
	}

	@AcrossTestConfiguration(modules = { DynamicFormsModule.NAME, AcrossHibernateJpaModule.NAME, EntityModule.NAME, AdminWebModule.NAME })
	protected static class Config
	{
	}
}
