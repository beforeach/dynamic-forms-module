/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSetRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionType;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypeRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public class ITDataDefinitionCreation extends AbstractFormsApplicationIT
{
	@Autowired
	private DynamicDefinitionDataSetRepository dynamicDefinitionDataSetRepository;

	@Autowired
	private DynamicDefinitionRepository definitionRepository;

	@Autowired
	private DynamicDefinitionTypeRepository definitionTypeRepository;

	@Test
	public void canCreateFormDataDefinition() {
		DynamicDefinitionDataSet dataSet = new DynamicDefinitionDataSet();
		dataSet.setName( "Forms" );
		dataSet.setKey( "KEY-FORMS" );
		dynamicDefinitionDataSetRepository.save( dataSet );

		DynamicDefinitionType type = definitionTypeRepository.findByName( "form" ).orElse( null );

		DynamicDefinition dataDefinition = DynamicDefinition.builder()
		                                                    .name( "name" )
		                                                    .type( type )
		                                                    .dataSet( dataSet )
		                                                    .key( "nam" )
		                                                    .build();

		DynamicDefinition persisted = definitionRepository.save( dataDefinition );
		assertNotNull( persisted.getId() );
		assertEquals( "Forms", persisted.getDataSet().getName() );
		assertEquals( "KEY-FORMS", persisted.getDataSet().getKey() );
		assertEquals( "form", persisted.getType().getName() );
	}
}
