/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package infrastructure;

import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

/**
 * Helper for (forward-only) iterating an entire document definition.
 */
@RequiredArgsConstructor
public class RawDocumentDefinitionAssert
{
	private final RawDocumentDefinition definition;

	private int fieldPosition = 0;
	private int typePosition = 0;

	public RawDocumentDefinitionAssert hasFields( int fieldCount ) {
		assertThat( definition.getContent() ).hasSize( fieldCount );
		return this;
	}

	public RawDocumentDefinitionAssert withNextField( Consumer<RawDocumentDefinitionFieldAssert> satisfies ) {
		satisfies.accept( new RawDocumentDefinitionFieldAssert( definition.getContent().get( fieldPosition++ ) ) );
		return this;
	}

	public RawDocumentDefinitionAssert hasTypes( int fieldCount ) {
		assertThat( definition.getTypes() ).hasSize( fieldCount );
		return this;
	}

	public RawDocumentDefinitionAssert withNextType( Consumer<RawDocumentDefinitionTypeAssert> satisfies ) {
		satisfies.accept( new RawDocumentDefinitionTypeAssert( definition.getTypes().get( typePosition++ ) ) );
		return this;
	}

	public RawDocumentDefinitionAssert resetFieldPosition() {
		fieldPosition = 0;
		return this;
	}

	public RawDocumentDefinitionAssert isEqualTo( RawDocumentDefinition rawDefinition ) {
		assertThat( definition ).isEqualTo( rawDefinition );
		return this;
	}

	public RawDocumentDefinitionAssert hasMessage( String messageCode, String text ) {
		return hasMessage( messageCode, text, "default" );
	}

	@SuppressWarnings("unchecked")
	public RawDocumentDefinitionAssert hasMessage( String messageCode, String text, String locale ) {
		if ( definition.getMessages().containsKey( locale ) ) {
			Map<String, Object> messagesForLocale = (Map<String, Object>) definition.getMessages().get( locale );
			if ( messagesForLocale.containsKey( messageCode ) ) {
				assertThat( messagesForLocale.get( messageCode ) ).isEqualTo( text );
				return this;
			}
		}

		if ( "default".equals( locale ) ) {
			assertThat( definition.getMessages().get( messageCode ) ).isEqualTo( text );
			return this;
		}

		fail( "Message code " + messageCode + " could not be found for locale " + locale );

		return this;
	}
}
