/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package infrastructure;

import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import lombok.NonNull;

import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

public class RawDocumentDefinitionTypeAssert extends AbstractRawDocumentDefinitionAssert<RawDocumentDefinition.BaseField>
{
	private int fieldPosition = 0;

	public RawDocumentDefinitionTypeAssert( RawDocumentDefinition.BaseField baseField ) {
		this.field = baseField;
	}

	public RawDocumentDefinitionTypeAssert hasFields( int fieldCount ) {
		assertThat( field.getFields() ).hasSize( fieldCount );
		return this;
	}

	public RawDocumentDefinitionTypeAssert withNextField( Consumer<RawDocumentDefinitionFieldAssert> satisfies ) {
		satisfies.accept( new RawDocumentDefinitionFieldAssert( field.getFields().get( fieldPosition++ ) ) );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasId( String id ) {
		assertThat( field.getId() ).isEqualTo( id );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasType( String type ) {
		assertThat( field.getBaseType() ).isEqualTo( type );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasPossibleValues( @NonNull Object possibleValues ) {
		isValueRestricted();
		assertThat( field.getPossibleValues() ).isEqualTo( possibleValues );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasNoValidators() {
		assertThat( field.getValidators().isEmpty() ).isTrue();
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasValidators( @NonNull List validators ) {
		assertThat( field.getValidators() ).isEqualTo( validators );
		return this;
	}

	public RawDocumentDefinitionTypeAssert isNotValueRestricted() {
		assertThat( field.hasPossibleValues() ).isFalse();
		return this;
	}

	public RawDocumentDefinitionTypeAssert isValueRestricted() {
		assertThat( field.hasPossibleValues() ).isTrue();
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasLabel( String label ) {
		assertThat( field.getLabel() ).isEqualTo( label );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasMessage( String messageCode, String value ) {
		assertThat( field.getMessages().get( messageCode ) ).isEqualTo( value );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasAttribute( String attributeName, Object value ) {
		assertThat( field.getAttributes().get( attributeName ) ).isEqualTo( value );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasDefaultValue( Object expectedValue ) {
		assertThat( field.getDefaultValue().getValue() ).isEqualTo( expectedValue );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasDefaultFormula( String expectedValue ) {
		assertThat( field.getDefaultValue().getFormula() ).isEqualTo( expectedValue );
		return this;
	}

	public RawDocumentDefinitionTypeAssert hasNoDefaultValue() {
		assertThat( field.getDefaultValue() ).isNull();
		return this;
	}
}
