/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package infrastructure;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldFormulaContext;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.EvaluationContext;

import java.util.function.BiFunction;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
public class DynamicDocumentDefinitionFieldAssert
{
	private final DynamicDocumentFieldDefinition field;

	private int fieldPosition = 0;

	public DynamicDocumentDefinitionFieldAssert hasId( String id ) {
		assertThat( field.getId() ).isEqualTo( id );
		return this;
	}

	@Deprecated
	public DynamicDocumentDefinitionFieldAssert hasCanonicalId( String id ) {
		assertThat( field.getCanonicalId() ).isEqualTo( id );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasTypeName( String typeName ) {
		assertThat( field.getTypeDefinition().getTypeName() ).isEqualTo( typeName );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasTypeDescriptor( TypeDescriptor typeDescriptor ) {
		assertThat( field.getTypeDefinition().getTypeDescriptor() ).isEqualTo( typeDescriptor );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert isACollection( boolean expectedValue ) {
		assertThat( field.isCollection() ).isEqualTo( expectedValue );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert isAMember( boolean expectedValue ) {
		assertThat( field.isMember() ).isEqualTo( expectedValue );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasCollectionFieldDefinition( String expectedId ) {
		assertThat( field.getCollectionFieldDefinition() ).isNotNull()
		                                                  .satisfies( f -> assertThat( f.getId() ).isEqualTo( expectedId ) );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasMemberFieldDefinition( String expectedId ) {
		assertThat( field.getMemberFieldDefinition() ).isNotNull()
		                                              .satisfies( f -> assertThat( f.getId() ).isEqualTo( expectedId ) );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasExpression( BiFunction expectedValue ) {
		assertThat( field.getExpression() ).isEqualTo( expectedValue );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasFields( int fieldCount ) {
		assertThat( field.getFields() ).hasSize( fieldCount );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasLabel( String label ) {
		assertThat( field.getLabel() ).isEqualTo( label );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert withNextField( Consumer<DynamicDocumentDefinitionFieldAssert> satisfies ) {
		satisfies.accept( new DynamicDocumentDefinitionFieldAssert( field.getFields().get( fieldPosition++ ) ) );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert resetFieldPosition() {
		fieldPosition = 0;
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert withPropertyDescriptor( Consumer<EntityPropertyDescriptor> consumer ) {
		assertThat( field.getEntityPropertyDescriptor() ).isNotNull();
		consumer.accept( field.getEntityPropertyDescriptor() );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert satisfies( Consumer<DynamicDocumentFieldDefinition> consumer ) {
		consumer.accept( field );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasInitializerExpression( BiFunction expectedValue ) {
		assertThat( field.getInitializerExpression() ).isEqualTo( expectedValue );
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasNoInitializerExpression() {
		assertThat( field.getInitializerExpression() ).isNull();
		return this;
	}

	public DynamicDocumentDefinitionFieldAssert hasInitializeExpressionValue( Object expected ) {
		assertThat( field.getInitializerExpression().apply( mock( EvaluationContext.class ), mock( DynamicDocumentFieldFormulaContext.class ) ) )
				.isEqualTo( expected );
		return this;
	}
}
