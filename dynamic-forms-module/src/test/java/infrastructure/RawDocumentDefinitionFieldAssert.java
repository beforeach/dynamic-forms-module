/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package infrastructure;

import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
public class RawDocumentDefinitionFieldAssert extends AbstractRawDocumentDefinitionAssert<RawDocumentDefinition.Field>
{
	private int fieldPosition = 0;

	public RawDocumentDefinitionFieldAssert( RawDocumentDefinition.Field field ) {
		this.field = field;
	}

	public RawDocumentDefinitionFieldAssert hasId( String id ) {
		assertThat( field.getId() ).isEqualTo( id );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasType( String type ) {
		assertThat( field.getType() ).isEqualTo( type );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasPossibleValues( @NonNull List possibleValues ) {
		isValueRestricted();
		assertThat( field.getPossibleValues() ).isEqualTo( possibleValues );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasNoValidators() {
		assertThat( field.getValidators().isEmpty() ).isTrue();
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasValidators( @NonNull List validators ) {
		assertThat( field.getValidators() ).isEqualTo( validators );
		return this;
	}

	public RawDocumentDefinitionFieldAssert isNotValueRestricted() {
		assertThat( field.hasPossibleValues() ).isFalse();
		return this;
	}

	public RawDocumentDefinitionFieldAssert isValueRestricted() {
		assertThat( field.hasPossibleValues() ).isTrue();
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasLabel( String label ) {
		assertThat( field.getLabel() ).isEqualTo( label );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasFormula( String formula ) {
		assertThat( field.getFormula() ).isEqualTo( formula );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasMessage( String messageCode, String text ) {
		return hasMessage( messageCode, text, "default" );
	}

	@SuppressWarnings("unchecked")
	public RawDocumentDefinitionFieldAssert hasMessage( String messageCode, String text, String locale ) {
		if ( field.getMessages().containsKey( locale ) ) {
			Map<String, Object> messagesForLocale = (Map<String, Object>) field.getMessages().get( locale );
			if ( messagesForLocale.containsKey( messageCode ) ) {
				assertThat( messagesForLocale.get( messageCode ) ).isEqualTo( text );
				return this;
			}
		}

		if ( "default".equals( locale ) ) {
			assertThat( field.getMessages().get( messageCode ) ).isEqualTo( text );
			return this;
		}

		fail( "Message code " + messageCode + " could not be found for locale " + locale );

		return this;
	}

	public RawDocumentDefinitionFieldAssert hasAttribute( String attributeName, Object value ) {
		assertThat( field.getAttributes().get( attributeName ) ).isEqualTo( value );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasFields( int fieldCount ) {
		assertThat( field.getFields() ).hasSize( fieldCount );
		return this;
	}

	public RawDocumentDefinitionFieldAssert withNextField( Consumer<RawDocumentDefinitionFieldAssert> satisfies ) {
		satisfies.accept( new RawDocumentDefinitionFieldAssert( field.getFields().get( fieldPosition++ ) ) );
		return this;
	}

	public RawDocumentDefinitionFieldAssert withMember( Consumer<RawDocumentDefinitionFieldAssert> satisfies ) {
		assertThat( field.getMember() ).isNotNull();
		satisfies.accept( new RawDocumentDefinitionFieldAssert( field.getMember() ) );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasDefaultValue( Object expectedValue ) {
		assertThat( field.getDefaultValue().getValue() ).isEqualTo( expectedValue );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasDefaultFormula( String expectedValue ) {
		assertThat( field.getDefaultValue().getFormula() ).isEqualTo( expectedValue );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasImplicitNoDefaultValue() {
		assertThat( field.getDefaultValue() ).isSameAs( RawDocumentDefinition.DefaultValue.NOT_SPECIFIED );
		return this;
	}

	public RawDocumentDefinitionFieldAssert hasExplicitNoDefaultValue() {
		assertThat( field.getDefaultValue() ).isNull();
		return this;
	}
}
