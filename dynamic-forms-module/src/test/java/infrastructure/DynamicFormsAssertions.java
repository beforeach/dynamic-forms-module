/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package infrastructure;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;

/**
 * Domain specific assertion helpers.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class DynamicFormsAssertions extends org.assertj.core.api.Assertions
{
	public static RawDocumentDefinitionAssert assertThat( RawDocumentDefinition definition ) {
		return new RawDocumentDefinitionAssert( definition );
	}

	public static RawDocumentDefinitionFieldAssert assertThat( RawDocumentDefinition.Field field ) {
		return new RawDocumentDefinitionFieldAssert( field );
	}

	public static DynamicDocumentDefinitionAssert assertThat( DynamicDocumentDefinition definition ) {
		return new DynamicDocumentDefinitionAssert( definition );
	}
}
