/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package infrastructure;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import lombok.RequiredArgsConstructor;

import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Helper for (forward-only) iterating an entire document definition.
 */
@RequiredArgsConstructor
public class DynamicDocumentDefinitionAssert
{
	private final DynamicDocumentDefinition definition;

	private int fieldPosition = 0;

	public DynamicDocumentDefinitionAssert hasFields( int fieldCount ) {
		assertThat( definition.getContent() ).hasSize( fieldCount );
		return this;
	}

	public DynamicDocumentDefinitionAssert withNextField( Consumer<DynamicDocumentDefinitionFieldAssert> satisfies ) {
		satisfies.accept( new DynamicDocumentDefinitionFieldAssert( definition.getContent().get( fieldPosition++ ) ) );
		return this;
	}

	public DynamicDocumentDefinitionAssert resetFieldPosition() {
		fieldPosition = 0;
		return this;
	}

	public DynamicDocumentDefinitionAssert satisfies( Consumer<DynamicDocumentDefinition> consumer ) {
		consumer.accept( definition );
		return this;
	}

	public DynamicDocumentDefinitionAssert withPropertyRegistry( Consumer<EntityPropertyRegistry> propertyRegistryConsumer ) {
		propertyRegistryConsumer.accept( definition.getEntityPropertyRegistry() );
		return this;
	}
}
