/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDefinitionVersionValidator
{
	@Mock
	private DynamicDefinitionVersionRepository repository;

	@Mock
	private Errors errors;

	private DynamicDefinitionVersionValidator validator;

	private DynamicDefinitionVersion newEntity = new DynamicDefinitionVersion();
	private DynamicDefinition newDynamicDefinition = new DynamicDefinition();
	private DynamicDefinitionVersion existingEntity = new DynamicDefinitionVersion();
	private DynamicDefinition existingDynamicDefinition = new DynamicDefinition();

	private QDynamicDefinitionVersion definitionVersion = QDynamicDefinitionVersion.dynamicDefinitionVersion;

	@Before
	public void reset() {
		validator = new DynamicDefinitionVersionValidator( repository );
		newEntity.setVersion( "V2" );
		newEntity.setDefinition( newDynamicDefinition );

		existingEntity.setId( 66L );
		existingEntity.setVersion( "V2" );
		existingEntity.setDefinition( existingDynamicDefinition );
	}

	@Test
	public void newEntityWithSameVersion() {
		when( repository.count( definitionVersion.version.equalsIgnoreCase( "V2" ).and( definitionVersion.definition.eq( newDynamicDefinition ) ) ) )
				.thenReturn( 2L );
		validator.validate( newEntity, errors );
		verify( errors ).hasFieldErrors( "version" );
		verify( errors ).rejectValue( "version", "alreadyExists" );
		verify( errors ).hasFieldErrors( "definition" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void existingEntityWithSameVersion() {
		when( repository.count( definitionVersion.version.equalsIgnoreCase( "V2" ).and( definitionVersion.definition.eq( existingDynamicDefinition ) )
		                                                 .and( definitionVersion.id.ne( 66L ) ) ) ).thenReturn( 2L );
		validator.validate( existingEntity, errors );
		verify( errors ).hasFieldErrors( "version" );
		verify( errors ).rejectValue( "version", "alreadyExists" );
		verify( errors ).hasFieldErrors( "definition" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void validatesCorrectClass() {
		assertFalse( validator.supports( DynamicDefinition.class ) );
		assertTrue( validator.supports( DynamicDefinitionVersion.class ) );
		assertTrue( validator.supports( TestDynamicDefinitionVersionValidator.SpecialDefinitionVersion.class ) );
	}

	private static class SpecialDefinitionVersion extends DynamicDefinitionVersion
	{
	}
}
