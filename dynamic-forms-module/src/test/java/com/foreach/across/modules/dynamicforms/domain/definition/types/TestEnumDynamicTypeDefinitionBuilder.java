/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.bootstrapui.elements.builder.OptionFormElementBuilder;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.query.EntityQueryOps;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyDescriptor;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.bootstrapui.OptionsFormElementBuilderFactory;
import com.foreach.across.modules.entity.views.bootstrapui.options.OptionIterableBuilder;
import com.foreach.across.modules.entity.views.processors.EntityQueryFilterProcessor;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.format.Printer;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestEnumDynamicTypeDefinitionBuilder
{
	@SuppressWarnings("unused")
	@Spy
	private ConversionService conversionService = new DefaultConversionService();

	@Mock
	private SimpleDynamicTypeDefinitionBuilder simpleTypeBuilder;

	@Mock
	private DynamicDocumentFieldDefinition fieldDefinition;

	@InjectMocks
	private EnumDynamicTypeDefinitionBuilder builder;

	@Test
	public void acceptedIfSimpleTypeAndHavePossibleValues() {
		assertThat( builder.accepts( field( "mytype", Collections.singletonList( 1 ) ) ) ).isFalse();

		when( simpleTypeBuilder.accepts( any() ) ).thenReturn( true );
		assertThat( builder.accepts( field( "mytype", Collections.singletonList( 1 ) ) ) ).isTrue();
		assertThat( builder.accepts( field( "mytype", Collections.emptyList() ) ) ).isFalse();
	}

	@Test
	@SuppressWarnings("unchecked")
	public void singleValue() {
		when( simpleTypeBuilder.createTypeDefinitionBuilder( any() ) ).thenReturn(
				GenericDynamicTypeDefinition.builder()
				                            .typeDescriptor( TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( String.class ) ) )
				                            .typeName( "string" )
		);

		val typeDef = builder.buildTypeDefinition( null, field( "string", Arrays.asList( "1", "2" ) ) );
		assertThat( typeDef ).isNotNull();

		EntityPropertyDescriptorBuilder propBuilder = spy( EntityPropertyDescriptor.builder( "myprop" ) );
		typeDef.customizePropertyDescriptor( fieldDefinition, propBuilder );

		verify( propBuilder ).viewElementType( ViewElementMode.CONTROL, OptionsFormElementBuilderFactory.OPTIONS );
		verify( propBuilder ).viewElementType( ViewElementMode.FILTER_CONTROL, OptionsFormElementBuilderFactory.OPTIONS );
		verify( propBuilder ).viewElementType( ViewElementMode.FILTER_CONTROL.forMultiple(), OptionsFormElementBuilderFactory.OPTIONS );
		verify( propBuilder ).attribute( EntityQueryFilterProcessor.ENTITY_QUERY_OPERAND, EntityQueryOps.EQ );

		MutableEntityPropertyDescriptor property = propBuilder.build();
		OptionIterableBuilder options = property.getAttribute( OptionIterableBuilder.class );
		assertThat( options.isSorted() ).isTrue();

		List<OptionFormElementBuilder> opts = new ArrayList<>();
		options.buildOptions( null ).forEach( opts::add );
		assertThat( opts.get( 0 ).getLabel() ).isEqualTo( "1" );
		assertThat( opts.get( 0 ).getValue() ).isEqualTo( "1" );
		assertThat( opts.get( 0 ).getRawValue() ).isEqualTo( "1" );
		assertThat( opts.get( 1 ).getLabel() ).isEqualTo( "2" );
		assertThat( opts.get( 1 ).getValue() ).isEqualTo( "2" );
		assertThat( opts.get( 1 ).getRawValue() ).isEqualTo( "2" );

		assertThat( property.getAttribute( Printer.class ).print( "1", Locale.UK ) ).isEqualTo( "1" );

		assertThat( property.getAttribute( EntityAttributes.OPTIONS_ENHANCER ) ).isNotNull();
	}

	@Test
	@SuppressWarnings("unchecked")
	public void singleValueNonCollection() {
		when( simpleTypeBuilder.createTypeDefinitionBuilder( any() ) ).thenReturn(
				GenericDynamicTypeDefinition.builder()
				                            .typeDescriptor( TypeDescriptor.valueOf( String.class ) )
				                            .typeName( "string" )
		);

		val typeDef = builder.buildTypeDefinition( null, field( "string", Arrays.asList( "1", "2" ) ) );
		assertThat( typeDef ).isNotNull();

		EntityPropertyDescriptorBuilder propBuilder = spy( EntityPropertyDescriptor.builder( "myprop" ) );
		typeDef.customizePropertyDescriptor( fieldDefinition, propBuilder );

		verify( propBuilder ).viewElementType( ViewElementMode.CONTROL, OptionsFormElementBuilderFactory.OPTIONS );
		verify( propBuilder ).viewElementType( ViewElementMode.FILTER_CONTROL, OptionsFormElementBuilderFactory.OPTIONS );
		verify( propBuilder ).viewElementType( ViewElementMode.FILTER_CONTROL.forMultiple(), OptionsFormElementBuilderFactory.OPTIONS );
		verify( propBuilder ).attribute( EntityQueryFilterProcessor.ENTITY_QUERY_OPERAND, EntityQueryOps.EQ );

		MutableEntityPropertyDescriptor property = propBuilder.build();
		OptionIterableBuilder options = property.getAttribute( OptionIterableBuilder.class );
		assertThat( options.isSorted() ).isTrue();

		List<OptionFormElementBuilder> opts = new ArrayList<>();
		options.buildOptions( null ).forEach( opts::add );
		assertThat( opts.get( 0 ).getLabel() ).isEqualTo( "1" );
		assertThat( opts.get( 0 ).getValue() ).isEqualTo( "1" );
		assertThat( opts.get( 0 ).getRawValue() ).isEqualTo( "1" );
		assertThat( opts.get( 1 ).getLabel() ).isEqualTo( "2" );
		assertThat( opts.get( 1 ).getValue() ).isEqualTo( "2" );
		assertThat( opts.get( 1 ).getRawValue() ).isEqualTo( "2" );

		assertThat( property.getAttribute( Printer.class ).print( "1", Locale.UK ) ).isEqualTo( "1" );
	}

	@Test
	@SuppressWarnings("unchecked")
	public void labelAndValue() {
		when( simpleTypeBuilder.createTypeDefinitionBuilder( any() ) ).thenReturn(
				GenericDynamicTypeDefinition.builder()
				                            .typeDescriptor( TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( String.class ) ) )
				                            .typeName( "string" )
		);

		val typeDef = builder.buildTypeDefinition( null, field( "string",
		                                                        Arrays.asList( Collections.singletonMap( "yes", "Ok" ),
		                                                                       Collections.singletonMap( "no", "Not Ok" ) ) )
		);
		assertThat( typeDef ).isNotNull();

		EntityPropertyDescriptorBuilder propBuilder = spy( EntityPropertyDescriptor.builder( "myprop" ) );
		typeDef.customizePropertyDescriptor( fieldDefinition, propBuilder );

		verify( propBuilder ).viewElementType( ViewElementMode.CONTROL, OptionsFormElementBuilderFactory.OPTIONS );

		MutableEntityPropertyDescriptor property = propBuilder.build();

		val all = property.getAttribute( EnumDynamicTypeOptions.class );
		assertThat( all ).isNotNull();
		assertThat( (OptionIterableBuilder) property.getAttribute( OptionIterableBuilder.class ) ).isSameAs( all );
		assertThat( (Printer) property.getAttribute( Printer.class ) ).isSameAs( all );

		OptionIterableBuilder options = property.getAttribute( OptionIterableBuilder.class );
		assertThat( options.isSorted() ).isTrue();

		List<OptionFormElementBuilder> opts = new ArrayList<>();
		options.buildOptions( null ).forEach( opts::add );
		assertThat( opts.get( 0 ).getLabel() ).isEqualTo( "Ok" );
		assertThat( opts.get( 0 ).getValue() ).isEqualTo( "yes" );
		assertThat( opts.get( 0 ).getRawValue() ).isEqualTo( "yes" );
		assertThat( opts.get( 1 ).getLabel() ).isEqualTo( "Not Ok" );
		assertThat( opts.get( 1 ).getValue() ).isEqualTo( "no" );
		assertThat( opts.get( 1 ).getRawValue() ).isEqualTo( "no" );

		assertThat( property.getAttribute( Printer.class ).print( "yes", Locale.UK ) ).isEqualTo( "Ok" );
		assertThat( property.getAttribute( Printer.class ).print( "no", Locale.UK ) ).isEqualTo( "Not Ok" );
		assertThat( property.getAttribute( Printer.class ).print( Arrays.asList( "no", "yes" ), Locale.UK ) )
				.isEqualTo( "Not Ok, Ok" );
	}

	private RawDocumentDefinition.Field field( String type, List possibleValues ) {
		return RawDocumentDefinition.Field.builder().type( type ).possibleValues( possibleValues ).build();
	}
}
