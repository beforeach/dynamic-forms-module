/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public class TestComparators
{
	@Test
	public void dataSetComparator() {
		DynamicDefinitionDataSet dsOne = dataset( "AAA" );
		DynamicDefinitionDataSet dsTwo = dataset( "ABB" );
		DynamicDefinitionDataSet dsThree = dataset( "ABa" );
		DynamicDefinitionDataSet dsFour = dataset( "aaa" );
		List<DynamicDefinitionDataSet> datasets = Arrays.asList( dsThree, dsFour, dsOne, dsTwo );
		datasets.sort( DynamicDefinitionDataSet::compareTo );
		assertThat( datasets ).containsExactly( dsOne, dsTwo, dsThree, dsFour );
	}

	@Test
	public void definitionComparator() {
		DynamicDefinition defOne = definition( "AAA" );
		DynamicDefinition defTwo = definition( "ABB" );
		DynamicDefinition defThree = definition( "ABa" );
		DynamicDefinition defFour = definition( "aaa" );
		List<DynamicDefinition> definitions = Arrays.asList( defThree, defFour, defOne, defTwo );
		definitions.sort( DynamicDefinition::compareTo );
		assertThat( definitions ).containsExactly( defOne, defTwo, defThree, defFour );
	}

	@Test
	public void definitionVersionComparator() {
		DynamicDefinition defOne = definition( "AAA" );
		DynamicDefinition defTwo = definition( "ABB" );

		DynamicDefinitionVersion versionOne = definitionVersion( defOne, "ABB" );
		DynamicDefinitionVersion versionTwo = definitionVersion( defOne, "aaa" );
		DynamicDefinitionVersion versionThree = definitionVersion( defTwo, "AAA" );
		DynamicDefinitionVersion versionFour = definitionVersion( defTwo, "ABa" );
		List<DynamicDefinitionVersion> versions = Arrays.asList( versionFour, versionThree, versionOne, versionTwo );
		versions.sort( DynamicDefinitionVersion::compareTo );
		assertThat( versions ).containsExactly( versionOne, versionTwo, versionThree, versionFour );
	}

	@Test
	public void documentComparator() {
		DynamicDocument docOne = document( "AAA" );
		DynamicDocument docTwo = document( "ABB" );
		DynamicDocument docThree = document( "ABa" );
		DynamicDocument docFour = document( "aaa" );
		List<DynamicDocument> documents = Arrays.asList( docThree, docFour, docOne, docTwo );
		documents.sort( DynamicDocument::compareTo );
		assertThat( documents ).containsExactly( docOne, docTwo, docThree, docFour );
	}

	@Test
	public void documentVersionComparator() {
		DynamicDocument docOne = document( "AAA" );
		DynamicDocument docTwo = document( "ABB" );

		DynamicDocumentVersion versionOne = documentVersion( docOne, "ABB" );
		DynamicDocumentVersion versionTwo = documentVersion( docOne, "aaa" );
		DynamicDocumentVersion versionThree = documentVersion( docTwo, "AAA" );
		DynamicDocumentVersion versionFour = documentVersion( docTwo, "ABa" );
		List<DynamicDocumentVersion> versions = Arrays.asList( versionFour, versionThree, versionOne, versionTwo );
		versions.sort( DynamicDocumentVersion::compareTo );
		assertThat( versions ).containsExactly( versionOne, versionTwo, versionThree, versionFour );
	}

	private DynamicDefinitionDataSet dataset( String name ) {
		return DynamicDefinitionDataSet.builder().name( name ).build();
	}

	private DynamicDefinition definition( String name ) {
		return DynamicDefinition.builder().name( name ).build();
	}

	private DynamicDefinitionVersion definitionVersion( DynamicDefinition definition, String version ) {
		return DynamicDefinitionVersion.builder().definition( definition ).version( version ).build();
	}

	private DynamicDocument document( String name ) {
		return DynamicDocument.builder().name( name ).build();
	}

	private DynamicDocumentVersion documentVersion( DynamicDocument document, String version ) {
		return DynamicDocumentVersion.builder().document( document ).version( version ).build();
	}
}
