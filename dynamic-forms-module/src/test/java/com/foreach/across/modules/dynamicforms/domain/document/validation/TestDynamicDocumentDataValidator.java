/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.validation;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.GenericDynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData;
import lombok.val;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.validation.Errors;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;

import static com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory.documentDefinition;
import static com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory.fieldDefinition;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Ignore("possibly deprecated")
@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDocumentDataValidator
{
	@Mock
	private DynamicTypeDefinition typeDefinitionMock;

	@Mock
	private Errors errors;

	private DynamicDocumentDataValidator documentValidator = DynamicDocumentDataValidator.forPlainTarget();

	private DynamicDocumentData document;

	@Before
	public void before() {
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( String.class ) );

		val name = fieldDefinition( "name", typeDefinitionMock ).setInitializerExpression( ( c, f ) -> "<default>" );
		name.setValidators( Collections.singleton( required( "name", name ) ) );

		val street = fieldDefinition( "street", typeDefinitionMock );
		street.setValidators( Collections.singleton( required( "address.street", street ) ) );

		GenericDynamicTypeDefinition typeDefinition = mock( GenericDynamicTypeDefinition.class );
		when( typeDefinition.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( LinkedHashMap.class ) );

		DynamicDocumentFieldDefinition address = fieldDefinition( "address", typeDefinition )
				.setFields( Collections.singletonList( street ) );

		DynamicDocumentDefinition documentDefinition = documentDefinition( "my-doc", Arrays.asList( name, address ) );

		document = new DynamicDocumentData( documentDefinition );
	}

	@Test
	public void validationErrors() {
		documentValidator.validate( document, errors );

		verify( errors ).rejectValue( "name", "required" );
		verify( errors ).rejectValue( "address.street", "required" );
	}

	@Test
	public void validationErrorsForIndexedTarget() {
		documentValidator = DynamicDocumentDataValidator.forIndexedTarget( "" );

		documentValidator.validate( document, errors );
		verify( errors ).rejectValue( "[name]", "required" );
		verify( errors ).rejectValue( "[address.street]", "required" );

		documentValidator = DynamicDocumentDataValidator.forIndexedTarget( "binderMap" );

		reset( errors );
		documentValidator.validate( document, errors );
		verify( errors ).rejectValue( "binderMap[name]", "required" );
		verify( errors ).rejectValue( "binderMap[address.street]", "required" );
	}

	@Test
	public void noValidationErrors() {
		document.setValue( "name", "my name" );
		document.setValue( "address.street", "some street" );

		documentValidator.validate( document, errors );
		verifyZeroInteractions( errors );
	}

	private DynamicDocumentFieldValidator required( String path, DynamicDocumentFieldDefinition expectedDefinition ) {
		return ( field, errors ) -> {
			assertThat( field.getFieldPath() ).isEqualTo( path );
			assertThat( field.getDocument() ).isSameAs( document );
			assertThat( field.getFieldDefinition() ).isSameAs( expectedDefinition );

			if ( field.getFieldValue() == null ) {
				errors.rejectValue( field.getFieldKey(), "required" );
			}
		};
	}
}
