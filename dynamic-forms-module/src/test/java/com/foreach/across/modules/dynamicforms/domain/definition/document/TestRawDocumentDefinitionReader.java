/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.yaml.snakeyaml.Yaml;

import java.util.Arrays;
import java.util.Collections;

import static infrastructure.DynamicFormsAssertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestRawDocumentDefinitionReader
{
	@Test
	@SneakyThrows
	public void simpleDocument() {
		Yaml yaml = new Yaml();
		Object raw = yaml.load( new ClassPathResource( "yaml/definitions/doc-simple-types.yml" ).getInputStream() );

		ObjectMapper objectMapper = new ObjectMapper();
		String convertedJson = objectMapper.writeValueAsString( raw );

		RawDocumentDefinition definition = objectMapper
				.reader()
				.forType( RawDocumentDefinition.class )
				.withRootName( "document-definition" )
				.readValue( convertedJson );

		assertThat( definition )
				.hasTypes( 2 )
				.withNextType(
						field -> field.hasId( "telephone-number" )
						              .hasType( "number" )
						              .isNotValueRestricted()
						              .hasMessage( "description", "A custom telephone number." )
						              .hasAttribute( "one", "two" )
						              .hasAttribute( "two", Arrays.asList( 1, 2 ) )
						              .hasValidators( Arrays.asList( "not-blank", Collections.singletonMap( "pattern", "123" ) ) )
						              .hasDefaultFormula( "5+6" )
				)
				.withNextType(
						field -> field.hasId( "address" )
						              .hasType( "fieldset" )
						              .isNotValueRestricted()
						              .hasNoValidators()
						              .hasDefaultValue( Collections.singletonMap( "street", "Molenheide" ) )
						              .hasFields( 3 )
						              .withNextField(
								              fieldSetField -> fieldSetField
										              .hasId( "street" )
										              .hasDefaultValue( "Achterbos" )
										              .hasType( "string" )
										              .hasValidators( Arrays.asList( "required", Collections.singletonMap( "max-length", 10 ) ) )
						              )
						              .withNextField(
								              fieldSetField -> fieldSetField.hasId( "street-nr" )
								                                            .hasType( "number" )
								                                            .hasNoValidators()
								                                            .hasImplicitNoDefaultValue()
						              )
						              .withNextField(
								              fieldSetField -> fieldSetField.hasId( "zipcode" )
								                                            .hasType( "number" )
								                                            .hasNoValidators()
								                                            .hasExplicitNoDefaultValue()
						              )
				)
				.hasFields( 16 )
				.withNextField(
						field -> field.hasId( "single-line-text" )
						              .hasType( "string" )
						              .isNotValueRestricted()
						              .hasValidators( Collections.singletonList( "required" ) )
						              .hasDefaultValue( "myText" )
				)
				.withNextField(
						field -> field.hasId( "number-enum" )
						              .hasType( "number" )
						              .hasLabel( "Some number" )
						              .hasPossibleValues( Arrays.asList( 1, 2, 3 ) )
						              .hasNoValidators()
						              .hasDefaultValue( 3 )
				)
				.withNextField(
						field -> field.hasId( "string-enum" )
						              .hasType( "string[]" )
						              .hasPossibleValues( Arrays.asList( Collections.singletonMap( "yes", "Ok" ), Collections.singletonMap( "no", "Not Ok" ) ) )
						              .hasNoValidators()
						              .hasDefaultValue( Arrays.asList( "yes", "no" ) )
				)
				.withNextField(
						field -> field.hasId( "telephone" )
						              .hasType( "telephone-number" )
						              .isNotValueRestricted()
						              .hasMessage( "help", "Help text for telephone" )
						              .hasMessage( "help", "Vertaalde help text", "nl-BE" )
						              .hasMessage( "tooltip", "Tooltip French only", "fr" )
						              .hasNoValidators()
						              .hasImplicitNoDefaultValue()
				)
				.withNextField(
						field -> field.hasId( "location" )
						              .hasType( "address" )
						              .isNotValueRestricted()
						              .hasNoValidators()
						              .hasDefaultFormula( "5+4" )
				)
				.withNextField(
						field -> field.hasId( "multi-line-text" )
						              .hasType( "text" )
						              .isNotValueRestricted()
						              .hasNoValidators()
				)
				.withNextField(
						field -> field.hasId( "boolean" )
						              .hasType( "boolean" )
						              .isNotValueRestricted()
						              .hasNoValidators()
						              .hasDefaultValue( null )
				)
				.withNextField(
						field -> field.hasId( "date" )
						              .hasType( "date" )
						              .isNotValueRestricted()
						              .hasNoValidators()
						              .hasDefaultFormula( null )
				)
				.withNextField(
						field -> field.hasId( "datetime" )
						              .hasType( "datetime" )
						              .isNotValueRestricted()
						              .hasNoValidators()
						              .hasExplicitNoDefaultValue()
				)
				.withNextField(
						field -> field.hasId( "time" )
						              .hasType( "time" )
						              .isNotValueRestricted()
						              .hasNoValidators()
				)
				.withNextField(
						field -> field.hasId( "integer-number" )
						              .hasType( "integer" )
						              .isNotValueRestricted()
						              .hasNoValidators()
				)
				.withNextField(
						field -> field.hasId( "decimal-number" )
						              .hasType( "decimal" )
						              .isNotValueRestricted()
						              .hasNoValidators()
				)
				.withNextField(
						field -> field.hasId( "decimal-number-more-precision" )
						              .hasType( "decimal(4)" )
						              .isNotValueRestricted()
						              .hasNoValidators()
				)
				.withNextField(
						field -> field.hasId( "currency-eur" )
						              .hasType( "currency(EUR,4)" )
						              .isNotValueRestricted()
						              .hasNoValidators()
				)
				.withNextField(
						field -> field.hasId( "currency-usd" )
						              .hasType( "currency(USD,2)" )
						              .isNotValueRestricted()
						              .hasNoValidators()
				)
				.withNextField(
						field -> field.hasId( "currency-gbp" )
						              .hasType( "currency(GBP)" )
						              .isNotValueRestricted()
						              .hasNoValidators()
				)
				.hasMessage( "custom.message", "test" )
				.hasMessage( "other.message", "other" )
				.hasMessage( "custom.message", "test-nl", "nl-BE" );
	}
}
