/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.entity.registry.EntityConfiguration;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;

import java.util.LinkedHashSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestEntityDynamicTypeDefinitionBuilder
{
	@Mock
	private EntityRegistry entityRegistry;

	@InjectMocks
	private EntityDynamicTypeDefinitionBuilder builder;

	@Test
	public void acceptedIfPresentInEntityRegistryAndStartsWithAt() {
		when( entityRegistry.contains( "user" ) ).thenReturn( false );
		assertThat( builder.accepts( field( "user" ) ) ).isFalse();
		assertThat( builder.accepts( field( "@user" ) ) ).isFalse();

		when( entityRegistry.contains( "dynamicDocument" ) ).thenReturn( true );
		assertThat( builder.accepts( field( "dynamicDocument" ) ) ).isFalse();
		assertThat( builder.accepts( field( "@dynamicDocument" ) ) ).isTrue();
		assertThat( builder.accepts( field( "@dynamicDocument[]" ) ) ).isTrue();
	}

	@Test
	public void singleValue() {
		String entityType = "dynamicDocument";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( entityType ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocument.class );

		TypeDescriptor expectedType = TypeDescriptor.valueOf( DynamicDocument.class );
		val typeDef = builder.buildTypeDefinition( null, field( "@" + entityType ) );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( expectedType );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( "@" + entityType );
	}

	@Test
	public void multiValue() {
		String entityType = "dynamicDocument";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( entityType ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocument.class );

		val typeDef = builder.buildTypeDefinition( null, field( "@" + entityType + "[]" ) );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( TypeDescriptor.collection( LinkedHashSet.class, TypeDescriptor.valueOf( DynamicDocument.class ) ) );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( "@" + entityType + "[]" );
	}

	private RawDocumentDefinition.Field field( String type ) {
		return RawDocumentDefinition.Field.builder().id( "my-" + type ).type( type ).build();
	}
}
