/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.HtmlViewElement;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.support.DefaultConversionService;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestEnumDynamicTypeOptions
{
	private ConversionService conversionService = new DefaultConversionService();
	private TypeDescriptor typeDescriptor = TypeDescriptor.valueOf( Long.class );
	private EnumDynamicTypeOptions options = new EnumDynamicTypeOptions( conversionService, typeDescriptor );

	@Test
	public void singleObjectOption() {
		val option = options.addPossibleValue( "123" );
		assertThat( option ).isNotNull();
		assertThat( option.getLabel() ).isEqualTo( "123" );
		assertThat( option.getValue() ).isEqualTo( "123" );
		assertThat( option.getRawValue() ).isEqualTo( 123L );
		assertThat( option.getHtmlAttributes() ).isEmpty();
	}

	@Test
	public void singleKeyValueOption() {
		val option = options.addPossibleValue( Collections.singletonMap( 123, "one-two-three" ) );
		assertThat( option ).isNotNull();
		assertThat( option.getLabel() ).isEqualTo( "one-two-three" );
		assertThat( option.getValue() ).isEqualTo( "123" );
		assertThat( option.getRawValue() ).isEqualTo( 123L );
		assertThat( option.getHtmlAttributes() ).isEmpty();
	}

	@Test
	public void mapAsPossibleValue() {
		Map<String, Object> possibleValue = new HashMap<>();
		possibleValue.put( "value", 123 );
		possibleValue.put( "raw-value", 456 );
		possibleValue.put( "label", "One-two-three" );
		possibleValue.put( "html-attributes", Collections.singletonMap( "class", "css-class" ) );

		val option = options.addPossibleValue( possibleValue );
		assertThat( option ).isNotNull();
		assertThat( option.getLabel() ).isEqualTo( "One-two-three" );
		assertThat( option.getValue() ).isEqualTo( "123" );
		assertThat( option.getRawValue() ).isEqualTo( 456L );
		assertThat( option.getHtmlAttributes() )
				.hasSize( 1 )
				.containsEntry( "class", "css-class" );
	}

	@Test
	public void optionIterableBuilder() {
		options.addPossibleValue( "123" );
		options.addPossibleValue( Collections.singletonMap( 456, "4-5-6" ) );

		Map<String, Object> possibleValue = new HashMap<>();
		possibleValue.put( "value", 789 );
		possibleValue.put( "raw-value", 10 );
		possibleValue.put( "label", "7-8-9" );
		possibleValue.put( "html-attributes", Collections.singletonMap( "class", "sevenEightNine" ) );
		options.addPossibleValue( possibleValue );

		ViewElementBuilderContext builderContext = mock( ViewElementBuilderContext.class );

		val opts = options.buildOptions( builderContext );
		assertThat( opts.get( 0 ).getLabel() ).isEqualTo( "123" );
		assertThat( opts.get( 0 ).getValue() ).isEqualTo( "123" );
		assertThat( opts.get( 0 ).getRawValue() ).isEqualTo( 123L );
		assertThat( opts.get( 1 ).getLabel() ).isEqualTo( "4-5-6" );
		assertThat( opts.get( 1 ).getValue() ).isEqualTo( "456" );
		assertThat( opts.get( 1 ).getRawValue() ).isEqualTo( 456L );
		assertThat( opts.get( 2 ).getLabel() ).isEqualTo( "7-8-9" );
		assertThat( opts.get( 2 ).getValue() ).isEqualTo( "789" );
		assertThat( opts.get( 2 ).getRawValue() ).isEqualTo( 10L );
		assertThat( ( (HtmlViewElement) opts.get( 2 ).build( builderContext ) ).getAttributes() )
				.isEqualTo( Collections.singletonMap( "class", "sevenEightNine" ) );
	}

	@Test
	public void printer() {
		options.addPossibleValue( "123" );
		options.addPossibleValue( Collections.singletonMap( 456, "4-5-6" ) );

		Map<String, Object> possibleValue = new HashMap<>();
		possibleValue.put( "value", 789 );
		possibleValue.put( "raw-value", 10 );
		possibleValue.put( "label", "7-8-9" );
		possibleValue.put( "html-attributes", Collections.singletonMap( "class", "sevenEightNine" ) );
		options.addPossibleValue( possibleValue );

		assertThat( options.print( null, Locale.UK ) ).isEqualTo( "" );
		assertThat( options.print( 123L, Locale.UK ) ).isEqualTo( "123" );
		assertThat( options.print( 456L, Locale.UK ) ).isEqualTo( "4-5-6" );
		assertThat( options.print( 10L, Locale.UK ) ).isEqualTo( "7-8-9" );
		assertThat( options.print( 789L, Locale.UK ) ).isEqualTo( "789" );

		assertThat( options.print( Arrays.asList( "10", 456L ), Locale.UK ) ).isEqualTo( "7-8-9, 4-5-6" );
	}
}
