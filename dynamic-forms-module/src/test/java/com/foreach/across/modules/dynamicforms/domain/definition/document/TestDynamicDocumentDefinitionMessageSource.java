/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.DefaultMessageSourceResolvable;

import java.util.Collections;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestDynamicDocumentDefinitionMessageSource
{
	private DynamicDocumentDefinitionMessageSource messageSource = new DynamicDocumentDefinitionMessageSource();

	@Before
	public void before() {
		messageSource.addMessage( "my.message", "hello" );
		messageSource.addMessage( "my.message", Locale.ENGLISH, "allo" );
		messageSource.addMessages( Collections.singletonMap( "other.message", "say hi" ) );
		messageSource.addMessages( Collections.singletonMap( "other.message", "ullo to you" ), Locale.FRANCE );
	}

	@Test
	public void withoutParentMessageSource() {
		assertThat( messageSource.getMessage( "my.message", new Object[0], Locale.FRANCE ) ).isEqualTo( "hello" );
		assertThat( messageSource.getMessage( "other.message", new Object[0], Locale.FRANCE ) ).isEqualTo( "ullo to you" );
		assertThat( messageSource.getMessage( "my.message", new Object[0], Locale.US ) ).isEqualTo( "allo" );
		assertThat( messageSource.getMessage( "other.message", new Object[0], Locale.US ) ).isEqualTo( "say hi" );

		assertThatExceptionOfType( NoSuchMessageException.class )
				.isThrownBy( () -> messageSource.getMessage( "unknown", new Object[0], Locale.UK ) );
		assertThat( messageSource.getMessage( "unknown", new Object[0], "default", Locale.UK ) ).isEqualTo( "default" );
		assertThat( messageSource.getMessage( new DefaultMessageSourceResolvable( new String[] { "unknown" }, new Object[0], "default" ), Locale.UK ) )
				.isEqualTo( "default" );
	}

	@Test
	public void withParentMessageSource() {
		MessageSource parent = mock( MessageSource.class );
		MessageSource messageSource = this.messageSource.withParentMessageSource( parent );

		assertThat( messageSource.getMessage( "my.message", new Object[0], Locale.FRANCE ) ).isEqualTo( "hello" );
		assertThat( messageSource.getMessage( "other.message", new Object[0], Locale.FRANCE ) ).isEqualTo( "ullo to you" );
		assertThat( messageSource.getMessage( "my.message", new Object[0], Locale.US ) ).isEqualTo( "allo" );
		assertThat( messageSource.getMessage( "other.message", new Object[0], Locale.US ) ).isEqualTo( "say hi" );

		val args = new Object[] { 123 };

		when( parent.getMessage( "unknown", args, Character.toString( (char) 0 ), Locale.UK ) )
				.thenReturn( Character.toString( (char) 0 ) );

		assertThatExceptionOfType( NoSuchMessageException.class )
				.isThrownBy( () -> messageSource.getMessage( "unknown", args, Locale.UK ) );
		assertThat( messageSource.getMessage( "unknown", args, "default", Locale.UK ) ).isEqualTo( "default" );
		assertThat( messageSource.getMessage( new DefaultMessageSourceResolvable( new String[] { "unknown" }, args, "default" ), Locale.UK ) )
				.isEqualTo( "default" );

		when( parent.getMessage( "unknown", args, Character.toString( (char) 0 ), Locale.UK ) )
				.thenReturn( "known in parent" );

		assertThat( messageSource.getMessage( "unknown", args, Locale.UK ) ).isEqualTo( "known in parent" );
		assertThat( messageSource.getMessage( "unknown", args, "default", Locale.UK ) ).isEqualTo( "known in parent" );
		assertThat( messageSource.getMessage( new DefaultMessageSourceResolvable( new String[] { "unknown" }, args, "default" ), Locale.UK ) )
				.isEqualTo( "known in parent" );

	}

}
