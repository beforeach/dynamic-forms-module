/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.foreach.across.core.convert.StringToDateTimeConverter;
import com.foreach.across.modules.dynamicforms.config.DynamicDataObjectMapperConfiguration;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData;
import com.foreach.across.modules.dynamicforms.domain.document.data.mappers.JsonDocumentDataMapper;
import com.foreach.across.modules.entity.converters.EntityConverter;
import com.foreach.across.modules.entity.registry.EntityConfigurationImpl;
import com.foreach.across.modules.entity.registry.EntityModel;
import com.foreach.across.modules.entity.registry.EntityRegistryImpl;
import com.foreach.across.modules.hibernate.business.SettableIdBasedEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.domain.Persistable;
import org.springframework.format.support.DefaultFormattingConversionService;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.foreach.across.modules.dynamicforms.domain.definition.types.GenericDynamicTypeDefinition.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestJsonDocumentDataMapper
{
	private DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();
	private JsonDocumentDataMapper mapper;
	private DynamicDocumentData document;

	private User user;
	private Group group;
	private Role role;
	private LocalDate localDate = LocalDate.parse( "2010-03-15" );
	private LocalTime localTime = LocalTime.parse( "09:35" );
	private LocalDateTime localDateTime = LocalDateTime.parse( "2007-12-03T10:15:30" );
	private EntityRegistryImpl entityRegistry = new EntityRegistryImpl();

	@Before
	@SuppressWarnings("unchecked")
	public void setup() {
		conversionService.addConverter( new StringToDateTimeConverter( conversionService ) );
		List<DynamicDocumentFieldDefinition> fields = new ArrayList<>();
		fields.add( new DynamicDocumentFieldDefinition( builder().typeName( "number" )
		                                                         .typeDescriptor( TypeDescriptor.valueOf( Long.class ) ).build() )
				            .setId( "id" ) );
		fields.add( new DynamicDocumentFieldDefinition( builder().typeName( "string" )
		                                                         .typeDescriptor( TypeDescriptor.valueOf( String.class ) )
		                                                         .build() )
				            .setId( "name" ) );
		fields.add( new DynamicDocumentFieldDefinition( builder().typeName( "@user" )
		                                                         .typeDescriptor( TypeDescriptor.valueOf( User.class ) ).build() )
				            .setId( "user" ) );
		fields.add( new DynamicDocumentFieldDefinition( builder().typeName( "@group" )
		                                                         .typeDescriptor( TypeDescriptor.valueOf( Group.class ) )
		                                                         .build() )
				            .setId( "group" ) );
		fields.add( new DynamicDocumentFieldDefinition( builder().typeName( "@role" )
		                                                         .typeDescriptor( TypeDescriptor.valueOf( Role.class ) ).build() )
				            .setId( "role" ) );

		fields.add( new DynamicDocumentFieldDefinition( builder().typeName( "date" )
		                                                         .typeDescriptor( TypeDescriptor.valueOf( LocalDate.class ) ).build() )
				            .setId( "date" ) );
		fields.add( new DynamicDocumentFieldDefinition( builder().typeName( "datetime" )
		                                                         .typeDescriptor( TypeDescriptor.valueOf( LocalDateTime.class ) ).build() )
				            .setId( "datetime" ) );
		fields.add( new DynamicDocumentFieldDefinition( builder().typeName( "time" )
		                                                         .typeDescriptor( TypeDescriptor.valueOf( LocalTime.class ) ).build() )
				            .setId( "time" ) );

		DynamicDocumentDefinition definition = mock( DynamicDocumentDefinition.class );
		when( definition.getContent() ).thenReturn( fields );
		document = new DynamicDocumentData( definition );

		user = User.builder().id( 666L ).username( "beast" ).build();
		registerEntity( user, "user", User.class );
		group = Group.builder().id( 33L ).groupName( "plebs" ).build();
		registerEntity( group, "group", Group.class );
		role = Role.builder().id( Short.valueOf( String.valueOf( 45 ) ) ).roleName( "admin" ).build();
		registerEntity( role, "role", Role.class );

		mapper = new JsonDocumentDataMapper( new DynamicDataObjectMapperConfiguration().dynamicDataObjectMapper(), conversionService );
	}

	@SuppressWarnings("unchecked")
	private void registerEntity( byId<Serializable> entityValue, String entityName, Class clazz ) {
		EntityModel entityModel = mock( EntityModel.class );

		when( entityModel.findOne( entityValue.getId() ) ).thenReturn( entityValue );
		when( entityModel.getIdType() ).thenReturn( entityValue.getId().getClass() );
		EntityConfigurationImpl entityConfiguration = new EntityConfigurationImpl<>( entityName, clazz );
		entityConfiguration.setEntityModel( entityModel );
		entityRegistry.register( entityConfiguration );
	}

	@Test
	public void nullDataDoesNothing() {
		mapper.load( document, null );

		assertThat( document.getContent() ).isEmpty();
	}

	@Test
	public void loadJsonData() {
		conversionService.addConverter( new EntityConverter<>( conversionService, entityRegistry ) );

		mapper.load( document,
		             "{\"id\":1,\"name\":\"hello\",\"user\":" + user.getId().toString() + ",\"group\":" + group.getId() + ",\"role\":" + role.getId()
				             + ",\"date\":\"2010-03-15\",\"datetime\":\"2007-12-03T10:15:30\",\"time\":\"09:35:00\"}" );

		Map<String, Object> content = document.getContent();
		assertThat( content.get( "id" ) ).isEqualTo( 1L );
		assertThat( content.get( "name" ) ).isEqualTo( "hello" );
		assertThat( content.get( "user" ) ).isEqualTo( user );
		assertThat( content.get( "group" ) ).isEqualTo( group );
		assertThat( content.get( "role" ) ).isEqualTo( role );
		assertThat( content.get( "date" ) ).isEqualTo( localDate );
		assertThat( content.get( "datetime" ) ).isEqualTo( localDateTime );
		assertThat( content.get( "time" ) ).isEqualTo( localTime );
	}

	@Test
	public void dumpJsonData() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put( "id", 1 );
		map.put( "name", "hello" );
		map.put( "user", user );
		map.put( "group", group );
		map.put( "role", role );
		map.put( "date", localDate );
		map.put( "datetime", localDateTime );
		map.put( "time", localTime );

		DynamicDocumentData documentData = mock( DynamicDocumentData.class );

		when( documentData.getContent() ).thenReturn( map );

		String json = mapper.dump( documentData );
		assertThat( json )
				.isNotNull()
				.isEqualTo(
						"{\"id\":1,\"name\":\"hello\",\"user\":666,\"group\":33,\"role\":45,\"date\":\"2010-03-15\",\"datetime\":\"2007-12-03T10:15:30\",\"time\":\"09:35:00\"}" );
	}

	@EqualsAndHashCode(callSuper = true)
	@Builder
	@Data
	private static class User extends SettableIdBasedEntity implements byId<Serializable>
	{
		private Long id;
		private String username;
	}

	@EqualsAndHashCode(callSuper = true)
	@Builder
	@Data
	private static class Group extends SettableIdBasedEntity implements byId<Serializable>
	{
		private Long id;
		private String groupName;
	}

	@Builder
	@Data
	private static class Role<T> implements Persistable<Short>, byId<Serializable>
	{
		private Short id;
		private String roleName;

		@Override
		public boolean isNew() {
			return false;
		}
	}

	private interface byId<T>
	{
		T getId();
	}
}
