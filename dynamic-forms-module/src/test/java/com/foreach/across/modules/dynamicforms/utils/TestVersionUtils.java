/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.utils;

import org.junit.Test;

import static com.foreach.across.modules.dynamicforms.utils.VersionUtils.getNextVersion;
import static org.junit.Assert.assertEquals;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public class TestVersionUtils
{
	@Test
	public void versionIsEmpty() {
		String version = "";
		assertEquals( "1", getNextVersion( version ) );
	}

	@Test
	public void onlyText() {
		String version = "alpha";
		assertEquals( "alpha 1", getNextVersion( version ) );
	}

	@Test
	public void singleNumber() {
		String version = "45";
		assertEquals( "46", getNextVersion( version ) );
	}

	@Test
	public void textAndSingleNumber() {
		String version = "alpha 121";
		assertEquals( "alpha 122", getNextVersion( version ) );
	}

	@Test
	public void textWithMultipleNumbers() {
		String version = "alpha 121.32.45";
		assertEquals( "alpha 122.32.45", getNextVersion( version ) );
	}

	@Test
	public void multipleNumbers() {
		String version = "34.56.12";
		assertEquals( "35.56.12", getNextVersion( version ) );
	}
}
