/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSet;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.GenericDynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.BuildDynamicDocumentFormulaEvaluationContext;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFormulaFunctions;
import com.google.common.collect.ImmutableMap;
import lombok.val;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;

import java.util.*;

import static com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory.documentDefinition;
import static com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory.fieldDefinition;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDocumentData
{
	@Mock
	private DynamicTypeDefinition typeDefinitionMock;

	@Before
	public void before() {
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( String.class ) );
	}

	@Test
	public void singleFieldOperations() {
		DynamicDocumentFieldDefinition name = fieldDefinition( "name", typeDefinitionMock )
				.setInitializerExpression( ( c, f ) -> "<default>" );
		DynamicDocumentData data = new DynamicDocumentData( documentDefinition( "my-doc", Collections.singletonList( name ) ) );

		assertThat( data.getFieldType( "name" ) )
				.hasValue( TypeDescriptor.valueOf( String.class ) );
		assertThat( data.getFieldType( "i-do-not-exist" ) )
				.isEmpty();

		val rawData = data.getContent();
		assertThat( rawData ).isEmpty();
		assertThat( data.<String>getValue( "name" ) ).isNull();
		assertThat( data.<String>findValue( "name" ) ).isNull();

		data.setValue( "name", "my name" );
		assertThat( rawData ).hasSize( 1 ).containsEntry( "name", "my name" );
		assertThat( data.<String>getValue( "name" ) ).isEqualTo( "my name" );
		assertThat( data.<String>findValue( "name" ) ).isNotNull().contains( "my name" );

		data.setValue( "name", null );
		assertThat( rawData ).hasSize( 1 ).containsKey( "name" );
		assertThat( data.<String>getValue( "name" ) ).isNull();
		assertThat( data.<String>findValue( "name" ) ).isNotNull().isEmpty();

		data.deleteValue( "name" );
		assertThat( rawData ).isEmpty();

		data.resetValue( "name" );
		assertThat( rawData ).hasSize( 1 ).containsEntry( "name", "<default>" );
	}

	@Test
	public void setUnknownFieldValues() {
		DynamicDocumentData data = new DynamicDocumentData( documentDefinition( "my-doc", Collections.emptyList() ) );

		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> data.setValue( "name", null ) )
				.satisfies( fe -> assertThat( fe.getFieldName() ).isEqualTo( "name" ) );
	}

	@Test
	public void simpleFieldValuesArePersistedToHierarchicalStructure() {
		DynamicDocumentDefinition documentDefinition = documentWithAddress();
		DynamicDocumentData data = new DynamicDocumentData( documentDefinition );

		data.setValue( "name", "This is my name..." );
		assertThat( data.<String>getValue( "name" ) ).isEqualTo( "This is my name..." );
		data.setValue( "name", "updated value" );
		assertThat( data.<String>getValue( "name" ) ).isEqualTo( "updated value" );

		data.setValue( "address.street", "my street" );
		assertThat( data.<String>getValue( "address.street" ) ).isEqualTo( "my street" );
		assertThat( data.<Map<String, Object>>getValue( "address" ) )
				.hasSize( 1 )
				.containsEntry( "street", "my street" );
	}

	@Test
	public void settingNestedFieldAsMap() {
		DynamicDocumentDefinition documentDefinition = documentWithAddress();
		DynamicDocumentData data = new DynamicDocumentData( documentDefinition );

		data.setValue( "address", Collections.singletonMap( "street", "my street" ) );
		assertThat( data.<String>getValue( "address.street" ) ).isEqualTo( "my street" );
		assertThat( data.<Map<String, Object>>getValue( "address" ) )
				.hasSize( 1 )
				.containsEntry( "street", "my street" );
	}

	@Test
	public void settingComplexNestedFieldAsMap() {
		DynamicDocumentDefinition documentDefinition = documentWithComplexAddress();
		DynamicDocumentData data = new DynamicDocumentData( documentDefinition );

		Map<String, String> street = new HashMap<>();
		street.put( "name", "my street" );
		street.put( "number", "15" );
		data.setValue( "address", Collections.singletonMap( "street", street ) );
		assertThat( data.<String>getValue( "address.street.name" ) ).isEqualTo( "my street" );
		assertThat( data.<Map<String, Object>>getValue( "address.street" ) )
				.hasSize( 2 )
				.containsEntry( "name", "my street" )
				.containsEntry( "number", "15" );
		assertThat( data.<Map<String, Object>>getValue( "address" ) )
				.hasSize( 1 )
				.containsKey( "street" );
	}

	@Test
	public void loadDataAddsAllData() {
		DynamicDocumentDefinition documentDefinition = documentWithAddress();
		DynamicDocumentData document = new DynamicDocumentData( documentDefinition );

		Map<String, Object> data = new HashMap<>();
		data.put( "name", "original name" );
		data.put( "address.street", "original street" );
		document.load( data );

		assertThat( document.<String>getValue( "name" ) ).isEqualTo( "original name" );
		assertThat( document.<String>getValue( "address.street" ) ).isEqualTo( "original street" );

		data.clear();
		data.put( "name", "my name" );
		data.put( "address", Collections.singletonMap( "street", "my street" ) );
		document.load( data );

		assertThat( document.<String>getValue( "name" ) ).isEqualTo( "my name" );
		assertThat( document.<String>getValue( "address.street" ) ).isEqualTo( "my street" );

		data.clear();
		data.put( "address.street", "street only" );
		document.load( data );

		assertThat( document.<String>getValue( "name" ) ).isEqualTo( "my name" );
		assertThat( document.<String>getValue( "address.street" ) ).isEqualTo( "street only" );
	}

	@Test
	public void simpleCalculatedField() {
		DynamicDocumentFieldDefinition x = fieldDefinition( "x", typeDefinitionMock );
		DynamicDocumentFieldDefinition y = fieldDefinition( "y", typeDefinitionMock );
		val sum = fieldDefinition( "sum", typeDefinitionMock )
				.setExpression( ( e, context ) -> ( (int) context.field( "x" ).get() ) + ( (int) context.field( "y" ).get() ) );

		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "calc", Arrays.asList( x, y, sum ) ) );
		assertThat( document.isCalculatedFieldsEnabled() ).isFalse();
		assertThat( document.getContent() ).doesNotContainKey( "sum" );
		document.updateCalculatedFields();
		assertThat( document.getContent() ).containsEntry( "sum", null );
		assertThat( document.isCalculatedFieldsEnabled() ).isFalse();

		assertThat( document.<Integer>getValue( "x" ) ).isNull();
		assertThat( document.<Integer>getValue( "y" ) ).isNull();
		assertThat( document.<Integer>getValue( "sum" ) ).isNull();

		document.setValue( "x", 10 );
		assertThat( document.<Integer>getValue( "x" ) ).isEqualTo( 10 );
		assertThat( document.<Integer>getValue( "y" ) ).isNull();
		assertThat( document.<Integer>getValue( "sum" ) ).isNull();

		document.setValue( "y", 5 );
		assertThat( document.<Integer>getValue( "x" ) ).isEqualTo( 10 );
		assertThat( document.<Integer>getValue( "y" ) ).isEqualTo( 5 );
		assertThat( document.<Integer>getValue( "sum" ) ).isNull();

		document.updateCalculatedFields();
		assertThat( document.<Integer>getValue( "sum" ) ).isEqualTo( 15 );
		assertThat( document.getContent() ).containsEntry( "sum", 15 );

		document.setValue( "x", 3 );
		assertThat( document.<Integer>getValue( "sum" ) ).isEqualTo( 15 );
		assertThat( document.getContent() ).containsEntry( "sum", 15 );

		document.setCalculatedFieldsEnabled( true );
		assertThat( document.<Integer>getValue( "sum" ) ).isEqualTo( 8 );
		assertThat( document.getContent() ).containsEntry( "sum", 8 );

		document.getContent().put( "sum", 50000 );
		assertThat( document.<Integer>getValue( "sum" ) ).isEqualTo( 8 );

		document.setCalculatedFieldsEnabled( false );
		document.getContent().put( "sum", 50000 );
		assertThat( document.<Integer>getValue( "sum" ) ).isEqualTo( 50000 );

		document.setValue( "y", 3 );
		document.updateCalculatedFields();
		assertThat( document.getContent() ).containsEntry( "sum", 6 );
	}

	@Test
	@SuppressWarnings("unchecked")
	public void fieldsetCalculatedField() {
		DynamicTypeDefinition fieldsetDefinition = mock( DynamicTypeDefinition.class );
		when( fieldsetDefinition.getTypeDescriptor() ).thenReturn( DynamicDataFieldSet.TYPE_DESCRIPTOR );

		DynamicDocumentFieldDefinition x = fieldDefinition( "x", typeDefinitionMock );
		DynamicDocumentFieldDefinition y = fieldDefinition( "y", typeDefinitionMock );
		val sum = fieldDefinition( "sum", typeDefinitionMock )
				.setExpression( ( e, context ) -> ( (int) context.field( "data.x" ).get() ) + ( (int) context.field( "data.y" ).get() ) );

		val fieldset = fieldDefinition( "data", fieldsetDefinition )
				.setFields( Arrays.asList( x, y, sum ) );

		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "calc", Collections.singletonList( fieldset ) ) );
		assertThat( document.isCalculatedFieldsEnabled() ).isFalse();
		assertThat( document.getContent() ).doesNotContainKey( "data" );
		assertThat( document.getContent() ).doesNotContainKey( "data.sum" );
		assertThat( document.<Map<Object, Object>>getValue( "data" ) ).isNull();
		document.updateCalculatedFields();
		assertThat( document.getContent() ).containsEntry( "data", Collections.singletonMap( "sum", null ) );
		assertThat( document.isCalculatedFieldsEnabled() ).isFalse();

		assertThat( document.<Integer>getValue( "data.x" ) ).isNull();
		assertThat( document.<Integer>getValue( "data.y" ) ).isNull();
		assertThat( document.<Integer>getValue( "data.sum" ) ).isNull();

		document.setValue( "data.x", 10 );
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 10 );
		assertThat( document.<Integer>getValue( "data.y" ) ).isNull();
		assertThat( document.<Integer>getValue( "data.sum" ) ).isNull();

		document.setValue( "data.y", 5 );
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 10 );
		assertThat( document.<Integer>getValue( "data.y" ) ).isEqualTo( 5 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isNull();

		document.updateCalculatedFields();
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 15 );
		assertThat( (Map<String, Object>) document.getContent().get( "data" ) )
				.containsEntry( "sum", 15 )
				.containsEntry( "x", 10 )
				.containsEntry( "y", 5 );

		document.setValue( "data.x", 3 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 15 );
		assertThat( (Map<String, Object>) document.getContent().get( "data" ) )
				.containsEntry( "sum", 15 )
				.containsEntry( "x", 3 )
				.containsEntry( "y", 5 );

		document.setCalculatedFieldsEnabled( true );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 8 );
		assertThat( (Map<String, Object>) document.getContent().get( "data" ) )
				.containsEntry( "sum", 8 )
				.containsEntry( "x", 3 )
				.containsEntry( "y", 5 );

		document.<Map<String, Object>>getValue( "data" ).put( "sum", 50000 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 8 );

		document.<Map<String, Object>>getValue( "data" ).put( "x", 10 );
		assertThat( document.<Map<String, Object>>getValue( "data" ) )
				.containsEntry( "sum", 15 )
				.containsEntry( "x", 10 )
				.containsEntry( "y", 5 );

		document.setValue( "data.x", 3 );

		document.setCalculatedFieldsEnabled( false );
		document.<Map<String, Object>>getValue( "data" ).put( "sum", 50000 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 50000 );

		document.setValue( "data.y", 3 );
		document.updateCalculatedFields();
		assertThat( (Map<String, Object>) document.getContent().get( "data" ) )
				.containsEntry( "sum", 6 )
				.containsEntry( "x", 3 )
				.containsEntry( "y", 3 );
		assertThat( document.<Map<String, Object>>getValue( "data" ) )
				.containsEntry( "sum", 6 )
				.containsEntry( "x", 3 )
				.containsEntry( "y", 3 );
	}

	@Test
	public void simpleCollectionField() {
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicTypeDefinition listDefinition = mock( DynamicTypeDefinition.class );

		val item = fieldDefinition( "list[]", typeDefinitionMock ).setMember( true );
		val list = fieldDefinition( "list", listDefinition ).setCollection( true ).setMemberFieldDefinition( item );
		item.setCollectionFieldDefinition( list );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "collection", Arrays.asList( list, item ) ) );

		assertThat( document.<ArrayList<Integer>>getValue( "list" ) ).isNull();
		assertThat( document.setValue( "list", Collections.emptyList() ) ).isNull();

		assertThat( document.<ArrayList<Integer>>getValue( "list" ) ).isNotNull().isEmpty();

		assertThat( document.setValue( "list", Arrays.asList( 1, 2 ) ) ).isEqualTo( Collections.emptyList() );
		assertThat( document.<ArrayList<Integer>>getValue( "list" ) ).containsExactly( 1, 2 );

		assertThat( document.setValue( "list", Collections.singleton( 5 ) ) ).isEqualTo( Arrays.asList( 1, 2 ) );
		assertThat( document.<ArrayList<Integer>>getValue( "list" ) ).containsExactly( 5 );
	}

	@Test
	public void collectionOfCollectionField() {
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicTypeDefinition listDefinition = mock( DynamicTypeDefinition.class );

		val listOfList = fieldDefinition( "list", listDefinition ).setCollection( true );
		val list = fieldDefinition( "list[]", listDefinition ).setMember( true ).setCollection( true ).setCollectionFieldDefinition( listOfList );
		listOfList.setMemberFieldDefinition( list );
		val item = fieldDefinition( "list[][]", typeDefinitionMock ).setMember( true ).setCollectionFieldDefinition( list );
		list.setMemberFieldDefinition( item );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "collection", Arrays.asList( listOfList, list, item ) ) );

		assertThat( document.<ArrayList<List<Integer>>>getValue( "list" ) ).isNull();
		assertThat( document.setValue( "list", Collections.emptyList() ) ).isNull();

		assertThat( document.<ArrayList<List<Integer>>>getValue( "list" ) ).isNotNull().isEmpty();

		assertThat( document.setValue( "list", Arrays.asList( Arrays.asList( 1, 2 ), Collections.singleton( 4 ) ) ) ).isEqualTo( Collections.emptyList() );
		assertThat( document.<ArrayList<List<Integer>>>getValue( "list" ) )
				.containsExactly( Arrays.asList( 1, 2 ), Collections.singletonList( 4 ) );

		assertThat( document.setValue( "list", Collections.singleton( Collections.singleton( 5 ) ) ) )
				.isEqualTo( Arrays.asList( Arrays.asList( 1, 2 ), Collections.singletonList( 4 ) ) );
		assertThat( document.<ArrayList<List<Integer>>>getValue( "list" ) )
				.containsExactly( Collections.singletonList( 5 ) );
	}

	@Test
	public void collectionWithFieldsetMember() {
		DynamicTypeDefinition fieldsetDefinition = mock( DynamicTypeDefinition.class );
		when( fieldsetDefinition.getTypeDescriptor() ).thenReturn( DynamicDataFieldSet.TYPE_DESCRIPTOR );
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicTypeDefinition listDefinition = mock( DynamicTypeDefinition.class );

		val list = fieldDefinition( "list", listDefinition ).setCollection( true );
		val value = fieldDefinition( "value", typeDefinitionMock );
		val item = fieldDefinition( "list[]", fieldsetDefinition )
				.setFields( Collections.singletonList( value ) )
				.setMember( true )
				.setCollectionFieldDefinition( list );
		list.setMemberFieldDefinition( item );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "collection", Arrays.asList( list, item ) ) );

		assertThat( document.<ArrayList<Map>>getValue( "list" ) ).isNull();
		assertThat( document.setValue( "list", Collections.emptyList() ) ).isNull();

		assertThat( document.<ArrayList<Map>>getValue( "list" ) ).isNotNull().isEmpty();

		assertThat( document.setValue( "list", Arrays.asList( Collections.singletonMap( "value", 123 ), Collections.singletonMap( "value", 456 ) ) ) )
				.isEqualTo( Collections.emptyList() );
		assertThat( document.<ArrayList<Map>>getValue( "list" ) )
				.containsExactly( Collections.singletonMap( "value", 123 ), Collections.singletonMap( "value", 456 ) );

		assertThat( document.setValue( "list", Collections.singleton( Collections.singletonMap( "value", 678 ) ) ) )
				.isEqualTo( Arrays.asList( Collections.singletonMap( "value", 123 ), Collections.singletonMap( "value", 456 ) ) );
		assertThat( document.<ArrayList<Map>>getValue( "list" ) ).containsExactly( Collections.singletonMap( "value", 678 ) );
	}

	@Test
	public void collectionCalculatedField() {
		DynamicTypeDefinition fieldsetDefinition = mock( DynamicTypeDefinition.class );
		when( fieldsetDefinition.getTypeDescriptor() ).thenReturn( DynamicDataFieldSet.TYPE_DESCRIPTOR );

		DynamicTypeDefinition listDefinition = mock( DynamicTypeDefinition.class );

		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );
		DynamicDocumentFieldDefinition x = fieldDefinition( "x", typeDefinitionMock );
		DynamicDocumentFieldDefinition y = fieldDefinition( "y", typeDefinitionMock )
				.setExpression( ( e, context ) -> context.field( "x" ).get() );

		val list = fieldDefinition( "list", listDefinition ).setCollection( true );
		val fieldset = fieldDefinition( "list[]", fieldsetDefinition )
				.setFields( Collections.singletonList( y ) )
				.setMember( true )
				.setCollectionFieldDefinition( list );
		list.setMemberFieldDefinition( fieldset );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "calc", Arrays.asList( x, list, fieldset ) ) );

		assertThat( document.isCalculatedFieldsEnabled() ).isFalse();
		assertThat( document.getContent() ).doesNotContainKey( "x" );
		assertThat( document.getContent() ).doesNotContainKey( "list" );
		assertThat( document.<Map<Object, Object>>getValue( "x" ) ).isNull();
		assertThat( document.<List<?>>getValue( "list" ) ).isNull();
		document.setValue( "x", 5 );
		document.updateCalculatedFields();
		assertThat( document.<Integer>getValue( "x" ) ).isEqualTo( 5 );
		assertThat( document.<List<?>>getValue( "list" ) ).isNull();

		document.setValue( "list", Arrays.asList( Collections.singletonMap( "y", 1 ), Collections.singletonMap( "y", 2 ) ) );
		assertThat( document.<List<Map<String, Object>>>getValue( "list" ) )
				.containsExactly( Collections.singletonMap( "y", 1 ), Collections.singletonMap( "y", 2 ) );
		document.updateCalculatedFields();
		assertThat( document.<List<Map<String, Object>>>getValue( "list" ) )
				.containsExactly( Collections.singletonMap( "y", 5 ), Collections.singletonMap( "y", 5 ) );

		document.setValue( "x", 7 );
		assertThat( document.<List<Map<String, Object>>>getValue( "list" ) )
				.containsExactly( Collections.singletonMap( "y", 5 ), Collections.singletonMap( "y", 5 ) );

		document.setCalculatedFieldsEnabled( true );
		assertThat( document.<List<Map<String, Object>>>getValue( "list" ) )
				.containsExactly( Collections.singletonMap( "y", 7 ), Collections.singletonMap( "y", 7 ) );
		document.setValue( "list", Arrays.asList( Collections.singletonMap( "y", 1 ), Collections.singletonMap( "y", 2 ) ) );
		assertThat( document.<List<Map<String, Object>>>getValue( "list" ) )
				.containsExactly( Collections.singletonMap( "y", 7 ), Collections.singletonMap( "y", 7 ) );
		document.setValue( "x", 10 );
		assertThat( document.<List<Map<String, Object>>>getValue( "list" ) )
				.containsExactly( Collections.singletonMap( "y", 10 ), Collections.singletonMap( "y", 10 ) );
	}

	@Test
	@SuppressWarnings("unchecked")
	public void fieldsetCalculatedRelativeField() {
		DynamicTypeDefinition fieldsetDefinition = mock( DynamicTypeDefinition.class );
		when( fieldsetDefinition.getTypeDescriptor() ).thenReturn( DynamicDataFieldSet.TYPE_DESCRIPTOR );

		DynamicDocumentFieldDefinition x = fieldDefinition( "x", typeDefinitionMock );
		DynamicDocumentFieldDefinition y = fieldDefinition( "y", typeDefinitionMock );
		val sum = fieldDefinition( "sum", typeDefinitionMock )
				.setExpression( ( e, context ) -> ( (int) context.field( ".x" ).get() ) + ( (int) context.field( ".y" ).get() ) );

		val fieldset = fieldDefinition( "data", fieldsetDefinition )
				.setFields( Arrays.asList( x, y, sum ) );

		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "calc", Collections.singletonList( fieldset ) ) );
		assertThat( document.isCalculatedFieldsEnabled() ).isFalse();
		assertThat( document.getContent() ).doesNotContainKey( "data" );
		assertThat( document.getContent() ).doesNotContainKey( "data.sum" );
		assertThat( document.<Map<Object, Object>>getValue( "data" ) ).isNull();
		document.updateCalculatedFields();
		assertThat( document.getContent() ).containsEntry( "data", Collections.singletonMap( "sum", null ) );
		assertThat( document.isCalculatedFieldsEnabled() ).isFalse();

		assertThat( document.<Integer>getValue( "data.x" ) ).isNull();
		assertThat( document.<Integer>getValue( "data.y" ) ).isNull();
		assertThat( document.<Integer>getValue( "data.sum" ) ).isNull();

		document.setValue( "data.x", 10 );
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 10 );
		assertThat( document.<Integer>getValue( "data.y" ) ).isNull();
		assertThat( document.<Integer>getValue( "data.sum" ) ).isNull();

		document.setValue( "data.y", 5 );
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 10 );
		assertThat( document.<Integer>getValue( "data.y" ) ).isEqualTo( 5 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isNull();

		document.updateCalculatedFields();
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 15 );
		assertThat( (Map<String, Object>) document.getContent().get( "data" ) )
				.containsEntry( "sum", 15 )
				.containsEntry( "x", 10 )
				.containsEntry( "y", 5 );

		document.setValue( "data.x", 3 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 15 );
		assertThat( (Map<String, Object>) document.getContent().get( "data" ) )
				.containsEntry( "sum", 15 )
				.containsEntry( "x", 3 )
				.containsEntry( "y", 5 );

		document.setCalculatedFieldsEnabled( true );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 8 );
		assertThat( (Map<String, Object>) document.getContent().get( "data" ) )
				.containsEntry( "sum", 8 )
				.containsEntry( "x", 3 )
				.containsEntry( "y", 5 );

		document.<Map<String, Object>>getValue( "data" ).put( "sum", 50000 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 8 );

		document.<Map<String, Object>>getValue( "data" ).put( "x", 10 );
		assertThat( document.<Map<String, Object>>getValue( "data" ) )
				.containsEntry( "sum", 15 )
				.containsEntry( "x", 10 )
				.containsEntry( "y", 5 );

		document.setValue( "data.x", 3 );

		document.setCalculatedFieldsEnabled( false );
		document.<Map<String, Object>>getValue( "data" ).put( "sum", 50000 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 50000 );

		document.setValue( "data.y", 3 );
		document.updateCalculatedFields();
		assertThat( (Map<String, Object>) document.getContent().get( "data" ) )
				.containsEntry( "sum", 6 )
				.containsEntry( "x", 3 )
				.containsEntry( "y", 3 );
		assertThat( document.<Map<String, Object>>getValue( "data" ) )
				.containsEntry( "sum", 6 )
				.containsEntry( "x", 3 )
				.containsEntry( "y", 3 );
	}

	@Test
	public void simpleFieldAccessors() {
		DynamicDocumentDefinition documentDefinition = documentWithComplexAddress();
		DynamicDocumentData document = new DynamicDocumentData( documentDefinition );

		document.setValue( "name", "my name" );
		document.setValue( "address.street.name", "some street" );
		document.setValue( "address.street.number", "123" );

		DynamicDocumentFieldAccessor name = document.getFieldAccessor( "name" );
		assertThat( name.getFieldPath() ).isEqualTo( "name" );
		assertThat( name.get() ).isEqualTo( "my name" );

		DynamicDocumentFieldAccessor address = document.getFieldAccessor( "address" );
		assertThat( address.getFieldPath() ).isEqualTo( "address" );
		ImmutableMap<String, String> streetValue = ImmutableMap.of( "name", "some street", "number", "123" );
		assertThat( address.get() ).isEqualTo( Collections.singletonMap( "street", streetValue ) );

		DynamicDocumentFieldAccessor street = document.getFieldAccessor( "address.street" );
		assertThat( street.getFieldPath() ).isEqualTo( "address.street" );
		assertThat( street.get() ).isEqualTo( streetValue );

		DynamicDocumentFieldAccessor streetFromAddress = address.getFieldAccessor( "street" );
		assertThat( streetFromAddress.get() ).isEqualTo( streetValue );
		assertThat( streetFromAddress ).isSameAs( street );

		DynamicDocumentFieldAccessor streetName = address.getFieldAccessor( "street.name" );
		assertThat( streetName.getFieldPath() ).isEqualTo( "address.street.name" );
		assertThat( streetName.get() ).isEqualTo( "some street" );
		DynamicDocumentFieldAccessor streetNumber = address.getFieldAccessor( "street.number" );
		assertThat( streetNumber.getFieldPath() ).isEqualTo( "address.street.number" );
		assertThat( streetNumber.get() ).isEqualTo( "123" );

		assertThat( streetFromAddress.getFieldAccessor( "name" ) ).isSameAs( streetName );
		assertThat( document.getFieldAccessor( "address.street.name" ) ).isSameAs( streetName );
		assertThat( streetFromAddress.getFieldAccessor( "number" ) ).isSameAs( streetNumber );
		assertThat( document.getFieldAccessor( "address.street.number" ) ).isSameAs( streetNumber );

		DynamicDocumentFieldAccessorResolver resolver = (DynamicDocumentFieldAccessorResolver) streetName;
		assertThat( resolver.findFieldAccessor( ".number" ) ).contains( streetNumber );
		assertThat( resolver.findFieldAccessor( "..street" ) ).contains( street );
		assertThat( resolver.findFieldAccessor( "...address" ) ).contains( address );
		assertThat( resolver.findFieldAccessor( "...name" ) ).contains( name );
		assertThat( resolver.findFieldAccessor( "....name" ) ).isEmpty();
	}

	@Test
	public void collectionFieldAccessors() {
		DynamicTypeDefinition fieldsetDefinition = mock( DynamicTypeDefinition.class );
		when( fieldsetDefinition.getTypeDescriptor() ).thenReturn( DynamicDataFieldSet.TYPE_DESCRIPTOR );
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicTypeDefinition listDefinition = mock( DynamicTypeDefinition.class );
		when( listDefinition.getTypeDescriptor() ).thenReturn(
				TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( DynamicDataFieldSet.class ) ) );

		val listDef = fieldDefinition( "list", listDefinition ).setCollection( true );
		val xDef = fieldDefinition( "x", typeDefinitionMock );
		val yDef = fieldDefinition( "y", typeDefinitionMock );
		val sum = fieldDefinition( "sum", typeDefinitionMock )
				.setExpression(
						( e, context ) -> new DynamicDocumentFormulaFunctions().sum( context.field( "list.x" ).get(), context.field( "list.y" ).get() )
				);
		val itemDef = fieldDefinition( "list[]", fieldsetDefinition )
				.setFields( Arrays.asList( xDef, yDef, sum ) )
				.setMember( true )
				.setCollectionFieldDefinition( listDef );
		listDef.setMemberFieldDefinition( itemDef );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "collection", Arrays.asList( listDef, itemDef ) ) );
		ImmutableMap<String, Integer> itemOne = ImmutableMap.of( "x", 5, "y", 10 );
		ImmutableMap<String, Integer> itemTwo = ImmutableMap.of( "x", 1, "y", 2 );
		document.setValue( "list", Arrays.asList( itemOne, itemTwo ) );

		DynamicDocumentFieldAccessor list = document.getFieldAccessor( "list" );
		assertThat( list ).isNotNull();
		DynamicDocumentFieldAccessor items = document.getFieldAccessor( "list[]" );
		assertThat( items ).isNotNull();
		assertThat( list.get() ).isEqualTo( items.get() );
		assertThat( items.getFieldPath() ).isEqualTo( "list[]" );
		assertThat( list.getFieldPath() ).isEqualTo( "list" );
		assertThat( list.getFieldType() ).isEqualTo( TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( DynamicDataFieldSet.class ) ) );
		assertThat( list.getFieldDefinition().getId() ).isEqualTo( "list" );
		assertThat( list.get() ).isEqualTo( Arrays.asList( itemOne, itemTwo ) );

		DynamicDocumentFieldAccessor item = document.getFieldAccessor( "list[0]" );
		assertThat( item ).isNotNull();
		assertThat( item.getFieldPath() ).isEqualTo( "list[0]" );
		assertThat( item.getFieldType() ).isEqualTo( TypeDescriptor.valueOf( DynamicDataFieldSet.class ) );
		assertThat( item.getFieldDefinition().getId() ).isEqualTo( "list[]" );
		assertThat( item.get() ).isEqualTo( itemOne );

		DynamicDocumentFieldAccessor x = document.getFieldAccessor( "list[0].x" );
		assertThat( x ).isNotNull();
		assertThat( x.getFieldPath() ).isEqualTo( "list[0].x" );
		assertThat( x.getFieldType() ).isEqualTo( TypeDescriptor.valueOf( Integer.class ) );
		assertThat( x.getFieldDefinition().getId() ).isEqualTo( "x" );
		assertThat( x.get() ).isEqualTo( 5 );
		assertThat( x.set( 7 ) ).isEqualTo( 5 );

		assertThat( x.get() ).isEqualTo( 7 );
		assertThat( item.getFieldAccessor( "x" ).get() ).isEqualTo( 7 );
		assertThat( list.getFieldAccessor( "[0].x" ).get() ).isEqualTo( 7 );
		assertThat( list.getFieldAccessor( "[1].x" ).get() ).isEqualTo( 1 );

		x = list.getFieldAccessor( "[].x" );
		assertThat( x ).isNotNull();
		assertThat( x.getFieldPath() ).isEqualTo( "list[].x" );
		assertThat( x.getFieldType() ).isEqualTo( TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( Integer.class ) ) );
		assertThat( x.getFieldDefinition().getId() ).isEqualTo( "x" );
		assertThat( x.get() ).isEqualTo( Arrays.asList( 7, 1 ) );
		assertThat( list.getFieldAccessor( "[].x" ) ).isSameAs( x );

		x.set( Arrays.asList( 6, 7 ) );
		assertThat( list.get() ).isEqualTo( Arrays.asList( ImmutableMap.of( "x", 6, "y", 10 ), ImmutableMap.of( "x", 7, "y", 2 ) ) );

		assertThat( document.getFieldAccessor( "list[].x" ).get() ).isEqualTo( Arrays.asList( 6, 7 ) );
		assertThat( document.<List>getValue( "list[].x" ) ).isEqualTo( Arrays.asList( 6, 7 ) );

		x = document.getFieldAccessor( "list[0].x" );
		assertThat( x ).isNotNull();
		assertThat( x.getFieldPath() ).isEqualTo( "list[0].x" );
		assertThat( x.getFieldType() ).isEqualTo( TypeDescriptor.valueOf( Integer.class ) );
		assertThat( x.getFieldDefinition().getId() ).isEqualTo( "x" );
		assertThat( x.get() ).isEqualTo( 6 );
		x.set( 12 );

		DynamicDocumentFieldAccessor y = list.getFieldAccessor( "[].y" );
		assertThat( y.get() ).isEqualTo( Arrays.asList( 10, 2 ) );

		y.delete();
		assertThat( list.get() ).isEqualTo( Arrays.asList( Collections.singletonMap( "x", 12 ), Collections.singletonMap( "x", 7 ) ) );
		assertThat( y.get() ).isEqualTo( Arrays.asList( null, null ) );

		assertThat( y.exists() ).isTrue();
		y.set( Arrays.asList( 4, 5 ) );
		assertThat( list.get() ).isEqualTo( Arrays.asList( ImmutableMap.of( "x", 12, "y", 4 ), ImmutableMap.of( "x", 7, "y", 5 ) ) );

		document.deleteValue( "list[].x" );
		assertThat( list.get() ).isEqualTo( Arrays.asList( Collections.singletonMap( "y", 4 ), Collections.singletonMap( "y", 5 ) ) );
		document.setValue( "list[].x", Arrays.asList( 0, 0 ) );
		assertThat( list.get() ).isEqualTo( Arrays.asList( ImmutableMap.of( "x", 0, "y", 4 ), ImmutableMap.of( "x", 0, "y", 5 ) ) );
	}

	@Test
	public void initializeSimpleFieldsWithValue() {
		DynamicDocumentFieldDefinition name = fieldDefinition( "name", typeDefinitionMock )
				.setInitializerExpression( ( e, f ) -> "John Doe" );
		DynamicDocumentFieldDefinition city = fieldDefinition( "city", typeDefinitionMock )
				.setInitializerExpression( ( e, f ) -> null );  // explicit null value
		DynamicDocumentFieldDefinition country = fieldDefinition( "country", typeDefinitionMock );
		DynamicDocumentDefinition documentDefinition = documentDefinition( "my-doc", Arrays.asList( name, city, country ) );
		DynamicDocumentData data = new DynamicDocumentData( documentDefinition );

		assertThat( data.<String>getDefaultValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( data.<String>getDefaultValue( "city" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "country" ) ).isNull();

		Map<String, Object> rawData = data.getContent();
		assertThat( rawData ).isEmpty();
		assertThat( data.<String>getValue( "name" ) ).isNull();
		assertThat( data.<String>findValue( "name" ) ).isNull();
		assertThat( data.<String>getValue( "city" ) ).isNull();
		assertThat( data.<String>findValue( "city" ) ).isNull();
		assertThat( data.<String>getValue( "country" ) ).isNull();
		assertThat( data.<String>findValue( "country" ) ).isNull();

		data.initializeFields();
		assertThat( rawData ).hasSize( 2 );
		assertThat( data.<String>getValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( data.<String>getValue( "city" ) ).isNull();
		assertThat( data.<String>findValue( "city" ) ).isNotNull();
		assertThat( data.<String>getValue( "country" ) ).isNull();
		assertThat( data.<String>findValue( "country" ) ).isNull();

		assertThat( data.<String>getDefaultValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( data.<String>getDefaultValue( "city" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "country" ) ).isNull();

		data = new DynamicDocumentData( documentDefinition );
		rawData = data.getContent();
		assertThat( rawData ).isEmpty();
		rawData.put( "city", "Antwerp" );

		data.initializeFields();
		assertThat( rawData ).hasSize( 2 );
		assertThat( data.<String>getValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( data.<String>getValue( "city" ) ).isEqualTo( "Antwerp" );

		rawData.put( "name", "Fred Astaire" );
		data.initializeFields();
		assertThat( data.<String>getValue( "name" ) ).isEqualTo( "Fred Astaire" );
		assertThat( data.<String>getValue( "city" ) ).isEqualTo( "Antwerp" );

		assertThat( data.<String>getDefaultValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( data.<String>getDefaultValue( "city" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "country" ) ).isNull();

		data = new DynamicDocumentData( documentDefinition );
		assertThat( data.getFieldAccessor( "name" ).exists() ).isFalse();
		assertThat( data.getFieldAccessor( "name" ).get() ).isNull();
		data.getFieldAccessor( "name" ).initialize();
		assertThat( data.getFieldAccessor( "name" ).get() ).isEqualTo( "John Doe" );
		assertThat( data.getFieldAccessor( "name" ).exists() ).isTrue();

		assertThat( data.<String>getDefaultValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( data.<String>getDefaultValue( "city" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "country" ) ).isNull();

		data = new DynamicDocumentData( documentDefinition );
		assertThat( data.getFieldAccessor( "name" ).exists() ).isFalse();
		assertThat( data.getFieldAccessor( "name" ).get() ).isNull();
		assertThat( data.getFieldAccessor( "name" ).getOrCreate() ).isEqualTo( "John Doe" );
		assertThat( data.getFieldAccessor( "name" ).exists() ).isTrue();

		assertThat( data.<String>getDefaultValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( data.<String>getDefaultValue( "city" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "country" ) ).isNull();
	}

	@Test
	@Ignore("AXDFM-8 - default values in fieldset/collection items are not correctly supported")
	@SuppressWarnings("unchecked")
	public void initializeFieldsetWithValue() {
		DynamicDocumentFieldDefinition name = fieldDefinition( "name", typeDefinitionMock );
		DynamicDocumentFieldDefinition streetName = fieldDefinition( "name", typeDefinitionMock );
		val streetNumber = fieldDefinition( "number", typeDefinitionMock );

		GenericDynamicTypeDefinition typeDefinition = mock( GenericDynamicTypeDefinition.class );
		when( typeDefinition.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( DynamicDataFieldSet.class ) );

		DynamicDocumentFieldDefinition street = fieldDefinition( "street", typeDefinition )
				.setFields( Arrays.asList( streetName, streetNumber ) );

		val address = fieldDefinition( "address", typeDefinition )
				.setFields( Collections.singletonList( street ) );

		DynamicDocumentDefinition documentDefinition = documentDefinition( "my-doc", Arrays.asList( name, address ) );

		DynamicDocumentData data = new DynamicDocumentData( documentDefinition );
		Map<String, Object> rawData = data.getContent();
		assertThat( rawData ).isEmpty();

		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<Map>getDefaultValue( "address" ) ).isNull();

		data.initializeFields();
		assertThat( rawData ).isEmpty();

		address.setInitializerExpression( ( e, c ) -> Collections.singletonMap( "street", Collections.singletonMap( "name", "Some street" ) ) );

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isNull();

		data.initializeFields();
		assertThat( rawData ).hasSize( 1 );
		assertThat( data.<String>getValue( "address.street.name" ) ).isEqualTo( "Some street" );
		assertThat( data.<String>getValue( "address.street.number" ) ).isNull();

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isNull();

		data.initializeFields();
		assertThat( rawData ).hasSize( 1 );
		assertThat( data.<String>getValue( "address.street.name" ) ).isEqualTo( "Some street" );
		assertThat( data.<String>getValue( "address.street.number" ) ).isNull();

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isNull();

		streetNumber.setInitializerExpression( ( c, f ) -> "123" );

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isEqualTo( "123" );

		data.initializeFields();
		assertThat( rawData ).hasSize( 1 );
		assertThat( data.<String>getValue( "address.street.name" ) ).isEqualTo( "Some street" );
		assertThat( data.<String>getValue( "address.street.number" ) ).isEqualTo( "123" );

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isEqualTo( "123" );

		data = new DynamicDocumentData( documentDefinition );
		assertThat( data.getFieldAccessor( "address" ).exists() ).isFalse();
		assertThat( data.getFieldAccessor( "address" ).get() ).isNull();
		data.getFieldAccessor( "address" ).initialize();
		assertThat( data.getFieldAccessor( "address" ).get() )
				.isEqualTo( Collections.singletonMap( "street", ImmutableMap.of( "name", "Some street", "number", "123" ) ) );
		assertThat( data.getFieldAccessor( "address" ).exists() ).isTrue();

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isEqualTo( "123" );

		data = new DynamicDocumentData( documentDefinition );
		assertThat( data.getFieldAccessor( "address" ).exists() ).isFalse();
		assertThat( data.getFieldAccessor( "address" ).get() ).isNull();
		assertThat( data.getFieldAccessor( "address" ).getOrCreate() )
				.isEqualTo( Collections.singletonMap( "street", ImmutableMap.of( "name", "Some street", "number", "123" ) ) );
		assertThat( data.getFieldAccessor( "address" ).exists() ).isTrue();

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isEqualTo( "123" );

		// implicit additional field initialization when setting a value
		data = new DynamicDocumentData( documentDefinition );
		data.setValue( "address", ImmutableMap.of( "street", Collections.singletonMap( "name", "hello street" ) ) );
		assertThat( data.<Map>getValue( "address" ) )
				.isEqualTo( Collections.singletonMap( "street", ImmutableMap.of( "name", "hello street", "number", "123" ) ) );

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isEqualTo( "123" );

		// explicitly initialize fields that do not exist yet
		data = new DynamicDocumentData( documentDefinition );
		DynamicDataFieldSet addr = new DynamicDataFieldSet();
		DynamicDataFieldSet str = new DynamicDataFieldSet();
		addr.put( "street", str );
		str.put( "name", "my street" );
		data.getContent().put( "address", addr );
		assertThat( data.<Map>getValue( "address" ) )
				.isEqualTo( Collections.singletonMap( "street", ImmutableMap.of( "name", "my street" ) ) );
		data.initializeFields();
		assertThat( data.<Map>getValue( "address" ) )
				.isEqualTo( Collections.singletonMap( "street", ImmutableMap.of( "name", "my street", "number", "123" ) ) );

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isEqualTo( "123" );

		// skip initialize fields if an explicit null value is set
		data = new DynamicDocumentData( documentDefinition );
		data.setValue( "address", null );
		assertThat( data.<Map>getValue( "address" ) ).isNull();
		assertThat( data.getFieldAccessor( "address" ).exists() ).isTrue();

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isEqualTo( "123" );

		data = new DynamicDocumentData( documentDefinition );
		data.getContent().put( "address", null );
		data.initializeFields();
		assertThat( data.<Map>getValue( "address" ) ).isNull();
		assertThat( data.getFieldAccessor( "address" ).exists() ).isTrue();

		assertThat( data.<Map>getDefaultValue( "address" ) ).containsEntry( "street", Collections.singletonMap( "name", "Some street" ) );
		assertThat( data.<Map>getDefaultValue( "address.street" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.name" ) ).isNull();
		assertThat( data.<String>getDefaultValue( "address.street.number" ) ).isEqualTo( "123" );
	}

	@Test
	@Ignore("AXDFM-8 - default values in fieldset/collection items are not correctly supported")
	@SuppressWarnings("unchecked")
	public void initializeFieldsWithFormula() {
		DynamicTypeDefinition fieldsetDefinition = mock( DynamicTypeDefinition.class );
		when( fieldsetDefinition.getTypeDescriptor() ).thenReturn( DynamicDataFieldSet.TYPE_DESCRIPTOR );

		DynamicDocumentFieldDefinition x = fieldDefinition( "x", typeDefinitionMock )
				.setInitializerExpression( ( e, context ) -> (int) context.field( ".y" ).get() + 1 );
		DynamicDocumentFieldDefinition y = fieldDefinition( "y", typeDefinitionMock )
				.setInitializerExpression( ( e, context ) -> 1 );
		val sum = fieldDefinition( "sum", typeDefinitionMock )
				.setInitializerExpression( ( e, context ) -> ( (int) context.field( ".x" ).get() ) + ( (int) context.field( ".y" ).get() ) )
				.setExpression( ( e, context ) -> ( (int) context.field( ".x" ).get() ) + ( (int) context.field( ".y" ).get() ) );

		val fieldset = fieldDefinition( "data", fieldsetDefinition )
				.setFields( Arrays.asList( x, y, sum ) );

		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicDocumentDefinition documentDefinition = documentDefinition( "calc", Collections.singletonList( fieldset ) );
		DynamicDocumentData document = new DynamicDocumentData( documentDefinition );
		assertThat( document.getContent() ).isEmpty();
		assertThat( document.<Map>getDefaultValue( "data" ) ).isNull();
		assertThat( document.<Integer>getDefaultValue( "data.y" ) ).isEqualTo( 1 );
		assertThat( document.<Integer>getDefaultValue( "data.x" ) ).isEqualTo( 2 );
		assertThat( document.<Integer>getDefaultValue( "data.sum" ) ).isEqualTo( 3 );

		document.getFieldAccessor( "data" ).initialize();
		assertThat( document.getFieldAccessor( "data" ).get() ).isEqualTo( ImmutableMap.of( "x", 2, "y", 1, "sum", 3 ) );

		document = new DynamicDocumentData( documentDefinition );
		document.initializeFields();
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 2 );
		assertThat( document.<Integer>getValue( "data.y" ) ).isEqualTo( 1 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 3 );

		document.setValue( "data.y", 5 );
		document.initializeFields();
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 2 );
		assertThat( document.<Integer>getValue( "data.y" ) ).isEqualTo( 5 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 3 );

		document.updateCalculatedFields();
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 2 );
		assertThat( document.<Integer>getValue( "data.y" ) ).isEqualTo( 5 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 7 );

		// get or create causes initialization of a fieldset
		document = new DynamicDocumentData( documentDefinition );
		assertThat( document.getFieldAccessor( "data" ).getOrCreate() )
				.isEqualTo( ImmutableMap.of( "x", 2, "y", 1, "sum", 3 ) );

		// setting a fieldset field value, causes initialization of the fieldset
		document = new DynamicDocumentData( documentDefinition );
		document.setEvaluationContext( BuildDynamicDocumentFormulaEvaluationContext.createDefaultEvaluationContext() );

		assertThat( document.getContent() ).isEmpty();

		document.setValue( "data.y", 2 );
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 2 );
		assertThat( document.<Integer>getValue( "data.y" ) ).isEqualTo( 2 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 3 );

		// setting a fieldset value, causes initialization of the individual fields, but before the value is assigned
		document = new DynamicDocumentData( documentDefinition );
		assertThat( document.getContent() ).isEmpty();
		document.setValue( "data", Collections.singletonMap( "y", 2 ) );
		assertThat( document.<Integer>getValue( "data.y" ) ).isEqualTo( 2 );
		assertThat( document.<Integer>getValue( "data.x" ) ).isEqualTo( 2 );
		assertThat( document.<Integer>getValue( "data.sum" ) ).isEqualTo( 3 );
	}

	@Test
	public void initializeCollection() {
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicTypeDefinition listDefinition = mock( DynamicTypeDefinition.class );
		val listDef = fieldDefinition( "list", listDefinition ).setCollection( true ).setInitializerExpression( ( c, f ) -> Arrays.asList( 1, 2 ) );
		val itemDef = fieldDefinition( "list[]", typeDefinitionMock )
				.setMember( true )
				.setCollectionFieldDefinition( listDef );
		listDef.setMemberFieldDefinition( itemDef );

		DynamicDocumentDefinition documentDefinition = documentDefinition( "collection", Arrays.asList( listDef, itemDef ) );
		DynamicDocumentData document = new DynamicDocumentData( documentDefinition );
		document.getFieldAccessor( "list" ).initialize();
		assertThat( document.getFieldAccessor( "list" ).get() ).isEqualTo( Arrays.asList( 1, 2 ) );

		document = new DynamicDocumentData( documentDefinition );
		assertThat( document.getFieldAccessor( "list" ).get() ).isNull();
		assertThat( document.getFieldAccessor( "list" ).getOrCreate() ).isEqualTo( Arrays.asList( 1, 2 ) );
	}

	@Test
	public void initializeFieldsOfCollectionMember() {
		DynamicTypeDefinition fieldsetDefinition = mock( DynamicTypeDefinition.class );
		when( fieldsetDefinition.getTypeDescriptor() ).thenReturn( DynamicDataFieldSet.TYPE_DESCRIPTOR );
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( Integer.class ) );

		DynamicTypeDefinition listDefinition = mock( DynamicTypeDefinition.class );
		val listDef = fieldDefinition( "list", listDefinition ).setCollection( true );
		val xDef = fieldDefinition( "x", typeDefinitionMock ).setInitializerExpression( ( e, c ) -> 3 );
		val yDef = fieldDefinition( "y", typeDefinitionMock ).setInitializerExpression( ( e, c ) -> 2 );
		val sum = fieldDefinition( "sum", typeDefinitionMock )
				.setInitializerExpression(
						( e, context ) -> (int) context.field( ".x" ).get() + (int) context.field( ".y" ).get()
				);
		val itemDef = fieldDefinition( "list[]", fieldsetDefinition )
				.setFields( Arrays.asList( xDef, yDef, sum ) )
				.setMember( true )
				.setCollectionFieldDefinition( listDef );
		listDef.setMemberFieldDefinition( itemDef );

		DynamicDocumentData document = new DynamicDocumentData( documentDefinition( "collection", Arrays.asList( listDef, itemDef ) ) );
		ImmutableMap<String, Integer> itemOne = ImmutableMap.of( "x", 5, "y", 10 );
		ImmutableMap<String, Integer> itemTwo = ImmutableMap.of( "x", 1, "y", 2 );
		document.setValue( "list", Arrays.asList( itemOne, itemTwo ) );

		DynamicDocumentFieldAccessor list = document.getFieldAccessor( "list" );
		assertThat( list ).isNotNull();
		assertThat( list.get() ).isEqualTo(
				Arrays.asList( ImmutableMap.of( "x", 5, "y", 10, "sum", 5 ),
				               ImmutableMap.of( "x", 1, "y", 2, "sum", 5 ) )
		);

		assertThat( document.getFieldAccessor( "list[0]" ).get() )
				.isEqualTo( ImmutableMap.of( "x", 5, "y", 10, "sum", 5 ) );
	}

	private DynamicDocumentDefinition documentWithAddress() {
		DynamicDocumentFieldDefinition name = fieldDefinition( "name", typeDefinitionMock ).setInitializerExpression( ( c, f ) -> "<default>" );
		DynamicDocumentFieldDefinition street = fieldDefinition( "street", typeDefinitionMock );

		GenericDynamicTypeDefinition typeDefinition = mock( GenericDynamicTypeDefinition.class );
		when( typeDefinition.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( DynamicDataFieldSet.class ) );

		DynamicDocumentFieldDefinition address = fieldDefinition( "address", typeDefinition )
				.setFields( Collections.singletonList( street ) );

		return documentDefinition( "my-doc", Arrays.asList( name, address ) );
	}

	private DynamicDocumentDefinition documentWithComplexAddress() {
		DynamicDocumentFieldDefinition name = fieldDefinition( "name", typeDefinitionMock ).setInitializerExpression( ( c, f ) -> "<default>" );
		DynamicDocumentFieldDefinition streetName = fieldDefinition( "name", typeDefinitionMock );
		DynamicDocumentFieldDefinition streetNumber = fieldDefinition( "number", typeDefinitionMock );

		GenericDynamicTypeDefinition typeDefinition = mock( GenericDynamicTypeDefinition.class );
		when( typeDefinition.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( DynamicDataFieldSet.class ) );

		DynamicDocumentFieldDefinition street = fieldDefinition( "street", typeDefinition )
				.setFields( Arrays.asList( streetName, streetNumber ) );

		DynamicDocumentFieldDefinition address = fieldDefinition( "address", typeDefinition )
				.setFields( Collections.singletonList( street ) );

		return documentDefinition( "my-doc", Arrays.asList( name, address ) );
	}

}
