/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.bootstrapui.elements.NumericFormElementConfiguration;
import com.foreach.across.modules.bootstrapui.elements.TextboxFormElement;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestSimpleDynamicTypeDefinitionBuilder
{
	@Mock
	private DynamicDocumentFieldDefinition fieldDefinition;

	@Mock
	private EntityPropertyDescriptorBuilder descriptorBuilder;

	@InjectMocks
	private SimpleDynamicTypeDefinitionBuilder builder;

	@Test
	public void accepts() {
		assertAccepted( true, "string", "char", "number", "boolean", "text", "number(short)", "number(5)", "decimal", "decimal(2)", "decimal(1)",
		                "currency(EUR)", "percentage(5)", "percentage(5,100)", "date", "datetime", "time" );
		assertAccepted( true, "string[]", "char[]", "number[]", "boolean[]", "text[]", "number(short)[]", "number(5)[]", "decimal[]", "decimal(2)[]",
		                "decimal(1)[]", "currency(eur)[]", "percentage(5)[]", "percentage(5,100)[]", "date[]", "datetime[]", "time[]" );

		assertAccepted( false, "long", "long[]", "@user", "fieldset" );
	}

	@Test
	public void textType() {
		verifyTypeDefinition( "text", TypeDescriptor.valueOf( String.class ) );
		verifyZeroInteractions( descriptorBuilder );

		verifyTypeDefinition( "text[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( String.class ) ) );
		verifyZeroInteractions( descriptorBuilder );
	}

	@Test
	public void stringTypeIsShortText() {
		verifyTypeDefinition( "string", TypeDescriptor.valueOf( String.class ) );
		verify( descriptorBuilder ).attribute( TextboxFormElement.Type.class, TextboxFormElement.Type.TEXT );
		verifyNoMoreInteractions( descriptorBuilder );

		reset( descriptorBuilder );

		verifyTypeDefinition( "string[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( String.class ) ) );
		verify( descriptorBuilder ).attribute( TextboxFormElement.Type.class, TextboxFormElement.Type.TEXT );
		verifyNoMoreInteractions( descriptorBuilder );
	}

	@Test
	public void charTypeIsShortText() {
		verifyTypeDefinition( "char", TypeDescriptor.valueOf( Character.class ) );
		verify( descriptorBuilder ).attribute( TextboxFormElement.Type.class, TextboxFormElement.Type.TEXT );
		verifyZeroInteractions( descriptorBuilder );

		reset( descriptorBuilder );

		verifyTypeDefinition( "char[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( Character.class ) ) );
		verify( descriptorBuilder ).attribute( TextboxFormElement.Type.class, TextboxFormElement.Type.TEXT );
		verifyZeroInteractions( descriptorBuilder );
	}

	@Test
	public void booleanType() {
		verifyTypeDefinition( "boolean", TypeDescriptor.valueOf( Boolean.class ) );
		verifyZeroInteractions( descriptorBuilder );

		verifyTypeDefinition( "boolean[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( Boolean.class ) ) );
		verifyZeroInteractions( descriptorBuilder );
	}

	@Test
	public void numberType() {
		verifyTypeDefinition( "number", TypeDescriptor.valueOf( Long.class ) );
		verifyZeroInteractions( descriptorBuilder );

		verifyTypeDefinition( "number[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( Long.class ) ) );
		verifyZeroInteractions( descriptorBuilder );

		Map<String, Class> types = new HashMap<String, Class>()
		{{
			put( "integer", Integer.class );
			put( "short", Short.class );
			put( "byte", Byte.class );
		}};

		types.forEach( ( key, value ) -> {
			verifyTypeDefinition( "number(" + key + ")", TypeDescriptor.valueOf( value ) );
			verifyZeroInteractions( descriptorBuilder );

			verifyTypeDefinition( "number(" + key + ")[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( value ) ) );
			verifyZeroInteractions( descriptorBuilder );
		} );
	}

	@Test
	public void decimalType() {
		{
			verifyTypeDefinition( "decimal", TypeDescriptor.valueOf( BigDecimal.class ) );
			ArgumentCaptor<NumericFormElementConfiguration> captor = ArgumentCaptor.forClass( NumericFormElementConfiguration.class );
			verify( descriptorBuilder ).attribute( eq( NumericFormElementConfiguration.class ), captor.capture() );
			assertThat( captor.getValue().get( "decimalPlaces" ) ).isEqualTo( 2 );
		}
		reset( descriptorBuilder );
		{
			verifyTypeDefinition( "decimal(5)", TypeDescriptor.valueOf( BigDecimal.class ) );
			ArgumentCaptor<NumericFormElementConfiguration> captor = ArgumentCaptor.forClass( NumericFormElementConfiguration.class );
			verify( descriptorBuilder ).attribute( eq( NumericFormElementConfiguration.class ), captor.capture() );
			assertThat( captor.getValue().get( "decimalPlaces" ) ).isEqualTo( 5 );
		}
	}

	@Test
	public void currencyType() {
		{
			verifyTypeDefinition( "currency(mxn)", TypeDescriptor.valueOf( BigDecimal.class ) );
			ArgumentCaptor<NumericFormElementConfiguration> captor = ArgumentCaptor.forClass( NumericFormElementConfiguration.class );
			verify( descriptorBuilder ).attribute( eq( NumericFormElementConfiguration.class ), captor.capture() );
			assertThat( captor.getValue().get( "decimalPlaces" ) ).isEqualTo( 2 );
			NumericFormElementConfiguration currencyConfig = new NumericFormElementConfiguration();
			currencyConfig.setCurrency( Currency.getInstance( "MXN" ) );
			assertThat( captor.getValue().getCurrency() ).isEqualTo( currencyConfig.getCurrency() );
			assertThat( captor.getValue().getFormat() ).isEqualTo( currencyConfig.getFormat() );
		}
		reset( descriptorBuilder );
		{
			verifyTypeDefinition( "currency(SgD,4)", TypeDescriptor.valueOf( BigDecimal.class ) );
			ArgumentCaptor<NumericFormElementConfiguration> captor = ArgumentCaptor.forClass( NumericFormElementConfiguration.class );
			verify( descriptorBuilder ).attribute( eq( NumericFormElementConfiguration.class ), captor.capture() );
			assertThat( captor.getValue().get( "decimalPlaces" ) ).isEqualTo( 4 );
			NumericFormElementConfiguration currencyConfig = new NumericFormElementConfiguration();
			currencyConfig.setCurrency( Currency.getInstance( "SGD" ) );
			assertThat( captor.getValue().getCurrency() ).isEqualTo( currencyConfig.getCurrency() );
			assertThat( captor.getValue().getFormat() ).isEqualTo( currencyConfig.getFormat() );
		}
	}

	@Test
	public void percentageType() {
		{
			verifyTypeDefinition( "percentage", TypeDescriptor.valueOf( BigDecimal.class ) );
			ArgumentCaptor<NumericFormElementConfiguration> captor = ArgumentCaptor.forClass( NumericFormElementConfiguration.class );
			verify( descriptorBuilder ).attribute( eq( NumericFormElementConfiguration.class ), captor.capture() );
			assertThat( captor.getValue().get( "decimalPlaces" ) ).isEqualTo( 0 );
			NumericFormElementConfiguration percentageConfig = new NumericFormElementConfiguration();
			percentageConfig.setFormat( NumericFormElementConfiguration.Format.PERCENT );
			percentageConfig.setMultiplier( 1 );
			assertThat( captor.getValue().getMultiplier() ).isEqualTo( percentageConfig.getMultiplier() );
			assertThat( captor.getValue().getFormat() ).isEqualTo( percentageConfig.getFormat() );
		}
		reset( descriptorBuilder );
		{
			verifyTypeDefinition( "percentage(2)", TypeDescriptor.valueOf( BigDecimal.class ) );
			ArgumentCaptor<NumericFormElementConfiguration> captor = ArgumentCaptor.forClass( NumericFormElementConfiguration.class );
			verify( descriptorBuilder ).attribute( eq( NumericFormElementConfiguration.class ), captor.capture() );
			assertThat( captor.getValue().get( "decimalPlaces" ) ).isEqualTo( 0 );
			NumericFormElementConfiguration percentageConfig = new NumericFormElementConfiguration();
			percentageConfig.setFormat( NumericFormElementConfiguration.Format.PERCENT );
			percentageConfig.setMultiplier( 2 );
			assertThat( captor.getValue().getMultiplier() ).isEqualTo( percentageConfig.getMultiplier() );
			assertThat( captor.getValue().getFormat() ).isEqualTo( percentageConfig.getFormat() );
		}
		reset( descriptorBuilder );
		{
			verifyTypeDefinition( "percentage(7,4)", TypeDescriptor.valueOf( BigDecimal.class ) );
			ArgumentCaptor<NumericFormElementConfiguration> captor = ArgumentCaptor.forClass( NumericFormElementConfiguration.class );
			verify( descriptorBuilder ).attribute( eq( NumericFormElementConfiguration.class ), captor.capture() );
			assertThat( captor.getValue().get( "decimalPlaces" ) ).isEqualTo( 4 );
			NumericFormElementConfiguration percentageConfig = new NumericFormElementConfiguration();
			percentageConfig.setFormat( NumericFormElementConfiguration.Format.PERCENT );
			percentageConfig.setMultiplier( 7 );
			assertThat( captor.getValue().getMultiplier() ).isEqualTo( percentageConfig.getMultiplier() );
			assertThat( captor.getValue().getFormat() ).isEqualTo( percentageConfig.getFormat() );
		}
	}

	@Test
	public void dateType() {
		verifyTypeDefinition( "date", TypeDescriptor.valueOf( LocalDate.class ) );
		verifyZeroInteractions( descriptorBuilder );

		verifyTypeDefinition( "date[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( LocalDate.class ) ) );
		verifyZeroInteractions( descriptorBuilder );
	}

	@Test
	public void dateTimeType() {
		verifyTypeDefinition( "datetime", TypeDescriptor.valueOf( LocalDateTime.class ) );
		verifyZeroInteractions( descriptorBuilder );

		verifyTypeDefinition( "datetime[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( LocalDateTime.class ) ) );
		verifyZeroInteractions( descriptorBuilder );
	}

	@Test
	public void timeType() {
		verifyTypeDefinition( "time", TypeDescriptor.valueOf( LocalTime.class ) );
		verifyZeroInteractions( descriptorBuilder );

		verifyTypeDefinition( "time[]", TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( LocalTime.class ) ) );
		verifyZeroInteractions( descriptorBuilder );
	}

	private void verifyTypeDefinition( String typeName, TypeDescriptor expectedType ) {
		val typeDef = builder.buildTypeDefinition( null, field( typeName ) );
		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( expectedType );
		assertThat( typeDef.getTypeName() ).isEqualTo( typeName );

		typeDef.customizePropertyDescriptor( fieldDefinition, descriptorBuilder );
	}

	private void assertAccepted( boolean accepted, String... types ) {
		Stream.of( types ).forEach( type -> assertThat( builder.accepts( field( type ) ) ).isEqualTo( accepted ) );
	}

	private RawDocumentDefinition.Field field( String type ) {
		return RawDocumentDefinition.Field.builder().type( type ).build();
	}
}
