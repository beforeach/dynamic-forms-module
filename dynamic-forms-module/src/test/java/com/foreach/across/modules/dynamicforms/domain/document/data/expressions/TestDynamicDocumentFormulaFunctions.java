/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data.expressions;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.2
 */
@SuppressWarnings("unchecked")
public class TestDynamicDocumentFormulaFunctions
{
	private DynamicDocumentFormulaFunctions utils = new DynamicDocumentFormulaFunctions();

	@Test
	public void sum() {
		assertThat( utils.sum( 1 ) ).isEqualTo( 1d );
		assertThat( utils.sum( 1, 2 ) ).isEqualTo( 3d );
		assertThat( utils.sum( 1, null, Collections.singleton( 2 ), Arrays.asList( 3, 4 ) ) ).isEqualTo( 10d );
		assertThat( utils.sum( null, null ) ).isEqualTo( 0d );
	}

	@Test
	public void average() {
		assertThat( utils.avg( 1 ) ).isEqualTo( 1d );
		assertThat( utils.avg( 1, 2 ) ).isEqualTo( 1.5d );
		assertThat( utils.avg( 1, null, Collections.singleton( 2 ), Arrays.asList( 3, 4 ) ) ).isEqualTo( 2.5d );
		assertThat( utils.avg( null, null ) ).isNull();
	}

	@Test
	public void min() {
		assertThat( utils.min( 1 ) ).isEqualTo( 1 );
		assertThat( utils.min( 1, 2 ) ).isEqualTo( 1 );
		assertThat( utils.min( 1, null, Collections.singleton( 2 ), Arrays.asList( 3, 4 ) ) ).isEqualTo( 1 );
		assertThat( utils.min( null, null ) ).isNull();
	}

	@Test
	public void max() {
		assertThat( utils.max( 1 ) ).isEqualTo( 1 );
		assertThat( utils.max( 1, 2 ) ).isEqualTo( 2 );
		assertThat( utils.max( 1, null, Collections.singleton( 2 ), Arrays.asList( 3, 4 ) ) ).isEqualTo( 4 );
		assertThat( utils.max( null, null ) ).isNull();
	}

	@Test
	public void flatten() {
		assertThat( utils.flatten( 1, 2, 3 ) ).containsExactly( 1, 2, 3 );
		assertThat( utils.flatten( null, null, Arrays.asList( null, null ) ) ).isEmpty();
		assertThat( utils.flatten( 1, Arrays.asList( 2, 3 ), new Object[] { "4", "5" }, Collections.singleton( 6 ) ) )
				.containsExactly( 1, 2, 3, "4", "5", 6 );
		assertThat( utils.flatten( Arrays.asList( Collections.singleton( 1 ), Arrays.asList( 2, new Object[] { 3, 4 } ) ) ) )
				.containsExactly( 1, 2, 3, 4 );
	}

	@Test
	public void sorted() {
		assertThat( utils.sorted( 1, 2, 3 ) ).containsExactly( 1, 2, 3 );
		assertThat( utils.sorted( 3, 2, 1 ) ).containsExactly( 1, 2, 3 );
		assertThat( utils.sorted( Arrays.asList( 2, 3, 1 ) ) ).containsExactly( 1, 2, 3 );
	}

	@Test
	@SuppressWarnings("all")
	public void reverse() {
		assertThat( utils.reverse( 1, 2, 3 ) ).containsExactly( 3, 2, 1 );
		assertThat( utils.reverse( new Object[] { 2, 1, 3 } ) ).containsExactly( 3, 1, 2 );
		assertThat( utils.reverse( Arrays.asList( 2, 3, 1 ) ) ).containsExactly( 1, 3, 2 );
	}

	@Test
	@SuppressWarnings("all")
	public void first() {
		assertThat( utils.first( 1, 2, 3 ) ).isEqualTo( 1 );
		assertThat( utils.first( new Object[] { "2", 1, 3 } ) ).isEqualTo( "2" );
		assertThat( utils.first( Arrays.asList( 2, 3, 1 ) ) ).isEqualTo( 2 );
		assertThat( utils.first( (Collection) null ) ).isNull();
		assertThat( utils.first( new Object[0] ) ).isNull();
		assertThat( utils.first( Collections.emptyList() ) ).isNull();
	}

	@Test
	@SuppressWarnings("all")
	public void last() {
		assertThat( utils.last( 1, 2, 3 ) ).isEqualTo( 3 );
		assertThat( utils.last( new Object[] { "2", 1, "3" } ) ).isEqualTo( "3" );
		assertThat( utils.last( Arrays.asList( 2, 3, 1 ) ) ).isEqualTo( 1 );
		assertThat( utils.last( (Collection) null ) ).isNull();
		assertThat( utils.last( new Object[0] ) ).isNull();
		assertThat( utils.last( Collections.emptyList() ) ).isNull();
	}
}
