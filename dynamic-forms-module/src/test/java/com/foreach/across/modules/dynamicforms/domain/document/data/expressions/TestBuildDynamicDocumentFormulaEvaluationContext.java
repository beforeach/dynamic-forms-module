/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data.expressions;

import org.junit.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestBuildDynamicDocumentFormulaEvaluationContext
{
	@Test
	public void defaultContextSupportsFormulaFunctions() {
		SpelExpressionParser parser = new SpelExpressionParser();
		Expression expression = parser.parseExpression( "min(10,15)" );

		EvaluationContext evaluationContext = BuildDynamicDocumentFormulaEvaluationContext.createDefaultEvaluationContext();
		assertThat( expression.getValue( evaluationContext, "someRootObject" ) ).isEqualTo( 10 );
	}
}
