/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestDynamicDataFieldSetToStringConverter
{
	private DynamicDataFieldSetToStringConverter converter = new DynamicDataFieldSetToStringConverter( new ObjectMapper() );

	@Test
	public void emptyFieldSet() {
		DynamicDataFieldSet fieldSet = new DynamicDataFieldSet();
		assertThat( converter.convert( fieldSet ) ).isEqualTo( "{}" );
	}

	@Test
	public void singleField() {
		DynamicDataFieldSet fieldSet = new DynamicDataFieldSet();
		fieldSet.put( "field", "value" );
		assertThat( converter.convert( fieldSet ) ).isEqualTo( "{\"field\":\"value\"}" );
	}

	@Test
	public void multipleSimpleFields() {
		DynamicDataFieldSet fieldSet = new DynamicDataFieldSet();
		fieldSet.put( "field", "value" );
		fieldSet.put( "count", 123L );
		assertThat( converter.convert( fieldSet ) ).isEqualTo( "{\"field\":\"value\",\"count\":123}" );
	}

	@Test
	public void nestedFieldSet() {
		DynamicDataFieldSet fieldSet = new DynamicDataFieldSet();
		fieldSet.put( "field", "value" );

		DynamicDataFieldSet nested = new DynamicDataFieldSet();
		nested.put( "field", 12 );
		fieldSet.put( "nested", nested );

		fieldSet.put( "count", 123L );

		assertThat( converter.convert( fieldSet ) ).isEqualTo( "{\"field\":\"value\",\"nested\":{\"field\":12},\"count\":123}" );
	}
}
