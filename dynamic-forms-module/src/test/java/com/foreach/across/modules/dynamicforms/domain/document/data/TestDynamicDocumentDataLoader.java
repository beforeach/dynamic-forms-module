/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionBuilder;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.*;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataLoader.Report;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidatorFactory;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.convert.ConversionException;
import org.springframework.core.convert.support.DefaultConversionService;

import java.util.*;
import java.util.function.Consumer;

import static com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.Field;
import static com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.builder;
import static com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataLoader.FieldErrorReason.ILLEGAL_VALUE;
import static com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataLoader.FieldErrorReason.UNKNOWN_FIELD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestDynamicDocumentDataLoader
{
	private DynamicDocumentData document;
	private DynamicDocumentData complexDocument;
	private DynamicDocumentData collectionDocument;
	private DynamicDocumentDataLoader loader;
	private DynamicDocumentDataLoader complexLoader;
	private DynamicDocumentDataLoader collectionLoader;

	@Before
	public void createDocument() {
		DynamicTypeDefinitionFactory typeDefinitionFactory = new DynamicTypeDefinitionFactory();
		typeDefinitionFactory.setTypeDefinitionBuilders(
				Arrays.asList( new FieldsetDynamicTypeDefinitionBuilder( new DynamicDataFieldSetTemplateRegistry() ), new SimpleDynamicTypeDefinitionBuilder() )
		);

		DynamicDocumentFieldValidatorFactory fieldValidatorFactory = new DynamicDocumentFieldValidatorFactory();

		RawDocumentDefinition rawDocumentDefinition = buildRawDocumentDefinition();

		document = new DynamicDocumentDefinitionBuilder( typeDefinitionFactory, fieldValidatorFactory, null )
				.build( rawDocumentDefinition, null )
				.createDocumentData();

		loader = new DynamicDocumentDataLoader( document );

		RawDocumentDefinition rawComplexDocumentDefinition = buildRawComplexDocumentDefinition();
		complexDocument = new DynamicDocumentDefinitionBuilder( typeDefinitionFactory, fieldValidatorFactory, null )
				.build( rawComplexDocumentDefinition, null )
				.createDocumentData();

		complexLoader = new DynamicDocumentDataLoader( complexDocument );

		RawDocumentDefinition rawCollectionDocumentDefinition = buildRawCollectionDocumentDefinition();
		collectionDocument = new DynamicDocumentDefinitionBuilder( typeDefinitionFactory, fieldValidatorFactory, null )
				.build( rawCollectionDocumentDefinition, null )
				.createDocumentData();

		collectionLoader = new DynamicDocumentDataLoader( collectionDocument );
	}

	private RawDocumentDefinition buildRawDocumentDefinition() {
		return
				builder()
						.name( "sample-document" )
						.field( Field.builder().id( "name" ).type( "string" ).build() )
						.field(
								Field.builder()
								     .type( "fieldset" )
								     .id( "settings" )
								     .field( Field.builder().id( "active" ).type( "boolean" ).build() )
								     .field( Field.builder().id( "year" ).type( "number[]" ).build() )
								     .build()
						)
						.build();
	}

	private RawDocumentDefinition buildRawComplexDocumentDefinition() {
		return
				builder()
						.name( "sample-document" )
						.field( Field.builder().id( "name" ).type( "string" ).build() )
						.field(
								Field.builder()
								     .type( "fieldset" )
								     .id( "address" )
								     .field( Field.builder().id( "zipCode" ).type( "number" ).build() )
								     .field( Field.builder()
								                  .id( "street" )
								                  .type( "fieldset" )
								                  .field( Field.builder().id( "name" ).type( "string" ).build() )
								                  .field( Field.builder().id( "number" ).type( "number" ).build() )
								                  .build() )
								     .build()
						)
						.build();
	}

	private RawDocumentDefinition buildRawCollectionDocumentDefinition() {
		return
				builder()
						.name( "sample-document" )
						.field(
								Field.builder().id( "list" ).type( "fieldset[]" )
								     .member(
										     Field.builder()
										          .type( "fieldset" )
										          .id( "item" )
										          .field( Field.builder().id( "number" ).type( "number" ).build() )
										          .field( Field.builder().id( "name" ).type( "string" ).build() )
										          .build()
								     )
								     .build()
						)
						.build();
	}

	@Test
	public void reportIsAlwaysReturned() {
		assertThat( load( Collections.emptyMap() ) ).isNotNull();
	}

	@Test
	public void simpleLoadingWithoutConversion() {
		Map<String, Object> data = new HashMap<>();
		data.put( "name", "John Doe" );

		Map<String, Object> settings = new HashMap<>();
		data.put( "settings", settings );
		settings.put( "active", true );
		settings.put( "year", new ArrayList<>( Arrays.asList( 123L, 456L ) ) );

		Report report = load( data );
		assertThat( report.hasErrors() ).isFalse();
		assertThat( report.hasUpdatedFieldValues() ).isTrue();
		assertThat( report.getModifiedFields() ).hasSize( 3 );
		assertThat( report.getFieldStatus( "name" ) )
				.isNotNull()
				.satisfies( status -> {
					assertThat( status.getPath() ).isEqualTo( "name" );
					assertThat( status.getOldValue() ).isNull();
					assertThat( status.getNewValue() ).isEqualTo( "John Doe" );
					assertThat( status.isCreated() ).isTrue();
					assertThat( status.isValueUpdated() ).isTrue();
				} );

		assertThat( document.<String>getValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( document.<Boolean>getValue( "settings.active" ) ).isTrue();
		assertThat( document.<Collection<Long>>getValue( "settings.year" ) )
				.isEqualTo( Arrays.asList( 123L, 456L ) );
	}

	@Test
	public void simpleLoadingWithConversionAndChangeTracking() {
		loader.setConversionService( new DefaultConversionService() );

		document.getContent().put( "name", "John Doe" );
		DynamicDataFieldSet fieldSet = new DynamicDataFieldSet();
		fieldSet.putAll( Collections.singletonMap( "active", false ) );
		document.getContent().put( "settings", fieldSet );

		Map<String, Object> data = new HashMap<>();
		data.put( "name", "John Doe" );

		Map<String, Object> settings = new HashMap<>();
		data.put( "settings", settings );
		settings.put( "active", "true" );
		settings.put( "year", new Long[] { 123L, 456L } );

		Report report = load( data );
		assertThat( report.hasErrors() ).isFalse();
		assertThat( report.hasUpdatedFieldValues() ).isTrue();
		assertThat( report.getModifiedFields() ).hasSize( 3 );
		assertThat( report.getFieldStatus( "name" ) )
				.satisfies( status -> {
					assertThat( status.getPath() ).isEqualTo( "name" );
					assertThat( status.getOldValue() ).isEqualTo( "John Doe" );
					assertThat( status.getNewValue() ).isEqualTo( "John Doe" );
					assertThat( status.isCreated() ).isFalse();
					assertThat( status.isValueUpdated() ).isFalse();
				} );
		assertThat( report.getFieldStatus( "settings.active" ) )
				.satisfies( status -> {
					assertThat( status.getPath() ).isEqualTo( "settings.active" );
					assertThat( status.getOldValue() ).isEqualTo( false );
					assertThat( status.getNewValue() ).isEqualTo( true );
					assertThat( status.isCreated() ).isFalse();
					assertThat( status.isValueUpdated() ).isTrue();
				} );
		assertThat( report.getFieldStatus( "settings.year" ) )
				.satisfies( status -> {
					assertThat( status.getPath() ).isEqualTo( "settings.year" );
					assertThat( status.getOldValue() ).isNull();
					assertThat( status.getNewValue() ).isEqualTo( Arrays.asList( 123L, 456L ) );
					assertThat( status.isCreated() ).isTrue();
					assertThat( status.isValueUpdated() ).isTrue();
				} );

		assertThat( document.<String>getValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( document.<Boolean>getValue( "settings.active" ) ).isTrue();
		assertThat( document.<Collection<Long>>getValue( "settings.year" ) )
				.isEqualTo( Arrays.asList( 123L, 456L ) );
	}

	@Test
	public void loadingWithComplexContent() {
		complexLoader.setConversionService( new DefaultConversionService() );

		DynamicDataFieldSet street = new DynamicDataFieldSet();
		street.put( "name", "my street" );

		DynamicDataFieldSet address = new DynamicDataFieldSet();
		address.put( "zipCode", 2123L );
		address.put( "street", street );

		complexDocument.getContent().put( "name", "John Doe" );
		complexDocument.getContent().put( "address", address );

		Map<String, Object> data = new HashMap<>();
		data.put( "name", "John Doe" );

		Map<String, Object> streetData = new HashMap<>();
		streetData.put( "name", "some street" );
		streetData.put( "number", 72L );

		Map<String, Object> addressData = new HashMap<>();
		addressData.put( "zipCode", 5234L );
		addressData.put( "street", streetData );
		data.put( "address", addressData );

		complexLoader.load( data );
		Report report = complexLoader.getReport();

		assertThat( report.hasErrors() ).isFalse();
		assertThat( report.hasUpdatedFieldValues() ).isTrue();
		assertThat( report.getModifiedFields() ).hasSize( 4 );
		assertThat( report.getFieldStatus( "name" ) )
				.isNotNull()
				.satisfies( status -> {
					assertThat( status.getPath() ).isEqualTo( "name" );
					assertThat( status.getOldValue() ).isEqualTo( "John Doe" );
					assertThat( status.getNewValue() ).isEqualTo( "John Doe" );
					assertThat( status.isCreated() ).isFalse();
					assertThat( status.isValueUpdated() ).isFalse();
				} );
		assertThat( report.getFieldStatus( "address.zipCode" ) )
				.isNotNull()
				.satisfies( status -> {
					assertThat( status.getPath() ).isEqualTo( "address.zipCode" );
					assertThat( status.getOldValue() ).isEqualTo( 2123L );
					assertThat( status.getNewValue() ).isEqualTo( 5234L );
					assertThat( status.isCreated() ).isFalse();
					assertThat( status.isValueUpdated() ).isTrue();
				} );
		assertThat( report.getFieldStatus( "address.street.name" ) )
				.isNotNull()
				.satisfies( status -> {
					assertThat( status.getPath() ).isEqualTo( "address.street.name" );
					assertThat( status.getOldValue() ).isEqualTo( "my street" );
					assertThat( status.getNewValue() ).isEqualTo( "some street" );
					assertThat( status.isCreated() ).isFalse();
					assertThat( status.isValueUpdated() ).isTrue();
				} );
		assertThat( report.getFieldStatus( "address.street.number" ) )
				.satisfies( status -> {
					assertThat( status.getPath() ).isEqualTo( "address.street.number" );
					assertThat( status.getOldValue() ).isNull();
					assertThat( status.getNewValue() ).isEqualTo( 72L );
					assertThat( status.isCreated() ).isTrue();
					assertThat( status.isValueUpdated() ).isTrue();
				} );

		assertThat( complexDocument.<String>getValue( "name" ) ).isEqualTo( "John Doe" );
		assertThat( complexDocument.<Long>getValue( "address.zipCode" ) ).isEqualTo( 5234L );
		assertThat( complexDocument.<String>getValue( "address.street.name" ) ).isEqualTo( "some street" );
		assertThat( complexDocument.<Long>getValue( "address.street.number" ) ).isEqualTo( 72L );
	}

	@Test
	public void loadingWithCollectionContent() {
		collectionLoader.setIgnoreIllegalFieldValues( true );
		collectionLoader.setConversionService( new DefaultConversionService() );

		Map<String, Object> data = new HashMap<>();
		data.put( "list", Collections.singletonList( ImmutableMap.of( "name", "item-1", "number", 1L ) ) );
		collectionLoader.load( data );

		Report report = collectionLoader.getReport();
		assertThat( report.hasErrors() ).isFalse();
		assertThat( report.getModifiedFields() ).hasSize( 2 );
		assertThat( report.getFieldStatus( "list[0].name" ) )
				.satisfies( fieldStatus( "list[0].name", null, "item-1", true ) );

		assertThat( collectionDocument.<List<Long>>getValue( "list[].number" ) ).containsExactly( 1L );

		collectionLoader.setFieldValue( "list[].number", Collections.singleton( 2L ) );
		assertThat( report.hasErrors() ).isFalse();
		assertThat( collectionDocument.<List<Long>>getValue( "list[].number" ) ).containsExactly( 2L );

		collectionLoader.setFieldValue( "list[].number", Collections.singleton( 3 ) );
		assertThat( report.hasErrors() ).isFalse();

		data.put( "list", Collections.singletonList( ImmutableMap.of( "name", "item-1", "number", 10 ) ) );
		collectionLoader.load( data );
		assertThat( report.hasErrors() ).isFalse();

		data.put( "list", Collections.singletonList( ImmutableMap.of( "name", "item-1", "number", "X" ) ) );
		collectionLoader.load( data );
		assertThat( report.hasErrors() ).isTrue();
		assertThat( report.hasError( "list[0].number" ) ).isTrue();
		assertThat( report.getError( "list[0].number" ) )
				.satisfies( fieldError( "list[0].number", ILLEGAL_VALUE, "X", ConversionException.class ) );

		collectionLoader.reset();

		collectionLoader.setFieldValue( "list[].number", Collections.singleton( "X" ) );
		assertThat( report.hasErrors() ).isTrue();
		assertThat( report.getError( "list[0].number" ) )
				.satisfies( fieldError( "list[0].number", ILLEGAL_VALUE, "X", ConversionException.class ) );
	}

	@Test
	public void exceptionIsThrownForUnknownField() {
		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> load( Collections.singletonMap( "unknown-field", null ) ) );
	}

	@Test
	public void exceptionIsThrownForIllegalFieldValue() {
		assertThatExceptionOfType( ClassCastException.class )
				.isThrownBy( () -> load( Collections.singletonMap( "settings.year", "bad-value" ) ) );
	}

	@Test
	public void unknownFieldIsReportedIfSettingIsTrue() {
		loader.setIgnoreUnknownFields( true );

		Report report = load( Collections.singletonMap( "unknown-field", "my-val" ) );
		assertThat( report.hasErrors() ).isTrue();
		assertThat( report.getErrors() ).hasSize( 1 );
		assertThat( report.getError( "unknown-field" ) )
				.satisfies( fieldError( "unknown-field", UNKNOWN_FIELD, "my-val", DynamicDocumentDefinition.NoSuchFieldException.class ) );

		assertThatExceptionOfType( ClassCastException.class )
				.isThrownBy( () -> loader.load( Collections.singletonMap( "settings.year", "bad-value" ) ) );
	}

	@Test
	public void illegalFieldFalueIsReportedIfSettingIsTrue() {
		loader.setIgnoreIllegalFieldValues( true );

		Map<String, Object> settings = new HashMap<>();
		settings.put( "year", 123 );

		Report report = load( Collections.singletonMap( "settings", settings ) );
		assertThat( report.hasErrors() ).isTrue();
		assertThat( report.hasUpdatedFieldValues() ).isFalse();
		assertThat( report.getErrors() ).hasSize( 1 );
		assertThat( report.getError( "settings.year" ) )
				.satisfies( fieldError( "settings.year", ILLEGAL_VALUE, 123, ClassCastException.class ) );

		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> loader.load( Collections.singletonMap( "unknown-field", null ) ) );
	}

	private Consumer<DynamicDocumentDataLoader.FieldStatus> fieldStatus( String path, Object oldValue, Object newValue, boolean created ) {
		return status -> {
			assertThat( status ).isNotNull();
			assertThat( status.getPath() ).isEqualTo( path );
			assertThat( status.getOldValue() ).isEqualTo( oldValue );
			assertThat( status.getNewValue() ).isEqualTo( newValue );
			assertThat( status.isCreated() ).isEqualTo( created );
		};
	}

	private Consumer<DynamicDocumentDataLoader.FieldError> fieldError( String path,
	                                                                   DynamicDocumentDataLoader.FieldErrorReason reason,
	                                                                   Object newValue,
	                                                                   Class<? extends Throwable> exceptionType ) {
		return status -> {
			assertThat( status ).isNotNull();
			assertThat( status.getPath() ).isEqualTo( path );
			assertThat( status.getCause() ).isNotNull().isInstanceOf( exceptionType );
			assertThat( status.getReason() ).isEqualTo( reason );
			assertThat( status.getValue() ).isEqualTo( newValue );
		};
	}

	private Report load( Map<String, Object> data ) {
		loader.load( data );
		return loader.getReport();
	}
}
