/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.Field;
import com.google.common.collect.ImmutableMap;
import lombok.val;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.BaseField;
import static com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.DefaultValue;
import static infrastructure.DynamicFormsAssertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestRawDocumentDefinition
{
	@Test
	public void fieldAsInstanceOfSemantics() {
		val templateMessages = new HashMap<String, Object>();
		templateMessages.put( "my", "message" );
		templateMessages.put( "text", "initial" );
		templateMessages.put( "nl-BE", ImmutableMap.builder().put( "text", "initial-nl" ).put( "nl-template", "y" ).build() );

		val instanceMessages = new HashMap<String, Object>();
		instanceMessages.put( "local", "text" );
		instanceMessages.put( "text", "updated" );
		instanceMessages.put( "nl-BE", ImmutableMap.builder().put( "text", "instance-nl" ).put( "other", "z" ).build() );

		val templateAttributes = new LinkedHashMap<String, Object>();
		templateAttributes.put( "eql", "test" );
		templateAttributes.put( "value", 123 );

		val instanceAttributes = new LinkedHashMap<String, Object>();
		instanceAttributes.put( "extra", 456 );
		instanceAttributes.put( "value", 10 );

		val template = Field.builder()
		                    .id( "base-field" )
		                    .type( "some-type" )
		                    .label( "Base field label" )
		                    .messages( templateMessages )
		                    .attributes( templateAttributes )
		                    .validators( Collections.singletonList( "required" ) )
		                    .possibleValues( Arrays.asList( 1, 2 ) )
		                    .field( Field.builder().id( "name" ).type( "string" ).build() )
		                    .formula( "x" )
		                    .build();

		val instance = Field.builder()
		                    .id( "address" )
		                    .type( "base-field" )
		                    .label( "Address" )
		                    .messages( instanceMessages )
		                    .attributes( instanceAttributes )
		                    .validators( Arrays.asList( "required", 123 ) )
		                    .possibleValues( Arrays.asList( 6, 7 ) )
		                    .field( Field.builder().id( "user-count" ).type( "number" ).build() )
		                    .formula( "y" )
		                    .build();

		val merged = instance.asInstanceOf( template );

		assertThat( merged )
				.hasId( "address" )
				.hasType( "some-type" )
				.hasLabel( "Address" )
				.hasFormula( "y" )
				// messages are merged
				.hasMessage( "my", "message" )
				.hasMessage( "text", "updated" )
				.hasMessage( "local", "text" )
				.hasMessage( "text", "instance-nl", "nl-BE" )
				.hasMessage( "nl-template", "y", "nl-BE" )
				.hasMessage( "other", "z", "nl-BE" )
				// attributes are merged
				.hasAttribute( "eql", "test" )
				.hasAttribute( "extra", 456 )
				.hasAttribute( "value", 10 )
				// validators are merged in order
				.hasValidators( Arrays.asList( "required", 123 ) )
				// possible values are replaced
				.hasPossibleValues( Arrays.asList( 6, 7 ) )
				// fields are added in order
				.withNextField( field -> field.hasId( "name" ).hasType( "string" ) )
				.withNextField( field -> field.hasId( "user-count" ).hasType( "number" ) );

		// Possible values from template are kept if field def is not more restrictive
		instance.setPossibleValues( null );
		val second = instance.asInstanceOf( template );
		assertThat( second ).hasPossibleValues( Arrays.asList( 1, 2 ) );
	}

	@Test
	public void memberIsAttachedIfNoneSetButOneOnTemplate() {
		val original = Field.builder()
		                    .id( "address" )
		                    .type( "base-field" )
		                    .label( "Address" )
		                    .build();

		val templateMember = Field.builder()
		                          .formula( "member-x" )
		                          .build();

		val template = Field.builder()
		                    .id( "base-field" )
		                    .type( "some-type" )
		                    .label( "Base field label" )
		                    .member( templateMember )
		                    .formula( "x" )
		                    .build();

		val merged = original.asInstanceOf( template );

		assertThat( merged )
				.hasId( "address" )
				.hasType( "some-type" )
				.hasLabel( "Address" )
				.hasFormula( "x" )
				.withMember( m -> m.hasFormula( "member-x" ).hasFields( 0 ) );
	}

	@Test
	public void memberFieldIsNestedInstanceOf() {
		val originalMember = Field.builder()
		                          .formula( "member-x" )
		                          .build();

		val original = Field.builder()
		                    .id( "address" )
		                    .type( "base-field" )
		                    .label( "Address" )
		                    .member( originalMember )
		                    .build();

		val templateMember = Field.builder()
		                          .field( Field.builder().id( "name" ).defaultValue( DefaultValue.builder().formula( "aa" ).build() ).type( "string" ).build() )
		                          .defaultValue( DefaultValue.builder().value( "ff" ).build() )
		                          .build();

		val template = Field.builder()
		                    .id( "base-field" )
		                    .type( "some-type" )
		                    .label( "Base field label" )
		                    .member( templateMember )
		                    .formula( "x" )
		                    .build();

		val merged = original.asInstanceOf( template );

		assertThat( merged )
				.hasId( "address" )
				.hasType( "some-type" )
				.hasLabel( "Address" )
				.hasFormula( "x" )
				.withMember(
						m -> m.hasFormula( "member-x" )
						      .hasDefaultValue( "ff" )
						      .hasFields( 1 )
						      .withNextField( field -> {
							      field.hasId( "name" )
							           .hasDefaultFormula( "aa" )
							           .hasType( "string" );
						      } )
				);
	}

	@Test
	public void noDefaultValueSet() {
		val instance = Field.builder().build();
		assertThat( instance.getDefaultValue() ).isEqualTo( DefaultValue.NOT_SPECIFIED );

		val template = BaseField.builder().build();
		assertThat( template.getDefaultValue() ).isEqualTo( DefaultValue.NOT_SPECIFIED );

		val member = instance.asInstanceOf( template );
		assertThat( member.getDefaultValue() ).isEqualTo( DefaultValue.NOT_SPECIFIED );
	}

	@Test
	public void defaultValueNotSpecifiedOnContentButNoValueOnType() {
		val instance = Field.builder().build();
		val template = BaseField.builder()
		                        .defaultValue( null ).build();

		val member = instance.asInstanceOf( template );
		assertThat( member.getDefaultValue() ).isNull();
	}

	@Test
	public void defaultValueNotSpecifiedOnTypeButNoValueOnContent() {
		val instance = Field.builder().defaultValue( null ).build();
		val template = BaseField.builder()
		                        .build();

		val member = instance.asInstanceOf( template );
		assertThat( member.getDefaultValue() ).isNull();
	}

	@Test
	public void defaultValueOnContentButNotOnType() {
		val instance = Field.builder()
		                    .defaultValue( DefaultValue.builder().value( "ff" ).build() )
		                    .build();
		val template = BaseField.builder().build();

		val member = instance.asInstanceOf( template );
		assertThat( member ).hasDefaultValue( "ff" );
	}

	@Test
	public void defaultFormulaOnContentButNotOnType() {
		val instance = Field.builder().build();
		val template = BaseField.builder()
		                        .defaultValue( DefaultValue.builder().formula( "ff" ).build() )
		                        .build();

		val member = instance.asInstanceOf( template );
		assertThat( member ).hasDefaultFormula( "ff" );
	}

	@Test
	public void defaultValueOnTypeButNotOnContent() {
		val instance = Field.builder().build();
		val template = BaseField.builder()
		                        .defaultValue( DefaultValue.builder().value( "ff" ).build() )
		                        .build();

		val member = instance.asInstanceOf( template );
		assertThat( member ).hasDefaultValue( "ff" );
	}

	@Test
	public void defaultFormulaOnTypeButNotOnContent() {
		val instance = Field.builder().build();
		val template = BaseField.builder()
		                        .defaultValue( DefaultValue.builder().formula( "ff" ).build() )
		                        .build();

		val member = instance.asInstanceOf( template );
		assertThat( member ).hasDefaultFormula( "ff" );
	}

	@Test
	public void defaultValueOnContentAndType() {
		val instance = Field.builder()
		                    .defaultValue( DefaultValue.builder().value( "aa" ).build() )
		                    .build();
		val template = BaseField.builder()
		                        .defaultValue( DefaultValue.builder().value( "ff" ).build() )
		                        .build();

		val member = instance.asInstanceOf( template );
		assertThat( member ).hasDefaultValue( "aa" );
	}

	@Test
	public void defaultFormulaOnContentAndType() {
		val instance = Field.builder()
		                    .defaultValue( DefaultValue.builder().formula( "aa" ).build() )
		                    .build();
		val template = BaseField.builder()
		                        .defaultValue( DefaultValue.builder().formula( "ff" ).build() )
		                        .build();

		val member = instance.asInstanceOf( template );
		assertThat( member ).hasDefaultFormula( "aa" );
	}

	@Test
	public void defaultValueOnContentAndDefaultFormulaOnType() {
		val instance = Field.builder()
		                    .defaultValue( DefaultValue.builder().value( "aa" ).build() )
		                    .build();
		val template = BaseField.builder()
		                        .defaultValue( DefaultValue.builder().formula( "ff" ).build() )
		                        .build();

		val member = instance.asInstanceOf( template );
		assertThat( member ).hasDefaultValue( "aa" );
		assertThat( member ).hasDefaultFormula( null );
	}

	@Test
	public void defaultFormulaOnContentAndDefaultValueOnType() {
		val instance = Field.builder()
		                    .defaultValue( DefaultValue.builder().formula( "aa" ).build() )
		                    .build();
		val template = BaseField.builder()
		                        .defaultValue( DefaultValue.builder().value( "ff" ).build() )
		                        .build();

		val member = instance.asInstanceOf( template );
		assertThat( member ).hasDefaultFormula( "aa" );
		assertThat( member ).hasDefaultValue( null );
	}
}
