/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSet;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.GenericDynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;

import java.util.*;

import static com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory.documentDefinition;
import static com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory.fieldDefinition;
import static infrastructure.DynamicFormsAssertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDocumentDataAccessor
{
	private Map<String, Object> data = new LinkedHashMap<>();
	private DynamicDocumentDataAccessor accessor;

	@Mock
	private DynamicTypeDefinition typeDefinitionMock;

	@Mock
	private DynamicDocumentService documentService;

	private DynamicDocumentDefinitionTestFactory.FieldDefinition nameDef, addressStreetDef, addressDef, itemDef, itemListDef, userAddressStreetDef,
			userAddressDef, userAddressListDef, userEmailDef, userEmailListDef, userDef, userListDef, userNameDef;

	@Before
	public void before() {
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( String.class ) );

		GenericDynamicTypeDefinition fieldsetTypeDef = mock( GenericDynamicTypeDefinition.class );
		when( fieldsetTypeDef.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( DynamicDataFieldSet.class ) );

		GenericDynamicTypeDefinition stringListTypeDef = mock( GenericDynamicTypeDefinition.class );
		when( stringListTypeDef.getTypeDescriptor() ).thenReturn( TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( String.class ) ) );

		GenericDynamicTypeDefinition fieldsetListTypeDef = mock( GenericDynamicTypeDefinition.class );
		when( fieldsetListTypeDef.getTypeDescriptor() ).thenReturn(
				TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( DynamicDataFieldSet.class ) )
		);

		// simple type
		nameDef = fieldDefinition( "name", typeDefinitionMock )
				.setCanonicalId( "name" )
				.setInitializerExpression( ( c, f ) -> "<default>" );

		// fieldset
		addressStreetDef = fieldDefinition( "street", typeDefinitionMock )
				.setCanonicalId( "address.street" );

		addressDef = fieldDefinition( "address", fieldsetTypeDef )
				.setCanonicalId( "address" )
				.setFields( Collections.singletonList( addressStreetDef ) );

		// calculated field
		DynamicDocumentFieldDefinition calculated = fieldDefinition( "calculated", typeDefinitionMock )
				.setCanonicalId( "calculated" )
				.setExpression( ( context, object ) -> null );

		// collection with a simple type
		itemDef = fieldDefinition( "items[]", typeDefinitionMock )
				.setCanonicalId( "items[]" )
				.setMember( true );
		itemListDef = fieldDefinition( "items", stringListTypeDef )
				.setCanonicalId( "items" )
				.setCollection( true )
				.setMemberFieldDefinition( itemDef );
		itemDef.setCollectionFieldDefinition( itemListDef );

		// collection with fieldset type
		userAddressStreetDef = fieldDefinition( "street", typeDefinitionMock )
				.setCanonicalId( "users[].address[].street" );

		userAddressDef = fieldDefinition( "address", fieldsetTypeDef )
				.setCanonicalId( "users[].address[]" )
				.setFields( Collections.singletonList( userAddressStreetDef ) );
		userAddressListDef = fieldDefinition( "address", fieldsetListTypeDef )
				.setCollection( true )
				.setCanonicalId( "users[].address" )
				.setMemberFieldDefinition( userAddressDef );
		userAddressDef.setMember( true ).setCollectionFieldDefinition( userAddressListDef );

		userEmailDef = fieldDefinition( "emails[]", typeDefinitionMock )
				.setCanonicalId( "users[].emails[]" )
				.setMember( true );
		userEmailListDef = fieldDefinition( "emails", stringListTypeDef )
				.setCanonicalId( "users[].emails" )
				.setCollection( true )
				.setMemberFieldDefinition( userEmailDef );
		userEmailDef.setCollectionFieldDefinition( userEmailListDef );
		DynamicDocumentFieldDefinition innerCalculated = fieldDefinition( "calculated", typeDefinitionMock )
				.setCanonicalId( "users[].calculated" )
				.setExpression( ( context, object ) -> null );
		userNameDef = fieldDefinition( "name", typeDefinitionMock ).setCanonicalId( "users[].name" );
		userDef = fieldDefinition( "users[]", fieldsetTypeDef )
				.setCanonicalId( "users[]" )
				.setMember( true )
				.setFields( Arrays.asList( userNameDef, userEmailListDef, userAddressListDef,
				                           innerCalculated ) );
		userListDef = fieldDefinition( "users", fieldsetListTypeDef )
				.setCanonicalId( "users" )
				.setCollection( true )
				.setMemberFieldDefinition( userDef );
		userDef.setCollectionFieldDefinition( userListDef );

		// nested document
		GenericDynamicTypeDefinition docType = mock( GenericDynamicTypeDefinition.class );
		when( docType.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( DynamicDocumentVersion.class ) );

		DynamicDocumentFieldDefinition nestedDoc = fieldDefinition( "document", docType ).setCanonicalId( "document" );
		DynamicDocumentDefinition documentDefinition = documentDefinition( "my-doc",
		                                                                   Arrays.asList( nameDef, addressDef, itemListDef, userListDef, calculated,
		                                                                                  nestedDoc ) );

		accessor = new DynamicDocumentDataAccessor( documentDefinition, data );
		accessor.setDocumentService( documentService );
	}

	@Test
	public void fieldAccessorTypes() {
		TypeDescriptor listOfString = TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( String.class ) );
		TypeDescriptor listOfFieldSet = TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( DynamicDataFieldSet.class ) );

		data.put( "items", Collections.singletonList( "value" ) );
		data.put( "users", Collections.singletonList( ImmutableMap.of( "name", "John Doe", "emails", Collections.singletonList( "john@doe.com" ) ) ) );

		// simple type
		assertThatFieldAccessor( "name" )
				.isSingleFieldValue()
				.isForField( "name" )
				.hasPath( "name" )
				.hasType( TypeDescriptor.valueOf( String.class ) )
				.isSameAs( accessor.getFieldAccessor( "name" ) );

		// fieldset type
		assertThatFieldAccessor( "address" )
				.isFieldSetValue()
				.isForField( "address" )
				.hasPath( "address" )
				.hasType( TypeDescriptor.valueOf( DynamicDataFieldSet.class ) )
				.isSameAs( accessor.getFieldAccessor( "address" ) );
		assertThatFieldAccessor( "address.street" )
				.isSingleFieldValue()
				.isForField( "address.street" )
				.hasPath( "address.street" )
				.hasType( TypeDescriptor.valueOf( String.class ) )
				.isSameAs( accessor.getFieldAccessor( "address" ).getFieldAccessor( "street" ) );

		// simple collection type
		assertThatFieldAccessor( "items" )
				.isListValue()
				.isForField( "items" )
				.hasPath( "items" )
				.hasType( listOfString )
				.isSameAs( accessor.getFieldAccessor( "items" ) );
		assertThatFieldAccessor( "items[]" )
				.isListItemFieldValue()
				.isForField( "items[]" )
				.hasPath( "items[]" )
				.hasType( listOfString )
				.isSameAs( accessor.getFieldAccessor( "items" ).getFieldAccessor( "[]" ) );

		// calculated type
		assertThatFieldAccessor( "calculated" )
				.isCalculatedFieldValue()
				.isForField( "calculated" )
				.hasPath( "calculated" )
				.hasType( TypeDescriptor.valueOf( String.class ) )
				.isSameAs( accessor.getFieldAccessor( "calculated" ) );

		// collection with fieldset type
		assertThatFieldAccessor( "users" )
				.isListValue()
				.isForField( "users" )
				.hasPath( "users" )
				.hasType( listOfFieldSet )
				.isSameAs( accessor.getFieldAccessor( "users" ) );
		assertThatFieldAccessor( "users[]" )
				.isListItemFieldValue()
				.isForField( "users[]" )
				.hasPath( "users[]" )
				.hasType( listOfFieldSet )
				.isSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[]" ) );
		assertThatFieldAccessor( "users[].name" )
				.isListItemFieldValue()
				.isForField( "users[].name" )
				.hasPath( "users[].name" )
				.hasType( listOfString )
				.isSameAs( accessor.getFieldAccessor( "users[]" ).getFieldAccessor( "name" ) )
				.isSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[].name" ) )
				.isSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[]" ).getFieldAccessor( "name" ) );
		assertThatFieldAccessor( "users[].emails" )
				.isListItemFieldValue()
				.isForField( "users[].emails" )
				.hasPath( "users[].emails" )
				.hasType( TypeDescriptor.collection( ArrayList.class, listOfString ) )
				.isSameAs( accessor.getFieldAccessor( "users[]" ).getFieldAccessor( "emails" ) )
				.isSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[].emails" ) )
				.isSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[]" ).getFieldAccessor( "emails" ) );
		assertThatFieldAccessor( "users[].emails[]" )
				.isListItemFieldValue()
				.isForField( "users[].emails[]" )
				.hasPath( "users[].emails[]" )
				.hasType( TypeDescriptor.collection( ArrayList.class, listOfString ) )
				.isSameAs( accessor.getFieldAccessor( "users[]" ).getFieldAccessor( "emails[]" ) )
				.isSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[].emails" ).getFieldAccessor( "[]" ) )
				.isSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[]" ).getFieldAccessor( "emails[]" ) );
		assertThatFieldAccessor( "users[].calculated" )
				.isListItemFieldValue()
				.isForField( "users[].calculated" )
				.hasPath( "users[].calculated" )
				.hasType( listOfString );

		// nested document
		assertThatFieldAccessor( "document" )
				.isNestedDocumentValue()
				.isForField( "document" )
				.hasPath( "document" )
				.hasType( TypeDescriptor.valueOf( DynamicDocumentVersion.class ) )
				.isSameAs( accessor.getFieldAccessor( "document" ) );

		// single collection items
		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "items[1]" ) )
				.satisfies( e -> assertThat( e.getFieldName() ).isEqualTo( "items[1]" ) );

		assertThatFieldAccessor( "items[0]" )
				.isSingleFieldValue()
				.isForField( "items[]" )
				.hasPath( "items[0]" )
				.hasType( TypeDescriptor.valueOf( String.class ) )
				.isNotSameAs( accessor.getFieldAccessor( "items[0]" ) )
				.isNotSameAs( accessor.getFieldAccessor( "items" ).getFieldAccessor( "[0]" ) );

		assertThatFieldAccessor( "users[0]" )
				.isFieldSetValue()
				.isForField( "users[]" )
				.hasPath( "users[0]" )
				.hasType( TypeDescriptor.valueOf( DynamicDataFieldSet.class ) )
				.isNotSameAs( accessor.getFieldAccessor( "users[0]" ) )
				.isNotSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[0]" ) );
		assertThatFieldAccessor( "users[0].name" )
				.isSingleFieldValue()
				.isForField( "users[].name" )
				.hasPath( "users[0].name" )
				.hasType( TypeDescriptor.valueOf( String.class ) )
				.isNotSameAs( accessor.getFieldAccessor( "users[0].name" ) )
				.isNotSameAs( accessor.getFieldAccessor( "users" ).getFieldAccessor( "[0].name" ) );
		assertThatFieldAccessor( "users[0].calculated" )
				.isCalculatedFieldValue()
				.isForField( "users[].calculated" )
				.hasPath( "users[0].calculated" )
				.hasType( TypeDescriptor.valueOf( String.class ) );
		assertThatFieldAccessor( "users[0].emails" )
				.isListValue()
				.isForField( "users[].emails" )
				.hasPath( "users[0].emails" )
				.hasType( listOfString );
		assertThatFieldAccessor( "users[0].emails[]" )
				.isListItemFieldValue()
				.isForField( "users[].emails[]" )
				.hasPath( "users[0].emails[]" )
				.hasType( listOfString );
		assertThatFieldAccessor( "users[0].emails[0]" )
				.isSingleFieldValue()
				.isForField( "users[].emails[]" )
				.hasPath( "users[0].emails[0]" )
				.hasType( TypeDescriptor.valueOf( String.class ) );

		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "users[0].emails[1]" ) )
				.satisfies( e -> assertThat( e.getFieldName() ).isEqualTo( "users[0].emails[1]" ) );
	}

	@Test
	public void segmentSeparatorHelper() {
		assertThat( accessor.segmentSeparatorIndex( "" ) ).isEqualTo( -1 );
		assertThat( accessor.segmentSeparatorIndex( "[0]" ) ).isEqualTo( -1 );
		assertThat( accessor.segmentSeparatorIndex( "[0][]" ) ).isEqualTo( 3 );
		assertThat( accessor.segmentSeparatorIndex( "test" ) ).isEqualTo( -1 );
		assertThat( accessor.segmentSeparatorIndex( "test[0]" ) ).isEqualTo( 4 );
		assertThat( accessor.segmentSeparatorIndex( "test[0].name" ) ).isEqualTo( 4 );
		assertThat( accessor.segmentSeparatorIndex( "test.name" ) ).isEqualTo( 4 );
		assertThat( accessor.segmentSeparatorIndex( "[0].name" ) ).isEqualTo( 3 );
		assertThat( accessor.segmentSeparatorIndex( "test[0].name[1].title" ) ).isEqualTo( 4 );
		assertThat( accessor.segmentSeparatorIndex( "test[0][1]" ) ).isEqualTo( 4 );
	}

	@Test
	public void isIndexedSegmentHelper() {
		assertThat( accessor.isIndexedSegment( "name" ) ).isFalse();
		assertThat( accessor.isIndexedSegment( "[]" ) ).isTrue();
		assertThat( accessor.isIndexedSegment( "[123456]" ) ).isTrue();
		assertThat( accessor.isIndexedSegment( "123456]" ) ).isFalse();
		assertThat( accessor.isIndexedSegment( "[123456" ) ).isFalse();
		assertThat( accessor.isIndexedSegment( "[0]bad" ) ).isFalse();
	}

	@Test
	public void itemIndexHelper() {
		assertThat( accessor.itemIndex( "[]" ) ).isEqualTo( -1 );
		assertThat( accessor.itemIndex( "[0]" ) ).isEqualTo( 0 );
		assertThat( accessor.itemIndex( "[123456]" ) ).isEqualTo( 123456 );
	}

	@Test
	public void buildFieldPathHelper() {
		assertThat( accessor.buildFieldPath( "", "" ) ).isEqualTo( "" );
		assertThat( accessor.buildFieldPath( "", "name" ) ).isEqualTo( "name" );
		assertThat( accessor.buildFieldPath( "user", "name" ) ).isEqualTo( "user.name" );
		assertThat( accessor.buildFieldPath( "user", "" ) ).isEqualTo( "user" );
		assertThat( accessor.buildFieldPath( "user", "[]" ) ).isEqualTo( "user[]" );
		assertThat( accessor.buildFieldPath( "user[]", "name" ) ).isEqualTo( "user[].name" );
		assertThat( accessor.buildFieldPath( "user", "[0]" ) ).isEqualTo( "user[0]" );
		assertThat( accessor.buildFieldPath( "user[0]", "name" ) ).isEqualTo( "user[0].name" );
		assertThat( accessor.buildFieldPath( "user[]", "[0]" ) ).isEqualTo( "user[][0]" );
		assertThat( accessor.buildFieldPath( "user[0][1]", "[2]" ) ).isEqualTo( "user[0][1][2]" );
	}

	@Test
	public void findFieldAccessorReturnsEmptyIfNoSuchField() {
		assertThat( accessor.findFieldAccessor( "unknown" ) ).isEmpty();
		assertThat( accessor.findFieldAccessor( "user.name" ) ).isEmpty();
		assertThat( accessor.findFieldAccessor( "address.city" ) ).isEmpty();
	}

	@Test
	public void getFieldAccessorThrowsExceptionIfNoSuchField() {
		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "unknown" ) )
				.satisfies( fe -> assertThat( fe.getFieldName() ).isEqualTo( "unknown" ) );
		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "user.name" ) )
				.satisfies( fe -> assertThat( fe.getFieldName() ).isEqualTo( "user" ) );
		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "address.city" ) )
				.satisfies( fe -> assertThat( fe.getFieldName() ).isEqualTo( "address.city" ) );
	}

	@Test
	public void simpleFieldAccessor() {
		DynamicDocumentFieldAccessor field = accessor.getFieldAccessor( "name" );
		assertThat( data ).isEmpty();
		assertThat( field.set( "some name" ) ).isNull();
		assertThat( data ).containsEntry( "name", "some name" );
		assertThat( field.set( "update" ) ).isEqualTo( "some name" );
		assertThat( data ).containsEntry( "name", "update" );
		assertThat( field.set( null ) ).isEqualTo( "update" );
		assertThat( data ).containsEntry( "name", null );
		assertThat( field.delete() ).isNull();
		assertThat( data ).isEmpty();
		assertThat( field.delete() ).isNull();

		assertThat( accessor.getFieldAccessor( "name" ) ).isSameAs( field );
	}

	@Test
	public void simpleFieldAccessorDefaultValues() {
		nameDef.setInitializerExpression( ( c, f ) -> "hello" );

		DynamicDocumentFieldAccessor field = accessor.getFieldAccessor( "name" );
		assertThat( field.getDefaultValue() ).isEqualTo( "hello" );
		assertThat( field.get() ).isNull();

		field.initialize();
		assertThat( field.get() ).isEqualTo( "hello" );
		assertThat( field.set( "goodbye" ) ).isEqualTo( "hello" );
		assertThat( field.get() ).isEqualTo( "goodbye" );

		assertThat( field.getDefaultValue() ).isEqualTo( "hello" );
	}

	@Test
	public void fieldsetAccessor() {
		DynamicDocumentFieldAccessor address = accessor.getFieldAccessor( "address" );
		assertThat( data ).isEmpty();
		assertThat( address.exists() ).isFalse();
		assertThat( address.set( Collections.singletonMap( "street", "one" ) ) ).isNull();
		assertThat( address.get() ).isEqualTo( Collections.singletonMap( "street", "one" ) );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", "one" ) );
		assertThat( address.set( Collections.singletonMap( "street", "two" ) ) ).isEqualTo( Collections.singletonMap( "street", "one" ) );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", "two" ) );
		assertThat( address.set( null ) ).isEqualTo( Collections.singletonMap( "street", "two" ) );
		assertThat( data ).containsEntry( "address", null );
		assertThat( address.delete() ).isNull();
		assertThat( data ).isEmpty();
		assertThat( address.delete() ).isNull();

		DynamicDocumentFieldAccessor street = address.getFieldAccessor( "street" );
		assertThat( street.exists() ).isFalse();
		assertThat( street.set( "three" ) ).isNull();
		assertThat( street.get() ).isEqualTo( "three" );
		assertThat( street.set( "four" ) ).isEqualTo( "three" );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", "four" ) );
		assertThat( street.delete() ).isEqualTo( "four" );
		assertThat( street.exists() ).isFalse();
		assertThat( data ).containsEntry( "address", Collections.emptyMap() );
	}

	@Test
	@Ignore("AXDFM-8 - default values in fieldset/collection items are not correctly supported")
	public void fieldsetFieldAccessorDefaultValues() {
		addressStreetDef.setInitializerExpression( ( c, f ) -> "some street" );

		DynamicDocumentFieldAccessor address = accessor.getFieldAccessor( "address" );
		DynamicDocumentFieldAccessor street = address.getFieldAccessor( "street" );

		assertThat( street.getDefaultValue() ).isEqualTo( "some street" );
		assertThat( address.getDefaultValue() ).isNull();
		assertThat( street.get() ).isNull();

		street.initialize();
		assertThat( street.get() ).isEqualTo( "some street" );
		assertThat( street.set( "other street" ) ).isEqualTo( "some street" );
		assertThat( street.get() ).isEqualTo( "other street" );

		assertThat( address.get() ).isEqualTo( Collections.singletonMap( "street", "other street" ) );

		assertThat( address.getDefaultValue() ).isNull();
		assertThat( street.getDefaultValue() ).isEqualTo( "some street" );
	}

	@Test
	public void fieldsetAccessorDefaultValues() {
		addressStreetDef.setInitializerExpression( ( c, f ) -> "some street" );
		addressDef.setInitializerExpression( ( c, f ) -> Collections.singletonMap( "street", "other street" ) );

		DynamicDocumentFieldAccessor address = accessor.getFieldAccessor( "address" );
		DynamicDocumentFieldAccessor street = address.getFieldAccessor( "street" );

		assertThat( address.getDefaultValue() ).isEqualTo( Collections.singletonMap( "street", "other street" ) );
		assertThat( street.getDefaultValue() ).isEqualTo( "some street" );
		assertThat( address.get() ).isNull();
		assertThat( street.get() ).isNull();

		street.initialize();
		assertThat( street.get() ).isEqualTo( "some street" );
		assertThat( street.set( "3rd street" ) ).isEqualTo( "some street" );
		assertThat( street.get() ).isEqualTo( "3rd street" );

		assertThat( address.get() ).isEqualTo( Collections.singletonMap( "street", "3rd street" ) );
		assertThat( address.getDefaultValue() ).isEqualTo( Collections.singletonMap( "street", "other street" ) );
		assertThat( street.getDefaultValue() ).isEqualTo( "some street" );
	}

	@Test
	public void simpleListFieldAccessor() {
		DynamicDocumentFieldAccessor items = accessor.getFieldAccessor( "items" );
		assertThat( data ).isEmpty();
		assertThat( items.set( Arrays.asList( "1", "2" ) ) ).isNull();
		assertThat( data ).containsEntry( "items", Arrays.asList( "1", "2" ) );
		assertThat( items.set( Arrays.asList( "3", "2" ) ) ).isEqualTo( Arrays.asList( "1", "2" ) );
		assertThat( data ).containsEntry( "items", Arrays.asList( "3", "2" ) );
		assertThat( items.set( null ) ).isEqualTo( Arrays.asList( "3", "2" ) );
		assertThat( data ).containsEntry( "items", null );
		assertThat( items.delete() ).isNull();
		assertThat( data ).isEmpty();
		assertThat( items.delete() ).isNull();

		items.set( Collections.singletonList( "1" ) );
		DynamicDocumentFieldAccessor item = items.getFieldAccessor( "[0]" );
		assertThat( item.exists() ).isTrue();
		assertThat( item.set( "123" ) ).isEqualTo( "1" );
		assertThat( data ).containsEntry( "items", Collections.singletonList( "123" ) );
		assertThat( item.set( "456" ) ).isEqualTo( "123" );
		assertThat( data ).containsEntry( "items", Collections.singletonList( "456" ) );
		assertThat( item.delete() ).isEqualTo( "456" );
		assertThat( data ).containsEntry( "items", Collections.emptyList() );
		assertThat( item.delete() ).isNull();
		assertThat( item.exists() ).isFalse();

		assertThat( items.exists() ).isTrue();
		assertThat( items.get() ).isEqualTo( Collections.emptyList() );
	}

	@Test
	public void simpleListFieldAccessorDefaultValues() {
		itemListDef.setInitializerExpression( ( c, f ) -> Arrays.asList( "1", "2" ) );

		DynamicDocumentFieldAccessor items = accessor.getFieldAccessor( "items" );
		assertThat( items.getDefaultValue() ).isEqualTo( Arrays.asList( "1", "2" ) );
		assertThat( items.get() ).isNull();

		items.initialize();
		assertThat( items.get() ).isEqualTo( Arrays.asList( "1", "2" ) );
		assertThat( items.set( Collections.singletonList( "3" ) ) ).isEqualTo( Arrays.asList( "1", "2" ) );
		assertThat( items.get() ).isEqualTo( Collections.singletonList( "3" ) );

		assertThat( items.getDefaultValue() ).isEqualTo( Arrays.asList( "1", "2" ) );
	}

	@Test
	public void simpleListMemberFieldAccessorDefaultValues() {
		itemDef.setInitializerExpression( ( c, f ) -> "123" );

		DynamicDocumentFieldAccessor item = accessor.getFieldAccessor( "items[]" );
		assertThat( item.getDefaultValue() ).isEqualTo( "123" );

		accessor.getFieldAccessor( "items" ).set( Collections.singletonList( "1" ) );
		assertThat( accessor.getFieldAccessor( "items[0]" ).getDefaultValue() ).isEqualTo( "123" );
	}

	@Test
	public void fieldAccessorThrowsExceptionWhenSettingValueOfWrongType() {
		assertThatExceptionOfType( ClassCastException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "name" ).set( 123L ) );

		assertThatExceptionOfType( ClassCastException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "address.street" ).set( 123L ) );

		assertThatExceptionOfType( ClassCastException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "address" ).set( Collections.singletonMap( "street", 123L ) ) );
	}

	@Test
	public void fieldsetListFieldAccessor() {
		val john = ImmutableMap.of( "name", "John", "emails", Arrays.asList( "email-one", "email-two" ) );
		val jane = ImmutableMap.of( "name", "Jane", "emails", Collections.emptyList() );

		DynamicDocumentFieldAccessor users = accessor.getFieldAccessor( "users" );
		assertThat( data ).isEmpty();
		assertThat( users.set( Arrays.asList( john, jane ) ) ).isNull();
		assertThat( data ).containsEntry( "users", Arrays.asList( john, jane ) );
		assertThat( users.set( Collections.singleton( jane ) ) ).isEqualTo( Arrays.asList( john, jane ) );
		assertThat( data ).containsEntry( "users", Collections.singletonList( jane ) );
		assertThat( users.set( null ) ).isEqualTo( Collections.singletonList( jane ) );
		assertThat( data ).containsEntry( "users", null );
		assertThat( users.delete() ).isNull();
		assertThat( data ).isEmpty();
		assertThat( users.delete() ).isNull();

		users.set( Arrays.asList( john, jane ) );

		DynamicDocumentFieldAccessor user = users.getFieldAccessor( "[0]" );
		user.set( jane );
		assertThat( user.get() ).isEqualTo( jane );
		assertThat( users.get() ).isEqualTo( Arrays.asList( jane, jane ) );
		user.getFieldAccessor( "name" ).set( "Jane updated" );
		user.getFieldAccessor( "emails" ).set( Arrays.asList( "email-3", "email-4" ) );
		assertThat( user.getFieldAccessor( "emails[1]" ).delete() ).isEqualTo( "email-4" );
		assertThat( user.get() ).isEqualTo( ImmutableMap.of( "name", "Jane updated", "emails", Collections.singletonList( "email-3" ) ) );
	}

	@Test
	public void fieldsetListFieldAccessorDefaultValues() {
		val john = ImmutableMap.of( "name", "John", "emails", Arrays.asList( "email-one", "email-two" ) );
		userDef.setInitializerExpression( ( c, f ) -> john );

		DynamicDocumentFieldAccessor users = accessor.getFieldAccessor( "users[]" );
		assertThat( users.getDefaultValue() ).isEqualTo( john );
	}

	@Test
	public void aggregatedListFieldAccessor() {
		val john = ImmutableMap.of( "name", "John", "emails", Arrays.asList( "email-1", "email-2" ) );
		val jane = ImmutableMap.of( "name", "Jane", "emails", Collections.singletonList( "email-3" ) );
		val jack = ImmutableMap.of( "name", "Jack", "emails", Collections.emptyList() );

		DynamicDocumentDataAccessor.AbstractFieldAccessor usersList = accessor.getFieldAccessor( "users" );
		DynamicDocumentFieldAccessor users = accessor.getFieldAccessor( "users[]" );

		assertThat( usersList.exists() ).isFalse();
		assertThat( users.exists() ).isFalse();

		usersList.set( Arrays.asList( john, jane ) );
		assertThat( users.exists() ).isTrue();
		assertThat( users.get() ).isEqualTo( Arrays.asList( john, jane ) );
		assertThat( users.set( Arrays.asList( john, jack ) ) ).isEqualTo( Arrays.asList( john, jane ) );

		DynamicDocumentFieldAccessor names = accessor.getFieldAccessor( "users[].name" );
		assertThat( names.get() ).isEqualTo( Arrays.asList( "John", "Jack" ) );
		assertThat( accessor.getFieldAccessor( "users[]" ).getFieldAccessor( "name" ).get() ).isEqualTo( Arrays.asList( "John", "Jack" ) );

		DynamicDocumentFieldAccessor emails = accessor.getFieldAccessor( "users[].emails" );
		assertThat( emails.get() ).isEqualTo( Arrays.asList( Arrays.asList( "email-1", "email-2" ), Collections.emptyList() ) );

		assertThat( emails.delete() )
				.isEqualTo( Arrays.asList( Arrays.asList( "email-1", "email-2" ), Collections.emptyList() ) );
		assertThat( emails.get() ).isEqualTo( Arrays.asList( null, null ) );
		assertThat( usersList.get() )
				.isEqualTo( Arrays.asList( ImmutableMap.of( "name", "John" ), ImmutableMap.of( "name", "Jack" ) ) );

		assertThat( emails.set( Arrays.asList( Collections.singleton( "email-4" ), Arrays.asList( "email-5", "email-6" ) ) ) )
				.isEqualTo( Arrays.asList( null, null ) );
		assertThat( usersList.get() )
				.isEqualTo( Arrays.asList( ImmutableMap.of( "name", "John", "emails", Collections.singletonList( "email-4" ) ),
				                           ImmutableMap.of( "name", "Jack", "emails", Arrays.asList( "email-5", "email-6" ) ) ) );

		// lower lists
		DynamicDocumentFieldAccessor addressList = users.getFieldAccessor( "address" );
		assertThat( addressList.set( Arrays.asList( Collections.singletonList( Collections.singletonMap( "street", "one" ) ),
		                                            Collections.singletonList( Collections.singletonMap( "street", "two" ) ) ) ) )
				.isEqualTo( Arrays.asList( null, null ) );

		assertThat( users.getFieldAccessor( "address[].street" ).getFieldType() )
				.isEqualTo(
						TypeDescriptor.collection( ArrayList.class, TypeDescriptor.collection( ArrayList.class, TypeDescriptor.valueOf( String.class ) ) ) );
		assertThat( users.getFieldAccessor( "address[].street" ).get() )
				.isEqualTo( Arrays.asList( Collections.singletonList( "one" ), Collections.singletonList( "two" ) ) );
	}

	@Test
	public void aggregatedListFieldAccessorDefaultValues() {
		userEmailDef.setInitializerExpression( ( c, f ) -> "a@b.c" );
		userNameDef.setInitializerExpression( ( c, f ) -> "jane doe" );
		userAddressStreetDef.setInitializerExpression( ( c, f ) -> "my street" );

		DynamicDocumentFieldAccessor userName = accessor.getFieldAccessor( "users[].name" );
		assertThat( userName.getDefaultValue() ).isEqualTo( "jane doe" );

		DynamicDocumentFieldAccessor userEmail = accessor.getFieldAccessor( "users[].emails[]" );
		assertThat( userEmail.getDefaultValue() ).isEqualTo( "a@b.c" );

		DynamicDocumentFieldAccessor userAddressStreet = accessor.getFieldAccessor( "users[].address[].street" );
		assertThat( userAddressStreet.getDefaultValue() ).isEqualTo( "my street" );
	}

	@Test
	public void exists() {
		DynamicDocumentFieldAccessor name = accessor.getFieldAccessor( "name" );
		DynamicDocumentFieldAccessor address = accessor.getFieldAccessor( "address" );
		DynamicDocumentFieldAccessor street = accessor.getFieldAccessor( "address.street" );

		assertThat( data ).isEmpty();
		assertThat( name.exists() ).isFalse();
		assertThat( address.exists() ).isFalse();
		assertThat( street.exists() ).isFalse();

		data.put( "name", null );
		assertThat( name.exists() ).isTrue();

		data.put( "address", null );
		assertThat( address.exists() ).isTrue();
		assertThat( street.exists() ).isFalse();

		data.put( "address", Collections.singletonMap( "street", "some street" ) );
		assertThat( address.exists() ).isTrue();
		assertThat( street.exists() ).isTrue();
	}

	@Test
	public void existingNestedFieldAccessor() {
		data.put( "address", new LinkedHashMap<>() );

		DynamicDocumentFieldAccessor field = accessor.getFieldAccessor( "address.street" );

		assertThat( data ).hasSize( 1 ).containsKey( "address" );
		field.set( "some street" );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", "some street" ) );
		field.set( "update" );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", "update" ) );
		field.set( null );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", null ) );
		assertThat( field.delete() ).isNull();
		assertThat( data ).containsEntry( "address", Collections.emptyMap() );
		assertThat( field.delete() ).isNull();
	}

	@Test
	public void getOrCreateWillSetDefaultValueField() {
		DynamicDocumentFieldAccessor name = accessor.getFieldAccessor( "name" );
		DynamicDocumentFieldAccessor address = accessor.getFieldAccessor( "address" );

		assertThat( data ).isEmpty();
		assertThat( name.getOrCreate() ).isEqualTo( "<default>" );
		assertThat( data ).containsEntry( "name", "<default>" );

		val map = address.getOrCreate();
		assertThat( map ).isEqualTo( Collections.emptyMap() );
		assertThat( address.getOrCreate() ).isSameAs( map );
	}

	@Test
	public void nestedFieldAccessorCreatesIntermediates() {
		DynamicDocumentFieldAccessor field = accessor.getFieldAccessor( "address.street" );

		assertThat( data ).isEmpty();
		field.set( "some street" );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", "some street" ) );
		field.set( "update" );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", "update" ) );
		field.set( null );
		assertThat( data ).containsEntry( "address", Collections.singletonMap( "street", null ) );
		assertThat( field.delete() ).isNull();
		assertThat( data ).containsEntry( "address", Collections.emptyMap() );
		assertThat( field.delete() ).isNull();
		assertThat( accessor.getFieldAccessor( "address" ).get() ).isEqualTo( Collections.emptyMap() );
		assertThat( accessor.getFieldAccessor( "address" ).delete() ).isEqualTo( Collections.emptyMap() );
		assertThat( data ).isEmpty();

		assertThat( accessor.getFieldAccessor( "address.street" ) ).isSameAs( field );
	}

	@Test
	public void nestedDocumentFieldAccessor() {
		DynamicDocumentVersion dv = DynamicDocumentVersion.builder().build();
		val workspace = mock( DynamicDocumentWorkspace.class );
		when( documentService.createDocumentWorkspace( dv ) ).thenReturn( workspace );

		DynamicDocumentData nestedData = mock( DynamicDocumentData.class );
		when( workspace.getFields() ).thenReturn( nestedData );

		DynamicDocumentFieldAccessor nestedDoc = accessor.getFieldAccessor( "document" );

		// If no document attached, nested fields do not exist
		assertThat( accessor.findFieldAccessor( "document.title" ) ).isEmpty();

		// In case of nested document the accessor of the target document field is returned
		nestedDoc.set( dv );
		val titleAccessor = mock( DynamicDocumentDataAccessor.AbstractFieldAccessor.class );
		when( titleAccessor.get() ).thenReturn( "nested document title" );
		when( nestedData.getFieldAccessor( "title" ) ).thenReturn( titleAccessor );
		assertThat( accessor.getFieldAccessor( "document.title" ).get() ).isEqualTo( "nested document title" );
		assertThatExceptionOfType( UnsupportedOperationException.class )
				.isThrownBy( () -> accessor.getFieldAccessor( "document.title" ).set( "not allowed" ) );

		assertThat( accessor.getFieldAccessor( "document" ) ).isSameAs( nestedDoc );
	}

	private FieldAccessorAssert assertThatFieldAccessor( String path ) {
		DynamicDocumentFieldAccessor field = accessor.getFieldAccessor( path );
		assertThat( field ).isNotNull();
		return new FieldAccessorAssert( field );
	}

	@RequiredArgsConstructor
	private static class FieldAccessorAssert
	{
		private final DynamicDocumentFieldAccessor field;

		FieldAccessorAssert isForField( String canonicalId ) {
			assertThat( field.getFieldDefinition().getCanonicalId() ).isEqualTo( canonicalId );
			return this;
		}

		FieldAccessorAssert hasPath( String fieldPath ) {
			assertThat( field.getFieldPath() ).isEqualTo( fieldPath );
			return this;
		}

		FieldAccessorAssert hasType( TypeDescriptor typeDescriptor ) {
			assertThat( field.getFieldType() ).isEqualTo( typeDescriptor );
			return this;
		}

		FieldAccessorAssert isSingleFieldValue() {
			assertThat( field ).isInstanceOf( DynamicDocumentDataAccessor.SingleFieldValueAccessor.class );
			return this;
		}

		FieldAccessorAssert isCalculatedFieldValue() {
			assertThat( field ).isInstanceOf( DynamicDocumentDataAccessor.CalculatedFieldValueAccessor.class );
			return this;
		}

		FieldAccessorAssert isListValue() {
			assertThat( field ).isInstanceOf( DynamicDocumentDataAccessor.ListValueAccessor.class );
			return this;
		}

		FieldAccessorAssert isListItemFieldValue() {
			assertThat( field ).isInstanceOf( DynamicDocumentDataAccessor.ListItemFieldAccessor.class );
			return this;
		}

		FieldAccessorAssert isNestedDocumentValue() {
			assertThat( field ).isInstanceOf( DynamicDocumentDataAccessor.NestedDocumentAccessor.class );
			return this;
		}

		FieldAccessorAssert isFieldSetValue() {
			assertThat( field ).isInstanceOf( DynamicDocumentDataAccessor.FieldSetValueAccessor.class );
			return this;
		}

		FieldAccessorAssert isSameAs( DynamicDocumentFieldAccessor fieldAccessor ) {
			assertThat( field ).isSameAs( fieldAccessor );
			return this;
		}

		FieldAccessorAssert isNotSameAs( DynamicDocumentFieldAccessor fieldAccessor ) {
			assertThat( fieldAccessor ).isNotNull().isNotSameAs( field );
			return this;
		}
	}
}
