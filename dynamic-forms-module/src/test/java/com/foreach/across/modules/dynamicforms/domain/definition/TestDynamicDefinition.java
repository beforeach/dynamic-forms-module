/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * @author Steven Gentens
 * @since 0.01
 */
public class TestDynamicDefinition
{
	@Test
	public void defaultValues() {
		DynamicDefinition dynamicDefinition = new DynamicDefinition();
		verifyDefaultValues( dynamicDefinition );
		verifyDefaultValues( DynamicDefinition.builder().build() );
		verifyDefaultValues( dynamicDefinition.toDto() );
		verifyDefaultValues( dynamicDefinition.toBuilder().build() );
	}

	private void verifyDefaultValues( DynamicDefinition dynamicDefinition ) {
		assertNull( dynamicDefinition.getId() );
		assertNull( dynamicDefinition.getName() );
		assertNull( dynamicDefinition.getType() );
		assertNull( dynamicDefinition.getDataSet() );
		assertNull( dynamicDefinition.getLatestVersion() );
	}

	@Test
	public void builderSemantics() {
		Date timestamp = new Date();

		DynamicDefinition dynamicDefinition = DynamicDefinition.builder()
		                                                       .name( "My name" )
		                                                       .createdBy( "john" )
		                                                       .createdDate( timestamp )
		                                                       .lastModifiedBy( "josh" )
		                                                       .build();

		assertNull( dynamicDefinition.getId() );
		assertEquals( "My name", dynamicDefinition.getName() );
		assertEquals( "john", dynamicDefinition.getCreatedBy() );
		assertEquals( timestamp, dynamicDefinition.getCreatedDate() );
		assertEquals( "josh", dynamicDefinition.getLastModifiedBy() );
		assertNull( dynamicDefinition.getDataSet() );
		assertNull( dynamicDefinition.getLastModifiedDate() );
		assertNull( dynamicDefinition.getLatestVersion() );

		DynamicDefinitionDataSet dataset = new DynamicDefinitionDataSet();
		dataset.setName( "test" );
		dataset.setKey( "TEST" );
		dataset.setId( -1L );

		DynamicDefinitionVersion version = DynamicDefinitionVersion.builder().id( -1L ).build();

		DynamicDefinition other = dynamicDefinition.toBuilder()
		                                           .dataSet( dataset )
		                                           .latestVersion( version )
		                                           .id( 333L )
		                                           .lastModifiedDate( timestamp )
		                                           .build();
		assertNotSame( dynamicDefinition, other );

		assertEquals( Long.valueOf( 333L ), other.getId() );
		assertEquals( version, other.getLatestVersion() );
		assertEquals( "My name", dynamicDefinition.getName() );
		assertEquals( "john", other.getCreatedBy() );
		assertEquals( timestamp, other.getCreatedDate() );
		assertEquals( "josh", other.getLastModifiedBy() );
		assertEquals( timestamp, other.getLastModifiedDate() );
		assertEquals( dataset, other.getDataSet() );
	}

	@Test
	public void toStringOutput() {
		DynamicDefinitionDataSet ds = new DynamicDefinitionDataSet();
		ds.setKey( "jobs" );

		DynamicDefinitionType type = new DynamicDefinitionType();
		type.setName( DynamicDefinitionTypes.DOCUMENT );

		DynamicDefinition def = DynamicDefinition.builder()
		                                         .dataSet( ds )
		                                         .type( type )
		                                         .key( "interview-test" )
		                                         .build();

		assertNotNull( new DynamicDefinition().toString() );
		assertEquals( "jobs:interview-test", def.getFullKey() );
		assertEquals( "jobs:interview-test", def.toString() );
	}
}
