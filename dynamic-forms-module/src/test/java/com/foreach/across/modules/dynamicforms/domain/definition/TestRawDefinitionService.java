/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.view.RawViewDefinition;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;
import org.yaml.snakeyaml.Yaml;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestRawDefinitionService
{
	private RawDefinitionService rawDefinitionService;

	@Before
	public void setUp() {
		DynamicDefinitionTypeRepository mock = mock( DynamicDefinitionTypeRepository.class );
		when( mock.findByName( anyString() ) ).thenReturn( Optional.empty() );
		DynamicDefinitionTypeConfigurationRegistry definitionTypeRegistry = new DynamicDefinitionTypeConfigurationRegistry( mock );
		definitionTypeRegistry.register( "document-definition", RawDocumentDefinition.class, null );
		definitionTypeRegistry.register( "view-definition", RawViewDefinition.class, null );

		rawDefinitionService = new RawDefinitionService( definitionTypeRegistry );
	}

	@Test
	@SneakyThrows
	public void readViewDefinition() {
		String convertedJson = readAsYaml( "yaml/definitions/view-simple.yml" );
		RawViewDefinition definition = rawDefinitionService.readRawDefinition( convertedJson, RawViewDefinition.class );
		assertNotNull( definition );
		assertThat( definition ).hasFieldOrPropertyWithValue( "name", "createView" );
		assertThat( definition ).hasFieldOrPropertyWithValue( "type", "list" );
		assertThat( definition.getProperties() ).containsExactly( "name", "#username", "#address.street" );

		RawViewDefinition definitionByTypeName = (RawViewDefinition) rawDefinitionService.readRawDefinition( convertedJson, "view-definition" );
		assertThat( definitionByTypeName )
				.isEqualToComparingFieldByField( definition );
	}

	@Test
	public void writeViewDefinition() {
		RawViewDefinition definition = rawDefinitionService.readRawDefinition( readAsYaml( "yaml/definitions/view-simple.yml" ), RawViewDefinition.class );
		assertThat( rawDefinitionService.writeDefinition( definition ) )
				.isEqualTo(
						"{\"view-definition\":{\"name\":\"createView\",\"type\":\"list\",\"properties\":[\"name\",\"#username\",\"#address.street\"],\"filter\":{\"defaultSelector\":\"MULTI\",\"defaultQuery\":\"order by name ASC\",\"basePredicate\":\"domain = 1\",\"mode\":{\"basic\":true,\"advanced\":true},\"fields\":[{\"id\":\"name\"},{\"id\":\"title\",\"selector\":\"SINGLE\"},{\"id\":\"created\",\"selector\":\"SINGLE\"},{\"id\":\"people\",\"selector\":\"MULTI\"}]}}}" );
	}

	@Test
	public void readDocumentDefinition() {
		String convertedJson = readAsYaml( "yaml/definitions/workspace-v1.yml" );
		RawDocumentDefinition definition = rawDefinitionService.readRawDefinition( convertedJson, RawDocumentDefinition.class );
		assertNotNull( definition );
		assertThat( definition ).hasFieldOrPropertyWithValue( "name", "workspace-test" );
		assertThat( definition.getContent() )
				.hasSize( 2 );

		RawDocumentDefinition definitionByTypeName = (RawDocumentDefinition) rawDefinitionService.readRawDefinition( convertedJson, "document-definition" );
		assertThat( definitionByTypeName )
				.isEqualToComparingFieldByField( definition );
	}

	@Test
	public void writeDocumentDefinitionInDeterministicOrder() {
		RawDocumentDefinition definition = rawDefinitionService.readRawDefinition( readAsYaml( "yaml/definitions/workspace-v1.yml" ),
		                                                                           RawDocumentDefinition.class );
		assertThat( rawDefinitionService.writeDefinition( definition ) )
				.isEqualTo(
						"{\"document-definition\":{\"name\":\"workspace-test\"," +
								"\"content\":[" +
								"{\"type\":\"string\",\"id\":\"group-name\",\"validators\":[\"required\"],\"attributes\":{\"some-attribute\":1}}," +
								"{\"type\":\"number\",\"id\":\"user-count\"}]" +
								"}}" );
	}

	@Test
	public void writeDefaultValuesAsTheyHaveBeenRead() {
		RawDocumentDefinition definition = rawDefinitionService.readRawDefinition( readAsYaml( "yaml/definitions/default-values.yml" ),
		                                                                           RawDocumentDefinition.class );

		assertThat( rawDefinitionService.writeDefinition( definition ) )
				.isEqualTo( "{\"document-definition\":{" +
						            "\"content\":[" +
						            "{\"id\":\"one\",\"default\":{\"value\":1}}," +
						            "{\"id\":\"two\",\"default\":{\"formula\":\"x\"}}," +
						            "{\"id\":\"three\",\"default\":null}," +
						            "{\"id\":\"four\"}," +
						            "{\"id\":\"five\",\"default\":{\"value\":null}}" +
						            "]" +
						            "}}" );
	}

	@SneakyThrows
	private String readAsYaml( String path ) {
		Yaml yaml = new Yaml();
		Object raw = yaml.load( new ClassPathResource( path ).getInputStream() );

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString( raw );
	}
}
