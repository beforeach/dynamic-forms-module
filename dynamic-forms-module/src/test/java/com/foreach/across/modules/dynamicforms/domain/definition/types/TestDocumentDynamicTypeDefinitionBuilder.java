/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.EntityConfiguration;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;

import java.util.Collections;
import java.util.LinkedHashSet;

import static com.foreach.across.modules.dynamicforms.domain.definition.types.EntityDynamicTypeDefinitionBuilder.ATTR_OPTIONS_QUERY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDocumentDynamicTypeDefinitionBuilder
{
	@Mock
	private EntityRegistry entityRegistry;

	@Mock
	private DynamicDocumentFieldDefinition fieldDefinition;

	@InjectMocks
	private DocumentDynamicTypeDefinitionBuilder builder;

	@Test
	public void acceptedIfMatchesRegex() {
		assertThat( builder.accepts( field( "document" ) ) ).isTrue();
		assertThat( builder.accepts( field( "documentVersion" ) ) ).isTrue();
		assertThat( builder.accepts( field( "document(jobs:vacature)" ) ) ).isTrue();
		assertThat( builder.accepts( field( "documentVersion(obs:vacature@123)" ) ) ).isTrue();
		assertThat( builder.accepts( field( "documentVersion[]" ) ) ).isTrue();
		assertThat( builder.accepts( field( "document(jobs:vacature)[]" ) ) ).isTrue();
		assertThat( builder.accepts( field( "dynamicDocument" ) ) ).isFalse();
		assertThat( builder.accepts( field( "@dynamicDocument" ) ) ).isFalse();
		assertThat( builder.accepts( field( "@dynamicDocument[]" ) ) ).isFalse();
	}

	@Test
	public void document() {
		String entityType = "document";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( "dynamicDocument" ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocument.class );

		TypeDescriptor expectedType = TypeDescriptor.valueOf( DynamicDocument.class );
		val typeDef = builder.buildTypeDefinition( null, field( entityType ) );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( expectedType );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( entityType );
		EntityPropertyDescriptorBuilder descriptor = EntityPropertyDescriptor.builder( "my-property" );
		typeDef.customizePropertyDescriptor( fieldDefinition, descriptor );
		assertThat( descriptor.build().attributeMap() )
				.doesNotContainKey( EntityAttributes.OPTIONS_ENTITY_QUERY );
	}

	@Test
	public void documentSpecificVersion() {
		String entityType = "document(jobs:vacature@123)";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( "dynamicDocument" ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocument.class );

		TypeDescriptor expectedType = TypeDescriptor.valueOf( DynamicDocument.class );
		val typeDef = builder.buildTypeDefinition( null, field( entityType ) );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( expectedType );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( entityType );
		EntityPropertyDescriptorBuilder descriptor = EntityPropertyDescriptor.builder( "my-property" );
		typeDef.customizePropertyDescriptor( fieldDefinition, descriptor );
		assertThat( descriptor.build().attributeMap() )
				.containsEntry( DynamicDefinitionId.class.getName(), DynamicDefinitionId.fromString( "jobs:vacature@123" ) )
				.containsEntry( EntityAttributes.OPTIONS_ENTITY_QUERY, "latestVersion.definitionVersion = 'jobs:vacature@123'" );
	}

	@Test
	public void specificDocument() {
		String entityType = "document(jobs:vacature)";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( "dynamicDocument" ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocument.class );

		TypeDescriptor expectedType = TypeDescriptor.valueOf( DynamicDocument.class );
		val typeDef = builder.buildTypeDefinition( null, field( entityType ) );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( expectedType );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( entityType );
		EntityPropertyDescriptorBuilder descriptor = EntityPropertyDescriptor.builder( "my-property" );
		typeDef.customizePropertyDescriptor( fieldDefinition, descriptor );
		assertThat( descriptor.build().attributeMap() )
				.containsEntry( DynamicDefinitionId.class.getName(), DynamicDefinitionId.fromString( "jobs:vacature" ) )
				.containsEntry( EntityAttributes.OPTIONS_ENTITY_QUERY, "type = 'jobs:vacature'" );
	}

	@Test
	public void withOptionSortQuery() {
		String entityType = "document(jobs:vacature)";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( "dynamicDocument" ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocument.class );

		TypeDescriptor expectedType = TypeDescriptor.valueOf( DynamicDocument.class );
		val typeDef = builder.buildTypeDefinition( null,
		                                           RawDocumentDefinition.Field.builder().id( "my-" + entityType )
		                                                                      .attributes( Collections.singletonMap( ATTR_OPTIONS_QUERY, "order by name ASC" ) )
		                                                                      .type( entityType ).build() );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( expectedType );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( entityType );
		EntityPropertyDescriptorBuilder descriptor = EntityPropertyDescriptor.builder( "my-property" );
		typeDef.customizePropertyDescriptor( fieldDefinition, descriptor );
		assertThat( descriptor.build().attributeMap() )
				.containsEntry( DynamicDefinitionId.class.getName(), DynamicDefinitionId.fromString( "jobs:vacature" ) )
				.containsEntry( EntityAttributes.OPTIONS_ENTITY_QUERY, "type = 'jobs:vacature' order by name ASC" );
	}

	@Test
	public void withOptionQuery() {
		String entityType = "document(jobs:vacature)";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( "dynamicDocument" ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocument.class );

		TypeDescriptor expectedType = TypeDescriptor.valueOf( DynamicDocument.class );
		val typeDef = builder.buildTypeDefinition( null,
		                                           RawDocumentDefinition.Field.builder().id( "my-" + entityType )
		                                                                      .attributes( Collections.singletonMap( ATTR_OPTIONS_QUERY,
		                                                                                                             "field:name contains 'Java'" ) )
		                                                                      .type( entityType ).build() );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( expectedType );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( entityType );
		EntityPropertyDescriptorBuilder descriptor = EntityPropertyDescriptor.builder( "my-property" );
		typeDef.customizePropertyDescriptor( fieldDefinition, descriptor );
		assertThat( descriptor.build().attributeMap() )
				.containsEntry( DynamicDefinitionId.class.getName(), DynamicDefinitionId.fromString( "jobs:vacature" ) )
				.containsEntry( EntityAttributes.OPTIONS_ENTITY_QUERY, "type = 'jobs:vacature' and field:name contains 'Java'" );
	}

	@Test
	public void documentVersion() {
		String entityType = "documentVersion";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( "dynamicDocumentVersion" ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocumentVersion.class );

		TypeDescriptor expectedType = TypeDescriptor.valueOf( DynamicDocumentVersion.class );
		val typeDef = builder.buildTypeDefinition( null, field( entityType ) );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( expectedType );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( entityType );
	}

	@Test
	public void multiValue() {
		String entityType = "document";
		EntityConfiguration mockConfiguration = mock( EntityConfiguration.class );
		when( entityRegistry.getEntityConfiguration( "dynamicDocument" ) )
				.thenReturn( mockConfiguration );
		when( mockConfiguration.getEntityType() ).thenReturn( DynamicDocument.class );

		TypeDescriptor expectedType = TypeDescriptor.valueOf( DynamicDocument.class );
		val typeDef = builder.buildTypeDefinition( null, field( entityType + "[]" ) );

		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( TypeDescriptor.collection( LinkedHashSet.class, expectedType ) );
		assertThat( typeDef.getTypeName() )
				.isEqualTo( entityType + "[]" );
	}

	private RawDocumentDefinition.Field field( String type ) {
		return RawDocumentDefinition.Field.builder().id( "my-" + type ).type( type ).build();
	}
}
