/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import lombok.val;
import org.junit.Test;

import static com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId.create;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestDynamicDefinitionId
{
	@Test
	public void unableToCreateIfNoDataSetOfDefinitionKey() {
		assertThat( create( null, (String) null, null, null, null ) ).isNull();
		assertThat( create( null, null, DynamicDefinitionTypes.DOCUMENT, "jobs", null ) ).isNull();
		assertThat( create( null, "data", DynamicDefinitionTypes.DOCUMENT, null, null ) ).isNull();
	}

	@Test
	public void propertiesAndEquals() {
		val documentId = create( null, "data", DynamicDefinitionTypes.DOCUMENT, "jobs", null );
		assertThat( documentId ).isNotNull();
		assertThat( documentId.hasParent() ).isFalse();
		assertThat( documentId.isForVersion() ).isFalse();
		assertThat( documentId.getDefinitionType() ).isEqualTo( DynamicDefinitionTypes.DOCUMENT );
		assertThat( documentId.getDataSetKey() ).isEqualTo( "data" );
		assertThat( documentId.getDefinitionKey() ).isEqualTo( "jobs" );

		val unspecifiedTypeId = create( null, "data", null, "jobs", null );
		assertThat( unspecifiedTypeId ).isNotNull().isEqualTo( documentId );
		assertThat( unspecifiedTypeId.hasParent() ).isFalse();
		assertThat( unspecifiedTypeId.isForVersion() ).isFalse();
		assertThat( unspecifiedTypeId.getDefinitionType() ).isEqualTo( DynamicDefinitionTypes.DOCUMENT );
		assertThat( unspecifiedTypeId.getDataSetKey() ).isEqualTo( "data" );
		assertThat( unspecifiedTypeId.getDefinitionKey() ).isEqualTo( "jobs" );

		val viewId = create( null, "data", DynamicDefinitionTypes.VIEW, "jobs", null );
		assertThat( viewId ).isNotNull().isNotEqualTo( documentId );
		assertThat( viewId.hasParent() ).isFalse();
		assertThat( viewId.isForVersion() ).isFalse();
		assertThat( viewId.getVersion() ).isNull();
		assertThat( viewId.getDefinitionType() ).isEqualTo( DynamicDefinitionTypes.VIEW );
		assertThat( viewId.getDataSetKey() ).isEqualTo( "data" );
		assertThat( viewId.getDefinitionKey() ).isEqualTo( "jobs" );

		val documentVersionId = create( null, "data", DynamicDefinitionTypes.DOCUMENT, "jobs", "1.0-alpha" );
		assertThat( documentVersionId ).isNotNull().isNotEqualTo( documentId );
		assertThat( documentVersionId.hasParent() ).isFalse();
		assertThat( documentVersionId.isForVersion() ).isTrue();
		assertThat( documentVersionId.getVersion() ).isEqualTo( "1.0-alpha" );
		assertThat( documentVersionId.getDefinitionType() ).isEqualTo( DynamicDefinitionTypes.DOCUMENT );
		assertThat( documentVersionId.getDataSetKey() ).isEqualTo( "data" );
		assertThat( documentVersionId.getDefinitionKey() ).isEqualTo( "jobs" );

		assertThat( documentVersionId ).isEqualTo( documentId.asDefinitionVersionId( "1.0-alpha" ) );
		assertThat( documentId ).isEqualTo( documentVersionId.asDefinitionId() );

		val viewWithParent = create( documentId, null, DynamicDefinitionTypes.VIEW, "listView", null );
		assertThat( viewWithParent )
				.isEqualTo( create( documentVersionId, "ignored-ds", DynamicDefinitionTypes.VIEW, "listView", null ) );
		assertThat( viewWithParent ).isNotNull().isNotEqualTo( documentId );
		assertThat( viewWithParent.hasParent() ).isTrue();
		assertThat( viewWithParent.getParent() ).isEqualTo( documentId );
		assertThat( viewWithParent.isForVersion() ).isFalse();
		assertThat( viewWithParent.getVersion() ).isNull();
		assertThat( viewWithParent.getDefinitionType() ).isEqualTo( DynamicDefinitionTypes.VIEW );
		assertThat( viewWithParent.getDataSetKey() ).isEqualTo( "data" );
		assertThat( viewWithParent.getDefinitionKey() ).isEqualTo( "listView" );
	}

	@Test
	public void asString() {
		val documentId = create( null, "data", DynamicDefinitionTypes.DOCUMENT, "jobs", null );
		assertThat( documentId.toString() ).isEqualTo( "data:jobs" );

		val documentVersionId = create( null, "data", DynamicDefinitionTypes.DOCUMENT, "jobs", "1.0-alpha" );
		assertThat( documentVersionId.toString() ).isEqualTo( "data:jobs@1.0-alpha" );

		val viewId = create( null, "data", DynamicDefinitionTypes.VIEW, "randomView", null );
		assertThat( viewId.toString() ).isEqualTo( "data:view:randomView" );

		val documentWithParent = create( documentId, null, DynamicDefinitionTypes.DOCUMENT, "details", null );
		assertThat( documentWithParent.toString() ).isEqualTo( "data:jobs/details" );

		val viewWithParent = create( documentId, null, DynamicDefinitionTypes.VIEW, "listView", null );
		assertThat( viewWithParent.toString() ).isEqualTo( "data:jobs/view:listView" );

		val viewVersionWithParent = create( documentId, null, DynamicDefinitionTypes.VIEW, "listView", "123" );
		assertThat( viewVersionWithParent.toString() ).isEqualTo( "data:jobs/view:listView@123" );

		val nestedDummy = create( viewVersionWithParent, "ignored", null, "some-doc", "567" );
		assertThat( nestedDummy.toString() ).isEqualTo( "data:jobs/view:listView/some-doc@567" );
	}

	@Test
	public void fromString() {
		assertThat( DynamicDefinitionId.fromString( null ) ).isNull();
		assertThat( DynamicDefinitionId.fromString( "" ) ).isNull();
		assertThat( DynamicDefinitionId.fromString( "dataset-only" ) ).isNull();
		assertThat( DynamicDefinitionId.fromString( "not:a:valid:id" ) ).isNull();

		DynamicDefinitionId documentId = create( null, "data", DynamicDefinitionTypes.DOCUMENT, "jobs", null );
		assertThat( DynamicDefinitionId.fromString( "data:jobs" ) )
				.isEqualTo( documentId );
		assertThat( DynamicDefinitionId.fromString( "data:jobs@1.0-alpha" ) )
				.isEqualTo( create( null, "data", DynamicDefinitionTypes.DOCUMENT, "jobs", "1.0-alpha" ) );
		assertThat( DynamicDefinitionId.fromString( "data:view:randomView" ) )
				.isEqualTo( create( null, "data", DynamicDefinitionTypes.VIEW, "randomView", null ) );
		assertThat( DynamicDefinitionId.fromString( "data:jobs/details" ) )
				.isEqualTo( create( documentId, null, DynamicDefinitionTypes.DOCUMENT, "details", null ) );
		assertThat( DynamicDefinitionId.fromString( "data:jobs/view:listView" ) )
				.isEqualTo( create( documentId, null, DynamicDefinitionTypes.VIEW, "listView", null ) );
		DynamicDefinitionId viewVersionWithParent = create( documentId, null, DynamicDefinitionTypes.VIEW, "listView", "123" );
		assertThat( DynamicDefinitionId.fromString( "data:jobs/view:listView@123" ) )
				.isEqualTo( viewVersionWithParent );
		assertThat( DynamicDefinitionId.fromString( "data:jobs/view:listView/some-doc@567" ) )
				.isEqualTo( create( viewVersionWithParent, "ignored", null, "some-doc", "567" ) );
	}
}
