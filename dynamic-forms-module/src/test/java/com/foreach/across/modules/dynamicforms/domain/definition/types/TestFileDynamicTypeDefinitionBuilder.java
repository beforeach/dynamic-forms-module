/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.bootstrapui.elements.FormViewElement;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.filemanager.business.reference.FileReference;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;

/**
 * @author Steven Gentens
 * @since 0.0.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestFileDynamicTypeDefinitionBuilder
{
	@Mock
	private DynamicDocumentFieldDefinition fieldDefinition;

	@InjectMocks
	private FileDynamicTypeDefinitionBuilder builder;

	@Test
	public void accepts() {
		assertThat( builder.accepts( field( "something" ) ) ).isFalse();
		assertThat( builder.accepts( field( "file" ) ) ).isTrue();
		assertThat( builder.accepts( field( "file[]" ) ) ).isTrue();
	}

	@Test
	public void fileType() {
		val typeDef = builder.buildTypeDefinition( null, field( "file" ) );
		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( TypeDescriptor.valueOf( FileReference.class ) );
		assertThat( typeDef.getTypeName() ).isEqualTo( "file" );

		EntityPropertyDescriptorBuilder propBuilder = spy( EntityPropertyDescriptor.builder( "myprop" ) );
		typeDef.customizePropertyDescriptor( fieldDefinition, propBuilder );

		val prop = propBuilder.build();
		val encType = prop.getAttribute( EntityAttributes.FORM_ENCTYPE, String.class );
		assertThat( encType ).isEqualTo( FormViewElement.ENCTYPE_MULTIPART );

	}

	private RawDocumentDefinition.Field field( String type ) {
		return RawDocumentDefinition.Field.builder().id( "my-" + type ).type( type ).build();
	}
}
