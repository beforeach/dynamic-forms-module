/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.BaseField;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.DefaultValue;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.Field;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSet;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinitionFactory;
import com.foreach.across.modules.dynamicforms.domain.definition.types.GenericDynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldExpressionParser;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldFormulaContext;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidator;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidatorFactory;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.EvaluationContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static infrastructure.DynamicFormsAssertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 * @see test.TestBuildDocumentDefinitions
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unchecked")
public class TestDynamicDocumentDefinitionBuilder
{
	@Mock
	private DynamicTypeDefinitionFactory typeDefinitionFactory;

	@Mock
	private DynamicDocumentFieldValidatorFactory fieldValidatorFactory;

	@Mock
	private DynamicDocumentFieldValidator fieldValidator;

	@Mock
	private DynamicDocumentFieldExpressionParser fieldExpressionParser;

	@InjectMocks
	private DynamicDocumentDefinitionBuilder builder;

	@Before
	public void before() {
		when( typeDefinitionFactory.createTypeDefinition( ArgumentMatchers.<Field>argThat( field -> field != null && field.getType().equals( "string" ) ) ) )
				.thenReturn(
						GenericDynamicTypeDefinition.builder()
						                            .typeDescriptor( TypeDescriptor.valueOf( String.class ) )
						                            .typeName( "string" )
						                            .propertyDescriptorBuilderConsumer( ( field, builder ) -> builder.attribute( "customized", true ) )
						                            .build()
				);
		when( typeDefinitionFactory.createTypeDefinition( ArgumentMatchers.<Field>argThat( field -> field != null && field.getType().equals( "fieldset" ) ) ) )
				.thenReturn(
						GenericDynamicTypeDefinition.builder()
						                            .typeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
						                            .typeName( "fieldset" )
						                            .build()
				);

		when(
				fieldValidatorFactory.createValidator(
						ArgumentMatchers.<Field>argThat( field -> field != null && field.getId().equals( "name" ) ),
						any(),
						eq( "required" )
				)
		)
				.thenReturn( fieldValidator );
	}

	@Test
	@SuppressWarnings("unchecked")
	public void buildSimpleValidDefinition() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "my-doc" )
		                                         .field(
				                                         Field.builder()
				                                              .id( "name" )
				                                              .type( "string" )
				                                              .messages( Collections.singletonMap( "description", "custom description" ) )
				                                              .validators( Collections.singletonList( "required" ) )
				                                              .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "timestamp" )
				                                              .label( "Current time" )
				                                              .type( "string" )
				                                              .formula( "currentTime()" )
				                                              .build()
		                                         )
		                                         .build();

		BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> expression = mock( BiFunction.class );
		when( fieldExpressionParser.parseExpression( "currentTime()", TypeDescriptor.valueOf( String.class ) ) ).thenReturn( expression );

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 2 )
				.withNextField(
						field -> field.hasId( "name" )
						              .hasCanonicalId( "name" )
						              .hasLabel( "Name" )
						              .hasTypeName( "string" )
						              .hasTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
						              .hasNoInitializerExpression()
						              .isACollection( false )
						              .isAMember( false )
						              .satisfies( f -> assertThat( f.asRawDefinition() ).isEqualTo( rawDefinition.getContent().get( 0 ) ) )
						              .satisfies(
								              f -> assertThat( (Boolean) f.getEntityPropertyDescriptor().getAttribute( "customized", Boolean.class ) )
										              .isTrue()
						              )
						              .satisfies( definition -> {
							              assertThat( definition.getValidators() ).contains( fieldValidator );
							              verify( fieldValidator, times( 1 ) ).customizePropertyDescriptor( eq( definition ), any() );
						              } )
				)
				.withNextField(
						field -> field.hasId( "timestamp" )
						              .hasCanonicalId( "timestamp" )
						              .hasLabel( "Current time" )
						              .hasTypeName( "string" )
						              .hasTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
						              .hasExpression( expression )
						              .hasInitializerExpression( expression )
						              .isACollection( false )
						              .isAMember( false )
						              .withPropertyDescriptor( descriptor -> {
							              assertThat( descriptor.isWritable() ).isFalse();
						              } )
				)
				.satisfies( definition -> assertThat( definition.asRawDefinition() ).isEqualTo( rawDefinition ) )
				.satisfies( definition -> assertThat( definition.getDefinitionId() ).isEqualTo( DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.satisfies( definition ->
						            assertThat( definition.getMessageSource()
						                                  .getMessage( "DynamicDocument[dataset:document].properties.field:name[description]", null, null ) )
								            .isEqualTo( "custom description" )
				)
				.satisfies( definition ->
						            assertThat( definition.getEntityPropertyRegistry().select( EntityPropertySelector.of( "*" ) )
						                                  .stream()
						                                  .map( EntityPropertyDescriptor::getName )
						                                  .collect( Collectors.toList() ) )
								            .containsExactly( "field:name", "field:timestamp" )
				);
	}

	@Test
	public void buildNestedDefinition() {
		val rawDefinition = RawDocumentDefinition
				.builder()
				.name( "my-doc" )
				.field( Field.builder().id( "name" ).type( "string" ).build() )
				.field(
						Field.builder()
						     .id( "address" )
						     .type( "fieldset" )
						     .field( Field.builder().id( "street" ).type( "string" ).build() )
						     .build() )
				.build();

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "data:document" ) ) )
				.hasFields( 2 )
				.withNextField(
						field -> field.hasId( "name" )
						              .hasCanonicalId( "name" )
						              .hasTypeName( "string" )
						              .hasTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
						              .hasNoInitializerExpression()
						              .isACollection( false )
						              .isAMember( false )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:name" );
						              } )
				)
				.withNextField(
						field -> field.hasId( "address" )
						              .hasCanonicalId( "address" )
						              .hasTypeName( "fieldset" )
						              .hasTypeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
						              .hasNoInitializerExpression()
						              .isACollection( false )
						              .isAMember( false )
						              .withPropertyDescriptor( d -> {
							              assertThat( d.getName() ).isEqualTo( "field:address" );

							              assertThat( d.getController().createValue( null ) )
									              .isNotNull()
									              .isInstanceOf( DynamicDataFieldSet.class )
									              .isNotSameAs( d.getController().createValue( null ) );
						              } )
						              .hasFields( 1 )
						              .withNextField(
								              street -> street.hasId( "street" )
								                              .hasCanonicalId( "address.street" )
								                              .hasTypeName( "string" )
								                              .hasTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
								                              .withPropertyDescriptor( d -> {
									                              assertThat( d.getName() ).isEqualTo( "field:address.street" );
								                              } )
								                              .hasNoInitializerExpression()
								                              .isACollection( false )
								                              .isAMember( false )
						              )
				);
	}

	@Test
	public void circularDependenciesAreNotAllowedInBaseTypes() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "circular-definition" )
		                                         .type( BaseField.builder().id( "field1" ).baseType( "field2" ).build() )
		                                         .type( BaseField.builder().id( "field2" ).baseType( "field3" ).build() )
		                                         .type( BaseField.builder().id( "field3" ).baseType( "field1" ).build() )
		                                         .field(
				                                         Field.builder()
				                                              .id( "some-field" )
				                                              .type( "field1" )
				                                              .build()
		                                         )
		                                         .build();
		assertThatThrownBy( () -> builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) ).isInstanceOf(
				DynamicDocumentDefinition.CircularFieldTypeException.class ).hasMessage(
				"Circular field reference: 'field3'. It points back to field: 'field1' which is already defined." );

	}

	@Test
	public void circularDependenciesAreNotAllowedInFieldSets() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "circular-definition-in-fieldset" )
		                                         .type( BaseField.builder().id( "weird" ).baseType( "fieldset" )
		                                                         .field( RawDocumentDefinition.Field.builder().id( "foo" )
		                                                                                            .type( "field1" ).build() )
		                                                         .build() )
		                                         .type( BaseField.builder().id( "field1" ).baseType( "fieldset" )
		                                                         .field( Field.builder().id( "bar" ).type( "weird" ).build() ).build() )
		                                         .field(
				                                         Field.builder()
				                                              .id( "some-field" )
				                                              .type( "field1" )
				                                              .build()
		                                         )
		                                         .build();
		assertThatThrownBy( () -> builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) ).isInstanceOf(
				DynamicDocumentDefinition.CircularFieldTypeException.class ).hasMessage(
				"Circular field reference: 'foo'. It points back to field: 'field1' which is already defined." );
	}

	@Test
	public void defaultValueAndFormulaOnSameFieldIsNotAllowed() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "incorrect-default-value" )
		                                         .field( Field.builder().id( "bad-default" )
		                                                      .type( "string" )
		                                                      .defaultValue( DefaultValue.builder().value( "123" ).formula( "4" ).build() )
		                                                      .build() )
		                                         .build();
		assertThatThrownBy( () -> builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) ).isInstanceOf(
				DynamicDocumentDefinition.IllegalFieldConfigurationException.class ).hasMessage(
				"Illegal configuration for field 'bad-default': default value and default formula may not be combined" );
	}

	@Test
	public void multipleFieldsInFieldsetCanUseSameTypeDefinition() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "multiple-fields-can-use-same-type" )
		                                         .type( BaseField.builder().id( "my-type" ).baseType( "string" )
		                                                         .possibleValues( Arrays.asList( "john", "jane", "joe" ) )
		                                                         .build()
		                                         )
		                                         .field( Field.builder()
		                                                      .id( "my-fieldset" )
		                                                      .type( "fieldset" )
		                                                      .field( Field.builder().id( "travels-by-car" ).type( "my-type" ).build() )
		                                                      .field( Field.builder().id( "travels-by-bike" ).type( "my-type" ).build() )
		                                                      .build()
		                                         )
		                                         .build();
		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 1 )
				.withNextField( field -> field.hasFields( 2 ) );

	}

	@Test
	public void fieldsetTypeAlwaysHasDefaultValueCreated() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "my-doc" )
		                                         .field( Field.builder().id( "name" ).label( "Some name" ).type( "fieldset" ).build() )
		                                         .build();

		assertThat( builder.build( rawDefinition, null ) )
				.hasFields( 1 )
				.withNextField(
						field -> field.hasId( "name" )
						              .hasCanonicalId( "name" )
						              .hasLabel( "Some name" )
						              .hasTypeName( "fieldset" )
						              .hasTypeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
						              .hasNoInitializerExpression()
				)
				.satisfies( definition -> assertThat( definition.getDefinitionId() ).isNull() );
	}

	@Test
	public void exceptionThrownIfFieldNameHasIllegalCharacters() {
		val rawDefinition = RawDocumentDefinition
				.builder()
				.name( "my-doc" )
				.field( Field.builder().id( "illegal/field name" ).type( "string" ).build() )
				.build();

		assertThatExceptionOfType( DynamicDocumentDefinition.IllegalFieldIdException.class )
				.isThrownBy( () -> builder.build( rawDefinition, DynamicDefinitionId.fromString( "data:document" ) ) )
				.satisfies( ife -> assertThat( ife.getFieldName() ).isEqualTo( "illegal/field name" ) );
	}

	@Test
	public void initializerValueForSimpleDefinition() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "initializer-doc" )
		                                         .field(
				                                         Field.builder()
				                                              .id( "firstName" )
				                                              .type( "string" )
				                                              .defaultValue( DefaultValue.builder().value( "a" ).build() )
				                                              .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "lastName" )
				                                              .type( "string" )
				                                              .defaultValue( DefaultValue.builder().formula( "x" ).build() )
				                                              .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "city" )
				                                              .type( "string" )
				                                              .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "street" )
				                                              .type( "string" )
				                                              .defaultValue( DefaultValue.builder().value( null ).build() )
				                                              .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "number" )
				                                              .type( "string" )
				                                              .defaultValue( DefaultValue.builder().formula( null ).build() )
				                                              .build()
		                                         ).build();

		BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> intializeExpressionFormula = mock( BiFunction.class );
		when( fieldExpressionParser.parseExpression( "x", TypeDescriptor.valueOf( String.class ) ) ).thenReturn( intializeExpressionFormula );

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 5 )
				.withNextField( field -> field.hasId( "firstName" ).hasInitializeExpressionValue( "a" ) )
				.withNextField( field -> field.hasId( "lastName" ).hasInitializerExpression( intializeExpressionFormula ) )
				.withNextField( field -> field.hasId( "city" ).hasNoInitializerExpression() )
				.withNextField( field -> field.hasId( "street" ).hasInitializeExpressionValue( null ) )
				.withNextField( field -> field.hasId( "number" ).hasInitializeExpressionValue( null ) );

	}

	@Test
	public void initializerValueWithFieldsetDefinition() {
		HashMap<String, Object> defaultValuesOnFieldset = new HashMap<>();
		defaultValuesOnFieldset.put( "firstName", "Guido" );

		val rawDefinition = RawDocumentDefinition
				.builder()
				.name( "my-doc" )
				.field(
						Field.builder()
						     .id( "name" )
						     .type( "fieldset" )
						     .defaultValue( DefaultValue.builder().value( defaultValuesOnFieldset ).build() )
						     .field( Field.builder().id( "firstName" ).type( "string" ).build() )
						     .field(
								     Field.builder()
								          .id( "lastName" )
								          .type( "string" )
								          .defaultValue( DefaultValue.builder().value( "Pellemans" ).build() )
								          .build()
						     )
						     .build() )
				.build();

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "data:document" ) ) )
				.hasFields( 1 )
				.withNextField(
						field -> field.hasId( "name" )
						              .hasTypeName( "fieldset" )
						              .hasTypeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
						              .hasInitializeExpressionValue( defaultValuesOnFieldset )
						              .hasFields( 2 )
						              .withNextField(
								              street -> street.hasId( "firstName" )
								                              .hasTypeName( "string" )
								                              .hasNoInitializerExpression()
						              )
						              .withNextField(
								              street -> street.hasId( "lastName" )
								                              .hasTypeName( "string" )
								                              .hasInitializeExpressionValue( "Pellemans" )
						              )
				);
	}

	@Test
	public void initializerValueFromBaseType() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "initializer-doc" )
		                                         .type(
				                                         BaseField.builder()
				                                                  .id( "firstname-field" )
				                                                  .baseType( "string" )
				                                                  .build()
		                                         )
		                                         .type(
				                                         BaseField.builder()
				                                                  .id( "lastname-field" )
				                                                  .baseType( "string" )
				                                                  .defaultValue( DefaultValue.builder().value( "Pellemans" ).build() )
				                                                  .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "firstname" )
				                                              .type( "firstname-field" )
				                                              .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "lastname" )
				                                              .type( "lastname-field" )
				                                              .build()
		                                         )
		                                         .build();

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 2 )
				.withNextField( field -> field.hasId( "firstname" ).hasNoInitializerExpression() )
				.withNextField( field -> field.hasId( "lastname" ).hasInitializeExpressionValue( "Pellemans" ) );

	}

	@Test
	public void fieldsetInitializerValuesFromBaseTypes() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "initializer-doc" )
		                                         .type(
				                                         BaseField.builder()
				                                                  .id( "name-field" )
				                                                  .baseType( "fieldset" )
				                                                  .field(
						                                                  Field.builder()
						                                                       .id( "firstName" )
						                                                       .type( "string" )
						                                                       .defaultValue( null )
						                                                       .build()
				                                                  )
				                                                  .field(
						                                                  Field.builder()
						                                                       .id( "lastName" )
						                                                       .type( "string" )
						                                                       .defaultValue( DefaultValue.builder().value( "Pellemans" ).build() )
						                                                       .build()
				                                                  )
				                                                  .field(
						                                                  Field.builder()
						                                                       .id( "city" )
						                                                       .type( "string" )
						                                                       .build()
				                                                  )
				                                                  .field(
						                                                  Field.builder()
						                                                       .id( "street" )
						                                                       .type( "string" )
						                                                       .defaultValue( DefaultValue.builder().formula( "x" ).build() )
						                                                       .build()
				                                                  )
				                                                  .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "name" )
				                                              .type( "name-field" )
				                                              .build()
		                                         )
		                                         .build();

		BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> intializeExpressionFormula = mock( BiFunction.class );
		when( fieldExpressionParser.parseExpression( "x", TypeDescriptor.valueOf( String.class ) ) ).thenReturn( intializeExpressionFormula );

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 1 )
				.withNextField(
						field -> field.hasId( "name" )
						              .hasNoInitializerExpression()
						              .withNextField( firstName -> firstName.hasId( "firstName" ).hasNoInitializerExpression() )
						              .withNextField( lastName -> lastName.hasId( "lastName" ).hasInitializeExpressionValue( "Pellemans" ) )
						              .withNextField( city -> city.hasId( "city" ).hasNoInitializerExpression() )
						              .withNextField( street -> street.hasId( "street" ).hasInitializerExpression( intializeExpressionFormula ) )
				);
	}

	@Test
	public void initializerValueFromBaseTypeReplaced() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "initializer-doc" )
		                                         .type(
				                                         BaseField.builder()
				                                                  .id( "name" )
				                                                  .baseType( "string" )
				                                                  .defaultValue( DefaultValue.builder().value( "John" ).build() )
				                                                  .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "firstname" )
				                                              .type( "name" )
				                                              .defaultValue( DefaultValue.builder().value( "Guido" ).build() )
				                                              .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "lastname" )
				                                              .type( "name" )
				                                              .defaultValue( null )
				                                              .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "nickname" )
				                                              .type( "name" )
				                                              .defaultValue( DefaultValue.builder().value( null ).build() )
				                                              .build()
		                                         )
		                                         .build();

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 3 )
				.withNextField( field -> field.hasId( "firstname" ).hasInitializeExpressionValue( "Guido" ) )
				.withNextField( field -> field.hasId( "lastname" ).hasNoInitializerExpression() )
				.withNextField( field -> field.hasId( "nickname" ).hasInitializeExpressionValue( null ) );
	}

	@Test
	public void fieldsetInitializerValuesFromBaseTypeReplaced() {
		HashMap<String, Object> defaultValuesForContentFieldSet = new HashMap<>();
		defaultValuesForContentFieldSet.put( "lastName", "Drets" );

		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "initializer-doc" )
		                                         .type(
				                                         BaseField.builder()
				                                                  .id( "user-field" )
				                                                  .baseType( "fieldset" )
				                                                  .field(
						                                                  Field.builder()
						                                                       .id( "firstName" )
						                                                       .type( "string" )
						                                                       .defaultValue( null )
						                                                       .build()
				                                                  )
				                                                  .field(
						                                                  Field.builder()
						                                                       .id( "lastName" )
						                                                       .type( "string" )
						                                                       .defaultValue( DefaultValue.builder().value( "Pellemans" ).build() )
						                                                       .build()
				                                                  )
				                                                  .field(
						                                                  Field.builder()
						                                                       .id( "city" )
						                                                       .type( "string" )
						                                                       .build()
				                                                  )
				                                                  .field(
						                                                  Field.builder()
						                                                       .id( "street" )
						                                                       .type( "string" )
						                                                       .defaultValue( DefaultValue.builder().formula( "x" ).build() )
						                                                       .build()
				                                                  )
				                                                  .build()
		                                         )
		                                         .field(
				                                         Field.builder()
				                                              .id( "name" )
				                                              .type( "user-field" )
				                                              .defaultValue(
						                                              DefaultValue.builder()
						                                                          .value( defaultValuesForContentFieldSet )
						                                                          .build()
				                                              )
				                                              .build()
		                                         )
		                                         .build();

		BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> intializeExpressionFormula = mock( BiFunction.class );
		when( fieldExpressionParser.parseExpression( "x", TypeDescriptor.valueOf( String.class ) ) ).thenReturn( intializeExpressionFormula );

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 1 )
				.withNextField(
						field -> field
								.hasInitializeExpressionValue( defaultValuesForContentFieldSet )
								.withNextField( firstName -> firstName.hasId( "firstName" ).hasNoInitializerExpression() )
								.withNextField( lastName -> lastName.hasId( "lastName" ).hasInitializeExpressionValue( "Pellemans" ) )
								.withNextField( city -> city.hasId( "city" ).hasNoInitializerExpression() )
								.withNextField( street -> street.hasId( "street" ).hasInitializerExpression( intializeExpressionFormula ) )
				);

	}

	@Test
	public void formulaIsUsedAsInitializerExpressionIfNoneSetExplicitly() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "initializer-doc" )
		                                         .field(
				                                         Field.builder()
				                                              .id( "name" )
				                                              .type( "string" )
				                                              .formula( "x" )
				                                              .build()
		                                         )
		                                         .build();

		BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> intializeExpressionFormula = mock( BiFunction.class );
		when( fieldExpressionParser.parseExpression( "x", TypeDescriptor.valueOf( String.class ) ) ).thenReturn( intializeExpressionFormula );

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 1 )
				.withNextField(
						field -> field.hasId( "name" )
						              .hasInitializerExpression( intializeExpressionFormula )
				);
	}

	@Test
	public void defaultValueTakesPrecedenceOverFieldFormula() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "initializer-doc" )
		                                         .field(
				                                         Field.builder()
				                                              .id( "name" )
				                                              .type( "string" )
				                                              .formula( "x" )
				                                              .defaultValue( DefaultValue.builder().formula( "y" ).build() )
				                                              .build()
		                                         )
		                                         .build();

		BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> intializeExpressionFormula = mock( BiFunction.class );
		when( fieldExpressionParser.parseExpression( "y", TypeDescriptor.valueOf( String.class ) ) ).thenReturn( intializeExpressionFormula );

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 1 )
				.withNextField(
						field -> field.hasId( "name" )
						              .hasInitializerExpression( intializeExpressionFormula )
				);
	}

	@Test
	public void explicitNullDefaultValueBehavesAsRegularDefaultValue() {
		val rawDefinition = RawDocumentDefinition.builder()
		                                         .name( "initializer-doc" )
		                                         .field(
				                                         Field.builder()
				                                              .id( "name" )
				                                              .type( "string" )
				                                              .formula( "x" )
				                                              .defaultValue( DefaultValue.builder().formula( null ).build() )
				                                              .build()
		                                         )
		                                         .build();

		assertThat( builder.build( rawDefinition, DynamicDefinitionId.fromString( "dataset:document" ) ) )
				.hasFields( 1 )
				.withNextField(
						field -> field.hasId( "name" ).hasInitializeExpressionValue( null )
				);
	}
}
