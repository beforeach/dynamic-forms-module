/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDefinitionTypeConfigurationRegistry
{
	private DynamicDefinitionTypeConfigurationRegistry definitionTypeRegistry;

	@Before
	public void setUp() {
		DynamicDefinitionTypeRepository mock = mock( DynamicDefinitionTypeRepository.class );
		when( mock.findByName( anyString() ) ).thenReturn( Optional.empty() );
		when( mock.save( any( DynamicDefinitionType.class ) ) )
				.thenAnswer( (Answer<DynamicDefinitionType>) invocation -> {
					             DynamicDefinitionType argument = invocation.getArgument( 0 );
					             argument.setId( -1L );
					             return argument;
				             }
				);
		definitionTypeRegistry = new DynamicDefinitionTypeConfigurationRegistry( mock );
	}

	@Test
	public void getConfigurationByName() {
		DynamicDefinitionTypeConfiguration configuration = definitionTypeRegistry.register( "my-type", CustomRawDefinition.class, null );
		DynamicDefinitionTypeConfiguration retrievedConfiguration = definitionTypeRegistry.getByName( "my-type" );
		assertThat( retrievedConfiguration )
				.isEqualToComparingFieldByField( configuration );
	}

	@Test
	public void getConfigurationByClass() {
		DynamicDefinitionTypeConfiguration configuration = definitionTypeRegistry.register( "my-type", CustomRawDefinition.class, null );
		DynamicDefinitionTypeConfiguration retrievedConfiguration = definitionTypeRegistry.getByClass( CustomRawDefinition.class );
		assertThat( retrievedConfiguration )
				.isEqualToComparingFieldByField( configuration );
	}

	private class CustomRawDefinition implements RawDefinition
	{

	}
}
