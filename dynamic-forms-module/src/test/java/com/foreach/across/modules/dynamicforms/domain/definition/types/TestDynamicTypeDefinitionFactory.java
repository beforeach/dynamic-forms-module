/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.Field;
import lombok.val;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestDynamicTypeDefinitionFactory
{
	private DynamicTypeDefinitionFactory typeDefinitionFactory = new DynamicTypeDefinitionFactory();

	@Test
	public void exceptionIsThrownWhenTypeIsUnknown() {
		Field field = Field.builder().type( "bad-type" ).build();

		assertThatExceptionOfType( DynamicDocumentDefinition.IllegalFieldTypeException.class )
				.isThrownBy( () -> typeDefinitionFactory.createTypeDefinition( field ) )
				.satisfies( ife -> assertThat( ife.getFieldType() ).isEqualTo( "bad-type" ) );
	}

	@Test
	public void firstBuilderIsUsedWhenMultiple() {
		val one = mock( DynamicTypeDefinitionBuilder.class );
		val two = mock( DynamicTypeDefinitionBuilder.class );
		val three = mock( DynamicTypeDefinitionBuilder.class );

		typeDefinitionFactory.setTypeDefinitionBuilders( Arrays.asList( one, two, three ) );

		val result = mock( DynamicTypeDefinition.class );

		Field field = Field.builder().type( "bad-type" ).build();
		when( one.accepts( any() ) ).thenReturn( true );
		when( one.buildTypeDefinition( typeDefinitionFactory, field ) ).thenReturn( result );

		assertThat( typeDefinitionFactory.createTypeDefinition( field ) ).isSameAs( result );
		verifyZeroInteractions( two, three );

		typeDefinitionFactory.setTypeDefinitionBuilders( Arrays.asList( three, two, one ) );
		when( three.accepts( field ) ).thenReturn( true );
		assertThat( typeDefinitionFactory.createTypeDefinition( field ) ).isNull();

		verify( one, times( 1 ) ).accepts( field );
		verify( three, times( 1 ) ).accepts( field );
		verifyZeroInteractions( two );
	}
}
