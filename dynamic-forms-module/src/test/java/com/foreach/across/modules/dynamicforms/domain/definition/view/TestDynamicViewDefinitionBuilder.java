/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.view;

import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static infrastructure.DynamicFormsAssertions.assertThat;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDynamicViewDefinitionBuilder
{
	@InjectMocks
	private DynamicViewDefinitionBuilder builder;

	@Test
	public void buildSimpleValidDefinition() {
		val rawDefinition = RawViewDefinition.builder()
		                                     .name( "my-doc" )
		                                     .type( "list" )
		                                     .property( "name" )
		                                     .property( "foobar" )
		                                     .property( "field:dynamicfield" )
		                                     .build();

		val viewDefinition = builder.build( rawDefinition );
		assertThat( viewDefinition.getProperties() ).hasSize( 3 );
		assertThat( viewDefinition.getProperties() ).containsExactly( "name", "foobar", "field:dynamicfield" );
	}

	@Test
	public void buildValidDefinitionWithFilter() {
		val rawDefinition = RawViewDefinition.builder()
		                                     .filter( RawViewDefinition.Filter.builder()
		                                                                      .mode( RawViewDefinition.Mode.builder().build() )
		                                                                      .build() )
		                                     .build();

		val viewDefinition = builder.build( rawDefinition );
		DynamicViewFilterDefinition filterDefinition = viewDefinition.getFilter();
		assertThat( filterDefinition ).hasFieldOrPropertyWithValue( "basicMode", false )
		                              .hasFieldOrPropertyWithValue( "advancedMode", false );
		assertThat( filterDefinition.getBasePredicate() ).isNull();
		;
		assertThat( filterDefinition.getDefaultQuery() ).isNull();
		assertThat( filterDefinition.getDefaultSelector() ).isEqualTo( DynamicViewDefinitionFilterSelector.SINGLE );
		assertThat( filterDefinition.getFields() ).hasSize( 0 );
	}

	@Test
	public void buildValidDefinitionWithFilterAdvancedOnly() {
		val rawDefinition = RawViewDefinition.builder()
		                                     .filter( RawViewDefinition.Filter.builder()
		                                                                      .mode( RawViewDefinition.Mode.builder().advanced( true ).build() )
		                                                                      .build() )
		                                     .build();

		val viewDefinition = builder.build( rawDefinition );
		DynamicViewFilterDefinition filterDefinition = viewDefinition.getFilter();
		assertThat( filterDefinition ).hasFieldOrPropertyWithValue( "basicMode", false )
		                              .hasFieldOrPropertyWithValue( "advancedMode", true );
		assertThat( filterDefinition.getBasePredicate() ).isNull();
		;
		assertThat( filterDefinition.getDefaultQuery() ).isNull();
		assertThat( filterDefinition.getDefaultSelector() ).isEqualTo( DynamicViewDefinitionFilterSelector.SINGLE );
		assertThat( filterDefinition.getFields() ).hasSize( 0 );
	}

	@Test
	public void buildValidDefinitionWithFilterSingleFields() {
		val rawDefinition = RawViewDefinition.builder()
		                                     .filter( RawViewDefinition.Filter.builder()
		                                                                      .field( RawViewDefinition.Field.builder().id( "name" ).build() )
		                                                                      .field( RawViewDefinition.Field.builder().id( "title" ).selector(
				                                                                      DynamicViewDefinitionFilterSelector.SINGLE ).build() )
		                                                                      .build() )
		                                     .build();

		val viewDefinition = builder.build( rawDefinition );
		DynamicViewFilterDefinition filterDefinition = viewDefinition.getFilter();
		assertThat( filterDefinition ).hasFieldOrPropertyWithValue( "basicMode", true )
		                              .hasFieldOrPropertyWithValue( "advancedMode", false );
		assertThat( filterDefinition.getBasePredicate() ).isNull();
		;
		assertThat( filterDefinition.getDefaultQuery() ).isNull();
		assertThat( filterDefinition.getDefaultSelector() ).isEqualTo( DynamicViewDefinitionFilterSelector.SINGLE );
		assertThat( filterDefinition.getFields() ).hasSize( 2 );
		assertThat( filterDefinition.getFields() ).extracting( "id" ).containsExactly( "name", "title" );
		assertThat( filterDefinition.getFields() ).extracting( "selector" ).containsExactly( DynamicViewDefinitionFilterSelector.SINGLE,
		                                                                                     DynamicViewDefinitionFilterSelector.SINGLE );
	}

	@Test
	public void buildValidDefinitionWithFilterSingleAndMultiFields() {
		val rawDefinition = RawViewDefinition.builder()
		                                     .filter( RawViewDefinition.Filter.builder()
		                                                                      .field( RawViewDefinition.Field.builder().id( "name" ).build() )
		                                                                      .field( RawViewDefinition.Field.builder().id( "title" ).selector(
				                                                                      DynamicViewDefinitionFilterSelector.SINGLE ).build() )
		                                                                      .field( RawViewDefinition.Field.builder().id( "people" ).selector(
				                                                                      DynamicViewDefinitionFilterSelector.MULTI ).build() )
		                                                                      .basePredicate( "people is not null" )
		                                                                      .build() )
		                                     .build();

		val viewDefinition = builder.build( rawDefinition );
		DynamicViewFilterDefinition filterDefinition = viewDefinition.getFilter();
		assertThat( filterDefinition ).hasFieldOrPropertyWithValue( "basicMode", true )
		                              .hasFieldOrPropertyWithValue( "advancedMode", false );
		assertThat( filterDefinition.getBasePredicate() ).isEqualTo( "people is not null" )
		;
		assertThat( filterDefinition.getDefaultQuery() ).isNull();
		assertThat( filterDefinition.getDefaultSelector() ).isEqualTo( DynamicViewDefinitionFilterSelector.MULTI );
		assertThat( filterDefinition.getFields() ).hasSize( 3 );
		assertThat( filterDefinition.getFields() ).extracting( "id" ).containsExactly( "name", "title", "people" );
		assertThat( filterDefinition.getFields() ).extracting( "selector" ).containsExactly( DynamicViewDefinitionFilterSelector.SINGLE,
		                                                                                     DynamicViewDefinitionFilterSelector.SINGLE,
		                                                                                     DynamicViewDefinitionFilterSelector.MULTI );
	}

}
