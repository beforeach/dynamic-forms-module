/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data.expressions;

import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentFieldAccessor;
import com.google.common.collect.ImmutableMap;
import lombok.val;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.expression.EvaluationContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDocumentFieldExpressionParser
{
	private DynamicDocumentFieldExpressionParser parser = new DynamicDocumentFieldExpressionParser( new DefaultConversionService() );
	private EvaluationContext evaluationContext = BuildDynamicDocumentFormulaEvaluationContext.createDefaultEvaluationContext();
	private DynamicDocumentFieldFormulaContext fieldContext;

	@Test
	public void nullFieldAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", 123 ) );
		parseAndAssert( "field('x').get() + (field('x.y.z')?.get()?:0) + 5", "128" );
		parseAndAssert( "$(x) + $(x.y.z:0) + 5", 128L );
	}

	@Test
	public void fieldAccessorWithPrimitive() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", 123 ).put( "x.y.z", 123 ) );
		parseAndAssert( "field('x').get() + field('x.y.z').get() + 5", "251" );
		parseAndAssert( "$(x) + $(x.y.z) + 5", 251L );
	}

	@Test
	public void fieldAccessorWithString() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", "123" ).put( "x.y.z", "123" ) );

		parseAndAssert( "field('x').get() + field('x.y.z').get() + 5", "1231235" );
		parseAndAssert( "$(x) + $(x.y.z) + 5", 1231235L );
	}

	@Test
	public void fieldAccessorWithArray() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", 123 ).put( "x.y.z", 123 ) );
		parseAndAssert( "{field('x').get(),field('x.y.z').get(),5}", "123,123,5" );
		parseAndAssert( "{$(x),$(x.y.z),5}", Lists.newArrayList( 123, 123, 5 ) );
	}

	@Test
	public void sumAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", 1 ).put( "y", 2 ).put( "z", 3 ) );
		parseAndAssert( "sum($(x),$(y),$(z),$(unknown:0))", "6.0" );
	}

	@Test
	public void minAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", -1 ).put( "y", 17 ) );
		parseAndAssert( "min($(x), $(y))", "-1" );
	}

	@Test
	public void maxAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", -1 ).put( "y", 17 ) );
		parseAndAssert( "max($(x), $(y))", "17" );
	}

	@Test
	public void avgAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", 3 ).put( "y", 4 ) );
		parseAndAssert( "avg($(x), $(y))", "3.5" );
	}

	@Test
	public void isEmptyAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "array", new String[] { "a", "b", "c" } )
		                                                             .put( "emptyArray", new String[0] )
		                                                             .put( "collection", Arrays.asList( "a", "b", "c" ) )
		                                                             .put( "emptyCollection", Collections.emptyList() ) );
		parseAndAssert( "isEmpty($(array))", false );
		parseAndAssert( "isEmpty($(emptyArray))", true );
		parseAndAssert( "isEmpty($(collection))", false );
		parseAndAssert( "isEmpty($(emptyCollection))", true );
		parseAndAssert( "isEmpty({'a', 'b', 'c'})", false );
		parseAndAssert( "isEmpty({})", true );
	}

	@Test
	public void isNotEmptyAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "array", new String[] { "a", "b", "c" } )
		                                                             .put( "emptyArray", new String[0] )
		                                                             .put( "collection", Arrays.asList( "a", "b", "c" ) )
		                                                             .put( "emptyCollection", Collections.emptyList() ) );
		parseAndAssert( "isNotEmpty($(array))", true );
		parseAndAssert( "isNotEmpty($(emptyArray))", false );
		parseAndAssert( "isNotEmpty($(collection))", true );
		parseAndAssert( "isNotEmpty($(emptyCollection))", false );
		parseAndAssert( "isNotEmpty({'a', 'b', 'c'})", true );
		parseAndAssert( "isNotEmpty({})", false );
	}

	@Test
	public void andAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", true )
		                                                             .put( "y", false )
		                                                             .put( "z", true ) );
		parseAndAssert( "and({true, true, true})", true );
		parseAndAssert( "and({true, false, true})", false );
		parseAndAssert( "and({false, false, false})", false );
		parseAndAssert( "and({$(x), $(y), $(z)})", false );
		parseAndAssert( "and({$(x), $(z)})", true );
		parseAndAssert( "and({$(y)})", false );
	}

	@Test
	public void orAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", true )
		                                                             .put( "y", false )
		                                                             .put( "z", true ) );
		parseAndAssert( "or({true, true, true})", true );
		parseAndAssert( "or({true, false, true})", true );
		parseAndAssert( "or({false, false, false})", false );
		parseAndAssert( "or({$(x), $(y), $(z)})", true );
		parseAndAssert( "or({$(x), $(z)})", true );
		parseAndAssert( "or({$(y)})", false );
	}

	@Test
	public void concatAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", "john" )
		                                                             .put( "y", "joe" )
		                                                             .put( "z", "jane" ) );
		parseAndAssert( "concat({'a', 'b', 'c'})", "abc" );
		parseAndAssert( "concat({'abc'})", "abc" );
		parseAndAssert( "concat({$(x), $(y), $(z)})", "johnjoejane" );
	}

	@Test
	public void joinAccessor() {
		fieldContext = fields( ImmutableMap.<String, Object>builder().put( "x", "john" )
		                                                             .put( "y", "joe" )
		                                                             .put( "z", "jane" ) );
		parseAndAssert( "join('', {'a', 'b', 'c'})", "abc" );
		parseAndAssert( "join('/', {'a', 'b', 'c'})", "a/b/c" );
		parseAndAssert( "join(' - ',{$(x), $(y), $(z)})", "john - joe - jane" );
	}

	@Test
	public void operatorSupportsArray() {
		fieldContext = new DynamicDocumentFieldFormulaContext( null, null );
		parseAndAssert( "count('hello world'.split(''))", 11 );
		parseAndAssert( "countNull({'a',2,null,4})", "1" );
		parseAndAssert( "countNonNull({'a',2,null,4})", "3" );
		parseAndAssert( "sum({1,2,3,4})", 10 );
		parseAndAssert( "min({-4.6,-4.5,1,2,3})", Double.valueOf( "-4.6" ) );
		parseAndAssert( "max({1,2.2,3.1,3.2})", 3.2 );
		parseAndAssert( "max({948348942, 1,2,3})", "948348942" );
		parseAndAssert( "avg({1,4})", 2.5 );
		parseAndAssert( "sum(1,2,3,4)", 10.0 );
		parseAndAssert( "first(1,4)", 1 );
		parseAndAssert( "first({1,4})", 1 );
		parseAndAssert( "last(1,4)", 4 );
		parseAndAssert( "last({1,4})", 4 );
	}

	private void parseAndAssert( String expression, Object expectedValue ) {
		val parsedExpression = parser.parseExpression( expression, TypeDescriptor.valueOf( expectedValue.getClass() ) );
		assertThat( parsedExpression.apply( evaluationContext, fieldContext ) ).isEqualTo( expectedValue );
	}

	private DynamicDocumentFieldFormulaContext fields( ImmutableMap.Builder<String, Object> fields ) {
		DynamicDocumentFieldFormulaContext fieldContext = mock( DynamicDocumentFieldFormulaContext.class );

		for ( Map.Entry<String, Object> entry : fields.build().entrySet() ) {
			DynamicDocumentFieldAccessor field = mock( DynamicDocumentFieldAccessor.class );
			when( field.get() ).thenReturn( entry.getValue() );
			when( fieldContext.field( entry.getKey() ) ).thenReturn( field );
		}

		return fieldContext;
	}
}
