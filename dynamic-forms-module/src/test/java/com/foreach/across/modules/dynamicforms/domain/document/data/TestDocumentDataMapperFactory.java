/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import lombok.val;
import org.junit.Test;

import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class TestDocumentDataMapperFactory
{
	private DynamicDocumentDataMapperFactory mapperFactory = new DynamicDocumentDataMapperFactory();

	@Test
	public void illegalArgumentExceptionIfNoFactoryForFormat() {
		assertThatExceptionOfType( IllegalArgumentException.class )
				.isThrownBy( () -> mapperFactory.createMapper( DynamicDocumentDataMapper.JSON ) )
				.withMessage( "Unable to create a DynamicDocumentDataMapper for format 'JSON'" );
	}

	@Test
	@SuppressWarnings("unchecked")
	public void factoryMethodIsCalledForFormat() {
		Function<String, DynamicDocumentDataMapper> jsonFactory = mock( Function.class );
		Function<String, DynamicDocumentDataMapper> yamlFactory = mock( Function.class );
		mapperFactory.registerMapper( DynamicDocumentDataMapper.JSON, jsonFactory );
		mapperFactory.registerMapper( DynamicDocumentDataMapper.YAML, yamlFactory );

		val jsonMapper = mock( DynamicDocumentDataMapper.class );
		val yamlMapper = mock( DynamicDocumentDataMapper.class );
		when( jsonFactory.apply( DynamicDocumentDataMapper.JSON ) ).thenReturn( jsonMapper );
		when( yamlFactory.apply( DynamicDocumentDataMapper.YAML ) ).thenReturn( yamlMapper );

		assertThat( mapperFactory.createMapper( DynamicDocumentDataMapper.JSON ) ).isSameAs( jsonMapper );
		assertThat( mapperFactory.createMapper( DynamicDocumentDataMapper.YAML ) ).isSameAs( yamlMapper );
	}

	@Test
	@SuppressWarnings("unchecked")
	public void illegalArgumentExceptionIfFactoryReturnsNull() {
		Function<String, DynamicDocumentDataMapper> factory = mock( Function.class );
		mapperFactory.registerMapper( DynamicDocumentDataMapper.YAML, factory );

		assertThatExceptionOfType( IllegalArgumentException.class )
				.isThrownBy( () -> mapperFactory.createMapper( DynamicDocumentDataMapper.YAML ) )
				.withMessage( "Unable to create a DynamicDocumentDataMapper for format 'YAML'" );

		verify( factory ).apply( DynamicDocumentDataMapper.YAML );
	}
}
