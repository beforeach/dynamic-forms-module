/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.view;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.yaml.snakeyaml.Yaml;

import static infrastructure.DynamicFormsAssertions.assertThat;

public class TestRawViewDefinitionReader
{
	@Test
	@SneakyThrows
	public void simpleDocument() {
		Yaml yaml = new Yaml();
		Object raw = yaml.load( new ClassPathResource( "yaml/definitions/view-simple.yml" ).getInputStream() );

		ObjectMapper objectMapper = new ObjectMapper();
		String convertedJson = objectMapper.writeValueAsString( raw );

		RawViewDefinition definition = objectMapper
				.reader()
				.forType( RawViewDefinition.class )
				.withRootName( "view-definition" )
				.readValue( convertedJson );

		assertThat( definition ).hasFieldOrPropertyWithValue( "name", "createView" );
		assertThat( definition ).hasFieldOrPropertyWithValue( "type", "list" );
		assertThat( definition.getProperties() ).containsExactly( "name", "#username", "#address.street" );

		RawViewDefinition.Filter filter = definition.getFilter();
		assertThat( filter ).isNotNull();
		assertThat( filter.getMode().getAdvanced() ).isEqualTo( true );
		assertThat( filter.getMode().getBasic() ).isEqualTo( true );
		assertThat( filter.getDefaultSelector() ).isEqualTo( DynamicViewDefinitionFilterSelector.MULTI );
		assertThat( filter.getDefaultQuery() ).isEqualTo( "order by name ASC" );
		assertThat( filter.getBasePredicate() ).isEqualTo( "domain = 1" );
		assertThat( filter.getFields() ).hasSize( 4 );
		assertThat( filter.getFields() ).extracting( "id" ).containsExactly( "name", "title", "created", "people" );
		assertThat( filter.getFields() ).extracting( "selector" ).containsExactly(
				null,
				DynamicViewDefinitionFilterSelector.SINGLE,
				DynamicViewDefinitionFilterSelector.SINGLE,
				DynamicViewDefinitionFilterSelector.MULTI
		);
	}
}
