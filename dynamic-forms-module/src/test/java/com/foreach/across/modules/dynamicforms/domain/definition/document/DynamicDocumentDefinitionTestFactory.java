/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldFormulaContext;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidator;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import org.springframework.expression.EvaluationContext;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Factory for manually building dynamic definitions for testing.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class DynamicDocumentDefinitionTestFactory
{
	public static FieldDefinition fieldDefinition( String id, DynamicTypeDefinition typeDefinition ) {
		return new FieldDefinition( id, typeDefinition );
	}

	public static DynamicDocumentDefinition documentDefinition( String name, List<DynamicDocumentFieldDefinition> fields ) {
		return new DynamicDocumentDefinition( name, DynamicDefinitionId.fromString( "test-factory:document" ), fields );
	}

	public static class FieldDefinition extends DynamicDocumentFieldDefinition
	{
		FieldDefinition( String id, DynamicTypeDefinition typeDefinition ) {
			super( typeDefinition );
			setId( id );
		}

		@Override
		public FieldDefinition setCanonicalId( String canonicalId ) {
			return (FieldDefinition) super.setCanonicalId( canonicalId );
		}

		@Override
		public FieldDefinition setCollection( boolean collection ) {
			return (FieldDefinition) super.setCollection( collection );
		}

		@Override
		public FieldDefinition setMember( boolean member ) {
			return (FieldDefinition) super.setMember( member );
		}

		@Override
		public FieldDefinition setFields( List<DynamicDocumentFieldDefinition> fields ) {
			return (FieldDefinition) super.setFields( fields );
		}

		@Override
		public FieldDefinition setEntityPropertyDescriptor( EntityPropertyDescriptor entityPropertyDescriptor ) {
			return (FieldDefinition) super.setEntityPropertyDescriptor( entityPropertyDescriptor );
		}

		@Override
		public FieldDefinition setMemberFieldDefinition( DynamicDocumentFieldDefinition memberFieldDefinition ) {
			return (FieldDefinition) super.setMemberFieldDefinition( memberFieldDefinition );
		}

		@Override
		public FieldDefinition setExpression( BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> expression ) {
			return (FieldDefinition) super.setExpression( expression );
		}

		@Override
		public FieldDefinition setInitializerExpression( BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> expression ) {
			return (FieldDefinition) super.setInitializerExpression( expression );
		}

		@Override
		public FieldDefinition setValidators( Collection<DynamicDocumentFieldValidator> validators ) {
			return (FieldDefinition) super.setValidators( validators );
		}
	}
}
