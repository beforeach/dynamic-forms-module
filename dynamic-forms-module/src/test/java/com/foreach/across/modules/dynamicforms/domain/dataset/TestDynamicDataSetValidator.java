/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.dataset;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDataSetValidator
{
	@Mock
	private DynamicDefinitionDataSetRepository repository;

	@Mock
	private Errors errors;

	private DynamicDefinitionDataSetValidator validator;

	private DynamicDefinitionDataSet newEntity = new DynamicDefinitionDataSet();
	private DynamicDefinitionDataSet existingEntity = new DynamicDefinitionDataSet();

	private QDynamicDefinitionDataSet dataset = QDynamicDefinitionDataSet.dynamicDefinitionDataSet;

	@Before
	public void reset() {
		validator = new DynamicDefinitionDataSetValidator( repository );
		newEntity.setKey( "T2" );
		newEntity.setName( "TEST" );

		existingEntity.setId( 66L );
		existingEntity.setKey( "T2" );
		existingEntity.setName( "TEST" );
	}

	@Test
	public void newEntityWithSameNameAndWithSameKey() {
		when( repository.count( dataset.key.equalsIgnoreCase( "T2" ) ) ).thenReturn( 2L );
		when( repository.count( dataset.name.equalsIgnoreCase( "TEST" ) ) ).thenReturn( 1L );
		validator.validate( newEntity, errors );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void newEntityWithSameName() {
		when( repository.count( dataset.name.equalsIgnoreCase( "TEST" ) ) ).thenReturn( 2L );
		validator.validate( newEntity, errors );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void newEntityWithSameKey() {
		when( repository.count( dataset.key.equalsIgnoreCase( "T2" ) ) ).thenReturn( 2L );
		validator.validate( newEntity, errors );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void existingEntityWithSameNameAndWithSameKey() {
		when( repository.count( dataset.key.equalsIgnoreCase( "T2" ).and( dataset.id.ne( 66L ) ) ) ).thenReturn( 2L );
		when( repository.count( dataset.name.equalsIgnoreCase( "TEST" ).and( dataset.id.ne( 66L ) ) ) ).thenReturn( 1L );
		validator.validate( existingEntity, errors );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void existingEntityWithSameName() {
		when( repository.count( dataset.name.equalsIgnoreCase( "TEST" ).and( dataset.id.ne( 66L ) ) ) ).thenReturn( 1L );
		validator.validate( existingEntity, errors );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void existingEntityWithSameKey() {
		when( repository.count( dataset.key.equalsIgnoreCase( "T2" ).and( dataset.id.ne( 66L ) ) ) ).thenReturn( 2L );
		validator.validate( existingEntity, errors );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void validatesCorrectClass() {
		assertFalse( validator.supports( DynamicDefinition.class ) );
		assertTrue( validator.supports( DynamicDefinitionDataSet.class ) );
		assertTrue( validator.supports( SpecialDataSet.class ) );
	}

	@Test
	public void keyAllowsAlfaNumeric() {
		DynamicDefinitionDataSet dataSet = new DynamicDefinitionDataSet();
		dataSet.setName( "T" );
		dataSet.setKey( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" );
		validator.validate( dataSet, errors );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verifyNoMoreInteractions( errors );
	}

	private static class SpecialDataSet extends DynamicDefinitionDataSet
	{
	}
}
