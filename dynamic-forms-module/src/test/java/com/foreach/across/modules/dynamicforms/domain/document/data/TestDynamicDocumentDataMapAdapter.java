/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSet;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.GenericDynamicTypeDefinition;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.MethodInvocationException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory.documentDefinition;
import static com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionTestFactory.fieldDefinition;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDocumentDataMapAdapter
{
	@Mock
	private DynamicTypeDefinition typeDefinitionMock;

	private Map<String, Object> data;
	private DynamicDocumentData document;

	private DynamicDocumentDataAsMapAdapter adapter;

	@Before
	public void before() {
		when( typeDefinitionMock.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( String.class ) );

		DynamicDocumentFieldDefinition name = fieldDefinition( "name", typeDefinitionMock )
				.setInitializerExpression( ( c, f ) -> "<default>" );
		DynamicDocumentFieldDefinition street = fieldDefinition( "street", typeDefinitionMock );

		GenericDynamicTypeDefinition typeDefinition = mock( GenericDynamicTypeDefinition.class );
		when( typeDefinition.getTypeDescriptor() ).thenReturn( TypeDescriptor.valueOf( DynamicDataFieldSet.class ) );

		DynamicDocumentFieldDefinition address = fieldDefinition( "address", typeDefinition )
				.setFields( Collections.singletonList( street ) );

		DynamicDocumentDefinition documentDefinition = documentDefinition( "my-doc", Arrays.asList( name, address ) );

		document = new DynamicDocumentData( documentDefinition );
		data = document.getContent();

		adapter = document.asFlatMap();
	}

	@Test
	public void keySetBasedMethodsOperateOnLeafFields() {
		assertThat( adapter.isEmpty() ).isTrue();
		assertThat( adapter.keySet() ).isEmpty();
		assertThat( adapter.size() ).isEqualTo( 0 );
		assertThat( adapter.values() ).isEmpty();
		assertThat( adapter.entrySet() ).isEmpty();

		data.put( "name", "some name" );
		assertThat( adapter.isEmpty() ).isFalse();
		assertThat( adapter.keySet() ).hasSize( 1 ).contains( "name" );
		assertThat( adapter.size() ).isEqualTo( 1 );
		assertThat( adapter.values() ).hasSize( 1 ).contains( "some name" );

		data.put( "name", Collections.singletonMap( "not-a-sub-field", "field" ) );
		assertThat( adapter.isEmpty() ).isFalse();
		assertThat( adapter.keySet() ).hasSize( 1 ).contains( "name" );
		assertThat( adapter.size() ).isEqualTo( 1 );
		assertThat( adapter.values() ).hasSize( 1 ).contains( Collections.singletonMap( "not-a-sub-field", "field" ) );

		data.put( "address", Collections.singletonMap( "street", "my street" ) );
		assertThat( adapter.isEmpty() ).isFalse();
		assertThat( adapter.keySet() ).hasSize( 2 ).containsExactly( "name", "address.street" );
		assertThat( adapter.size() ).isEqualTo( 2 );
		assertThat( adapter.values() )
				.hasSize( 2 )
				.containsExactly( Collections.singletonMap( "not-a-sub-field", "field" ), "my street" );
		assertThat( adapter.entrySet() ).hasSize( 2 );

		adapter.clear();
		assertThat( adapter.isEmpty() ).isTrue();
		assertThat( data ).isEmpty();
	}

	@Test
	public void updatingThroughEntrySetIsPossible() {
		data.put( "name", "some name" );

		val entry = adapter.entrySet().iterator().next();
		assertThat( entry.getKey() ).isEqualTo( "name" );
		assertThat( entry.getValue() ).isEqualTo( "some name" );
		assertThat( entry.setValue( "new value" ) ).isEqualTo( "some name" );

		assertThat( entry.getValue() ).isEqualTo( "new value" );
		assertThat( data ).isEqualTo( Collections.singletonMap( "name", "new value" ) );
	}

	@Test
	public void rawExceptionsAreThrownIfNoBinderPrefix() {
		assertThatExceptionOfType( ClassCastException.class )
				.isThrownBy( () -> adapter.put( "name", 123L ) );
		assertThatExceptionOfType( DynamicDocumentDefinition.NoSuchFieldException.class )
				.isThrownBy( () -> adapter.put( "some-unknown-field", 123L ) );
	}

	@Test
	public void conversionServiceIsUsedWhenSet() {
		ConversionService conversionService = mock( ConversionService.class );
		when( conversionService.convert( 123L, TypeDescriptor.forObject( 123L ), TypeDescriptor.valueOf( String.class ) ) ).thenReturn( "converted" );

		adapter.setConversionService( conversionService );

		adapter.put( "name", 123L );
		assertThat( adapter.get( "name" ) ).isEqualTo( "converted" );
		assertThat( data.get( "name" ) ).isEqualTo( "converted" );
	}

	@Test
	public void exceptionsAreWrappedWhenBinderPrefixIsSpecified() {
		adapter.setBinderPrefix( "mydata" );

		assertThatExceptionOfType( ConversionNotSupportedException.class )
				.isThrownBy( () -> adapter.put( "name", 123L ) )
				.satisfies( e -> {
					assertThat( e.getPropertyName() ).isEqualTo( "mydata[name]" );
					assertThat( e.getRequiredType() ).isEqualTo( String.class );
				} );

		assertThatExceptionOfType( MethodInvocationException.class )
				.isThrownBy( () -> adapter.put( "some-unknown-field", 123L ) )
				.satisfies( e -> assertThat( e.getPropertyName() ).isEqualTo( "mydata[some-unknown-field]" ) );

		ConversionService conversionService = mock( ConversionService.class );
		when( conversionService.convert( 123L, TypeDescriptor.forObject( 123L ), TypeDescriptor.valueOf( String.class ) ) )
				.thenThrow( new ConversionFailedException( TypeDescriptor.forObject( 123L ), TypeDescriptor.valueOf( BigDecimal.class ), 123L, null ) );

		adapter.setConversionService( conversionService );
		assertThatExceptionOfType( ConversionNotSupportedException.class )
				.isThrownBy( () -> adapter.put( "name", 123L ) )
				.satisfies( e -> {
					assertThat( e.getPropertyName() ).isEqualTo( "mydata[name]" );
					assertThat( e.getRequiredType() ).isEqualTo( BigDecimal.class );
				} );
	}

	@Test
	public void containsKey() {
		assertThat( adapter.containsKey( "name" ) ).isFalse();

		data.put( "name", "some name" );
		assertThat( adapter.containsKey( "name" ) ).isTrue();

		data.put( "address", Collections.singletonMap( "street", "my street" ) );
		assertThat( adapter.containsKey( "address" ) ).isTrue();
		assertThat( adapter.containsKey( "address.street" ) ).isTrue();
	}

	@Test
	public void containsValue() {
		assertThat( adapter.containsValue( "my street" ) ).isFalse();

		data.put( "address", Collections.singletonMap( "street", "my street" ) );
		assertThat( adapter.containsValue( "my street" ) ).isTrue();
	}

	@Test
	public void getPutRemove() {
		assertThat( adapter.get( "name" ) ).isNull();

		assertThat( adapter.put( "name", "some name" ) ).isNull();
		assertThat( data.get( "name" ) ).isEqualTo( "some name" );
		assertThat( adapter.get( "name" ) ).isEqualTo( "some name" );

		assertThat( adapter.put( "name", "updated" ) ).isEqualTo( "some name" );
		assertThat( data.get( "name" ) ).isEqualTo( "updated" );
		assertThat( adapter.get( "name" ) ).isEqualTo( "updated" );

		assertThat( adapter.remove( "name" ) ).isEqualTo( "updated" );
		assertThat( data ).isEmpty();
		assertThat( adapter ).isEmpty();
	}

	@Test
	public void putAll() {
		Map<String, Object> flatValues = new HashMap<>();
		flatValues.put( "address.street", "my street" );
		flatValues.put( "name", "my name" );
		adapter.putAll( flatValues );

		assertThat( data )
				.hasSize( 2 )
				.containsEntry( "name", "my name" )
				.containsEntry( "address", Collections.singletonMap( "street", "my street" ) );
	}
}
