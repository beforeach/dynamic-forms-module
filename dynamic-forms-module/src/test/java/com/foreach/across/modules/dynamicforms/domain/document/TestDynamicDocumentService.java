/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionType;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypes;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@ExtendWith(MockitoExtension.class)
class TestDynamicDocumentService
{
	private final DynamicDefinitionType DOCUMENT_TYPE = DynamicDefinitionType.builder().name( DynamicDefinitionTypes.DOCUMENT ).build();

	@Mock
	private AutowireCapableBeanFactory beanFactory;

	@Mock
	private DynamicDocumentWorkspace workspace;

	@InjectMocks
	private DynamicDocumentService documentService;

	@BeforeEach
	void before() {
		Mockito.lenient().when( beanFactory.createBean( DynamicDocumentWorkspace.class ) ).thenReturn( workspace );
	}

	@Test
	void onlyDocumentDefinitionsAreSupported() {
		val definition = mock( DynamicDefinition.class );
		when( definition.getType() ).thenReturn( DynamicDefinitionType.builder().name( DynamicDefinitionTypes.TYPE ).build() );

		assertThatExceptionOfType( IllegalArgumentException.class )
				.isThrownBy( () -> documentService.createDocumentWorkspace( definition ) );

		val version = mock( DynamicDefinitionVersion.class );
		assertThatExceptionOfType( IllegalArgumentException.class )
				.isThrownBy( () -> documentService.createDocumentWorkspace( version ) );

		when( version.getDefinition() ).thenReturn( definition );
		assertThatExceptionOfType( IllegalArgumentException.class )
				.isThrownBy( () -> documentService.createDocumentWorkspace( version ) );
	}

	@Test
	void workspaceFromDefinition() {
		val def = mock( DynamicDefinition.class );
		when( def.getType() ).thenReturn( DOCUMENT_TYPE );
		val defVersion = mock( DynamicDefinitionVersion.class );
		when( defVersion.getDefinition() ).thenReturn( def );
		when( def.getLatestVersion() ).thenReturn( defVersion );

		val ws = documentService.createDocumentWorkspace( def );
		assertThat( ws ).isSameAs( workspace );

		val inOrder = Mockito.inOrder( ws );
		inOrder.verify( ws ).setDefinition( def );
		inOrder.verify( ws ).setDefinitionVersion( defVersion );
		inOrder.verify( ws ).initialize();
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	void workspaceFromDefinitionVersion() {
		val def = mock( DynamicDefinition.class );
		when( def.getType() ).thenReturn( DOCUMENT_TYPE );
		val defVersion = mock( DynamicDefinitionVersion.class );
		when( defVersion.getDefinition() ).thenReturn( def );

		val ws = documentService.createDocumentWorkspace( defVersion );
		assertThat( ws ).isSameAs( workspace );

		val inOrder = Mockito.inOrder( ws );
		inOrder.verify( ws ).setDefinition( def );
		inOrder.verify( ws ).setDefinitionVersion( defVersion );
		inOrder.verify( ws ).initialize();
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	void workspaceFromDocumentWithoutLatestVersion() {
		val def = mock( DynamicDefinition.class );
		val defVersion = mock( DynamicDefinitionVersion.class );
		when( def.getLatestVersion() ).thenReturn( defVersion );
		val doc = mock( DynamicDocument.class );
		when( doc.getType() ).thenReturn( def );

		val ws = documentService.createDocumentWorkspace( doc );
		assertThat( ws ).isSameAs( workspace );

		val inOrder = Mockito.inOrder( ws );
		inOrder.verify( ws ).setDefinition( def );
		inOrder.verify( ws ).setDefinitionVersion( defVersion );
		inOrder.verify( ws ).setDocument( doc );
		inOrder.verify( ws ).initialize();
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	void workspaceFromDocumentWithLatestVersion() {
		val def = mock( DynamicDefinition.class );
		val doc = mock( DynamicDocument.class );
		when( doc.getType() ).thenReturn( def );
		val docVersion = mock( DynamicDocumentVersion.class );
		val docDefVersion = mock( DynamicDefinitionVersion.class );
		when( doc.getLatestVersion() ).thenReturn( docVersion );
		when( docVersion.getDefinitionVersion() ).thenReturn( docDefVersion );

		val ws = documentService.createDocumentWorkspace( doc );
		assertThat( ws ).isSameAs( workspace );

		val inOrder = Mockito.inOrder( ws );
		inOrder.verify( ws ).setDefinition( def );
		inOrder.verify( ws ).setDefinitionVersion( docDefVersion );
		inOrder.verify( ws ).setDocument( doc );
		inOrder.verify( ws ).setDocumentVersion( docVersion );
		inOrder.verify( ws ).initialize();
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	void workspaceFromDocumentVersion() {
		val def = mock( DynamicDefinition.class );
		val doc = mock( DynamicDocument.class );
		when( doc.getType() ).thenReturn( def );
		val docVersion = mock( DynamicDocumentVersion.class );
		when( docVersion.getDocument() ).thenReturn( doc );

		val docDefVersion = mock( DynamicDefinitionVersion.class );
		when( docVersion.getDefinitionVersion() ).thenReturn( docDefVersion );

		val ws = documentService.createDocumentWorkspace( docVersion );
		assertThat( ws ).isSameAs( workspace );

		val inOrder = Mockito.inOrder( ws );
		inOrder.verify( ws ).setDefinition( def );
		inOrder.verify( ws ).setDefinitionVersion( docDefVersion );
		inOrder.verify( ws ).setDocument( doc );
		inOrder.verify( ws ).setDocumentVersion( docVersion );
		inOrder.verify( ws ).initialize();
		inOrder.verifyNoMoreInteractions();
	}

}
