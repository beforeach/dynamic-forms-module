/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.Field;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.bootstrapui.elements.ViewElementFieldset;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestFieldsetDynamicTypeDefinitionBuilder
{
	@Mock
	private DynamicDocumentFieldDefinition fieldDefinition;

	private DynamicDataFieldSetTemplateRegistry fieldsetTemplateRegistry = new DynamicDataFieldSetTemplateRegistry();

	private FieldsetDynamicTypeDefinitionBuilder builder;

	@Before
	public void setUp() {
		builder = new FieldsetDynamicTypeDefinitionBuilder( fieldsetTemplateRegistry );
	}

	@Test
	public void accepts() {
		assertThat( builder.accepts( field( "something" ) ) ).isFalse();
		assertThat( builder.accepts( field( "fieldset" ) ) ).isTrue();
		assertThat( builder.accepts( field( "fieldset[]" ) ) ).isTrue();
	}

	@Test
	public void fieldsetType() {
		Field field = field( "fieldset", field( "number" ), field( "string" ) );
		field.setAttributes( Collections.singletonMap( "template", "fieldset" ) );
		val typeDef = builder.buildTypeDefinition( null, field );
		assertThat( typeDef )
				.isNotNull()
				.isInstanceOf( GenericDynamicTypeDefinition.class );
		assertThat( typeDef.getTypeDescriptor() )
				.isEqualTo( DynamicDataFieldSet.TYPE_DESCRIPTOR );
		assertThat( typeDef.getTypeName() ).isEqualTo( "fieldset" );

		DynamicDocumentFieldDefinition myNumber = mock( DynamicDocumentFieldDefinition.class );
		when( myNumber.getCanonicalId() ).thenReturn( "myFieldSet.myNumber" );
		DynamicDocumentFieldDefinition myString = mock( DynamicDocumentFieldDefinition.class );
		when( myString.getCanonicalId() ).thenReturn( "myFieldSet.myString" );
		when( fieldDefinition.getFields() ).thenReturn( Arrays.asList( myNumber, myString ) );

		EntityPropertyDescriptorBuilder propBuilder = spy( EntityPropertyDescriptor.builder( "myprop" ) );
		typeDef.customizePropertyDescriptor( fieldDefinition, propBuilder );

		verify( propBuilder ).viewElementType( ViewElementMode.FORM_WRITE, ViewElementFieldset.ELEMENT_TYPE );
		verify( propBuilder ).viewElementType( ViewElementMode.FORM_READ, ViewElementFieldset.ELEMENT_TYPE );
		verify( propBuilder ).attribute( ViewElementFieldset.TEMPLATE, ViewElementFieldset.TEMPLATE_FIELDSET );

		val prop = propBuilder.build();
		val selector = prop.getAttribute( EntityAttributes.FIELDSET_PROPERTY_SELECTOR, EntityPropertySelector.class );
		assertThat( selector ).isEqualTo( EntityPropertySelector.of( "field:myFieldSet.myNumber", "field:myFieldSet.myString" ) );
	}

	@Test
	public void exceptionIfCollectionButNoMember() {
		assertThatExceptionOfType( DynamicDocumentDefinition.IllegalFieldConfigurationException.class )
				.isThrownBy(
						() -> builder.buildTypeDefinition( null,
						                                   Field.builder().id( "myfield" )
						                                        .type( "fieldset[]" )
						                                        .build() )
				)
				.withMessage( "Illegal configuration for field 'myfield': a fieldset collection must have a 'member' fieldset specification" );
	}

	@Test
	public void exceptionIfCollectionAndFieldsSpecified() {
		assertThatExceptionOfType( DynamicDocumentDefinition.IllegalFieldConfigurationException.class )
				.isThrownBy(
						() -> builder.buildTypeDefinition( null,
						                                   Field.builder().id( "myfield" )
						                                        .type( "fieldset[]" )
						                                        .member( Field.builder().field( Field.builder().id( "member-field" ).build() ).build() )
						                                        .field( Field.builder().id( "child" ).build() )
						                                        .build() )
				)
				.withMessage(
						"Illegal configuration for field 'myfield': a fieldset collection may not have any direct 'fields', use the 'member' fieldset specification instead" );
	}

	@Test
	public void exceptionIfNoFields() {
		assertThatExceptionOfType( DynamicDocumentDefinition.IllegalFieldConfigurationException.class )
				.isThrownBy( () -> builder.buildTypeDefinition( null, Field.builder().id( "myfield" ).type( "fieldset" ).build() ) )
				.withMessage( "Illegal configuration for field 'myfield': a singular fieldset must have one or more 'fields' specified" );

	}

	private Field field( String type, Field... children ) {
		return Field.builder().id( "my-" + type ).type( type ).fields( Arrays.asList( children ) ).build();
	}
}
