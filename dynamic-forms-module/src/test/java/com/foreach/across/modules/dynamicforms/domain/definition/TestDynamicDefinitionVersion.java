/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * @author Steven Gentens
 * @since 0.01
 */
public class TestDynamicDefinitionVersion
{
	@Test
	public void defaultValues() {
		DynamicDefinitionVersion definitionVersion = new DynamicDefinitionVersion();
		verifyDefaultValues( definitionVersion );
		verifyDefaultValues( DynamicDefinitionVersion.builder().build() );
		verifyDefaultValues( definitionVersion.toDto() );
		verifyDefaultValues( definitionVersion.toBuilder().build() );
	}

	private void verifyDefaultValues( DynamicDefinitionVersion definitionVersion ) {
		assertNull( definitionVersion.getId() );
		assertNull( definitionVersion.getDefinition() );
		assertNull( definitionVersion.getDefinitionContent() );
		assertNull( definitionVersion.getVersion() );
		assertNull( definitionVersion.getRemarks() );
		assertFalse( definitionVersion.isPublished() );
	}

	@Test
	public void builderSemantics() {
		Date timestamp = new Date();

		DynamicDefinitionVersion definitionVersion = DynamicDefinitionVersion.builder()
		                                                                     .version( "2" )
		                                                                     .definitionContent( "{'name':'string'}" )
		                                                                     .createdBy( "john" )
		                                                                     .createdDate( timestamp )
		                                                                     .lastModifiedBy( "josh" )
		                                                                     .build();

		assertNull( definitionVersion.getId() );
		assertEquals( "{'name':'string'}", definitionVersion.getDefinitionContent() );
		assertEquals( "2", definitionVersion.getVersion() );
		assertEquals( "john", definitionVersion.getCreatedBy() );
		assertEquals( timestamp, definitionVersion.getCreatedDate() );
		assertEquals( "josh", definitionVersion.getLastModifiedBy() );
		assertNull( definitionVersion.getRemarks() );
		assertNull( definitionVersion.getLastModifiedDate() );
		assertFalse( definitionVersion.isPublished() );

		DynamicDefinition dynamicDefinition = DynamicDefinition.builder().id( -1L ).build();

		DynamicDefinitionVersion other = definitionVersion.toBuilder()
		                                                  .definition( dynamicDefinition )
		                                                  .remarks( "my remarks" )
		                                                  .published( true )
		                                                  .id( 333L )
		                                                  .lastModifiedDate( timestamp )
		                                                  .build();
		assertNotSame( definitionVersion, other );

		assertEquals( Long.valueOf( 333L ), other.getId() );
		assertEquals( "{'name':'string'}", other.getDefinitionContent() );
		assertEquals( "john", other.getCreatedBy() );
		assertEquals( timestamp, other.getCreatedDate() );
		assertEquals( "josh", other.getLastModifiedBy() );
		assertEquals( "my remarks", other.getRemarks() );
		assertEquals( timestamp, other.getLastModifiedDate() );
		assertTrue( other.isPublished() );
		assertEquals( dynamicDefinition, other.getDefinition() );
	}
}
