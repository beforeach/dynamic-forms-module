/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.validation.validators;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentFieldValue;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import java.util.Collections;

import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestRequiredFieldValidator
{
	@Mock
	private Errors errors;

	@Mock
	private DynamicDocumentFieldDefinition fieldDefinition;

	private RequiredFieldValidator validator = new RequiredFieldValidator( true );

	@Test
	public void valueMustNotBeNull() {
		validateRejected( null );
	}

	@Test
	public void stringMustNotBeEmpty() {
		validateRejected( "" );
	}

	@Test
	public void arrayMustNotBeEmpty() {
		validateRejected( new int[0] );
	}

	@Test
	public void collectionMustNotBeEmpty() {
		validateRejected( Collections.emptyList() );
	}

	@Test
	public void validValues() {
		validate( 123L );
		validate( "dsjkfldsj" );
		validate( new int[] { 1 } );
		validate( Collections.singleton( null ) );
	}

	@Test
	public void entityPropertyDescriptorModifiedOnlyIfIndicateIsTrue() {
		val builder = mock( EntityPropertyDescriptorBuilder.class );

		validator = new RequiredFieldValidator( false );
		validator.customizePropertyDescriptor( fieldDefinition, builder );
		verifyZeroInteractions( builder );

		validator = new RequiredFieldValidator( true );
		validator.customizePropertyDescriptor( fieldDefinition, builder );
		verify( builder ).attribute( EntityAttributes.PROPERTY_REQUIRED, true );
	}

	private void validateRejected( Object value ) {
		DynamicDocumentFieldValue v = new DynamicDocumentFieldValue( null, "my.field", "my.field", null, value );
		reset( errors );
		validator.validate( v, errors );
		verify( errors ).rejectValue( "my.field", "required" );
	}

	private void validate( Object value ) {
		DynamicDocumentFieldValue v = new DynamicDocumentFieldValue( null, "my.field", "my.field", null, value );
		reset( errors );
		validator.validate( v, errors );
		verifyZeroInteractions( errors );
	}
}
