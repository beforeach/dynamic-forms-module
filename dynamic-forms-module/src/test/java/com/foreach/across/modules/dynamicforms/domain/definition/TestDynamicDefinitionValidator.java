/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestDynamicDefinitionValidator
{
	@Mock
	private DynamicDefinitionRepository repository;

	@Mock
	private Errors errors;

	private DynamicDefinitionValidator validator;

	private DynamicDefinition newEntity = new DynamicDefinition();
	private DynamicDefinition existingEntity = new DynamicDefinition();
	private DynamicDefinitionDataSet newEntityDataSet = new DynamicDefinitionDataSet();
	private DynamicDefinitionDataSet existingEntityDataSet = new DynamicDefinitionDataSet();
	private DynamicDefinitionType newDefinitionType = new DynamicDefinitionType();
	private DynamicDefinitionType existingDefinitionType = new DynamicDefinitionType();

	private QDynamicDefinition definition = QDynamicDefinition.dynamicDefinition;

	@Before
	public void reset() {
		validator = new DynamicDefinitionValidator( repository );

		existingEntityDataSet.setId( 500L );

		newEntity.setKey( "T2" );
		newEntity.setName( "TEST" );
		newEntity.setDataSet( newEntityDataSet );
		newEntity.setType( newDefinitionType );

		existingEntity.setId( 66L );
		existingEntity.setKey( "T2" );
		existingEntity.setName( "TEST" );
		existingEntity.setDataSet( existingEntityDataSet );
		existingEntity.setType( existingDefinitionType );
	}

	@Test
	public void newEntityWithSameNameAndWithSameKey() {
		when( repository.count( definition.key.equalsIgnoreCase( "T2" ).and( definition.parent.isNull() ).and( definition.dataSet.eq( newEntityDataSet ) )
		                                      .and( definition.type.eq( newDefinitionType ) ) ) ).thenReturn( 2L );
		when( repository.count( definition.name.equalsIgnoreCase( "TEST" ).and( definition.parent.isNull() )
		                                       .and( definition.dataSet.eq( newEntityDataSet ) ) ) ).thenReturn( 1L );
		validator.validate( newEntity, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void newEntityWithSameName() {
		when( repository.count( definition.name.equalsIgnoreCase( "TEST" ).and( definition.parent.isNull() )
		                                       .and( definition.dataSet.eq( newEntityDataSet ) ) ) ).thenReturn( 2L );
		validator.validate( newEntity, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void newEntityWithSameKey() {
		when( repository.count( definition.key.equalsIgnoreCase( "T2" ).and( definition.parent.isNull() ).and( definition.dataSet.eq( newEntityDataSet ) )
		                                      .and( definition.type.eq( newDefinitionType ) ) ) ).thenReturn( 2L );
		validator.validate( newEntity, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void existingEntityWithSameNameAndWithSameKey() {
		when( repository.count( definition.key.equalsIgnoreCase( "T2" ).and( definition.parent.isNull() ).and( definition.dataSet.eq( existingEntityDataSet ) )
		                                      .and( definition.type.eq( existingDefinitionType ) ).and( definition.id.ne( 66L ) ) ) ).thenReturn( 2L );
		when( repository.count( definition.name.equalsIgnoreCase( "TEST" ).and( definition.parent.isNull() )
		                                       .and( definition.dataSet.eq( existingEntityDataSet ) )
		                                       .and( definition.id.ne( 66L ) ) ) ).thenReturn( 1L );
		validator.validate( existingEntity, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void existingEntityWithSameName() {
		when( repository.count( definition.name.equalsIgnoreCase( "TEST" ).and( definition.parent.isNull() )
		                                       .and( definition.dataSet.eq( existingEntityDataSet ) )
		                                       .and( definition.id.ne( 66L ) ) ) ).thenReturn( 1L );
		validator.validate( existingEntity, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void existingEntityWithSameKey() {
		when( repository.count( definition.key.equalsIgnoreCase( "T2" ).and( definition.parent.isNull() ).and( definition.dataSet.eq( existingEntityDataSet ) )
		                                      .and( definition.type.eq( existingDefinitionType ) ).and( definition.id.ne( 66L ) ) ) ).thenReturn( 2L );
		validator.validate( existingEntity, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void validatesCorrectClass() {
		assertFalse( validator.supports( DynamicDefinitionDataSet.class ) );
		assertTrue( validator.supports( DynamicDefinition.class ) );
		assertTrue( validator.supports( SpecialDefinition.class ) );
	}

	@Test
	public void keyAllowsAlfaNumeric() {
		DynamicDefinition definition = new DynamicDefinition();
		definition.setName( "T" );
		definition.setKey( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" );
		definition.setDataSet( new DynamicDefinitionDataSet() );
		definition.setType( new DynamicDefinitionType() );
		validator.validate( definition, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void nameUniqueWithinParent() {
		DynamicDefinition parent = DynamicDefinition.builder()
		                                            .name( "parent" )
		                                            .key( "parent" )
		                                            .dataSet( existingEntityDataSet )
		                                            .type( new DynamicDefinitionType() )
		                                            .build();
		when( repository.count( definition.name.equalsIgnoreCase( "TEST" ).and( definition.parent.eq( parent ) ) ) ).thenReturn( 1L );
		DynamicDefinition entity = newEntity.toBuilder()
		                                    .parent( parent )
		                                    .build();
		validator.validate( entity, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "name", "alreadyExists" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void keyUniqueWithinParent() {
		DynamicDefinition parent = DynamicDefinition.builder()
		                                            .name( "parent" )
		                                            .key( "parent" )
		                                            .dataSet( existingEntityDataSet )
		                                            .type( new DynamicDefinitionType() )
		                                            .build();
		when( repository.count( definition.key.equalsIgnoreCase( "T2" ).and( definition.parent.eq( parent ) )
		                                      .and( definition.type.eq( newDefinitionType ) ) ) ).thenReturn( 1L );
		DynamicDefinition entity = newEntity.toBuilder()
		                                    .parent( parent )
		                                    .build();
		validator.validate( entity, errors );
		verify( errors ).hasFieldErrors( "parent" );
		verify( errors ).hasFieldErrors( "name" );
		verify( errors ).hasFieldErrors( "key" );
		verify( errors ).rejectValue( "key", "alreadyExists" );
		verify( errors, times( 2 ) ).hasFieldErrors( "dataSet" );
		verify( errors ).hasFieldErrors( "type" );
		verifyNoMoreInteractions( errors );
	}

	@Test
	public void entityWithCyclicDependency() {
		DynamicDefinition entity = DynamicDefinition.builder()
		                                            .name( "entity" )
		                                            .key( "entityt" )
		                                            .dataSet( existingEntityDataSet )
		                                            .type( new DynamicDefinitionType() )
		                                            .id( 15L )
		                                            .build();
		DynamicDefinition parent2 = DynamicDefinition.builder()
		                                             .name( "parent2" )
		                                             .key( "parent2" )
		                                             .dataSet( existingEntityDataSet )
		                                             .type( new DynamicDefinitionType() )
		                                             .id( 222L )
		                                             .parent( entity )
		                                             .build();
		DynamicDefinition parent1 = DynamicDefinition.builder()
		                                             .name( "parent1" )
		                                             .key( "parent2" )
		                                             .dataSet( existingEntityDataSet )
		                                             .type( new DynamicDefinitionType() )
		                                             .id( 111L )
		                                             .parent( parent2 )
		                                             .build();

		entity.setParent( parent1 );
		validator.validate( entity, errors );
		verify( errors ).rejectValue( "parent", "cyclicDependency" );
	}

	@Test
	public void newEntityCantHaveCyclicDependency() {
		DynamicDefinition entity = DynamicDefinition.builder()
		                                            .name( "entity" )
		                                            .key( "entityt" )
		                                            .dataSet( existingEntityDataSet )
		                                            .type( new DynamicDefinitionType() )
		                                            .build();
		validator.validate( entity, errors );
		verify( errors, times( 0 ) ).rejectValue( "parent", "cyclicDependency" );
	}

	private static class SpecialDefinition extends DynamicDefinition
	{
	}
}
