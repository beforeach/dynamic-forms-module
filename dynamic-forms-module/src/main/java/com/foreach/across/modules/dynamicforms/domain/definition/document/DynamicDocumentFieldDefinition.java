/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldFormulaContext;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidator;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.EvaluationContext;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Represents a single field in a document.
 */
@Setter(AccessLevel.PACKAGE)
@Accessors(chain = true)
public class DynamicDocumentFieldDefinition
{
	public static final String DESCRIPTOR_PREFIX = "field:";
	public static final String INDEXER = EntityPropertyRegistry.INDEXER;

	/**
	 * -- GETTER --
	 * Id of the field.
	 */
	@Getter
	private String id;

	/**
	 * -- GETTER --
	 * Canonical id (globally unique & fully qualified) of the field.
	 */
	@Getter
	private String canonicalId;

	/**
	 * -- GETTER --
	 * Descriptive label of the field.
	 */
	@Getter
	@NonNull
	private String label;

	/**
	 * -- GETTER --
	 * The actual type information for this field.
	 */
	@Getter
	@Setter
	private DynamicTypeDefinition typeDefinition;

	/**
	 * -- GETTER --
	 * In case of a collection field will hold a reference to the member field representing a collection item.
	 */
	@Setter
	@Getter
	private DynamicDocumentFieldDefinition memberFieldDefinition;

	/**
	 * -- GETTER --
	 * In case of a member field will hold a reference to the collection field the member belongs to.
	 */
	@Getter
	@Setter
	private DynamicDocumentFieldDefinition collectionFieldDefinition;

	/**
	 * -- GETTER --
	 * Sub-fields, in case of a fieldset-like type.
	 */
	@Getter
	@NonNull
	private List<DynamicDocumentFieldDefinition> fields = Collections.emptyList();

	/**
	 * -- GETTER --
	 * The list of validators that should be applied to this field.
	 */
	@Getter
	@NonNull
	private Collection<DynamicDocumentFieldValidator> validators = Collections.emptyList();

	/**
	 * -- GETTER --
	 * The expression that should be used to calculate this fields' value.
	 * The presence of an expression means a field is a calculated field and can't be set manually.
	 */
	@Getter
	private BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> expression;

	/**
	 * -- GETTER --
	 * The expression that should be used to initialize a new field instance.
	 * This corresponds to the 'default value' of the field.
	 */
	@Getter
	private BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> initializerExpression;

	/**
	 * -- GETTER --
	 * Does this field represent a collection. This means there is another field that will return {@code true}
	 * for {@link #isMember()} and which is available under {@link #getMemberFieldDefinition()}.
	 */
	@Getter
	private boolean collection;

	/**
	 * -- GETTER --
	 * Is this field the member representation of a collection item. This means there is another field
	 * that will return {@code true} for {@link #isCollection()} and which is available under {@link #getCollectionFieldDefinition()}.
	 */
	@Getter
	private boolean member;

	@Getter
	private EntityPropertyDescriptor entityPropertyDescriptor;

	@Getter
	private Collection<LocalizedMessage> messages = Collections.emptyList();

	private RawDocumentDefinition.AbstractField rawFieldDefinition;

	DynamicDocumentFieldDefinition( @NonNull DynamicTypeDefinition typeDefinition ) {
		this.typeDefinition = typeDefinition;
	}

	DynamicDocumentFieldDefinition setId( @NonNull String id ) {
		this.id = id;
		if ( StringUtils.isEmpty( label ) ) {
			label = id;
		}
		return this;
	}

	DynamicDocumentFieldDefinition setRawFieldDefinition( @NonNull RawDocumentDefinition.AbstractField rawFieldDefinition ) {
		this.rawFieldDefinition = RawDocumentDefinition.Field.copyOf( rawFieldDefinition );
		return this;
	}

	/**
	 * @return {@code true} if this is a calculated field
	 */
	public boolean isCalculatedField() {
		return expression != null;
	}

	/**
	 * @return a raw definition version
	 */
	public RawDocumentDefinition.AbstractField asRawDefinition() {
		return RawDocumentDefinition.AbstractField.copyOf( rawFieldDefinition );
	}
}
