/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.interceptors;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersionRepository;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentRepository;
import com.foreach.across.modules.hibernate.aop.EntityInterceptorAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Responsible for building the cascading full-delete of a {@link DynamicDefinition}.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
class DynamicDefinitionDeleteInterceptor extends EntityInterceptorAdapter<DynamicDefinition>
{
	private final DynamicDefinitionRepository definitionRepository;
	private final DynamicDefinitionVersionRepository definitionVersionRepository;
	private final DynamicDocumentRepository documentRepository;

	@Override
	public boolean handles( Class<?> entityClass ) {
		return DynamicDefinition.class.isAssignableFrom( entityClass );
	}

	@Override
	public void beforeDelete( DynamicDefinition definition ) {
		documentRepository.deleteAll( documentRepository.findAllByType( definition ) );

		definition.setLatestVersion( null );
		definitionRepository.save( definition );
		definitionVersionRepository.deleteAll( definitionVersionRepository.findByDefinitionOrderByCreatedDateDesc( definition ) );
	}
}
