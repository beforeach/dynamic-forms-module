/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

/**
 * Represents a specific version for the content template of a {@link DynamicDefinition}.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Table(name = "dfm_definition_version")
@Getter
@Setter
@Entity
@NoArgsConstructor
public class DynamicDefinitionVersion extends SettableIdAuditableEntity<DynamicDefinitionVersion> implements Comparable<DynamicDefinitionVersion>
{
	@Id
	@GeneratedValue(generator = "seq_dfm_definition_version_id")
	@GenericGenerator(
			name = "seq_dfm_definition_version_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_dfm_definition_version_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "definition_id")
	private DynamicDefinition definition;

	@NotNull
	@Column(name = "definition_content")
	private String definitionContent;

	@Column(name = "version")
	@NotNull
	@Length(max = 30)
	private String version;

	@Column(name = "remarks")
	private String remarks;

	@Column(name = "published")
	private boolean published;

	@Builder(toBuilder = true)
	public DynamicDefinitionVersion( Long id,
	                                 DynamicDefinition definition,
	                                 String definitionContent,
	                                 String version,
	                                 String remarks,
	                                 boolean published,
	                                 @Builder.ObtainVia(method = "getCreatedBy") String createdBy,
	                                 @Builder.ObtainVia(method = "getCreatedDate") Date createdDate,
	                                 @Builder.ObtainVia(method = "getLastModifiedBy") String lastModifiedBy,
	                                 @Builder.ObtainVia(method = "getLastModifiedDate") Date lastModifiedDate ) {
		setId( id );
		setDefinition( definition );
		setDefinitionContent( definitionContent );
		setVersion( version );
		setRemarks( remarks );
		setPublished( published );
		setCreatedBy( createdBy );
		setCreatedDate( createdDate );
		setLastModifiedBy( lastModifiedBy );
		setLastModifiedDate( lastModifiedDate );
	}

	/**
	 * @return a unique identifier for this definition version. Can be {@code null} if no definition or version configured.
	 */
	public DynamicDefinitionId getDefinitionVersionId() {
		if ( definition != null ) {
			DynamicDefinitionId definitionId = definition.getDefinitionId();
			if ( definitionId != null ) {
				return definitionId.asDefinitionVersionId( version );
			}
		}

		return null;
	}

	/**
	 * @return a unique string identifier for this definition version. Can be {@code null} if no definition or version configured.
	 */
	public String getFullKey() {
		return Objects.toString( getDefinitionVersionId(), null );
	}

	@Override
	public String toString() {
		if ( definition != null ) {
			return getFullKey();
		}
		return super.toString();
	}

	@Override
	public int compareTo( DynamicDefinitionVersion other ) {
		Comparator<DynamicDefinitionVersion> definitionComparator
				= Comparator.comparing( DynamicDefinitionVersion::getDefinition, DynamicDefinition::compareTo );
		Comparator<DynamicDefinitionVersion> versionComparator = Comparator.comparing( DynamicDefinitionVersion::getVersion, StringUtils::compare );
		return definitionComparator.thenComparing( versionComparator ).compare( this, other );
	}
}
