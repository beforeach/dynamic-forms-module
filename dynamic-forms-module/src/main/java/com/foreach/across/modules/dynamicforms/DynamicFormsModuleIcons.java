/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms;

import com.foreach.across.modules.bootstrapui.elements.icons.IconSet;
import com.foreach.across.modules.bootstrapui.elements.icons.IconSetRegistry;
import com.foreach.across.modules.bootstrapui.elements.icons.SimpleIconSet;
import com.foreach.across.modules.web.ui.elements.HtmlViewElement;

import static com.foreach.across.modules.bootstrapui.BootstrapUiModuleIcons.ICON_SET_FONT_AWESOME_SOLID;

public class DynamicFormsModuleIcons
{
	public static final String ICON_SET = DynamicFormsModule.NAME;

	public static final DynamicFormsModuleIcons dynamicFormsModuleIcons = new DynamicFormsModuleIcons();

	public HtmlViewElement view() {
		return IconSet.iconSet( DynamicFormsModule.NAME ).icon( "view" );
	}

	public HtmlViewElement edit() {
		return IconSet.iconSet( DynamicFormsModule.NAME ).icon( "edit" );
	}

	public static void registerIconSet() {
		SimpleIconSet iconSet = new SimpleIconSet();
		iconSet.add( "view", ( imageName ) -> IconSet.iconSet( ICON_SET_FONT_AWESOME_SOLID ).icon( "eye" ) );
		iconSet.add( "edit", ( imageName ) -> IconSet.iconSet( ICON_SET_FONT_AWESOME_SOLID ).icon( "edit" ) );
		IconSetRegistry.addIconSet( ICON_SET, iconSet );
	}
}
