/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.AbstractMessageSource;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Custom {@link MessageSource} that looks up messages from a {@link Map},
 * with support for fallback message codes (applicable to all locales).
 * <p/>
 * Use {@link #withParentMessageSource(MessageSource)} if you want to create
 * a new message source that looks up in the map first, but delegates to a
 * parent source if message not found.
 *
 * @author Arne Vandamme
 * @see org.springframework.context.support.StaticMessageSource
 * @since 0.0.1
 */
class DynamicDocumentDefinitionMessageSource extends AbstractMessageSource
{
	private final Map<String, String> messages = new HashMap<String, String>();
	private final Map<String, MessageFormat> cachedMessageFormats = new HashMap<String, MessageFormat>();

	@Override
	protected String resolveCodeWithoutArguments( String code, Locale locale ) {
		String msg = this.messages.get( code + localeSuffix( locale.getLanguage(), locale.getCountry(), locale.getVariant() ) );

		if ( msg == null && locale.getVariant() != null ) {
			msg = this.messages.get( code + localeSuffix( locale.getLanguage(), locale.getCountry(), null ) );
		}

		if ( msg == null && locale.getCountry() != null ) {
			msg = this.messages.get( code + localeSuffix( locale.getLanguage(), null, null ) );
		}

		return msg != null ? msg : this.messages.get( code );
	}

	@Override
	protected MessageFormat resolveCode( String code, Locale locale ) {
		String msg = resolveCodeWithoutArguments( code, locale );

		if ( msg == null ) {
			return null;
		}

		synchronized ( this.cachedMessageFormats ) {
			String key = code + localeSuffix( locale );
			MessageFormat messageFormat = this.cachedMessageFormats.get( key );
			if ( messageFormat == null ) {
				messageFormat = createMessageFormat( msg, locale );
				this.cachedMessageFormats.put( key, messageFormat );
			}
			return messageFormat;
		}
	}

	/**
	 * Associate the given message with the given code for all locales
	 * that do not specify a specific value.
	 *
	 * @param code the lookup code
	 * @param msg  the message associated with this lookup code
	 */
	public void addMessage( @NonNull String code, @NonNull String msg ) {
		addMessage( code, null, msg );
	}

	/**
	 * Associate the given message with the given code.
	 *
	 * @param code   the lookup code
	 * @param locale the locale that the message should be found within
	 * @param msg    the message associated with this lookup code
	 */
	public void addMessage( @NonNull String code, Locale locale, @NonNull String msg ) {
		this.messages.put( code + localeSuffix( locale ), msg );
		if ( logger.isTraceEnabled() ) {
			logger.trace( "Added message [" + msg + "] for code [" + code + "] and Locale [" + locale + "]" );
		}
	}

	/**
	 * Associate the given message values with the given keys as codes.
	 * Do so for all locales that do not specify a specific value.
	 *
	 * @param messages the messages to register, with messages codes
	 *                 as keys and message texts as values
	 */
	public void addMessages( @NonNull Map<String, String> messages ) {
		addMessages( messages, null );
	}

	/**
	 * Associate the given message values with the given keys as codes.
	 *
	 * @param messages the messages to register, with messages codes
	 *                 as keys and message texts as values
	 * @param locale   the locale that the messages should be found within
	 */
	public void addMessages( @NonNull Map<String, String> messages, Locale locale ) {
		for ( Map.Entry<String, String> entry : messages.entrySet() ) {
			addMessage( entry.getKey(), locale, entry.getValue() );
		}
	}

	private String localeSuffix( Locale locale ) {
		return locale != null ? localeSuffix( locale.getLanguage(), locale.getCountry(), locale.getVariant() ) : "";
	}

	private String localeSuffix( String language, String country, String variant ) {
		return '_' + StringUtils.defaultString( language ) + '_' + StringUtils.defaultString( country ) + '_' + StringUtils.defaultString( variant );
	}

	@Override
	public String toString() {
		return getClass().getName() + ": " + this.messages;
	}

	/**
	 * Return a new {@link MessageSource} that will lookup in the static messages but fall back
	 * to the parent message source if there is no result.
	 *
	 * @param parent message source, can be {@code null}
	 * @return new instance
	 */
	public MessageSource withParentMessageSource( MessageSource parent ) {
		return new FallbackMessageSource( this, parent );
	}

	@RequiredArgsConstructor
	private static class FallbackMessageSource implements MessageSource
	{
		private static final String NOT_FOUND = Character.toString( (char) 0 );

		private final MessageSource initial;
		private final MessageSource fallback;

		@Override
		public String getMessage( String code, Object[] args, Locale locale ) throws NoSuchMessageException {
			String msg = findMessage( code, args, locale );
			if ( msg != null ) {
				return msg;
			}
			throw new NoSuchMessageException( code, locale );
		}

		@Override
		public String getMessage( MessageSourceResolvable resolvable, Locale locale ) throws NoSuchMessageException {
			for ( String code : resolvable.getCodes() ) {
				String msg = findMessage( code, resolvable.getArguments(), locale );
				if ( msg != null ) {
					return msg;
				}
			}
			if ( resolvable.getDefaultMessage() != null ) {
				return resolvable.getDefaultMessage();
			}
			throw new NoSuchMessageException( resolvable.getCodes()[0], locale );
		}

		@Override
		public String getMessage( String code, Object[] args, String defaultMessage, Locale locale ) {
			String msg = findMessage( code, args, locale );
			return msg != null ? msg : defaultMessage;
		}

		private String findMessage( String code, Object[] args, Locale locale ) {
			String msg = initial.getMessage( code, args, NOT_FOUND, locale );
			if ( NOT_FOUND.equals( msg ) && fallback != null ) {
				msg = fallback.getMessage( code, args, NOT_FOUND, locale );
			}

			return NOT_FOUND.equals( msg ) ? null : msg;
		}
	}
}
