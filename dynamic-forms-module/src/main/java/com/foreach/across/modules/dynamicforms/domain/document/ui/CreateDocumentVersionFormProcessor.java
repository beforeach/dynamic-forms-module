/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui;

import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.modules.bootstrapui.elements.Style;
import com.foreach.across.modules.bootstrapui.elements.builder.ColumnViewElementBuilder;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData;
import com.foreach.across.modules.entity.conditionals.ConditionalOnAdminWeb;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderService;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.processors.ExtensionViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.EntityViewPageHelper;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.web.EntityViewModel;
import com.foreach.across.modules.web.ui.DefaultViewElementBuilderContext;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.HtmlViewElements;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilder;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilderSupport;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;

import static com.foreach.across.modules.bootstrapui.utils.BootstrapElementUtils.prefixControlNames;

/**
 * Builds the actual document data form and creates a new version of the current document when saving.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
@Exposed
@ConditionalOnAdminWeb
public class CreateDocumentVersionFormProcessor extends ExtensionViewProcessorAdapter<DynamicDocumentWorkspace>
{
	private final EntityViewPageHelper entityViewPageHelper;
	private final EntityViewElementBuilderService viewElementBuilderService;
	private final DynamicDocumentService documentService;

	@Override
	@SneakyThrows
	protected DynamicDocumentWorkspace createExtension( EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder ) {
		dataBinder.setDisallowedFields( controlPrefix() + ".fields", controlPrefix() + ".dataBinderTarget" );

		val dynamicDocument = entityViewRequest.getEntityViewContext().getEntity( DynamicDocument.class );
		val workspace = documentService.createDocumentWorkspace( dynamicDocument );
		workspace.createNewVersion( true );
		workspace.setDataBinderTarget( controlPrefix() );

		return workspace;
	}

	@Override
	protected void validateExtension( DynamicDocumentWorkspace workspace, Errors errors, HttpMethod httpMethod, EntityViewRequest entityViewRequest ) {
		workspace.setDataBinderTarget( null );
		if ( HttpMethod.POST.equals( httpMethod ) ) {
			workspace.validate( errors );
		}
	}

	@Override
	protected void doPost( DynamicDocumentWorkspace workspace, BindingResult bindingResult, EntityView entityView, EntityViewRequest entityViewRequest ) {
		if ( !bindingResult.hasErrors() ) {
			workspace.save();

			entityViewPageHelper.addGlobalFeedbackAfterRedirect( entityViewRequest, Style.SUCCESS, "feedback.versionCreated" );

			entityView.setRedirectUrl(
					entityViewRequest.getEntityViewContext().getLinkBuilder().forInstance( entityViewRequest.getEntityViewContext().getEntity() ).updateView()
					                 .toUriString()
			);
		}
	}

	@Override
	protected void render( DynamicDocumentWorkspace extension,
	                       EntityViewRequest entityViewRequest,
	                       EntityView entityView,
	                       ContainerViewElementBuilderSupport<?, ?> containerBuilder,
	                       ViewElementBuilderMap builderMap,
	                       ViewElementBuilderContext builderContext ) {
		builderMap.get( SingleEntityFormViewProcessor.LEFT_COLUMN, ColumnViewElementBuilder.class )
		          .add( renderDocumentForm( extension.getFields(), builderContext ) );
	}

	public ViewElement renderDocumentForm( DynamicDocumentData document, ViewElementBuilderContext builderContext ) {
		ContainerViewElementBuilder formBuilder = HtmlViewElements.html.builders.container();

		document.getDocumentDefinition()
		        .getContent()
		        .forEach(
				        field ->
						        formBuilder.add(
								        viewElementBuilderService.createElementBuilder( field.getEntityPropertyDescriptor(), ViewElementMode.FORM_WRITE )
						        )
		        );

		val subContext = new DefaultViewElementBuilderContext( builderContext );
		subContext.setAttribute( EntityViewModel.ENTITY, document );
		subContext.setAttribute( MessageSource.class, document.getDocumentDefinition().createMessageSource( subContext.getAttribute( MessageSource.class ) ) );

		return formBuilder
				.postProcessor( prefixControlNames( controlPrefix() ) )
				.build( subContext );
	}
}
