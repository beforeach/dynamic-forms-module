/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.validation;

import com.foreach.across.core.annotations.RefreshableCollection;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Responsible for creating {@link DynamicDocumentFieldValidator} instances.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Service
@RequiredArgsConstructor
public class DynamicDocumentFieldValidatorFactory
{
	private Collection<DynamicDocumentFieldValidatorBuilder> validatorBuilders = Collections.emptyList();

	@RefreshableCollection(includeModuleInternals = true, incremental = true)
	public void setValidatorBuilders( Collection<DynamicDocumentFieldValidatorBuilder> validatorBuilders ) {
		this.validatorBuilders = validatorBuilders;
	}

	/**
	 * Create a new validator instance for a given field and validator settings.
	 *
	 * @param field             to which the validator is aded
	 * @param fieldType         type definition for the field
	 * @param validatorSettings configuration settings of the validator
	 * @return type definition (never {@code null})
	 */
	public DynamicDocumentFieldValidator createValidator( @NonNull RawDocumentDefinition.AbstractField field,
	                                                      @NonNull DynamicTypeDefinition fieldType,
	                                                      @NonNull Object validatorSettings ) {
		val settingsAsMap = convertToMap( validatorSettings );
		val builder = validatorBuilders.stream()
		                               .filter( b -> b.accepts( field, fieldType, settingsAsMap ) )
		                               .findFirst()
		                               .orElseThrow( () -> new IllegalArgumentException( "Unable to create a validator for: " + validatorSettings ) );

		return builder.buildValidator( field, fieldType, settingsAsMap );
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> convertToMap( Object validatorSettings ) {
		if ( validatorSettings instanceof Map ) {
			return (Map<String, Object>) validatorSettings;
		}
		if ( validatorSettings instanceof String ) {
			return Collections.singletonMap( (String) validatorSettings, true );
		}

		throw new IllegalArgumentException( "Validator settings must be either a single String or a Map with String keys" );
	}
}
