/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import org.springframework.core.convert.TypeDescriptor;

/**
 * Single field accessor, wraps a {@link DynamicDocumentFieldDefinition} together with a data accessor
 * for retrieving the actual value, setting or deleting it.
 */
public interface DynamicDocumentFieldAccessor<T>
{
	/**
	 * @return the unique path to the specific field in the document
	 */
	String getFieldPath();

	/**
	 * @return the full field definition for this field
	 */
	DynamicDocumentFieldDefinition getFieldDefinition();

	/**
	 * @return the field type descriptor, only values matching the given type will be accepted by {@link #set(Object)}
	 */
	TypeDescriptor getFieldType();

	/**
	 * Set the field value. Return any previously set value.
	 */
	Object set( Object value );

	/**
	 * Get the field value, don't create the default if the field does not yet exist.
	 */
	T get();

	/**
	 * Get the default value registered for this field.
	 * This does apply the default value nor make any modifications to the field itself.
	 * The default value can be retrieved even if {@link #exists()} is {@code false}.
	 */
	Object getDefaultValue();

	/**
	 * Get the field value and if the field does not yet exist,
	 * create and set the default value. Mainly used for intermediate paths.
	 */
	T getOrCreate();

	/**
	 * Delete the field (if possible). Return the deleted value.
	 */
	Object delete();

	/**
	 * Does the actual field already exist?
	 * It's possible that the field has a {@code null} value set, but this will return false
	 * if for example a parent field does not yet exist.
	 * <p/>
	 * Note that if there is no field definition, an accessor will never be created.
	 * So this method can be used to check if a field instance exists, but you can always
	 * be sure the field instance is creatable if it does not exist yet.
	 */
	boolean exists();

	/**
	 * Update the calculated value of this field or any of its children.
	 * This method might have no impact at all if the field does not have a formula.
	 */
	void recalculate();

	/**
	 * Initialize this field with its default value.
	 */
	void initialize();

	/**
	 * Get an accessor for a child field. Depending on the type of field a different
	 * type of field accessor might be returned. A field that does not exist will result
	 * in an exception being thrown.
	 * <p/>
	 * The path is expected to be the suffix without the current field id.
	 *
	 * @param fieldPath field identifier
	 * @return accessor
	 */
	DynamicDocumentFieldAccessor getFieldAccessor( String fieldPath );
}
