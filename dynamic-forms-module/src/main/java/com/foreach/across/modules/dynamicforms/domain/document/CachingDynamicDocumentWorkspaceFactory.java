/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypes;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.val;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.util.Assert;

import java.util.Map;

/**
 * Default implementation of {@link DynamicDocumentWorkspaceFactory} which optionally allows
 * setting a shared definition cache using {@link #setDefinitionCache(Map)}.
 * <p/>
 * If no shared definition cache is specified, a new workspace will use its own separate cache.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
public class CachingDynamicDocumentWorkspaceFactory implements DynamicDocumentWorkspaceFactory
{
	private final AutowireCapableBeanFactory beanFactory;

	/**
	 * -- SETTER --
	 * Set the shared definition cache that should be used by all workspaces that this factory creates.
	 */
	@Setter
	private Map<Long, DynamicDocumentDefinition> definitionCache;

	/**
	 * Create a workspace for a new document using the latest version of the specified document definition.
	 *
	 * @param documentDefinition definition
	 * @return document workspace - with a new blank document
	 */
	@Override
	public DynamicDocumentWorkspace createDocumentWorkspace( @NonNull DynamicDefinition documentDefinition ) {
		Assert.isTrue(
				documentDefinition.getType() != null && DynamicDefinitionTypes.DOCUMENT.equals( documentDefinition.getType().getName() ),
				"Definition is not of type 'document'."
		);
		return createDocumentWorkspace( documentDefinition.getLatestVersion() );
	}

	/**
	 * Create a workspace for a new document using the specific version of the document definition.
	 *
	 * @param documentDefinitionVersion version of the definition that should be used
	 * @return document workspace - with a new blank document
	 */
	@Override
	public DynamicDocumentWorkspace createDocumentWorkspace( @NonNull DynamicDefinitionVersion documentDefinitionVersion ) {
		val documentDefinition = documentDefinitionVersion.getDefinition();
		Assert.notNull( documentDefinition, "Version not linked to a main definition." );
		Assert.isTrue(
				documentDefinition.getType() != null && DynamicDefinitionTypes.DOCUMENT.equals( documentDefinition.getType().getName() ),
				"Definition is not of type 'document'."
		);

		val workspace = beanFactory.createBean( DynamicDocumentWorkspace.class );

		applyDefinitionCache( workspace );

		workspace.setDefinition( documentDefinition );
		workspace.setDefinitionVersion( documentDefinitionVersion );
		workspace.initialize();
		return workspace;
	}

	/**
	 * Create a workspace for an existing document, initialized with the latest version of that document if there is one.
	 *
	 * @param document to create the workspace for
	 * @return document workspace
	 */
	@Override
	public DynamicDocumentWorkspace createDocumentWorkspace( @NonNull DynamicDocument document ) {
		return createDocumentWorkspace( document, document.getLatestVersion() );
	}

	/**
	 * Create a workspace for an existing document version.
	 *
	 * @param documentVersion that should be loaded in the workspace
	 * @return document workspace
	 */
	@Override
	public DynamicDocumentWorkspace createDocumentWorkspace( @NonNull DynamicDocumentVersion documentVersion ) {
		return createDocumentWorkspace( documentVersion.getDocument(), documentVersion );
	}

	/**
	 * Will clear the shared cache attached to this factory (if there is one).
	 */
	public void clearCache() {
		if ( definitionCache != null ) {
			definitionCache.clear();
		}
	}

	private DynamicDocumentWorkspace createDocumentWorkspace( DynamicDocument document, DynamicDocumentVersion documentVersion ) {
		val workspace = beanFactory.createBean( DynamicDocumentWorkspace.class );

		applyDefinitionCache( workspace );

		workspace.setDefinition( document.getType() );

		if ( documentVersion != null ) {
			workspace.setDefinitionVersion( documentVersion.getDefinitionVersion() );
		}
		else {
			workspace.setDefinitionVersion( document.getType().getLatestVersion() );
		}

		workspace.setDocument( document );
		workspace.setDocumentVersion( documentVersion );
		workspace.initialize();

		return workspace;
	}

	private void applyDefinitionCache( DynamicDocumentWorkspace workspace ) {
		if ( definitionCache != null ) {
			workspace.setCacheDefinitions( true );
			workspace.setDefinitionCache( definitionCache );
		}
	}
}
