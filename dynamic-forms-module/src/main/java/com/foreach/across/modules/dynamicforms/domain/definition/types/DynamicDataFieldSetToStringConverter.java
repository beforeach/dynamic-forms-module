/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;

/**
 * Converts a {@link DynamicDataFieldSet} to {@code String} by converting it to JSON.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Slf4j
@RequiredArgsConstructor
public class DynamicDataFieldSetToStringConverter implements Converter<DynamicDataFieldSet, String>
{
	@NonNull
	private final ObjectMapper objectMapper;

	@Override
	public String convert( DynamicDataFieldSet fieldset ) {
		try {
			return objectMapper.writeValueAsString( fieldset );
		}
		catch ( JsonProcessingException e ) {
			LOG.trace( "Unable to write fieldset value", e );
			return "";
		}
	}
}
