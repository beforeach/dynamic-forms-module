/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import lombok.NonNull;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Central component for retrieving a {@link DynamicDocumentDataMapper} for a particular output format.
 * Allows writing and reading document data in that format.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Service
public class DynamicDocumentDataMapperFactory
{
	private final Map<String, Function<String, DynamicDocumentDataMapper>> mapperFactories = new HashMap<>();

	/**
	 * Register a mapper factory for a particular format.
	 * The factory method must return a {@link DynamicDocumentDataMapper} and will receive the requested
	 * format as the single function parameter.
	 *
	 * @param format        the mapper is for
	 * @param mapperFactory for creating a new mapper instance
	 */
	public void registerMapper( @NonNull String format, @NonNull Function<String, DynamicDocumentDataMapper> mapperFactory ) {
		mapperFactories.put( format, mapperFactory );
	}

	/**
	 * Create a mapper for the given format.
	 * If the format is unknown or the factory method returns {@code null}, an {@link IllegalArgumentException} will be thrown.
	 *
	 * @param format to create a mapper for
	 * @param <T>    return type expected by the mapper
	 * @return mapper
	 */
	@SuppressWarnings("unchecked")
	public <T> DynamicDocumentDataMapper<T> createMapper( @NonNull String format ) {
		val mapperFactory = mapperFactories.get( format );

		if ( mapperFactory != null ) {
			DynamicDocumentDataMapper mapper = mapperFactory.apply( format );

			if ( mapper != null ) {
				return mapper;
			}
		}

		throw new IllegalArgumentException( "Unable to create a DynamicDocumentDataMapper for format '" + format + "'" );
	}
}
