/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.utils;

import org.thymeleaf.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public class VersionUtils
{
	public static final String PATTERN = "^[^0-9]*(\\d+).*$";

	public static String getNextVersion( String version ) {
		Pattern pattern = Pattern.compile( PATTERN, Pattern.CASE_INSENSITIVE );
		Matcher matcher = pattern.matcher( version );

		if ( matcher.matches() ) {
			String increment = Long.toString( Long.valueOf( matcher.group( 1 ) ) + 1L );
			return version.substring( 0, matcher.start( 1 ) ) + increment + version.substring( matcher.end( 1 ) );
		}
		else {
			return StringUtils.trim( version + " " + 1L );
		}
	}
}
