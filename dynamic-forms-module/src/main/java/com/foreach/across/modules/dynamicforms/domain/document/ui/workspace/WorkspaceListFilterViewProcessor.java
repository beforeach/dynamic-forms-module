/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewDefinitionFilterSelector;
import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewFilterDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewFilterFieldDefinition;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewProcessor;
import com.foreach.across.modules.entity.views.context.ConfigurableEntityViewContext;
import com.foreach.across.modules.entity.views.processors.EntityQueryFilterProcessor;
import com.foreach.across.modules.entity.views.processors.query.EntityQueryFilterConfiguration;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.spring.security.actions.AllowableAction;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.core.NamedThreadLocal;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.WebDataBinder;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Component
public class WorkspaceListFilterViewProcessor implements EntityViewProcessor
{
	/**
	 * Attribute that can be registered on an {@link com.foreach.across.modules.entity.config.builders.EntityListViewFactoryBuilder}
	 * to filter based on access permissions.
	 *
	 * @see com.foreach.across.modules.entity.config.builders.EntityListViewFactoryBuilder#showOnlyItemsWithAction
	 */
	public static final String SHOW_ONLY_ITEMS_WITH_ACTION = WorkspaceListFilterViewProcessor.class.getName() + ".onlyShowItemsWithAction";

	private static final ThreadLocal<EntityQueryFilterProcessor> processorHolder = new NamedThreadLocal<>( "WorkspaceListFilter" );

	private final AutowireCapableBeanFactory beanFactory;
	private final ViewDefinitionService viewDefinitionService;

	@Override
	public void prepareEntityViewContext( ConfigurableEntityViewContext entityViewContext ) {
		EntityQueryFilterProcessor entityQueryFilterProcessor = beanFactory.createBean( EntityQueryFilterProcessor.class );
		processorHolder.set( entityQueryFilterProcessor );
		processorHolder.get().prepareEntityViewContext( entityViewContext );
	}

	@Override
	public void authorizeRequest( EntityViewRequest entityViewRequest ) {
		processorHolder.get().authorizeRequest( entityViewRequest );
	}

	@Override
	public void initializeCommandObject( EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder ) {
		Object action = entityViewRequest.getViewFactory().getAttribute( SHOW_ONLY_ITEMS_WITH_ACTION );
		if ( action instanceof AllowableAction ) {
			processorHolder.get().setShowOnlyItemsWithAction( (AllowableAction) action );
		}

		DynamicViewDefinition definition = viewDefinitionService.getViewDefinition( entityViewRequest );

		if ( definition != null && definition.getFilter() != null ) {
			DynamicViewFilterDefinition filter = definition.getFilter();

			EntityQueryFilterConfiguration filterConfiguration = EntityQueryFilterConfiguration
					.builder()
					.advancedMode( filter.getAdvancedMode() )
					.basicMode( filter.getBasicMode() )
					.basePredicate( filter.getBasePredicate() )
					.defaultQuery( filter.getDefaultQuery() )
					.defaultToMultiValue( DynamicViewDefinitionFilterSelector.MULTI.equals( filter.getDefaultSelector() ) )
					.showProperties( filter.getFields().stream().map( DynamicViewFilterFieldDefinition::getId ).toArray( String[]::new ) )
					.singleValue(
							filter.getFields().stream()
							      .filter( f -> DynamicViewDefinitionFilterSelector.SINGLE.equals( f.getSelector() ) )
							      .map( DynamicViewFilterFieldDefinition::getId )
							      .toArray( String[]::new )
					)
					.multiValue(
							filter.getFields().stream()
							      .filter( f -> DynamicViewDefinitionFilterSelector.MULTI.equals( f.getSelector() ) )
							      .map( DynamicViewFilterFieldDefinition::getId )
							      .toArray( String[]::new )
					)
					.build();

			processorHolder.get().setFilterConfiguration( filterConfiguration );
		}
		processorHolder.get().initializeCommandObject( entityViewRequest, command, dataBinder );
	}

	@Override
	public void preProcess( EntityViewRequest entityViewRequest, EntityView entityView ) {
		processorHolder.get().preProcess( entityViewRequest, entityView );
	}

	@Override
	public void doControl( EntityViewRequest entityViewRequest, EntityView entityView, EntityViewCommand command ) {
		processorHolder.get().doControl( entityViewRequest, entityView, command );
	}

	@Override
	public void preRender( EntityViewRequest entityViewRequest, EntityView entityView ) {
		processorHolder.get().preRender( entityViewRequest, entityView );
	}

	@Override
	public void render( EntityViewRequest entityViewRequest, EntityView entityView ) {
		processorHolder.get().render( entityViewRequest, entityView );
	}

	@Override
	public void postRender( EntityViewRequest entityViewRequest, EntityView entityView ) {
		processorHolder.get().postRender( entityViewRequest, entityView );
	}

	@Override
	public void postProcess( EntityViewRequest entityViewRequest, EntityView entityView ) {
		processorHolder.get().postProcess( entityViewRequest, entityView );
		processorHolder.remove();
	}
}
