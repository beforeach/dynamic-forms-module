/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.ui;

import com.foreach.across.modules.bootstrapui.elements.ColumnViewElement;
import com.foreach.across.modules.bootstrapui.elements.FormGroupElement;
import com.foreach.across.modules.bootstrapui.elements.LabelFormElement;
import com.foreach.across.modules.bootstrapui.elements.TextboxFormElement;
import com.foreach.across.modules.bootstrapui.elements.builder.ColumnViewElementBuilder;
import com.foreach.across.modules.bootstrapui.elements.builder.FormGroupElementBuilder;
import com.foreach.across.modules.dynamicforms.domain.definition.*;
import com.foreach.across.modules.dynamicforms.utils.VersionUtils;
import com.foreach.across.modules.entity.bind.EntityPropertyControlName;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.helpers.EntityViewElementBatch;
import com.foreach.across.modules.entity.views.processors.ExtensionViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.builder.AbstractNodeViewElementBuilder;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilderSupport;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.foreach.across.modules.bootstrapui.ui.factories.BootstrapViewElements.bootstrap;
import static com.foreach.across.modules.web.ui.elements.HtmlViewElements.html;

/**
 * Adds the necessary fields to update the document of a version
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class ModifyDocumentDefinitionViewProcessor extends ExtensionViewProcessorAdapter<DynamicDefinitionVersion>
{
	public static final String DRAFT_VERSION = "draft";

	private final EntityViewElementBuilderHelper builderHelper;
	private final DynamicDefinitionVersionRepository definitionVersionRepository;
	private final YamlJsonObjectMapper yamlJsonObjectMapper;
	private final DynamicDocumentDefinitionService definitionService;
	private final DynamicDefinitionVersionValidator dynamicDefinitionVersionValidator;
	private final RawDefinitionService rawDefinitionService;
	private final DynamicDefinitionTypeConfigurationRegistry typeConfigurationRegistry;

	@Override
	protected DynamicDefinitionVersion createExtension( EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder ) {
		val definition = command.getEntity( DynamicDefinition.class );
		dataBinder.setDisallowedFields( /*controlPrefix() + ".version",*/ controlPrefix() + ".definition", controlPrefix() + ".id" );

		return DynamicDefinitionVersion.builder()
//		                               .version( VersionUtils.getNextVersion( definition.getLatestVersion().getVersion() ) )
                                       .definition( definition )
                                       .build();
	}

	@Override
	protected void validateExtension( DynamicDefinitionVersion extension, Errors errors, HttpMethod httpMethod, EntityViewRequest entityViewRequest ) {
		if ( HttpMethod.POST.equals( httpMethod ) ) {
			try {
				dynamicDefinitionVersionValidator.validate( extension, errors );

				String json = yamlJsonObjectMapper.yamlToJson( extension::getDefinitionContent );
				String typeName = extension.getDefinition().getType().getName();

				RawDefinition rawDefinition = rawDefinitionService.readRawDefinition( json, typeName );
				DynamicDefinitionTypeConfiguration configuration = typeConfigurationRegistry.getByName( typeName );

				if ( configuration == null ) {
					throw new IllegalArgumentException(
							"The specified definition type '" + typeName + "' is not registered in the type configuration registry." );
				}

				Validator validator = configuration.getValidator();
				if ( validator != null ) {
					validator.validate( rawDefinition, errors );
				}

				if ( !errors.hasFieldErrors() ) {
					extension.setDefinitionContent( rawDefinitionService.writeDefinition( rawDefinition ) );
				}
			}
			catch ( IOException | YAMLException e ) {
				errors.rejectValue( "definitionContent", "parsingFailed", new String[] { e.getMessage() }, "Error parsing input" );
			}
		}
	}

	/**
	 * Adds the fields required to build an {@link DynamicDefinitionVersion} to create a new version when editing a {@link DynamicDefinition}.
	 */
	@Override
	protected void render( DynamicDefinitionVersion extension,
	                       EntityViewRequest entityViewRequest,
	                       EntityView entityView,
	                       ContainerViewElementBuilderSupport<?, ?> containerBuilder,
	                       ViewElementBuilderMap builderMap,
	                       ViewElementBuilderContext builderContext ) {
		DynamicDefinition definition = entityViewRequest.getEntityViewContext().getEntity( DynamicDefinition.class );

		DynamicDefinitionVersion version;
		if ( HttpMethod.GET.equals( entityViewRequest.getHttpMethod() ) ) {
			version = getVersionForExtension( definition );
			version.setPublished( true );
			version.setDefinitionContent( StringUtils.isNotBlank( version.getDefinitionContent() )
					                              ? yamlJsonObjectMapper.jsonToYaml( version::getDefinitionContent )
					                              : " " );
		}
		else {
			version = extension;
		}

		// todo refactor to controlfactory (in combination with ReadDefinitionVersionViewProcessor)?
		EntityViewElementBatch<DynamicDefinitionVersion> batch = builderHelper.createBatchForEntityType( DynamicDefinitionVersion.class );
		batch.setViewElementMode( ViewElementMode.FORM_WRITE );
		batch.setPropertySelector( EntityPropertySelector.of( "definitionContent", "remarks", "published", "version" ) );
		batch.setEntity( version );
		batch.setParentViewElementBuilderContext( builderContext );
		batch.setAttribute( EntityPropertyControlName.class, EntityPropertyControlName.root( controlPrefix() ) );

		Map<String, ViewElement> formGroups = batch.build();
		DynamicDefinitionVersion latestDefinitionVersion = definition.getLatestVersion();
		String latestVersion = latestDefinitionVersion != null ? latestDefinitionVersion.getVersion() : "";
		FormGroupElement versionFormGroup = configureVersionFormGroup( (FormGroupElement) formGroups.get( "version" ), latestVersion );
		FormGroupElementBuilder currentVersion = bootstrap.builders
				.formGroup()
				.label( bootstrap.builders.label().text( "Current version" ) )
				.control( bootstrap.builders.textbox().disabled().text( latestVersion ) )
				.css( "col-md-6" );

		formGroups.remove( "version" );
		formGroups.put( "currentVersion", getVersionsWrapper()
				.add( currentVersion )
				.add( versionFormGroup )
				.build( builderContext )
		);

		builderMap.get( SingleEntityFormViewProcessor.RIGHT_COLUMN, ColumnViewElementBuilder.class )
		          .addAll( formGroups.values() );
	}

	private DynamicDefinitionVersion getVersionForExtension( DynamicDefinition definition ) {
		DynamicDefinitionVersion version = definitionVersionRepository.findByDefinitionAndVersionEquals( definition, DRAFT_VERSION ).orElse( null );
		if ( version == null ) {
			version = definition.getLatestVersion() != null ? definition.getLatestVersion() : new DynamicDefinitionVersion();
		}
		version = version.toDto();
		return version;
	}

	/**
	 * Creates a wrapping {@link ViewElement} for the version controls.
	 * The wrapping element will only be shown if the published control is checked.
	 */
	private AbstractNodeViewElementBuilder getVersionsWrapper() {
		Map<String, Object> options = new HashMap<>();
		options.put( "hide", true );

		Map<String, Object> qualifiers = new HashMap<>();
		qualifiers.put( "checked", true );

		Map<String, Object> attributes = new HashMap<>();
		attributes.put( "[id='extensions[ModifyDocumentDefinitionViewProcessor].published']", qualifiers );
		attributes.put( "options", options );

		return html.builders.div().attribute( "data-dependson", attributes );
	}

	/**
	 * Adds the required css to the given form group, as well as sets the label and text for the form group.
	 *
	 * @param versionFormGroup to modify
	 * @param latestVersion    to use as the control value
	 * @return the modified form group
	 */
	private FormGroupElement configureVersionFormGroup( FormGroupElement versionFormGroup, String latestVersion ) {
		versionFormGroup.addCssClass( "col-md-6" );
		TextboxFormElement versionControl = versionFormGroup.getControl( TextboxFormElement.class );
		( (LabelFormElement) versionFormGroup.getLabel() ).setText( "New version" );
		versionControl.setText( VersionUtils.getNextVersion( latestVersion ) );

		return versionFormGroup;
	}

	@Override
	protected void postRender( DynamicDefinitionVersion extension,
	                           EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		ContainerViewElementUtils.find( container, SingleEntityFormViewProcessor.RIGHT_COLUMN, ColumnViewElement.class )
		                         .ifPresent( rightColumn -> ContainerViewElementUtils
				                         .find( container, SingleEntityFormViewProcessor.LEFT_COLUMN, ColumnViewElement.class )
				                         .ifPresent( leftColumn -> {
					                         List<ViewElement> children = leftColumn.getChildren();
					                         for ( int i = children.size() - 1; i >= 0; i-- ) {
						                         rightColumn.addFirstChild( children.get( i ) );
					                         }
					                         leftColumn.clearChildren();
				                         } )
		                         );
		ContainerViewElementUtils.move( container, "formGroup-definitionContent", SingleEntityFormViewProcessor.LEFT_COLUMN );
	}

	/**
	 * Creates a new {@link DynamicDefinitionVersion} for the {@link DynamicDefinition} being updated.
	 */
	@Override
	protected void doPost( DynamicDefinitionVersion extension, BindingResult bindingResult, EntityView entityView, EntityViewRequest entityViewRequest ) {
		if ( !bindingResult.hasErrors() ) {
			if ( extension.getCreatedDate() == null ) {
				extension.setCreatedDate( new Date() );
			}

			if ( !extension.isPublished() ) {
				extension.setVersion( DRAFT_VERSION );
			}

			DynamicDefinitionVersion draft = definitionVersionRepository.findByDefinitionAndVersionEquals( extension.getDefinition(), DRAFT_VERSION ).orElse(
					null );
			if ( draft != null ) {
				extension.setId( draft.getId() );
			}

			definitionVersionRepository.save( extension );
		}
	}
}
