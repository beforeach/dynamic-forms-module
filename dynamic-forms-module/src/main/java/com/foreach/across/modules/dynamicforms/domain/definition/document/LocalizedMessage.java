/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import lombok.NonNull;
import lombok.Value;

import java.util.*;

/**
 * Represents a single message attached to a locale.
 *
 * @author Arne Vandamme
 * @since 0.0.2
 */
@Value
public class LocalizedMessage
{
	public static final String DEFAULT_LANGUAGE_TAG = "default";

	private final Locale locale;
	private final String messageCode;
	private final String message;

	private String key() {
		return languageTag() + messageCode;
	}

	private String languageTag() {
		return locale != null ? locale.toLanguageTag() : DEFAULT_LANGUAGE_TAG;
	}

	/**
	 * Parse map input data of messages into a collection of localized messages.
	 * A map entry value can be in turn a {@code Map} in which case the key is expected to
	 * be the language tag for the {@code Locale} and the child map the actual messages.
	 * If the top-level value is not a {@code Map}, it will be used as the actual message.
	 *
	 * @param messageMap as map
	 * @return collection of localized messages
	 */
	@SuppressWarnings("unchecked")
	static Collection<LocalizedMessage> parseAllFromMap( @NonNull Map<String, Object> messageMap ) {
		List<LocalizedMessage> messages = new ArrayList<>();

		messageMap.forEach( ( key, value ) -> {
			if ( value instanceof Map ) {
				Locale locale = DEFAULT_LANGUAGE_TAG.equals( key ) ? null : Locale.forLanguageTag( key );
				( (Map<String, Object>) value ).forEach(
						( messageCode, message ) -> messages.add( new LocalizedMessage( locale, messageCode, Objects.toString( message ) ) )
				);
			}
			else {
				messages.add( new LocalizedMessage( null, key, Objects.toString( value ) ) );
			}
		} );

		return messages;
	}

	/**
	 * Merge two collection of localized messages, only one combination of locale and message code will be kept.
	 *
	 * @param initial    collection of messages
	 * @param extensions collection that should be added or whose entries should override the initial value
	 * @return merged collection
	 */
	static Collection<LocalizedMessage> merge( @NonNull Collection<LocalizedMessage> initial, @NonNull Collection<LocalizedMessage> extensions ) {
		Map<String, LocalizedMessage> messages = new LinkedHashMap<>();
		initial.forEach( msg -> messages.put( msg.key(), msg ) );
		extensions.forEach( msg -> messages.put( msg.key(), msg ) );

		return messages.values();
	}

	/**
	 * Convert a collection of {@code LocalizedMessage} into a map structure that can be processed by {@link #parseAllFromMap(Map)}.
	 * All messages will be grouped by language tag.
	 *
	 * @param messages collection
	 * @return map
	 */
	@SuppressWarnings("unchecked")
	static Map<String, Object> convertToMap( @NonNull Collection<LocalizedMessage> messages ) {
		Map<String, Object> messageMap = new HashMap<>();

		messages.forEach(
				msg ->
						( (Map<String, String>) messageMap.computeIfAbsent( msg.languageTag(), l -> new HashMap<String, String>() ) )
								.put( msg.getMessageCode(), msg.getMessage() )

		);

		return messageMap;
	}
}
