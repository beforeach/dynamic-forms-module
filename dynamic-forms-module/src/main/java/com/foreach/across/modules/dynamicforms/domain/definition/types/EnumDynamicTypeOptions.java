/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.bootstrapui.elements.builder.OptionFormElementBuilder;
import com.foreach.across.modules.entity.views.bootstrapui.options.OptionIterableBuilder;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import lombok.*;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.format.Printer;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Represents the set of possible values allowed for an enum type.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
public class EnumDynamicTypeOptions implements OptionIterableBuilder, Printer<Object>, Iterable<EnumDynamicTypeOptions.Option>
{
	private static final TypeDescriptor RAW_TYPE = TypeDescriptor.valueOf( String.class );

	private final ConversionService conversionService;
	private final TypeDescriptor targetType;

	private final List<Option> options = new ArrayList<>();

	/**
	 * Add a possible value, usually from a definition.
	 * Accepted parameters are:
	 * <ul>
	 * <li>a single value object (serving as both value and label).</li>
	 * <li>a map with a single entry (entry.key = value, entry.value = label)</li>
	 * <li>a map with multiple entries, representing the different properties of the option</li>
	 * </ul>
	 * <p/>
	 * An exception will be thrown if
	 *
	 * @param possibleValue possible value
	 * @return the actual option that has been added
	 */
	@SuppressWarnings("unchecked")
	public Option addPossibleValue( @NonNull Object possibleValue ) {
		Option option = new Option();
		if ( possibleValue instanceof Map ) {
			Map<Object, Object> optionDetails = (Map) possibleValue;
			if ( optionDetails.size() == 1 ) {
				optionDetails.forEach(
						( value, label ) -> {
							option.value = convertToRawType( value );
							option.label = convertToRawType( label );
							option.rawValue = option.value;
						}
				);
			}
			else if ( optionDetails.size() > 1 ) {
				option.value = convertToRawType( optionDetails.get( "value" ) );
				option.rawValue = optionDetails.getOrDefault( "raw-value", option.value );
				option.label = convertToRawType( optionDetails.getOrDefault( "label", option.value ) );

				option.htmlAttributes.putAll( (Map<String, Object>) optionDetails.getOrDefault( "html-attributes", Collections.emptyMap() ) );
			}
		}
		else {
			String value = convertToRawType( possibleValue );
			option.value = value;
			option.label = value;
			option.rawValue = option.value;
		}

		options.add( option );

		return option;
	}

	private String convertToRawType( Object value ) {
		return (String) conversionService.convert( value, TypeDescriptor.forObject( value ), RAW_TYPE );
	}

	@Override
	public boolean isSorted() {
		return true;
	}

	@Override
	public List<OptionFormElementBuilder> buildOptions( ViewElementBuilderContext builderContext ) {
		List<OptionFormElementBuilder> elements = new ArrayList<>( options.size() );
		options.forEach( option -> elements.add(
				new OptionFormElementBuilder().label( option.getLabel() )
				                              .value( option.getValue() )
				                              .rawValue( option.getRawValue() )
				                              .attributes( option.getHtmlAttributes() )
		                 )
		);
		return elements;
	}

	@Override
	public String print( Object value, Locale locale ) {
		if ( value != null ) {
			if ( value instanceof Collection ) {
				Map<Object, String> labelsByRawValue = new HashMap<>();
				options.forEach( o -> labelsByRawValue.put( o.getRawValue(), o.getLabel() ) );

				return ( (Collection<?>) value )
						.stream()
						.map( v -> conversionService.convert( v, TypeDescriptor.forObject( v ), targetType ) )
						.map( v -> labelsByRawValue.getOrDefault( v, v.toString() ) )
						.collect( Collectors.joining( ", " ) );
			}

			Object actualValue = conversionService.convert( value, TypeDescriptor.forObject( value ), targetType );
			Option option = findOption( actualValue );
			return option != null ? option.getLabel() : value.toString();
		}
		return "";
	}

	private Option findOption( Object actualValue ) {
		for ( Option candidate : options ) {
			if ( Objects.equals( candidate.getRawValue(), actualValue ) ) {
				return candidate;
			}
		}
		return null;
	}

	@Override
	public Iterator<Option> iterator() {
		return options.iterator();
	}

	@Override
	public void forEach( Consumer<? super Option> action ) {
		options.forEach( action );
	}

	@Override
	public Spliterator<Option> spliterator() {
		return options.spliterator();
	}

	/**
	 * Represent a single possible value.
	 */
	@Getter
	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public class Option
	{
		private String value;
		private String label;
		private Object rawValue;
		private Map<String, Object> htmlAttributes = new LinkedHashMap<>();

		public Object getRawValue() {
			return conversionService.convert( rawValue, TypeDescriptor.forObject( rawValue ), targetType );
		}
	}
}
