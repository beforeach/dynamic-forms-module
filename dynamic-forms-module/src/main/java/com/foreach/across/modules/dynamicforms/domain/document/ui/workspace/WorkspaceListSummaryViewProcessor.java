/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewDefinition;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.bootstrapui.processors.element.EntitySummaryViewActionProcessor;
import com.foreach.across.modules.entity.views.bootstrapui.util.SortableTableBuilder;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SortableTableRenderingViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.resource.WebResource;
import com.foreach.across.modules.web.resource.WebResourceRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.annotation.RequestScope;

/**
 * Request scoped view processor which checks if a summary view is registered for a dynamic document type.
 * If that is the case, it will be added to the list view being rendered.
 *
 * @author Steven Gentens
 * @since 0.0.2
 */
@Component
@RequiredArgsConstructor
@RequestScope
public class WorkspaceListSummaryViewProcessor extends EntityViewProcessorAdapter
{
	private final ViewDefinitionService viewDefinitionService;

	private DynamicViewDefinition viewDefinition;

	@Override
	protected void doControl( EntityViewRequest entityViewRequest,
	                          EntityView entityView,
	                          EntityViewCommand command,
	                          BindingResult bindingResult,
	                          HttpMethod httpMethod ) {
		viewDefinition = viewDefinitionService.getViewDefinitionByName( entityViewRequest, EntityView.SUMMARY_VIEW_NAME );
	}

	@Override
	protected void registerWebResources( EntityViewRequest entityViewRequest, EntityView entityView, WebResourceRegistry webResourceRegistry ) {
		if ( viewDefinition != null ) {
			webResourceRegistry.add( WebResource.JAVASCRIPT_PAGE_END, "/static/entity/js/expandable.js", WebResource.VIEWS );
		}
	}

	@Override
	protected void createViewElementBuilders( EntityViewRequest entityViewRequest, EntityView entityView, ViewElementBuilderMap builderMap ) {
		if ( viewDefinition != null ) {
			SortableTableBuilder sortableTableBuilder = builderMap.get( SortableTableRenderingViewProcessor.TABLE_BUILDER, SortableTableBuilder.class );

			if ( sortableTableBuilder != null ) {
				sortableTableBuilder.valueRowProcessor( new EntitySummaryViewActionProcessor( entityViewRequest.getEntityViewContext().getLinkBuilder(),
				                                                                              EntityView.SUMMARY_VIEW_NAME ) );
			}
		}
	}
}
