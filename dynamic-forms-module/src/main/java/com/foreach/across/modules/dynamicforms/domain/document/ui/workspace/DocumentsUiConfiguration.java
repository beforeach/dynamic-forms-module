/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.core.context.info.AcrossModuleInfo;
import com.foreach.across.core.context.support.AcrossContextOrderedMessageSource;
import com.foreach.across.modules.bootstrapui.elements.TextboxFormElement;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDocumentDefinitionService;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RequestBoundDynamicDocumentDefinitionMessageSource;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentRepository;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspaceValidator;
import com.foreach.across.modules.dynamicforms.domain.document.query.DynamicDocumentWorkspaceEntityQueryFacadeResolver;
import com.foreach.across.modules.dynamicforms.domain.document.ui.DocumentVersionHistoryViewProcessor;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.conditionals.ConditionalOnAdminWeb;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.query.EntityQueryFacadeFactory;
import com.foreach.across.modules.entity.registry.EntityAssociation;
import com.foreach.across.modules.entity.registry.EntityFactory;
import com.foreach.across.modules.entity.support.EntityMessageCodeResolver;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.processors.AssociationHeaderViewProcessor;
import com.foreach.across.modules.entity.views.processors.SingleEntityPageStructureViewProcessor;
import com.foreach.across.modules.entity.views.processors.SortableTableRenderingViewProcessor;
import com.foreach.across.modules.hibernate.modules.config.EnableTransactionManagementConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.Ordered;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.core.EntityInformation;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.Validator;

import static com.foreach.across.modules.entity.views.EntityViewCustomizers.basicSettings;
import static com.foreach.across.modules.entity.views.EntityViewCustomizers.formSettings;
import static com.foreach.across.modules.entity.views.EntityViewFactoryAttributes.defaultAccessValidator;

/**
 * Configures the top-level <em>Documents</em> admin section, which allows CRUD on documents of all types.
 * This is done by mapping the {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace}
 * as a top-level entity configuration.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@ConditionalOnAdminWeb
@Configuration
@RequiredArgsConstructor
@EnableTransactionManagement(order = EnableTransactionManagementConfiguration.INTERCEPT_ORDER, proxyTargetClass = true)
class DocumentsUiConfiguration implements EntityConfigurer
{
	private static final int FORM_PROPERTIES_PROCESSOR_ORDER = 1000000;

	private final AcrossModuleInfo moduleInfo;
	private final DynamicDocumentRepository documentRepository;
	private final DynamicDocumentService documentService;
	private final DynamicDocumentWorkspaceEntityQueryFacadeResolver entityQueryFacadeResolver;
	private final GlobalListViewProcessor globalListViewProcessor;
	private final WorkspaceEntityPropertyRegistryViewProcessor propertyRegistryViewProcessor;
	private final WorkspaceFormViewProcessor formViewProcessor;
	private final WorkspaceListFilterViewProcessor workspaceListFilterViewProcessor;
	private final DynamicDocumentWorkspaceValidator workspaceValidator;
	private final DocumentPageStructureViewProcessor pageStructureViewProcessor;
	private final DocumentVersionHistoryViewProcessor historyViewProcessor;
	private final DynamicPropertySelectorListViewProcessor propertiesListViewProcessor;
	private final DynamicPropertySelectorFormViewProcessor propertiesFormViewProcessor;
	private final DynamicDefinitionRepository definitionRepository;
	private final DynamicDocumentDefinitionService documentDefinitionService;
	private final WorkspaceListSummaryViewProcessor workspaceListSummaryViewProcessor;

	@Bean
	public RequestBoundDynamicDocumentDefinitionMessageSource dynamicDocumentMessageSource( ApplicationContext applicationContext ) {
		AcrossContextOrderedMessageSource acrossMessageSource = applicationContext.getBean( AcrossContextOrderedMessageSource.class );
		RequestBoundDynamicDocumentDefinitionMessageSource messageSource = new RequestBoundDynamicDocumentDefinitionMessageSource();
		acrossMessageSource.push( messageSource );
		return messageSource;
	}

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		registerWorkspaceEntityConfiguration( entities );
		registerWorkspaceEntityAssociationOnDefinition( entities );
	}

	private void registerWorkspaceEntityConfiguration( EntitiesConfigurationBuilder entities ) {
		entities.create()
		        .entityType( DynamicDocumentWorkspace.class, true )
		        .as( DynamicDocumentWorkspace.class )
		        .hide()
		        .displayName( "Document" )
		        .attribute( AcrossModuleInfo.class, moduleInfo )
		        .attribute( EntityQueryFacadeFactory.class, entityQueryFacadeResolver )
		        .attribute( Validator.class, workspaceValidator )
		        .properties(
				        props -> props.property( "cacheDefinitions" ).writable( false ).readable( false ).hidden( true ).and()
				                      .property( "allowVersionUpdates" ).writable( false ).readable( false ).hidden( true ).and()
				                      .property( "dataBinderTarget" ).writable( false ).readable( false ).hidden( true ).and()
				                      .property( "fields" ).writable( false ).readable( false ).hidden( true ).and()
				                      .property( "calculatedFieldsEnabled" ).writable( false ).readable( false ).hidden( true ).and()
				                      .property( "versionLabel" )
				                      .displayName( "Version" )
				                      .hidden( true )
				                      .attribute( TextboxFormElement.Type.class, TextboxFormElement.Type.TEXT )
				                      .attribute( Sort.Order.class, new Sort.Order( "document.latestVersion.version" ) )
				                      .and()
				                      .property( "documentName" )
				                      .displayName( "Name" )
				                      .attribute( TextboxFormElement.Type.class, TextboxFormElement.Type.TEXT )
				                      .attribute( Sort.Order.class, new Sort.Order( "document.name" ) )
				                      .and()
				                      .property( "document.type" )
				                      .attribute( Sort.Order.class, new Sort.Order( "document.type.name" ) )
				                      .attribute( EntityAttributes.OPTIONS_ENTITY_QUERY, "type.name = 'document'" )

		        )
		        .entityModel( model ->
				                      model.labelPrinter( ( ws, locale ) -> ws.getDocumentName() )
				                           .entityInformation( new WorkspaceEntityInformation() )
				                           .entityFactory( new WorkspaceEntityFactory() )
				                           .findOneMethod(
						                           id -> documentService.createDocumentWorkspace(
								                           documentRepository.findById( Long.parseLong( id.toString() ) )
								                                             .orElse( null )
						                           )
				                           )
				                           .saveMethod( ws -> {
					                           ws.save();
					                           return ws;
				                           } )
				                           .deleteMethod( ws -> documentRepository.delete( ws.getDocument() ) )
		        )
		        .listView(
				        lvb -> lvb.showProperties( "document.name", "document.type", "document.latestVersion.version", "document.lastModified" )
				                  .defaultSort( new Sort( Sort.Direction.DESC, "document.lastModified" ) )
				                  .entityQueryFilter( filter -> filter.basicMode( true ).advancedMode( false ).showProperties() )
				                  .viewProcessor( definitionContextViewProcessor( true ), Ordered.HIGHEST_PRECEDENCE )
				                  .viewProcessor( propertyRegistryViewProcessor )
				                  .viewProcessor( globalListViewProcessor )
				                  .viewProcessor( workspaceListSummaryViewProcessor )
				                  .postProcess( SortableTableRenderingViewProcessor.class, p -> p.setSummaryViewName( null ) )
		        )
		        .view( EntityView.SUMMARY_VIEW_NAME,
		               fvb -> fvb.viewElementMode( ViewElementMode.FORM_READ )
		                         .showProperties( /* trigger property rendering view processor */ )
		                         .viewProcessor( definitionContextViewProcessor( true ), Ordered.HIGHEST_PRECEDENCE )
		                         .viewProcessor( propertiesFormViewProcessor, FORM_PROPERTIES_PROCESSOR_ORDER )
		                         .viewProcessor( propertyRegistryViewProcessor )
		        )
		        .createOrUpdateFormView(
				        fvb -> fvb.showProperties( "documentName", "field:*" )
				                  .viewProcessor( definitionContextViewProcessor( true ), Ordered.HIGHEST_PRECEDENCE )
				                  .viewProcessor( propertiesFormViewProcessor, FORM_PROPERTIES_PROCESSOR_ORDER )
				                  .viewProcessor( propertyRegistryViewProcessor )
				                  .viewProcessor( formViewProcessor )
		        )
		        .detailView(
				        fvb -> fvb.showProperties( "documentName", "field:*" )
				                  .viewProcessor( definitionContextViewProcessor( false ), Ordered.HIGHEST_PRECEDENCE )
				                  .viewProcessor( propertiesFormViewProcessor, FORM_PROPERTIES_PROCESSOR_ORDER )
				                  .viewProcessor( propertyRegistryViewProcessor )
				                  .viewProcessor( formViewProcessor )
		        )
		        .formView( "history",
		                   basicSettings()
				                   .adminMenu( "/history" )
				                   .accessValidator(
						                   defaultAccessValidator().and( ( view, context ) -> !context.getEntity( DynamicDocumentWorkspace.class ).getDocument()
						                                                                              .isVersionable() )
				                   )
				                   .andThen( formSettings().forExtension( true ).addFormButtons( false ) )
				                   .andThen( vb -> vb.viewProcessor( definitionContextViewProcessor( false ), Ordered.HIGHEST_PRECEDENCE )
				                                     .viewProcessor( propertyRegistryViewProcessor )
				                                     .viewProcessor( historyViewProcessor ) )
		        )
		        .postProcessor( entityConfiguration -> {
			        EntityMessageCodeResolver codeResolver = entityConfiguration.getEntityMessageCodeResolver();
			        codeResolver.setPrefixes( "DynamicFormsModule.entities.dynamicDocumentWorkspace", "DynamicFormsModule.entities" );
			        codeResolver.setFallbackCollections( "EntityModule.entities" );
		        } );
	}

	private void registerWorkspaceEntityAssociationOnDefinition( EntitiesConfigurationBuilder entities ) {
		entities.withType( DynamicDefinition.class )
		        .association(
				        ab -> ab.name( "documents" )
				                .targetEntityType( DynamicDocumentWorkspace.class )
				                .targetProperty( "document.type" )
				                .associationType( EntityAssociation.Type.EMBEDDED )
				                .parentDeleteMode( EntityAssociation.ParentDeleteMode.WARN )
				                .show()
				                .attribute( EntityFactory.class, new WorkspaceEntityFactory() )
				                .listView(
						                lvb -> lvb.showProperties( "documentName", "versionLabel", "document.lastModified" )
						                          .defaultSort( new Sort( Sort.Direction.DESC, "document.lastModifiedDate" ) )
						                          .viewProcessor( definitionContextViewProcessor( true ), Ordered.HIGHEST_PRECEDENCE )
						                          .viewProcessor( propertyRegistryViewProcessor )
						                          .viewProcessor( workspaceListFilterViewProcessor )
						                          .removeViewProcessor( SingleEntityPageStructureViewProcessor.class.getName() )
						                          .viewProcessor( propertiesListViewProcessor )
						                          .viewProcessor( workspaceListSummaryViewProcessor )
						                          .postProcess( SortableTableRenderingViewProcessor.class, p -> p.setSummaryViewName( null ) )
				                )
				                .view( EntityView.SUMMARY_VIEW_NAME,
				                       fvb -> fvb.viewElementMode( ViewElementMode.FORM_READ )
				                                 .showProperties( /* trigger property rendering view processor */ )
				                                 .viewProcessor( definitionContextViewProcessor( true ), Ordered.HIGHEST_PRECEDENCE )
				                                 .viewProcessor( propertiesFormViewProcessor, FORM_PROPERTIES_PROCESSOR_ORDER )
				                                 .viewProcessor( propertyRegistryViewProcessor )
				                )
				                .createOrUpdateFormView(
						                uvb -> uvb.showProperties( "documentName", "field:*" )
						                          .viewProcessor( definitionContextViewProcessor( true ), Ordered.HIGHEST_PRECEDENCE + 1 )
						                          .viewProcessor( propertyRegistryViewProcessor )
						                          .viewProcessor( formViewProcessor )
						                          .removeViewProcessor( SingleEntityPageStructureViewProcessor.class.getName() )
						                          .viewProcessor( pageStructureViewProcessor )
						                          .viewProcessor( propertiesFormViewProcessor, FORM_PROPERTIES_PROCESSOR_ORDER )
						                          .removeViewProcessor( AssociationHeaderViewProcessor.class.getName() )
				                )
				                .deleteFormView(
						                dvb -> dvb.viewProcessor( definitionContextViewProcessor( false ), Ordered.HIGHEST_PRECEDENCE )
						                          .viewProcessor( propertyRegistryViewProcessor )
						                          .removeViewProcessor( SingleEntityPageStructureViewProcessor.class.getName() )
						                          .viewProcessor( pageStructureViewProcessor )
						                          .removeViewProcessor( AssociationHeaderViewProcessor.class.getName() )
				                )
				                .detailView(
						                fvb -> fvb.showProperties( "documentName", "field:*" )
						                          .viewProcessor( definitionContextViewProcessor( false ), Ordered.HIGHEST_PRECEDENCE )
						                          .viewProcessor( propertyRegistryViewProcessor )
						                          .viewProcessor( formViewProcessor )
						                          .removeViewProcessor( SingleEntityPageStructureViewProcessor.class.getName() )
						                          .viewProcessor( pageStructureViewProcessor )
						                          .viewProcessor( propertiesFormViewProcessor, FORM_PROPERTIES_PROCESSOR_ORDER )
						                          .removeViewProcessor( AssociationHeaderViewProcessor.class.getName() )
				                )
				                .formView( "history",
				                           basicSettings()
						                           .adminMenu( "/history" )
						                           .accessValidator( defaultAccessValidator().and(
								                           ( view, context ) -> !context.getEntity( DynamicDocumentWorkspace.class ).getDocument()
								                                                        .isVersionable() )
						                           )
						                           .andThen( formSettings().forExtension( true ).addFormButtons( false ) )
						                           .andThen( vb -> vb.viewProcessor( definitionContextViewProcessor( false ), Ordered.HIGHEST_PRECEDENCE )
						                                             .viewProcessor( propertyRegistryViewProcessor )
						                                             .removeViewProcessor( SingleEntityPageStructureViewProcessor.class.getName() )
						                                             .viewProcessor( pageStructureViewProcessor )
						                                             .viewProcessor( historyViewProcessor )
						                                             .removeViewProcessor( AssociationHeaderViewProcessor.class.getName() ) )
				                )
		        );
	}

	@Bean
	@Scope("prototype")
	DynamicDocumentDefinitionContextViewProcessor definitionContextViewProcessor( boolean useLatestVersion ) {
		DynamicDocumentDefinitionContextViewProcessor processor
				= new DynamicDocumentDefinitionContextViewProcessor( definitionRepository, documentDefinitionService );
		return processor.useLatestDefinitionVersion( useLatestVersion );
	}

	class WorkspaceEntityInformation implements EntityInformation<DynamicDocumentWorkspace, Long>
	{
		@Override
		public boolean isNew( DynamicDocumentWorkspace entity ) {
			return entity.getDocument().isNew();
		}

		@Override
		public Long getId( DynamicDocumentWorkspace entity ) {
			return entity.getDocument().getId();
		}

		@Override
		public Class<Long> getIdType() {
			return Long.class;
		}

		@Override
		public Class<DynamicDocumentWorkspace> getJavaType() {
			return DynamicDocumentWorkspace.class;
		}
	}

	class WorkspaceEntityFactory implements EntityFactory<DynamicDocumentWorkspace>
	{
		@Override
		public DynamicDocumentWorkspace createNew( Object... args ) {
			DynamicDefinition definition = args.length == 0
					? DynamicDocumentDefinitionContextViewProcessor.currentDefinition()
					: (DynamicDefinition) args[0];

			if ( definition == null ) {
				throw new IllegalArgumentException( "Unable to create a new document workspace - no definition specified" );
			}

			return createDto( documentService.createDocumentWorkspace( definition ) );
		}

		@Override
		public DynamicDocumentWorkspace createDto( DynamicDocumentWorkspace workspace ) {
			workspace.createNewVersion( true );
			workspace.setDataBinderTarget( "entity" );
			return workspace;
		}
	}
}
