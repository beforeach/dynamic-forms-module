/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.Date;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "dfm_document_version")
@NoArgsConstructor
public class DynamicDocumentVersion extends SettableIdAuditableEntity<DynamicDocumentVersion> implements Comparable<DynamicDocumentVersion>
{
	@Id
	@GeneratedValue(generator = "seq_dfm_document_version_id")
	@GenericGenerator(
			name = "seq_dfm_document_version_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@Parameter(name = "sequenceName", value = "seq_dfm_document_version_id"),
					@Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	/**
	 * The document for which this version is applicable.
	 */
	@NotNull
	@ManyToOne
	@JoinColumn(name = "document_id")
	private DynamicDocument document;

	/**
	 * The definition template used for this specific document version.
	 */
	@NotNull
	@ManyToOne
	@JoinColumn(name = "definition_version_id")
	private DynamicDefinitionVersion definitionVersion;

	/**
	 * The version of the document.
	 */
	@NotNull
	@Column(name = "version")
	@Length(max = 30)
	private String version;

	/**
	 * The content of the document
	 */
	@NotNull
	@Column(name = "document_content")
	private String documentContent;

	@Builder(toBuilder = true)
	public DynamicDocumentVersion( Long id,
	                               DynamicDocument document,
	                               DynamicDefinitionVersion definitionVersion,
	                               String version,
	                               String documentContent,
	                               @Builder.ObtainVia(method = "getCreatedBy") String createdBy,
	                               @Builder.ObtainVia(method = "getCreatedDate") Date createdDate,
	                               @Builder.ObtainVia(method = "getLastModifiedBy") String lastModifiedBy,
	                               @Builder.ObtainVia(method = "getLastModifiedDate") Date lastModifiedDate ) {
		setId( id );
		setDocument( document );
		setDefinitionVersion( definitionVersion );
		setVersion( version );
		setDocumentContent( documentContent );
		setCreatedBy( createdBy );
		setCreatedDate( createdDate );
		setLastModifiedBy( lastModifiedBy );
		setLastModifiedDate( lastModifiedDate );
	}

	@Override
	public int compareTo( DynamicDocumentVersion other ) {
		Comparator<DynamicDocumentVersion> documentComparator = Comparator.comparing( DynamicDocumentVersion::getDocument, DynamicDocument::compareTo );
		Comparator<DynamicDocumentVersion> versionComparator = Comparator.comparing( DynamicDocumentVersion::getVersion, StringUtils::compare );
		return documentComparator.thenComparing( versionComparator ).compare( this, other );
	}
}
