/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.foreach.across.modules.dynamicforms.domain.definition.RawDefinition;
import lombok.*;
import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Stateless representation of a view definition.
 *
 * @author Marc Vanbrabant
 * @see DynamicViewDefinitionBuilder
 * @since 0.0.1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonRootName("view-definition")
public class RawViewDefinition implements Serializable, RawDefinition
{
	private String name;
	private String type;

	@JsonDeserialize(as = ArrayList.class, contentAs = String.class)
	@Singular("property")
	@NonNull
	private List<String> properties = new ArrayList<>();

	public List<String> getProperties() {
		return properties != null ? properties : Collections.emptyList();
	}

	private RawViewDefinition.Filter filter;

	/**
	 * Create a new definition that is an exact - deep - copy of the existing definition.
	 *
	 * @param definition to create a copy of
	 * @return new instance
	 */
	public static RawViewDefinition copyOf( @NonNull RawViewDefinition definition ) {
		return SerializationUtils.clone( definition );
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class Filter implements Serializable
	{
		private DynamicViewDefinitionFilterSelector defaultSelector;
		private String defaultQuery;
		private String basePredicate;
		private Mode mode;

		@JsonDeserialize(as = ArrayList.class, contentAs = RawViewDefinition.Field.class)
		@Singular("field")
		private List<RawViewDefinition.Field> fields = new ArrayList<>();

		public Mode getMode() {
			if ( mode == null ) {
				mode = new Mode();
			}
			return mode;
		}
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class Mode implements Serializable
	{
		private Boolean basic;
		private Boolean advanced;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class Field implements Serializable
	{
		private String id;
		private DynamicViewDefinitionFilterSelector selector;
		private String operand;
	}
}
