/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.query;

import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.entity.query.AbstractEntityQueryExecutor;
import com.foreach.across.modules.entity.query.EntityQuery;
import com.foreach.across.modules.entity.query.EntityQueryFacade;
import com.foreach.across.modules.entity.query.EntityQueryFacadeResolver;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

/**
 * Custom {@link com.foreach.across.modules.entity.query.EntityQueryExecutor} that supports querying workspaces.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Scope("prototype")
@Component
@RequiredArgsConstructor
class WorkspaceEntityQueryExecutor extends AbstractEntityQueryExecutor<DynamicDocumentWorkspace>
{
	private final EntityQueryFacadeResolver entityQueryFacadeResolver;
	private final EntityRegistry entityRegistry;
	private final DynamicDocumentService documentService;

	@Setter
	private EntityPropertyRegistry propertyRegistry;

	@Override
	protected Iterable<DynamicDocumentWorkspace> executeQuery( EntityQuery query ) {
		return new WorkspaceEntityQuery( propertyRegistry, query )
				.execute( documentService, documentExecutor() );
	}

	private EntityQueryFacade documentExecutor() {
		return entityQueryFacadeResolver.forEntityConfiguration( entityRegistry.getEntityConfiguration( DynamicDocument.class ) );
	}

	@Override
	protected Iterable<DynamicDocumentWorkspace> executeQuery( EntityQuery query, Sort sort ) {
		return new WorkspaceEntityQuery( propertyRegistry, query, sort )
				.execute( documentService, documentExecutor() );
	}

	@Override
	protected Page<DynamicDocumentWorkspace> executeQuery( EntityQuery query, Pageable pageable ) {
		return new WorkspaceEntityQuery( propertyRegistry, query, pageable )
				.execute( documentService, documentExecutor() );
	}
}
