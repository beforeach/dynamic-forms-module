/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * Represents a unique string identifier of either a {@link DynamicDefinition} or specific {@link DynamicDefinitionVersion}.
 * Can be converted from and to {@code String}.
 * <p/>
 * The following are examples of a definition id:
 * <ul>
 * <li><em>dataset:document</em> - a document definition with key "document" in dataset with key "dataset"</li>
 * <li><em>dataset:views:name</em> - a view with key "name" in dataset "dataset"</li>
 * <li><em>dataset:document/child</em> - a child document (with key "child") of the document with key "document" in dataset with key "dataset"</li>
 * <li><em>dataset:document/views:name</em> - a child view (with key "name") of the document with key "document" in dataset with key "dataset"</li>
 * </ul>
 * In the examples above the id always refers to a {@link DynamicDefinition}. Optionally a version label can be
 * added in which case the id refers to a {@link DynamicDefinitionVersion} instead. Examples:
 * <ul>
 * <li>dataset:document@1.0</li>
 * <li>dataset:views:name@1.0</li>
 * <li>dataset:document/child@1.0</li>
 * </ul>
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Getter
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DynamicDefinitionId
{
	private String dataSetKey;
	private DynamicDefinitionId parent;
	private String definitionType;
	private String definitionKey;
	private String version;

	public boolean hasParent() {
		return parent != null;
	}

	public boolean isForVersion() {
		return version != null;
	}

	/**
	 * @return a new instance of the definition id with version information removed
	 */
	public DynamicDefinitionId asDefinitionId() {
		return create( parent, dataSetKey, definitionType, definitionKey, null );
	}

	/**
	 * Return a new instance of the definition id for the specific version.
	 *
	 * @param version to request
	 * @return a new instance of the definition id for the version specified
	 */
	public DynamicDefinitionId asDefinitionVersionId( String version ) {
		return create( parent, dataSetKey, definitionType, definitionKey, version );
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();

		if ( hasParent() ) {
			s.append( parent.toString() ).append( '/' );
		}
		else {
			s.append( dataSetKey ).append( ':' );
		}

		if ( !DynamicDefinitionTypes.DOCUMENT.equals( definitionType ) ) {
			s.append( definitionType ).append( ':' );
		}

		s.append( definitionKey );

		if ( isForVersion() ) {
			s.append( '@' ).append( version );
		}

		return s.toString();
	}

	/**
	 * Converts a full key string into an id.
	 *
	 * @param key
	 * @return key or {@code null} if not a valid definition id
	 */
	public static DynamicDefinitionId fromString( String key ) {
		if ( StringUtils.isNotEmpty( key ) ) {
			String[] keyAndVersion = StringUtils.split( key, "@" );

			DynamicDefinitionId id = null;

			String[] keys = StringUtils.split( keyAndVersion[0], "/" );

			for ( String segment : keys ) {
				id = parseSingleKey( segment, id );
			}

			if ( id != null && keyAndVersion.length > 1 ) {
				id.version = keyAndVersion[1];
			}

			return id;
		}

		return null;
	}

	private static DynamicDefinitionId parseSingleKey( String key, DynamicDefinitionId parent ) {
		String[] segments = StringUtils.split( key, ":" );

		if ( parent != null ) {
			if ( segments.length > 2 ) {
				return null;
			}

			String definitionType = segments.length == 2 ? segments[0] : null;
			String definitionKey = segments.length == 2 ? segments[1] : segments[0];

			return create( parent, null, definitionType, definitionKey, null );
		}
		else {
			if ( segments.length < 2 || segments.length > 3 ) {
				return null;
			}

			String dataSetKey = segments[0];
			String definitionType = segments.length == 3 ? segments[1] : null;
			String definitionKey = segments.length == 3 ? segments[2] : segments[1];

			return create( null, dataSetKey, definitionType, definitionKey, null );
		}
	}

	static DynamicDefinitionId create( DynamicDefinition parent,
	                                   DynamicDefinitionDataSet dataSet,
	                                   DynamicDefinitionType definitionType,
	                                   String key,
	                                   String version ) {
		DynamicDefinitionDataSet ds = dataSet != null ? dataSet : ( parent != null ? parent.getDataSet() : null );

		return create( parent != null ? parent.getDefinitionId() : null,
		               ds != null ? ds.getKey() : null,
		               definitionType != null ? definitionType.getName() : null,
		               key,
		               version
		);
	}

	static DynamicDefinitionId create( DynamicDefinitionId parent, String dataSetKey, String definitionType, String definitionKey, String version ) {
		if ( ( dataSetKey == null && parent == null ) || definitionKey == null ) {
			return null;
		}

		DynamicDefinitionId id = new DynamicDefinitionId();
		id.parent = parent != null && parent.isForVersion() ? parent.asDefinitionId() : parent;
		id.dataSetKey = parent != null ? parent.dataSetKey : dataSetKey;
		id.definitionType = definitionType != null ? definitionType : DynamicDefinitionTypes.DOCUMENT;
		id.definitionKey = definitionKey;
		id.version = version;

		return id;
	}
}
