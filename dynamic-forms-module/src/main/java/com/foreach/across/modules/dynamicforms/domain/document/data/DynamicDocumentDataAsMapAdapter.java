/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.MethodInvocationException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.ConverterNotFoundException;
import org.springframework.core.convert.TypeDescriptor;

import java.beans.PropertyChangeEvent;
import java.util.*;

/**
 * Adapter exposing a {@link DynamicDocumentData} instance as a flattened {@link Map}.
 * Useful when requiring a type that can be used with a {@link org.springframework.validation.DataBinder}.
 * <p/>
 * Note single value methods like {@link #get(Object)} and {@link #put(String, Object)} should not be any
 * slower than using the {@link DynamicDocumentData} directly, but others like {@link #keySet()},
 * {@link #containsKey(Object)}, {@link #containsValue(Object)} and {@link #size()} can be very slow as they
 * will first retrieve the entire flattened list of fields.
 * <p/>
 * When retrieving keys or values, only the distinctly registered fields will be returned.
 * It can be possible to set a different property path using {@link #put(String, Object)}, but not have
 * that path present in the key collection; for example for indexed fields.
 * <p/>
 * You can configure this adapter for databinding by setting a {@code binderPrefix}, and optionally
 * specifying a {@link ConversionService} that should be used for input type conversion.
 *
 * @author Arne Vandamme
 * @see DynamicDocumentData#asFlatMap()
 * @since 0.0.1
 * @deprecated prefer using an {@link com.foreach.across.modules.entity.bind.EntityPropertiesBinder}
 */
@RequiredArgsConstructor
public class DynamicDocumentDataAsMapAdapter implements Map<String, Object>
{
	/**
	 * -- GETTER --
	 * The underlying data that this map is operating on.
	 */
	@Getter
	private final DynamicDocumentData data;

	/**
	 * Prefix when the map is being used for data binding.
	 * If set and an exception occurs when setting a field value, it will be
	 * rethrown as a {@link ConversionNotSupportedException} with {@link PropertyChangeEvent} information.
	 * <p/>
	 * The binder prefix would usually be the name of a property for this adapter instance
	 * on the object being bound. For example:
	 * <pre>{@code
	 * public class MyObject {
	 *     public DynamicDocumentDataAsMapAdapter getDataMap() {}
	 * }
	 * }</pre>
	 * If a {@link org.springframework.validation.DataBinder} is used for  {@code MyObject}, then
	 * the correct {@code binderPrefix} to set would be {@code dataMap}.
	 */
	@Setter
	private String binderPrefix = "";

	/**
	 * Optionally set a {@link ConversionService} that should be used to convert the input
	 * value to the required field type. It no {@code ConversionService} is set, the actual value
	 * must match the expected field type or a {@link ClassCastException} will occur.
	 * <p/>
	 * If a {@code ConversionService} is set, type conversion will be attempted and exceptions
	 * will only be thrown if conversion failes.
	 */
	@Setter
	private ConversionService conversionService;

	@Override
	public int size() {
		return keySet().size();
	}

	@Override
	public boolean isEmpty() {
		return data.getContent().isEmpty();
	}

	@Override
	public boolean containsKey( Object key ) {
		return data.findValue( (String) key ) != null;
	}

	@Override
	public boolean containsValue( Object value ) {
		return values().contains( value );
	}

	@Override
	public Object get( Object key ) {
		return data.getValue( (String) key );
	}

	@Override
	public Object put( String key, Object value ) {
		try {
			if ( conversionService != null ) {
				val field = data.getFieldAccessor( key );
				val valueToSet = conversionService.convert( value, TypeDescriptor.forObject( value ), field.getFieldType() );
				return field.set( valueToSet );
			}

			return data.setValue( key, value );
		}
		catch ( DynamicDocumentDataAccessor.TypeAwareClassCastException cce ) {
			if ( !StringUtils.isEmpty( binderPrefix ) ) {
				PropertyChangeEvent pce = new PropertyChangeEvent( this, binderPrefix + "[" + key + "]", null, value );
				throw new ConversionNotSupportedException( pce, cce.getTargetType().getType(), cce );
			}
			throw cce;
		}
		catch ( IllegalArgumentException | DynamicDocumentDefinition.NoSuchFieldException nsfe ) {
			if ( !StringUtils.isEmpty( binderPrefix ) ) {
				PropertyChangeEvent pce = new PropertyChangeEvent( this, binderPrefix + "[" + key + "]", null, value );
				throw new MethodInvocationException( pce, nsfe );
			}
			throw nsfe;
		}
		catch ( ConversionFailedException | ConverterNotFoundException cfe ) {
			if ( !StringUtils.isEmpty( binderPrefix ) ) {
				Class<?> targetType = cfe instanceof ConversionFailedException
						? ( (ConversionFailedException) cfe ).getTargetType().getType()
						: ( (ConverterNotFoundException) cfe ).getTargetType().getType();
				PropertyChangeEvent pce = new PropertyChangeEvent( this, binderPrefix + "[" + key + "]", null, value );
				throw new ConversionNotSupportedException( pce, targetType, cfe );
			}
			throw cfe;
		}
	}

	@Override
	public Object remove( Object key ) {
		return data.deleteValue( (String) key );
	}

	@Override
	public void putAll( Map<? extends String, ?> members ) {
		members.forEach( this::put );
	}

	@Override
	public void clear() {
		data.getContent().clear();
	}

	@Override
	public Collection<Object> values() {
		Set<String> keys = keySet();
		List<Object> values = new ArrayList<>( keys.size() );
		keys.forEach( k -> values.add( data.getValue( k ) ) );
		return values;
	}

	@Override
	public Set<Entry<String, Object>> entrySet() {
		Set<String> keys = keySet();
		Set<Entry<String, Object>> entries = new LinkedHashSet<>();
		keys.forEach(
				k -> entries.add( new Entry<String, Object>()
				{
					@Override
					public String getKey() {
						return k;
					}

					@Override
					public Object getValue() {
						return data.getValue( k );
					}

					@Override
					public Object setValue( Object value ) {
						return data.setValue( k, value );
					}
				} )
		);
		return entries;
	}

	@Override
	public Set<String> keySet() {
		Set<String> keys = new LinkedHashSet<>();
		addFields( "", data.getDocumentDefinition().getContent(), keys );
		return keys;
	}

	private void addFields( String parentPath, List<DynamicDocumentFieldDefinition> fields, Set<String> keys ) {
		fields.forEach(
				field -> {
					String candidate = parentPath + field.getId();
					if ( field.getFields().isEmpty() && data.findValue( candidate ) != null ) {
						keys.add( candidate );
					}
					else {
						addFields( candidate + ".", field.getFields(), keys );
					}
				}
		);
	}
}
