/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.dynamicforms.domain.definition.*;
import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewDefinition;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

// todo: create a better service for this
@Service
@RequiredArgsConstructor
public class ViewDefinitionService
{
	private final DynamicViewDefinitionService viewDefinitionService;
	private final DynamicDefinitionRepository dynamicDefinitionRepository;

	public DynamicViewDefinition getViewDefinition( EntityViewRequest entityViewRequest ) {
		return getViewDefinitionByName( entityViewRequest, entityViewRequest.getViewName() );
	}

	public DynamicViewDefinition getViewDefinitionByName( EntityViewRequest entityViewRequest, String name ) {
		DynamicDefinition documentDefinition = entityViewRequest.getEntityViewContext().getParentContext().getEntity( DynamicDefinition.class );
		QDynamicDefinition dynamicDefinition = QDynamicDefinition.dynamicDefinition;

		return dynamicDefinitionRepository
				.findOne(
						dynamicDefinition.type.name.equalsIgnoreCase( DynamicDefinitionTypes.VIEW )
						                           .and( dynamicDefinition.name.eq( name ) )
						                           .and( dynamicDefinition.parent.eq( documentDefinition ) )
						                           .and( dynamicDefinition.dataSet.eq( documentDefinition.getDataSet() ) )
				)
				.map( definition -> viewDefinitionService.loadViewDefinition( definition.getLatestVersion() ) )
				.orElse( null );
	}
}
