/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import java.util.HashSet;
import java.util.Set;

/**
 * Helper class for field calculation, ensures there are no infinite loops.
 *
 * @author Arne Vandamme
 * @since 0.0.2
 */
class FieldCalculationStack
{
	private final Set<String> calculated = new HashSet<>();

	/**
	 * Executes instantly, but does nothing in case the same field has already been calculated.
	 * A field is flagged as calculated even before the task is executed.
	 */
	void execute( String fieldPath, Runnable task ) {
		boolean first = calculated.isEmpty();

		if ( !calculated.contains( fieldPath ) ) {
			calculated.add( fieldPath );
			task.run();
		}

		if ( first ) {
			calculated.clear();
		}
	}
}
