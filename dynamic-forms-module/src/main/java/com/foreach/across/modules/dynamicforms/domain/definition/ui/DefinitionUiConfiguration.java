/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.ui;

import com.foreach.across.modules.bootstrapui.elements.BootstrapUiElements;
import com.foreach.across.modules.bootstrapui.elements.ButtonViewElement;
import com.foreach.across.modules.bootstrapui.elements.TextareaFormElement;
import com.foreach.across.modules.bootstrapui.elements.builder.TableViewElementBuilder;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.definition.*;
import com.foreach.across.modules.dynamicforms.web.DynamicFormsModuleUiResources;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.conditionals.ConditionalOnAdminWeb;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.registry.EntityAssociation;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.bootstrapui.options.OptionIterableBuilder;
import com.foreach.across.modules.entity.views.processors.DetailFormViewProcessor;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SaveEntityViewProcessor;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.entity.web.links.EntityViewLinks;
import com.foreach.across.modules.entity.web.links.SingleEntityViewLinkBuilder;
import com.foreach.across.modules.web.resource.WebResourceRegistry;
import com.foreach.across.modules.web.ui.IteratorViewElementBuilderContext;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.HtmlViewElement;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.foreach.across.modules.bootstrapui.styles.BootstrapStyles.css;
import static com.foreach.across.modules.bootstrapui.ui.factories.BootstrapViewElements.bootstrap;
import static com.foreach.across.modules.dynamicforms.DynamicFormsModuleIcons.dynamicFormsModuleIcons;
import static com.foreach.across.modules.dynamicforms.domain.definition.ui.ModifyDocumentDefinitionViewProcessor.DRAFT_VERSION;
import static com.foreach.across.modules.web.ui.elements.HtmlViewElements.html;
import static org.springframework.core.convert.TypeDescriptor.collection;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

/**
 * Configures EntityModule screens for definition related entities.
 */
@Configuration
@RequiredArgsConstructor
@ConditionalOnAdminWeb
class DefinitionUiConfiguration implements EntityConfigurer
{
	private final DynamicDefinitionVersionYamlJsonViewAdapter dynamicDefinitionVersionYamlJsonViewAdapter;
	private final EntityViewElementBuilderHelper builderHelper;
	private final DynamicDefinitionVersionRepository definitionVersionRepository;
	private final ModifyDocumentDefinitionViewProcessor modifyDocumentDefinitionViewProcessor;
	private final ReadDocumentDefinitionViewProcessor readDocumentDefinitionViewProcessor;
	private final EntityViewLinks entityViewLinks;
	private final ReadDefinitionVersionViewProcessor readDefinitionVersionViewProcessor;
	private final DynamicDefinitionRepository definitionRepository;
	private final SaveEntityViewProcessor saveEntityViewProcessor;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.withType( DynamicDefinition.class )
		        .attribute( EntityAttributes.LINK_TO_DETAIL_VIEW, true )
		        .properties( props -> props.property( EntityPropertyRegistry.LABEL ).valueFetcher( new DynamicDefinitionLabelPrinter() ) )
		        .association(
				        ab -> ab.name( "dynamicDefinitionVersion.definition" )
				                .parentDeleteMode( EntityAssociation.ParentDeleteMode.IGNORE )
		        )
		        .hide();

		entities.withType( DynamicDefinitionType.class ).hide();

		// todo: should be hidden as well, only accessible through dataset -> definition views
		entities.withType( DynamicDefinitionVersion.class )
		        .listView( lvb -> lvb.showProperties( ".", "createdDate" ) )
		        .createFormView( vb -> vb.viewProcessor( dynamicDefinitionVersionYamlJsonViewAdapter ) )
		        .properties( p -> p
				        .property( EntityPropertyRegistry.LABEL )
				        .valueFetcher( new DynamicDefinitionLabelPrinter() )
				        .and()
				        .property( "definitionContent" )
				        .viewElementPostProcessor( ViewElementMode.CONTROL, ( builderContext, element ) -> {
					        ( (HtmlViewElement) element ).addCssClass( "js-content-yaml" );
					        builderContext.getAttribute( WebResourceRegistry.class ).addPackage( DynamicFormsModuleUiResources.NAME );
				        } )
				        .viewElementType( ViewElementMode.VALUE, BootstrapUiElements.TEXTAREA )
				        .viewElementPostProcessor( ViewElementMode.VALUE, ( builderContext, element ) -> {
					        TextareaFormElement typed = (TextareaFormElement) element;
					        typed.addCssClass( "js-content-yaml", "js-readonly" );
					        builderContext.getAttribute( WebResourceRegistry.class )
					                      .addPackage( DynamicFormsModuleUiResources.NAME );
				        } )
		        )
		        .association( ab -> ab.name( "dynamicDefinition.latestVersion" )
		                              .targetEntityType( DynamicDefinition.class )
		                              .hidden( true )
		        );

		entities.withType( DynamicDefinitionDataSet.class )
		        .association(
				        a -> a.name( "dynamicDefinition.dataSet" )
				              .show()
				              .associationType( EntityAssociation.Type.EMBEDDED )
				              .listView( lvb -> lvb.showProperties( "type", "name", "fullKey", "parent", "latestVersion.version", "lastModified" ) )
				              .createFormView( fvb -> fvb.showProperties( ".", "~latestVersion" )
				                                         .properties(
						                                         props -> props.property( "parent" )
						                                                       .attribute( EntityAttributes.OPTIONS_ALLOWED_VALUES,
						                                                                   getParentDefinitionOptions() ) )
				              )
				              .detailView(
						              fvb -> fvb.showProperties( "type", "name", "parent", "fullKey", "latestVersion", "versions" )
						                        .properties(
								                        props -> props.property( "type" ).writable( false )
								                                      .and().property( "versions" )
								                                      .propertyType( collection( Set.class, valueOf( DynamicDefinitionVersion.class ) ) )
								                                      .viewElementBuilder( ViewElementMode.FORM_READ, this::buildRelatedVersions )
								                                      .and().property( "parent" )
								                                      .attribute( EntityAttributes.OPTIONS_ALLOWED_VALUES, getParentDefinitionOptions() )
						                        ).postProcess( SingleEntityFormViewProcessor.class, p -> p.setAddDefaultButtons( false ) )
						                        .removeViewProcessor( DetailFormViewProcessor.class.getName() )
						                        .viewProcessor( saveEntityViewProcessor )
						                        .viewProcessor( readDocumentDefinitionViewProcessor )
				              )
				              .updateFormView(
						              fvb -> fvb.postProcess( SingleEntityFormViewProcessor.class, p -> p.setAddDefaultButtons( true ) )
						                        .viewProcessor( modifyDocumentDefinitionViewProcessor )
						                        .showProperties( "type", "name", "parent", "fullKey" )
						                        .properties(
								                        props -> props.property( "type" ).writable( false )
								                                      .and().property( "parent" )
								                                      .attribute( EntityAttributes.OPTIONS_ALLOWED_VALUES, getParentDefinitionOptions() )
						                        )
				              )
				              .formView( "readDefinitionVersion",
				                         view -> view.viewElementMode( ViewElementMode.FORM_READ )
				                                     .viewProcessor( readDefinitionVersionViewProcessor )
				                                     .showProperties( "type", "name", "parent", "fullKey" )
				                                     .viewProcessor( new EntityViewProcessorAdapter()
				                                     {
					                                     @Override
					                                     protected void postRender( EntityViewRequest entityViewRequest,
					                                                                EntityView entityView,
					                                                                ContainerViewElement container,
					                                                                ViewElementBuilderContext builderContext ) {
						                                     ContainerViewElementUtils.remove( container, "btn-save" );
						                                     ContainerViewElementUtils.find( container, "btn-cancel", ButtonViewElement.class )
						                                                              .ifPresent( btn -> btn.set( css.button.outline.secondary ) );
					                                     }
				                                     } )
				              )
		        );
	}

	private OptionIterableBuilder getParentDefinitionOptions() {
		return builderContext -> {
			DynamicDefinition entity =
					EntityViewElementUtils.currentEntity( builderContext,
					                                      DynamicDefinition.class );
			return definitionRepository.findByDataSet( entity.getDataSet() )
			                           .stream()
			                           .filter( definition -> !Objects
					                           .equals( entity.getId(),
					                                    definition.getId() ) )
			                           .map( definition -> bootstrap.builders
					                           .option()
					                           .rawValue( definition )
					                           .value( definition.getId() )
					                           .text( definition.getName() )
					                           .controlName( "entity.parent" )
					                           .selected( definition.equals( entity.getParent() ) )
			                           )
			                           .collect( Collectors.toList() );

		};
	}

	/**
	 * Creates a {@link ViewElement} for all the versions related to a specific {@link DynamicDefinition}
	 */
	private ViewElement buildRelatedVersions( ViewElementBuilderContext builderContext ) {
		DynamicDefinition entity = EntityViewElementUtils.currentEntity( builderContext, DynamicDefinition.class );
		String urlToDefinition = entityViewLinks.linkTo( entity.getDataSet() )
		                                        .association( entity )
		                                        .toUriString();
		List<DynamicDefinitionVersion> versions = definitionVersionRepository.findByDefinitionOrderByCreatedDateDesc( entity );
		return html.builders.div().name( "related-versions" )
		                    .add( builderHelper.createSortableTableBuilder( DynamicDefinitionVersion.class )
		                                       .properties( "version", "published" )
		                                       .noSorting()
		                                       .hideResultNumber()
		                                       .items( versions )
		                                       .headerRowProcessor( ( ctx, row ) -> row.addChild( bootstrap.builders.table.headerCell().build( ctx ) ) )
		                                       .valueRowProcessor( ( ctx, row ) -> {
			                                       IteratorViewElementBuilderContext context = (IteratorViewElementBuilderContext) ctx;

			                                       DynamicDefinitionVersion definitionVersion =
					                                       (DynamicDefinitionVersion) context.getItem();

			                                       String linkToDefinitionDetails = entityViewLinks
					                                       .linkTo( entity.getDataSet() )
					                                       .association( entity )
					                                       .withFromUrl( urlToDefinition )
					                                       .withViewName( "readDefinitionVersion" )
					                                       .withQueryParam( "id", definitionVersion.getId() )
					                                       .toUriString();
			                                       TableViewElementBuilder.Cell actions = bootstrap.builders.table.cell().name( "actions" )
			                                                                                                      .add( bootstrap.builders.button()
			                                                                                                                              .icon( dynamicFormsModuleIcons
					                                                                                                                                     .view() )
			                                                                                                                              .iconOnly()
			                                                                                                                              .link( linkToDefinitionDetails )
			                                                                                                      );

			                                       if ( DRAFT_VERSION.equals( definitionVersion.getVersion() ) && !definitionVersion.isPublished() ) {
				                                       SingleEntityViewLinkBuilder linkToEntityAssociation = entityViewLinks.linkTo( entity.getDataSet() )
				                                                                                                            .association( entity );
				                                       String cancelUrl = linkToEntityAssociation.updateView().toUriString();
				                                       String updateUrl = linkToEntityAssociation.toUriString() + "?from=" + cancelUrl;

				                                       actions.add( bootstrap.builders.button().icon( dynamicFormsModuleIcons.edit() )
				                                                                      .iconOnly()
				                                                                      .link( updateUrl ) );
			                                       }

			                                       row.getChildren().add( actions.build( ctx ) );
		                                       } )
		                                       .build() )
		                    .build( builderContext );
	}
}
