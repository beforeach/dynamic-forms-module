/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.bootstrapui.elements.NumericFormElementConfiguration;
import com.foreach.across.modules.bootstrapui.elements.TextboxFormElement;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.val;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A {@link DynamicTypeDefinitionBuilder} for the following standard types:
 * <ul>
 * <li>string</li>
 * <li>text</li>
 * <li>boolean</li>
 * <li>number</li>
 * </ul>
 * <p/>
 * This builder usually comes last.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@Order
@RequiredArgsConstructor
public class SimpleDynamicTypeDefinitionBuilder implements DynamicTypeDefinitionBuilder
{
	private static final Pattern ARGUMENT_PATTERN = Pattern.compile(
			"(number)\\(([a-z0-9]+)\\)|(decimal)\\((\\d+)\\)|(currency)\\(([a-zA-Z]{3}),?\\(?(\\d?)\\)?\\)|(percentage)\\((\\d+)(,\\d+)?\\)$" );
	private final Map<String, TypeDescriptor> baseTypes = new HashMap<String, TypeDescriptor>()
	{{
		put( "char", TypeDescriptor.valueOf( Character.class ) );
		put( "string", TypeDescriptor.valueOf( String.class ) );
		put( "text", TypeDescriptor.valueOf( String.class ) );
		put( "boolean", TypeDescriptor.valueOf( Boolean.class ) );
		put( "number", TypeDescriptor.valueOf( Long.class ) );
		put( "decimal", TypeDescriptor.valueOf( BigDecimal.class ) );
		put( "currency", TypeDescriptor.valueOf( BigDecimal.class ) );
		put( "percentage", TypeDescriptor.valueOf( BigDecimal.class ) );
		put( "time", TypeDescriptor.valueOf( LocalTime.class ) );
		put( "date", TypeDescriptor.valueOf( LocalDate.class ) );
		put( "datetime", TypeDescriptor.valueOf( LocalDateTime.class ) );
	}};

	@Override
	public boolean accepts( RawDocumentDefinition.AbstractField field ) {
		String type = strippedFromArray( field.getType() );
		return baseTypes.containsKey( type ) || ARGUMENT_PATTERN.matcher( type ).matches();
	}

	@Override
	public DynamicTypeDefinition buildTypeDefinition( DynamicTypeDefinitionFactory factory, RawDocumentDefinition.AbstractField field ) {
		GenericDynamicTypeDefinition.GenericDynamicTypeDefinitionBuilder builder = createTypeDefinitionBuilder( field );
		return builder.build();
	}

	@SneakyThrows
	GenericDynamicTypeDefinition.GenericDynamicTypeDefinitionBuilder createTypeDefinitionBuilder( RawDocumentDefinition.AbstractField field ) {
		val builder = GenericDynamicTypeDefinition
				.builder()
				.typeName( field.getType() );

		String strippedFieldType = strippedFromArray( field.getType() );
		TypeDescriptor baseType;
		Matcher matcher = ARGUMENT_PATTERN.matcher( strippedFieldType );
		if ( matcher.matches() ) {
			if ( StringUtils.startsWith( field.getType(), "number(" ) ) {
				baseType = TypeDescriptor.valueOf(
						ClassUtils.getClass( StringUtils.prependIfMissing( StringUtils.capitalize( matcher.group( 2 ) ), "java.lang.", "java.lang." ) ) );
			}
			else if ( StringUtils.startsWith( field.getType(), "currency(" ) ) {
				baseType = baseTypes.get( matcher.group( 5 ) );
			}
			else if ( StringUtils.startsWith( field.getType(), "percentage(" ) ) {
				baseType = baseTypes.get( matcher.group( 8 ) );
			}
			else {
				baseType = baseTypes.get( matcher.group( 3 ) );
			}
		}
		else {
			baseType = baseTypes.get( strippedFieldType );
		}
		if ( isArray( field.getType() ) ) {
			builder.typeDescriptor( TypeDescriptor.collection( ArrayList.class, baseType ) );
		}
		else {
			builder.typeDescriptor( baseType );
		}

		builder.propertyDescriptorBuilderConsumer( createPropertyDescriptorConsumer( field ) );

		return builder;
	}

	private String strippedFromArray( String fieldType ) {
		return StringUtils.stripEnd( fieldType, "[]" );
	}

	private boolean isArray( String fieldType ) {
		return StringUtils.endsWith( fieldType, "[]" );
	}

	private BiConsumer<DynamicDocumentFieldDefinition, EntityPropertyDescriptorBuilder> createPropertyDescriptorConsumer( RawDocumentDefinition.AbstractField field ) {
		if ( StringUtils.startsWith( field.getType(), "decimal" ) ) {
			Matcher matcher = ARGUMENT_PATTERN.matcher( strippedFromArray( field.getType() ) );
			if ( matcher.matches() ) {
				String precision = matcher.group( 4 );
				NumericFormElementConfiguration formElementConfiguration = new NumericFormElementConfiguration();
				formElementConfiguration.setDecimalPositions( Integer.parseInt( precision ) );
				return ( fieldDef, descriptorBuilder ) -> descriptorBuilder.attribute( NumericFormElementConfiguration.class, formElementConfiguration );
			}
			else {
				return ( fieldDef, descriptorBuilder ) -> descriptorBuilder.attribute( NumericFormElementConfiguration.class,
				                                                                       new NumericFormElementConfiguration() );
			}
		}
		if ( StringUtils.startsWith( field.getType(), "currency" ) ) {
			Matcher matcher = ARGUMENT_PATTERN.matcher( strippedFromArray( field.getType() ) );
			if ( matcher.matches() ) {
				String precision = StringUtils.defaultIfBlank( matcher.group( 7 ), "2" );
				NumericFormElementConfiguration formElementConfiguration = new NumericFormElementConfiguration();
				formElementConfiguration.setCurrency( Currency.getInstance( StringUtils.upperCase( matcher.group( 6 ) ) ) );
				formElementConfiguration.setDecimalPositions( Integer.parseInt( precision ) );
				return ( fieldDef, descriptorBuilder ) -> descriptorBuilder.attribute( NumericFormElementConfiguration.class, formElementConfiguration );
			}
			else {
				return ( fieldDef, descriptorBuilder ) -> descriptorBuilder.attribute( NumericFormElementConfiguration.class,
				                                                                       new NumericFormElementConfiguration() );
			}
		}
		if ( StringUtils.startsWith( field.getType(), "percentage" ) ) {
			Matcher matcher = ARGUMENT_PATTERN.matcher( strippedFromArray( field.getType() ) );
			if ( matcher.matches() ) {
				String multiplier = StringUtils.defaultIfBlank( matcher.group( 9 ), "1" );
				String precision = StringUtils.defaultIfBlank( matcher.group( 10 ), ",0" ).substring( 1 );
				NumericFormElementConfiguration formElementConfiguration = new NumericFormElementConfiguration();
				formElementConfiguration.setFormat( NumericFormElementConfiguration.Format.PERCENT );
				formElementConfiguration.setDecimalPositions( Integer.valueOf( precision ) );
				formElementConfiguration.setMultiplier( Integer.valueOf( multiplier ) );
				return ( fieldDef, descriptorBuilder ) -> descriptorBuilder.attribute( NumericFormElementConfiguration.class, formElementConfiguration );
			}
			else {
				NumericFormElementConfiguration formElementConfiguration = new NumericFormElementConfiguration();
				formElementConfiguration.setFormat( NumericFormElementConfiguration.Format.PERCENT );
				formElementConfiguration.setDecimalPositions( 0 );
				formElementConfiguration.setMultiplier( 1 );
				return ( fieldDef, descriptorBuilder ) -> descriptorBuilder.attribute( NumericFormElementConfiguration.class, formElementConfiguration );
			}
		}
		if ( StringUtils.startsWith( field.getType(), "string" ) || StringUtils.startsWith( field.getType(), "char" ) ) {
			return ( fieldDef, descriptorBuilder ) -> descriptorBuilder.attribute( TextboxFormElement.Type.class, TextboxFormElement.Type.TEXT );
		}

		return null;
	}
}
