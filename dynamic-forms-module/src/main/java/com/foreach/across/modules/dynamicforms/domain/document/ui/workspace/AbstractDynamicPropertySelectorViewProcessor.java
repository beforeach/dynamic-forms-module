/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewDefinition;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import org.springframework.beans.factory.annotation.Autowired;

class AbstractDynamicPropertySelectorViewProcessor extends EntityViewProcessorAdapter
{
	private ViewDefinitionService viewDefinitionService;

	String[] retrieveDynamicPropertyNames( EntityViewRequest entityViewRequest ) {
		DynamicViewDefinition definition = viewDefinitionService.getViewDefinition( entityViewRequest );

		if ( definition != null ) {
			return definition.getProperties().toArray( new String[0] );
		}
		else {
			return null;
		}
	}

	@Autowired
	public void setViewDefinitionService( ViewDefinitionService viewDefinitionService ) {
		this.viewDefinitionService = viewDefinitionService;
	}
}
