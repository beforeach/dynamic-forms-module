/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.context.MessageSource;

import java.util.Collections;
import java.util.List;

/**
 * Represents the definition of a single document.
 * <p/>
 * A {@code DynamicDocumentDefinition} should not be built directly but by creating a
 * {@link RawDocumentDefinition} and converting
 * it into a {@code DynamicDocumentDefinition} using a {@link DynamicDocumentDefinitionBuilder}.
 * The latter will perform validation of your definition, ensuring it is usable.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Setter(AccessLevel.PACKAGE)
@Accessors(chain = true)
public class DynamicDocumentDefinition
{
	@Getter
	private final String name;

	@Getter
	private final DynamicDefinitionId definitionId;

	@Getter
	private final List<DynamicDocumentFieldDefinition> content;

	@Getter
	private EntityPropertyRegistry entityPropertyRegistry;

	/**
	 * @return message source holding the custom messages for this document
	 */
	private DynamicDocumentDefinitionMessageSource messageSource;

	private RawDocumentDefinition rawDocumentDefinition;

	DynamicDocumentDefinition( @NonNull String name, DynamicDefinitionId definitionId, @NonNull List<DynamicDocumentFieldDefinition> content ) {
		this.name = name;
		this.definitionId = definitionId;
		this.content = Collections.unmodifiableList( content );
	}

	/**
	 * @return message source holding the custom messages for this document
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	void setMessageSource( DynamicDocumentDefinitionMessageSource messageSource ) {
		this.messageSource = messageSource;
	}

	void setRawDocumentDefinition( @NonNull RawDocumentDefinition rawDocumentDefinition ) {
		this.rawDocumentDefinition = RawDocumentDefinition.copyOf( rawDocumentDefinition );
	}

	/**
	 * Create a new blank set of document data, attached to this definition.
	 *
	 * @return new document
	 */
	public DynamicDocumentData createDocumentData() {
		return new DynamicDocumentData( this );
	}

	/**
	 * @return a raw definition version
	 */
	public RawDocumentDefinition asRawDefinition() {
		return RawDocumentDefinition.copyOf( rawDocumentDefinition );
	}

	/**
	 * Create a message source that first looks in the definition message source,
	 * but falls back to another defined message source if a message is not found.
	 *
	 * @param fallback message source, can be {@code null}
	 * @return new message source instance;
	 */
	public MessageSource createMessageSource( MessageSource fallback ) {
		return messageSource.withParentMessageSource( fallback );
	}

	/**
	 * Exception thrown in case a field path was requested that does not exist.
	 */
	public static class NoSuchFieldException extends RuntimeException
	{
		@Getter
		private final String fieldName;

		public NoSuchFieldException( String fieldName ) {
			super( "No field '" + fieldName + "' present in the document definition" );
			this.fieldName = fieldName;
		}
	}

	/**
	 * Thrown when an illegal field configuration has been detected.
	 */
	public static class IllegalFieldConfigurationException extends RuntimeException
	{
		@Getter
		private final String fieldName;

		@Getter
		private final String detailedMessage;

		public IllegalFieldConfigurationException( String fieldName, String message ) {
			super( "Illegal configuration for field '" + fieldName + "': " + message );
			this.fieldName = fieldName;
			this.detailedMessage = message;
		}
	}

	/**
	 * Exception thrown when an illegal field id is being specified.
	 */
	public static class IllegalFieldIdException extends RuntimeException
	{
		@Getter
		private final String fieldName;

		public IllegalFieldIdException( String fieldName ) {
			super( "Illegal field id: '" + fieldName + "', only alphanumeric characters, dashes and underscores are allowed" );
			this.fieldName = fieldName;
		}
	}

	/**
	 * Exception thrown when an illegal field type is being specified.
	 */
	public static class IllegalFieldTypeException extends RuntimeException
	{
		@Getter
		private final String fieldType;

		public IllegalFieldTypeException( String fieldType ) {
			super( "Illegal field type: '" + fieldType + "', could not resolve or create a matching type definition" );
			this.fieldType = fieldType;
		}
	}

	/**
	 * Exception thrown when an illegal field type is being specified.
	 */
	public static class CircularFieldTypeException extends RuntimeException
	{
		public CircularFieldTypeException( RawDocumentDefinition.AbstractField field, RawDocumentDefinition.AbstractField referenceField ) {
			super( "Circular field reference: '" + field.getId() + "'. It points back to field: '" + referenceField.getId() + "' which is already defined." );
		}
	}
}
