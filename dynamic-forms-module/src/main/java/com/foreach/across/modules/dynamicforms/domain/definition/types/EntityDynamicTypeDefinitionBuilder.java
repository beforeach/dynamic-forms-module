/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.function.BiConsumer;

/**
 * A {@link DynamicTypeDefinitionBuilder} for a type that refers to another entity
 * registered in the {@link com.foreach.across.modules.entity.registry.EntityRegistry}.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class EntityDynamicTypeDefinitionBuilder implements DynamicTypeDefinitionBuilder
{
	/**
	 * Contains the {@link com.foreach.across.modules.entity.EntityAttributes#OPTIONS_ENTITY_QUERY}.
	 */
	public static final String ATTR_OPTIONS_QUERY = "options-query";

	private final EntityRegistry entityRegistry;

	@Override
	public boolean accepts( RawDocumentDefinition.AbstractField field ) {
		if ( StringUtils.startsWith( field.getType(), "@" ) ) {
			String type = StringUtils.stripEnd( field.getType().substring( 1 ), "[]" );
			return entityRegistry.contains( type );
		}
		return false;
	}

	@Override
	public DynamicTypeDefinition buildTypeDefinition( DynamicTypeDefinitionFactory factory, RawDocumentDefinition.AbstractField field ) {
		String entityType = StringUtils.stripEnd( field.getType().substring( 1 ), "[]" );
		val builder = GenericDynamicTypeDefinition
				.builder()
				.typeName( field.getType() );

		Class<?> entityClass = entityRegistry.getEntityConfiguration( entityType ).getEntityType();

		if ( StringUtils.endsWith( field.getType(), "[]" ) ) {
			builder.typeDescriptor( TypeDescriptor.collection( LinkedHashSet.class, TypeDescriptor.valueOf( entityClass ) ) );
		}
		else {
			builder.typeDescriptor( TypeDescriptor.valueOf( entityClass ) );
		}

		return builder.propertyDescriptorBuilderConsumer( createDescriptorConsumer( field ) ).build();
	}

	private BiConsumer<DynamicDocumentFieldDefinition, EntityPropertyDescriptorBuilder> createDescriptorConsumer( RawDocumentDefinition.AbstractField field ) {
		return ( fieldDefinition, descriptor ) -> {
			if ( field.hasAttribute( ATTR_OPTIONS_QUERY ) ) {
				descriptor.attribute( EntityAttributes.OPTIONS_ENTITY_QUERY, field.getAttributes().get( ATTR_OPTIONS_QUERY ) );
			}
		};
	}

}
