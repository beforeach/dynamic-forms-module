/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;

import java.util.HashMap;
import java.util.function.Supplier;

@Component
public class YamlJsonObjectMapper
{
	private final ObjectMapper objectMapper = new ObjectMapper();
	private final Yaml yaml;

	public YamlJsonObjectMapper() {
		DumperOptions dumperOptions = new DumperOptions();
		dumperOptions.setDefaultFlowStyle( DumperOptions.FlowStyle.BLOCK );

		yaml = new Yaml( dumperOptions );
	}

	public String yamlToJson( Supplier<String> consumer ) throws JsonProcessingException, YAMLException {
		Object o = yaml.load( consumer.get() );
		if ( o == null ) {
			throw new YAMLException( "Value cannot be empty" );
		}
		else {
			return objectMapper.writeValueAsString( o );
		}
	}

	@SneakyThrows
	public String jsonToYaml( Supplier<String> consumer ) {
		return yaml.dump( objectMapper.readValue( consumer.get(), HashMap.class ) );
	}
}
