/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.processors.PropertyRenderingViewProcessor;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.WebDataBinder;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class DynamicPropertySelectorFormViewProcessor extends AbstractDynamicPropertySelectorViewProcessor
{
	@Override
	public void initializeCommandObject( EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder ) {
		String[] propertyNames = retrieveDynamicPropertyNames( entityViewRequest );

		if ( propertyNames != null ) {
			Map<String, EntityPropertyDescriptor> propertyDescriptors = PropertyRenderingViewProcessor.propertyDescriptors( entityViewRequest );
			propertyDescriptors.clear();
			EntityPropertySelector selector = EntityPropertySelector.of( propertyNames );
			entityViewRequest.getEntityViewContext().getPropertyRegistry().select( selector )
			                 .forEach( p -> propertyDescriptors.put( p.getName(), p ) );
		}
	}
}
