/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import lombok.NonNull;

import java.util.Map;

/**
 * Interface providing write-only access to document data fields.
 *
 * @author Arne Vandamme
 * @see DynamicDocumentData
 * @see DynamicDocumentDataLoader
 * @since 0.0.1
 */
public interface DynamicDocumentDataFieldWriter
{
	/**
	 * Set a single field value.
	 *
	 * @param field to set
	 * @param value to set
	 */
	void setFieldValue( String field, Object value );

	/**
	 * Set field values represented in a map hierarchy.
	 *
	 * @param data to set
	 */
	void setFieldValues( Map<String, Object> data );

	/**
	 * Create a new field writer for direct access to a {@link DynamicDocumentData} instance.
	 *
	 * @param documentData instance
	 * @return new field writer
	 */
	static DynamicDocumentDataFieldWriter forDocumentData( @NonNull DynamicDocumentData documentData ) {
		return new DynamicDocumentDataFieldWriter()
		{
			@Override
			public void setFieldValue( String field, Object value ) {
				documentData.setValue( field, value );
			}

			@Override
			public void setFieldValues( Map<String, Object> data ) {
				documentData.load( data );
			}
		};
	}
}
