/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public interface DynamicDefinitionRepository extends IdBasedEntityJpaRepository<DynamicDefinition>, QuerydslPredicateExecutor<DynamicDefinition>
{
	Optional<DynamicDefinition> findOneByDataSetAndParentAndName( DynamicDefinitionDataSet dataSet, DynamicDefinition parent, String name );

	List<DynamicDefinition> findByDataSetAndParent( DynamicDefinitionDataSet dataSet, DynamicDefinition parent );

	List<DynamicDefinition> findByDataSet( DynamicDefinitionDataSet dataSet );

	//TODO is this method necessary?
	List<DynamicDefinition> findByDataSetAndName( DynamicDefinitionDataSet dataSet, String name );

	@Override
	List<DynamicDefinition> findAll( Predicate predicate );

	@Override
	List<DynamicDefinition> findAll( Predicate predicate, Sort sort );

	@Override
	List<DynamicDefinition> findAll( Predicate predicate, OrderSpecifier<?>... orders );

	@Override
	List<DynamicDefinition> findAll( OrderSpecifier<?>... orders );
}
