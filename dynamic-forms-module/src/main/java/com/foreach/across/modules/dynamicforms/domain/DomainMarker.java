package com.foreach.across.modules.dynamicforms.domain;

/**
 * A simple marker interface to use for scanning the domain models.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
public interface DomainMarker {
}
