/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui;

import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.entity.views.support.ValueFetcher;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class DynamicDocumentLabelPrinter implements ValueFetcher<Object>
{
	@Override
	public Object getValue( Object documentOrVersion ) {
		if ( documentOrVersion instanceof DynamicDocument ) {
			return printDocumentLabel( (DynamicDocument) documentOrVersion );
		}
		if ( documentOrVersion instanceof DynamicDocumentVersion ) {
			return printDocumentLabel( (DynamicDocumentVersion) documentOrVersion );
		}
		return null;
	}

	private String printDocumentLabel( DynamicDocument document ) {
		return document.getName();
	}

	private String printDocumentLabel( DynamicDocumentVersion documentVersion ) {
		return documentVersion.getDocument().getName() + " (v " + documentVersion.getVersion() + ")";
	}
}
