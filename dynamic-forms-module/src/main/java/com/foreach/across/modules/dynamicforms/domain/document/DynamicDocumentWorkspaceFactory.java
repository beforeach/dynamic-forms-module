/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;

/**
 * Factory interface for creating {@link DynamicDocumentWorkspace} instances.
 *
 * @author Arne Vandamme
 * @see DynamicDocumentService
 * @see CachingDynamicDocumentWorkspaceFactory
 * @since 0.0.1
 */
public interface DynamicDocumentWorkspaceFactory
{
	/**
	 * Create a workspace for a new document using the latest version of the specified document definition.
	 *
	 * @param documentDefinition definition
	 * @return document workspace - with a new blank document
	 */
	DynamicDocumentWorkspace createDocumentWorkspace( DynamicDefinition documentDefinition );

	/**
	 * Create a workspace for a new document using the specific version of the document definition.
	 *
	 * @param documentDefinitionVersion version of the definition that should be used
	 * @return document workspace - with a new blank document
	 */
	DynamicDocumentWorkspace createDocumentWorkspace( DynamicDefinitionVersion documentDefinitionVersion );

	/**
	 * Create a workspace for an existing document, initialized with the latest version of that document if there is one.
	 *
	 * @param document to create the workspace for
	 * @return document workspace
	 */
	DynamicDocumentWorkspace createDocumentWorkspace( DynamicDocument document );

	/**
	 * Create a workspace for an existing document version.
	 *
	 * @param documentVersion that should be loaded in the workspace
	 * @return document workspace
	 */
	DynamicDocumentWorkspace createDocumentWorkspace( DynamicDocumentVersion documentVersion );
}
