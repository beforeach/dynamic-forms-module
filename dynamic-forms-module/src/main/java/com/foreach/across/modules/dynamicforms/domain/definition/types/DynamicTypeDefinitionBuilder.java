/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;

/**
 * Represents a builder for a single type definition.
 * A {@link DynamicTypeDefinitionFactory} will iterate through the list of builders,
 * and use {@link #accepts(RawDocumentDefinition.AbstractField)} to check which builder applies
 * for a given field. The default implementation will always use the first matching builder.
 *
 * @author Arne Vandamme
 * @see DynamicTypeDefinition
 * @see DynamicTypeDefinitionFactory
 * @since 0.0.1
 */
public interface DynamicTypeDefinitionBuilder
{
	/**
	 * Does this builder apply for the field specified?
	 *
	 * @param field to check
	 * @return true if builder should be used
	 */
	boolean accepts( RawDocumentDefinition.AbstractField field );

	/**
	 * Build a specific type definition for a field.
	 *
	 * @param factory providing access to base types
	 * @param field   from which to create the type
	 * @return type definition (never {@code null})
	 */
	DynamicTypeDefinition buildTypeDefinition( DynamicTypeDefinitionFactory factory, RawDocumentDefinition.AbstractField field );
}
