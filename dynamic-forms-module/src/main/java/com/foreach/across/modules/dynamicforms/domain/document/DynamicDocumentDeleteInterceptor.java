/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.hibernate.aop.EntityInterceptorAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Responsible for building the cascading full-delete of a {@link DynamicDocument}.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
class DynamicDocumentDeleteInterceptor extends EntityInterceptorAdapter<DynamicDocument>
{
	private final DynamicDocumentRepository documentRepository;
	private final DynamicDocumentVersionRepository documentVersionRepository;

	@Override
	public boolean handles( Class<?> entityClass ) {
		return DynamicDocument.class.isAssignableFrom( entityClass );
	}

	@Override
	public void beforeDelete( DynamicDocument document ) {
		document.setLatestVersion( null );

		documentRepository.save( document );
		documentVersionRepository.deleteAll( documentVersionRepository.findAllByDocumentOrderByLastModifiedDateDesc( document ) );
	}
}
