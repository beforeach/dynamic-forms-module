/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.entity.validators.EntityValidatorSupport;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.HashSet;

/**
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class DynamicDefinitionValidator extends EntityValidatorSupport<DynamicDefinition>
{
	private final DynamicDefinitionRepository repository;

	@Override
	public boolean supports( Class<?> aClass ) {
		return DynamicDefinition.class.isAssignableFrom( aClass );
	}

	@Override
	protected void postValidation( DynamicDefinition entity, Errors errors, Object... validationHints ) {
		QDynamicDefinition definition = QDynamicDefinition.dynamicDefinition;

		detectCyclicDependencies( entity, errors );

		if ( !errors.hasFieldErrors( "name" ) && !errors.hasFieldErrors( "dataSet" ) ) {
			BooleanExpression uniqueName = definition.name.equalsIgnoreCase( entity.getName() );

			if ( entity.hasParent() ) {
				uniqueName = uniqueName.and( definition.parent.eq( entity.getParent() ) );
			}
			else {
				uniqueName = uniqueName.and( definition.parent.isNull() )
				                       .and( definition.dataSet.eq( entity.getDataSet() ) );
			}

			if ( !entity.isNew() ) {
				uniqueName = uniqueName.and( definition.id.ne( entity.getId() ) );
			}

			if ( repository.count( uniqueName ) > 0 ) {
				errors.rejectValue( "name", "alreadyExists" );
			}
		}

		if ( !errors.hasFieldErrors( "key" ) && !errors.hasFieldErrors( "dataSet" ) && !errors.hasFieldErrors( "type" ) ) {
			BooleanExpression uniqueKey = definition.key.equalsIgnoreCase( entity.getKey() );

			if ( entity.hasParent() ) {
				uniqueKey = uniqueKey.and( definition.parent.eq( entity.getParent() ) );
			}
			else {
				uniqueKey = uniqueKey.and( definition.parent.isNull() )
				                     .and( definition.dataSet.eq( entity.getDataSet() ) );
			}

			uniqueKey = uniqueKey.and( definition.type.eq( entity.getType() ) );

			if ( !entity.isNew() ) {
				uniqueKey = uniqueKey.and( definition.id.ne( entity.getId() ) );
			}

			if ( repository.count( uniqueKey ) > 0 ) {
				errors.rejectValue( "key", "alreadyExists" );
			}
		}
	}

	private void detectCyclicDependencies( DynamicDefinition entity, Errors errors ) {
		if ( !errors.hasFieldErrors( "parent" ) && !entity.isNew() ) {
			val parents = new HashSet<Long>();
			parents.add( entity.getId() );
			DynamicDefinition parent = entity.getParent();
			while ( parent != null ) {
				if ( !parents.add( parent.getId() ) ) {
					errors.rejectValue( "parent", "cyclicDependency" );
					break;
				}
				parent = parent.getParent();
			}
		}
	}
}
