/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.bootstrapui.elements.FormViewElement;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.filemanager.FileManagerModule;
import com.foreach.across.modules.filemanager.business.reference.FileReference;
import com.foreach.across.modules.filemanager.business.reference.FileReferenceService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * A {@link DynamicTypeDefinitionBuilder} that supports file uploads via {@link FileReference} properties.
 * File uploads are only available if {@link FileManagerModule} is present.
 *
 * @author Steven Gentens
 * @since 0.0.2
 */
@Component
@RequiredArgsConstructor
@ConditionalOnAcrossModule(FileManagerModule.NAME)
@ConditionalOnBean(FileReferenceService.class)
public class FileDynamicTypeDefinitionBuilder implements DynamicTypeDefinitionBuilder
{
	public static final TypeDescriptor TYPE_DESCRIPTOR = TypeDescriptor.valueOf( FileReference.class );

	/**
	 * Attribute that can be set on a field of type 'file' to determine where the files should be stored,
	 * when saving a valid document.
	 */
	public static final String ATTR_FILE_REPOSITORY = "file-repository";

	private final FileReferenceService fileReferenceService;

	@Override
	public boolean accepts( RawDocumentDefinition.AbstractField field ) {
		return StringUtils.equals( StringUtils.stripEnd( field.getType(), "[]" ), "file" );
	}

	@Override
	public DynamicTypeDefinition buildTypeDefinition( DynamicTypeDefinitionFactory factory, RawDocumentDefinition.AbstractField field ) {
		val builder = GenericDynamicTypeDefinition
				.builder()
				.typeName( field.getType() );

		String repositoryId = fileRepositoryId( field );

		if ( StringUtils.endsWith( field.getType(), "[]" ) ) {
			builder.typeDescriptor( TypeDescriptor.collection( ArrayList.class, TYPE_DESCRIPTOR ) )
			       .propertyDescriptorBuilderConsumer( multiFileReferenceProperty( repositoryId ) );
		}
		else {
			builder.typeDescriptor( TYPE_DESCRIPTOR )
			       .propertyDescriptorBuilderConsumer( singleFileReferenceProperty( repositoryId ) );
		}

		return builder.build();
	}

	private BiConsumer<DynamicDocumentFieldDefinition, EntityPropertyDescriptorBuilder> multiFileReferenceProperty( String repositoryId ) {
		return ( fieldDefinition, descriptorBuilder ) -> {
			descriptorBuilder.attribute( EntityAttributes.FORM_ENCTYPE, FormViewElement.ENCTYPE_MULTIPART );
			if ( repositoryId != null ) {
				descriptorBuilder.controller(
						ctl -> ctl.<List<FileReference>>withBindingContext( List.class )
								.saveConsumer( ( ctx, list ) -> {
									if ( !list.isDeleted() && list.getNewValue() != null ) {
										fileReferenceService.changeFileRepository( list.getNewValue(), repositoryId, true );
									}
								} )
				);
			}
		};
	}

	private BiConsumer<DynamicDocumentFieldDefinition, EntityPropertyDescriptorBuilder> singleFileReferenceProperty( String repositoryId ) {
		return ( fieldDefinition, descriptorBuilder ) -> {
			descriptorBuilder.attribute( EntityAttributes.FORM_ENCTYPE, FormViewElement.ENCTYPE_MULTIPART );
			if ( repositoryId != null ) {
				descriptorBuilder.controller(
						ctl -> ctl.withBindingContext( FileReference.class )
						          .saveConsumer( ( ctx, fr ) -> {
							          if ( !fr.isDeleted() && fr.getNewValue() != null ) {
								          fileReferenceService.changeFileRepository( fr.getNewValue(), repositoryId, true );
							          }
						          } ) );
			}
		};
	}

	private String fileRepositoryId( RawDocumentDefinition.AbstractField field ) {
		return (String) field.getAttributes().get( ATTR_FILE_REPOSITORY );
	}
}
