/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data.expressions;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.*;
import org.springframework.expression.spel.support.ReflectiveMethodResolver;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.util.ArrayUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Inheritable class that holds a number of helper functions for building formulas on dynamic document fields.
 *
 * @author Arne Vandamme
 * @since 0.0.2
 */
@SuppressWarnings("unchecked")
public class DynamicDocumentFormulaFunctions
{
	public long count( List<Object> items ) {
		return (long) items.size();
	}

	public long countNonNull( List<Object> items ) {
		return items.stream().filter( Objects::nonNull ).count();
	}

	public long countNull( List<Object> items ) {
		return items.stream().filter( Objects::isNull ).count();
	}

	/**
	 * Flatten an array of objects into a single list with all {@code null} values removed.
	 * Will recursively flatten any array or collection member.
	 *
	 * @param objects to flatten
	 * @return flat list of non-null values
	 */
	public List flatten( Object... objects ) {
		return flatStream( objects ).filter( Objects::nonNull ).collect( Collectors.toList() );
	}

	/**
	 * Flatten an array of objects into a single stream. Will recursively flatten any array
	 * or collection member. Unlike {@link #flatten(Object...)} this will not remove {@code null} values.
	 *
	 * @param objects to flatten
	 * @return flat stream
	 */
	public Stream<Object> flatStream( Object... objects ) {
		return Stream.of( objects ).flatMap( this::toFlatStream );
	}

	private Stream<Object> toFlatStream( Object value ) {
		if ( value instanceof Collection ) {
			return ( (Collection<?>) value ).stream().flatMap( this::toFlatStream );
		}
		if ( value != null && value.getClass().isArray() ) {
			return Arrays.stream( (Object[]) value ).flatMap( this::toFlatStream );
		}
		if ( value instanceof Iterable ) {
			return StreamSupport.stream( ( (Iterable<?>) value ).spliterator(), false ).flatMap( this::toFlatStream );
		}

		return Stream.of( value );
	}

	/**
	 * Sort a collection of items.
	 *
	 * @param items to sort
	 * @return sorted list
	 */
	public List sorted( Collection<Object> items ) {
		List sorted = new ArrayList( items );
		Collections.sort( sorted );
		return sorted;
	}

	/**
	 * Sort an array of items.
	 *
	 * @param items to sort
	 * @return sorted list
	 */
	public List sorted( Object... items ) {
		return sorted( Arrays.asList( items ) );
	}

	/**
	 * Reverse an array of objects.
	 *
	 * @param items to reverse
	 * @return list in reversed order
	 */
	public List reverse( Object... items ) {
		return reverse( Arrays.asList( items ) );
	}

	/**
	 * Reverse a collection of objects.
	 *
	 * @param items to reverse
	 * @return list in reversed order
	 */
	public List reverse( Collection<Object> items ) {
		List reversed = new ArrayList( items );
		Collections.reverse( reversed );
		return reversed;
	}

	/**
	 * Get the first item from an array. Null will be returned if the array is empty.
	 *
	 * @param items to get the first one from
	 * @return item
	 */
	public Object first( Object... items ) {
		return items != null ? first( Arrays.asList( items ) ) : null;
	}

	/**
	 * Get the first item from a collection. Null will be returned if the collection is empty.
	 *
	 * @param items to get the first one from
	 * @return item
	 */
	public Object first( Collection items ) {
		return items != null && items.size() > 0 ? items.iterator().next() : null;
	}

	/**
	 * Get the last item from an array. Null will be returned if the array is empty.
	 *
	 * @param items to get the last one from
	 * @return item
	 */
	public Object last( Object... items ) {
		return items != null ? last( Arrays.asList( items ) ) : null;
	}

	/**
	 * Get the last item from a collection. Null will be returned if the collection is empty.
	 *
	 * @param items to get the last one from
	 * @return item
	 */
	public Object last( Collection items ) {
		return items != null && items.size() > 0 ? new ArrayList<>( items ).get( items.size() - 1 ) : null;
	}

	public Number min( Object... items ) {
		return asFlatNumberStream( items ).min( Comparator.comparing( Number::doubleValue ) ).orElse( null );
	}

	public Number max( Object... items ) {
		return asFlatNumberStream( items ).max( Comparator.comparing( Number::doubleValue ) ).orElse( null );
	}

	public Number sum( Object... items ) {
		return asFlatNumberStream( items ).mapToDouble( Number::doubleValue ).sum();
	}

	public Number avg( Object... items ) {
		OptionalDouble average = asFlatNumberStream( items ).mapToDouble( Number::doubleValue ).average();
		return average.isPresent() ? average.getAsDouble() : null;
	}

	private Stream<Number> asFlatNumberStream( Object value ) {
		if ( value == null ) {
			return Stream.empty();
		}
		if ( value instanceof Number ) {
			return Stream.of( (Number) value );
		}
		if ( value instanceof Collection ) {
			return ( (Collection<?>) value ).stream().flatMap( this::asFlatNumberStream );
		}
		if ( value.getClass().isArray() ) {
			return Arrays.stream( (Object[]) value ).flatMap( this::asFlatNumberStream );
		}
		if ( value instanceof Iterable ) {
			return StreamSupport.stream( ( (Iterable<?>) value ).spliterator(), false ).flatMap( this::asFlatNumberStream );
		}

		throw new IllegalArgumentException( "Unable to convert value " + value + " to a number" );
	}

	public boolean isNotEmpty( Collection collection ) {
		return !isEmpty( collection );
	}

	public boolean isNotEmpty( Object[] array ) {
		return !isEmpty( array );
	}

	public boolean isEmpty( Collection collection ) {
		return CollectionUtils.isEmpty( collection );
	}

	public boolean isEmpty( Object[] array ) {
		return ArrayUtils.isEmpty( array );
	}

	public boolean and( Boolean... items ) {
		return BooleanUtils.and( items );
	}

	public boolean or( Boolean... items ) {
		return BooleanUtils.or( items );
	}

	public String concat( String... items ) {
		return join( "", items );
	}

	public String join( String delimiter, String... items ) {
		return StringUtils.join( items, delimiter );
	}

	/**
	 * Create a {@link MethodResolver} that provides an instance of {@link DynamicDocumentFormulaFunctions} as if they
	 * are methods on the root object in an expression executing context. Useful for registration on a
	 * {@link org.springframework.expression.spel.support.StandardEvaluationContext}.
	 *
	 * @return method resolver
	 */
	public static MethodResolver asRootObjectMethodResolver() {
		return asRootObjectMethodResolver( new DynamicDocumentFormulaFunctions() );
	}

	/**
	 * Create a {@link MethodResolver} that exposes the methods of an object as if they
	 * are methods on the root object in an expression executing context. Useful for registration on a
	 * {@link org.springframework.expression.spel.support.StandardEvaluationContext}.
	 * <p/>
	 * Can be used to register a custom extension of {@link DynamicDocumentFormulaFunctions} but works for any class.
	 *
	 * @param functions to register as root methods
	 * @return method resolver
	 */
	public static MethodResolver asRootObjectMethodResolver( @NonNull Object functions ) {
		return new RootMethodResolver( functions );
	}

	@RequiredArgsConstructor
	private static class RootMethodResolver implements MethodResolver
	{
		private static final ReflectiveMethodResolver TARGET_RESOLVER = new ReflectiveMethodResolver();

		private final Object target;

		@Override
		public MethodExecutor resolve( EvaluationContext context,
		                               Object targetObject,
		                               String name,
		                               List<TypeDescriptor> argumentTypes ) throws AccessException {
			MethodExecutor me = TARGET_RESOLVER.resolve( context, target, name, argumentTypes );
			if ( me != null ) {
				return new Executor( me );
			}
			return null;
		}

		@RequiredArgsConstructor
		private class Executor implements MethodExecutor
		{
			private final MethodExecutor targetExecutor;

			@Override
			public TypedValue execute( EvaluationContext context, Object t, Object... arguments ) throws AccessException {
				return targetExecutor.execute( context, target, arguments );
			}
		}
	}
}
