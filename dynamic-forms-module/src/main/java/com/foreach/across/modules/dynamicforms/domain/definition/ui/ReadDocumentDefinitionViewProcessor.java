/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.ui;

import com.foreach.across.modules.bootstrapui.elements.Style;
import com.foreach.across.modules.bootstrapui.elements.builder.FormViewElementBuilder;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.entity.conditionals.ConditionalOnAdminWeb;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.views.support.EntityMessages;
import com.foreach.across.modules.entity.web.links.EntityViewLinkBuilder;
import com.foreach.across.modules.entity.web.links.EntityViewLinks;
import com.foreach.across.modules.entity.web.links.SingleEntityViewLinkBuilder;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilder;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.foreach.across.modules.bootstrapui.ui.factories.BootstrapViewElements.bootstrap;
import static com.foreach.across.modules.web.ui.elements.HtmlViewElements.html;

/**
 * {@link EntityViewProcessorAdapter} that configures the layout of the base view of a {@link DynamicDefinition} and
 * supplies navigation to it's edit view.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
@ConditionalOnAdminWeb
public class ReadDocumentDefinitionViewProcessor extends EntityViewProcessorAdapter
{
	private final EntityViewLinks entityViewLinks;

	@Override
	protected void createViewElementBuilders( EntityViewRequest entityViewRequest,
	                                          EntityView entityView,
	                                          ViewElementBuilderMap builderMap ) {
		FormViewElementBuilder form = (FormViewElementBuilder) builderMap.get( SingleEntityFormViewProcessor.FORM );
		DynamicDefinitionDataSet dataSet = entityViewRequest.getEntityViewContext().getParentContext().getEntity( DynamicDefinitionDataSet.class );
		DynamicDefinition entity = entityViewRequest.getCommand().getEntity( DynamicDefinition.class );
		EntityMessages messages = entityViewRequest.getEntityViewContext().getEntityMessages();

		EntityViewLinkBuilder linkToEntityAssociation = entityViewLinks.linkTo( dataSet ).association( "dynamicDefinition.dataSet" );
		String cancelUrl = linkToEntityAssociation.toUriString();
		SingleEntityViewLinkBuilder linkToAssociatedEntity = linkToEntityAssociation.withId( entity.getId() );
		String updateUrl = linkToAssociatedEntity.updateView().toUriString() + "?from=" + linkToAssociatedEntity.toUriString();

		ContainerViewElementBuilder container = html.builders.container().name( "buttons" );

		container.add( bootstrap.builders.button().name( "btn-update" )
		                                 .link( updateUrl )
		                                 .style( Style.PRIMARY )
		                                 .text( messages.withNameSingular( "actions.modify", entity ) ) )
		         .add( bootstrap.builders.button().name( "btn-cancel" )
		                                 .link( cancelUrl )
		                                 .text( messages.messageWithFallback( "actions.cancel" ) )
		         );

		form.add( container );
		builderMap.put( SingleEntityFormViewProcessor.FORM_BUTTONS, container );
	}

	@Override
	protected void postRender( EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		ContainerViewElementUtils.move( container, "related-versions", SingleEntityFormViewProcessor.RIGHT_COLUMN );
	}
}
