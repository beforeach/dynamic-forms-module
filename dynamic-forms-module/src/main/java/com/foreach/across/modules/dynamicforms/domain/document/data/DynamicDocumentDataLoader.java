/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.Reporter;
import lombok.*;
import org.springframework.core.convert.ConversionService;

import java.util.*;

/**
 * Helper object for bulk loading of data in a document.
 * Supports optional type conversion as well as ignoring unknown fields
 * or fields with illegal value according to the field type.
 * <p/>
 * Can actually perform full tracking of all field values updated if enabled,
 * this can be used to determine if a document has actually been updated.
 * <p/>
 *
 * @author Arne Vandamme
 * @see DynamicDocumentDataMapper
 * @since 0.0.1
 */
public class DynamicDocumentDataLoader implements DynamicDocumentDataFieldWriter, Reporter
{
	/**
	 * -- SETTER --
	 * The document in which to load the data.
	 */
	@NonNull
	private DynamicDocumentData document;

	private DynamicDocumentDataAccessor accessors;

	/**
	 * -- SETTER --
	 * A {@link ConversionService} that should be used to convert
	 * the input types to the expected field types.
	 */
	private ConversionService conversionService;

	/**
	 * -- SETTER --
	 * Should unknown fields be ignore. If {@code false} an
	 * exception will be thrown when an unknown field is specified.
	 */
	@Setter
	private boolean ignoreUnknownFields;

	/**
	 * -- SETTER --
	 * Should invalid field values be ignored. If {@code false} an
	 * exception will be thrown when attempting to set a field value
	 * of the wrong type.
	 */
	@Setter
	private boolean ignoreIllegalFieldValues;

	/**
	 * -- GETTER --
	 * Return the report information for the current document.
	 */
	@Getter
	private Report report;

	public DynamicDocumentDataLoader() {
	}

	public DynamicDocumentDataLoader( DynamicDocumentData document ) {
		setDocument( document );
	}

	public void setConversionService( ConversionService conversionService ) {
		this.conversionService = conversionService;
		if ( accessors != null ) {
			accessors.setConversionService( conversionService );
		}
	}

	/**
	 * Set the document this loader is bound to.
	 * Updating the document will also reset this data loader report.
	 *
	 * @param document to set
	 */
	public void setDocument( DynamicDocumentData document ) {
		this.document = document;
		accessors = document.createDataAccessor();
		accessors.setReporter( this );
		accessors.setConversionService( conversionService );
		reset();
	}

	/**
	 * Reset the current data loader, this simply clears the current report
	 * but leaves all other property values.
	 */
	public void reset() {
		report = new Report();
	}

	/**
	 * Set a single field value. Depending on the values of {@code ignoreUnknownFields} and
	 * {@code ignoreIllegalFieldValues} an exception will be thrown immediately if something goes wrong.
	 *
	 * @param field path
	 * @param value to set
	 */
	@Override
	public void setFieldValue( @NonNull String field, Object value ) {
		load( Collections.singletonMap( field, value ) );
	}

	/**
	 * See {@link #load(Map)}.
	 *
	 * @param data to set
	 */
	@Override
	public void setFieldValues( @NonNull Map<String, Object> data ) {
		load( data );
	}

	/**
	 * Load the data in the document. Depending on the values of {@code ignoreUnknownFields} and
	 * {@code ignoreIllegalFieldValues} an exception will be thrown immediately if something goes wrong.
	 * <p/>
	 * If errors should be ignored, the {@link Report} will still contain a list of {@link FieldError}
	 * records for all fields that could not be imported.
	 * <p/>
	 * This is identical to calling  {@link #setFieldValues(Map)}.
	 *
	 * @param data to import
	 */
	public void load( @NonNull Map<String, Object> data ) {
		if ( document == null ) {
			throw new IllegalStateException( "Unable to load fields - no document data has been set" );
		}

		handleFieldValues( data );
	}

	@SuppressWarnings("unchecked")
	private void handleFieldValues( Map<String, Object> data ) {
		data.forEach( ( key, value ) -> {
			try {
				accessors.getFieldAccessor( key ).set( value );
			}
			catch ( DynamicDocumentDefinition.NoSuchFieldException nsfe ) {
				unknownField( key, value, nsfe );
			}
		} );
	}

	@Override
	public void valueSet( DynamicDocumentFieldDefinition field, String fieldPath, Object oldValue, Object newValue, boolean created ) {
		FieldStatus fieldStatus = new FieldStatus( fieldPath, oldValue, newValue, created, !Objects.equals( oldValue, newValue ) );
		report.modifiedFields.add( fieldStatus );
		report.updatedFieldValues |= fieldStatus.isValueUpdated();
	}

	@Override
	@SneakyThrows
	public void unknownField( String fieldPath, Object value, Throwable exception ) {
		if ( ignoreUnknownFields ) {
			report.errors.add( new FieldError( fieldPath, FieldErrorReason.UNKNOWN_FIELD, value, exception ) );
		}
		else {
			throw exception;
		}
	}

	@Override
	@SneakyThrows
	public void illegalValue( DynamicDocumentFieldDefinition field, String fieldPath, Object value, Throwable exception ) {
		if ( ignoreIllegalFieldValues ) {
			report.errors.add( new FieldError( fieldPath, FieldErrorReason.ILLEGAL_VALUE, value, exception ) );
		}
		else {
			throw exception;
		}
	}

	/**
	 * Represents the reason why a field could not be set.
	 */
	public enum FieldErrorReason
	{
		UNKNOWN_FIELD,
		ILLEGAL_VALUE
	}

	/**
	 * Represents the failure information for a single field.
	 */
	@Value
	public static class FieldError
	{
		/**
		 * Full path to the field.
		 */
		private String path;

		/**
		 * Reason why the field could not be set.
		 */
		private FieldErrorReason reason;

		/**
		 * Value that was to be set.
		 */
		private Object value;

		/**
		 * Optional cause for the failure, can provide more detailed information.
		 */
		private Throwable cause;
	}

	/**
	 * Represents the successful update information for a single field.
	 */
	@Value
	public static class FieldStatus
	{
		/**
		 * Full path to the field.
		 */
		private String path;

		/**
		 * Previous value for that field.
		 */
		private Object oldValue;

		/**
		 * Value that was to be set.
		 */
		private Object newValue;

		/**
		 * Was the field created or did it exist already.
		 */
		private boolean created;

		/**
		 * Has the field value been updated. If the field already existed
		 * with the same value, this should be {@code false}.
		 */
		private boolean valueUpdated;
	}

	/**
	 * Report of loading data into a document. Contains a list of all fields
	 * for which something went wrong.
	 */
	public static class Report
	{
		@Getter
		private List<FieldError> errors = new ArrayList<>();

		@Getter
		private List<FieldStatus> modifiedFields = new ArrayList<>();

		private boolean updatedFieldValues;

		private Report() {
		}

		/**
		 * Check if a specific field had an error.
		 *
		 * @param path to the field
		 * @return true if there is an error for that field
		 */
		public boolean hasError( @NonNull String path ) {
			return getError( path ) != null;
		}

		/**
		 * Get the error object for a specific field.
		 *
		 * @param path to the field
		 * @return error or {@code null} if there is none
		 */
		public FieldError getError( @NonNull String path ) {
			for ( FieldError e : errors ) {
				if ( path.equals( e.getPath() ) ) {
					return e;
				}
			}
			return null;
		}

		/**
		 * Get the status object for a specific field.
		 *
		 * @param path to the field
		 * @return status or {@code null} if there is none
		 */
		public FieldStatus getFieldStatus( @NonNull String path ) {
			for ( FieldStatus status : modifiedFields ) {
				if ( path.equals( status.getPath() ) ) {
					return status;
				}
			}
			return null;
		}

		/**
		 * @return true if there are field errors
		 */
		public boolean hasErrors() {
			return !errors.isEmpty();
		}

		/**
		 * @return true if there is at least once field status with {@link FieldStatus#isValueUpdated()}
		 */
		public boolean hasUpdatedFieldValues() {
			return updatedFieldValues;
		}
	}
}
