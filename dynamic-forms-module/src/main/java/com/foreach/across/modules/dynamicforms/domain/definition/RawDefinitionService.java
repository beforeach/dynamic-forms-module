/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

/**
 * Service that converts json representations of {@link RawDefinition}s to {@link RawDefinition}s and vice versa.
 */
@Service
@RequiredArgsConstructor
public class RawDefinitionService
{
	private final DynamicDefinitionTypeConfigurationRegistry definitionTypeRegistry;

	/**
	 * Converts a json representation of a {@link DynamicDefinition} to a {@link RawDefinition}.
	 *
	 * @param content of the definition
	 * @param clazz   of the raw definition to convert to
	 * @return A {@link RawDefinition} based on the provided content and the given raw definition type.
	 */
	@SuppressWarnings("unchecked")
	public <T> T readRawDefinition( String content, Class<T> clazz ) {
		return (T) readRawDefinition( content, definitionTypeRegistry.getByClass( clazz ) );
	}

	/**
	 * Converts a json representation of a {@link DynamicDefinition} to a {@link RawDefinition}.
	 *
	 * @param content of the definition
	 * @param type type name of the {@link RawDefinition}.
	 * @return A {@link RawDefinition} based on the provided content and the given raw definition type.
	 */
	@SuppressWarnings("unchecked")
	public RawDefinition readRawDefinition( String content, String type ) {
		return (RawDefinition) readRawDefinition( content, definitionTypeRegistry.getByName( type ) );
	}

	/**
	 * Converts a json representation of a {@link DynamicDefinition} to a {@link RawDefinition}.
	 *
	 * @param content of the definition
	 * @param configuration configuration object of a {@link DynamicDefinitionType}.
	 * @return a {@link RawDefinition} based on the provided content and the given configuration.
	 */
	@SneakyThrows
	public Object readRawDefinition( String content, DynamicDefinitionTypeConfiguration configuration ) {
		return new ObjectMapper()
				.enable( DeserializationFeature.UNWRAP_ROOT_VALUE )
				.reader()
				.forType( configuration.getRawDefinitionClass() )
				.readValue( content );
	}

	/**
	 * Converts a given {@link RawDefinition} to its json representation.
	 *
	 * @param definition to convert
	 * @return a json representation of the {@link RawDefinition}.
	 */
	@SneakyThrows
	public String writeDefinition( RawDefinition definition ) {
		DynamicDefinitionTypeConfiguration configuration = definitionTypeRegistry.getByClass( definition.getClass() );
		return new ObjectMapper()
				.enable( SerializationFeature.WRAP_ROOT_VALUE )
				.writer()
				.forType( configuration.getRawDefinitionClass() )
				.writeValueAsString( definition );
	}
}
