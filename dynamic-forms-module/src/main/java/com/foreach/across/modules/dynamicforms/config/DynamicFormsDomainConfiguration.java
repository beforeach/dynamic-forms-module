/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.across.modules.dynamicforms.domain.DomainMarker;
import com.foreach.across.modules.dynamicforms.domain.definition.*;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSetToStringConverter;
import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.FormattingConversionService;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Configuration
@EnableAcrossJpaRepositories(basePackageClasses = DomainMarker.class)
@RequiredArgsConstructor
class DynamicFormsDomainConfiguration
{
	private final DynamicDefinitionService definitionService;
	private final DynamicDefinitionRepository definitionRepository;
	private final DynamicDefinitionVersionRepository definitionVersionRepository;

	@Autowired
	void registerConverters( FormattingConversionService conversionService, ObjectMapper dynamicDataObjectMapper ) {
		conversionService.addConverter( new DynamicDataFieldSetToStringConverter( dynamicDataObjectMapper ) );

		conversionService.addConverter( DynamicDefinitionId.class, DynamicDefinition.class, definitionService::findDefinition );
		conversionService.addConverter( DynamicDefinitionId.class, DynamicDefinitionVersion.class, definitionService::findDefinitionVersion );
		conversionService.addConverter( new StringToDynamicDefinitionConverter( definitionService, definitionRepository ) );
		conversionService.addConverter( new StringToDynamicDefinitionVersionConverter( definitionService, definitionVersionRepository ) );
	}
}
