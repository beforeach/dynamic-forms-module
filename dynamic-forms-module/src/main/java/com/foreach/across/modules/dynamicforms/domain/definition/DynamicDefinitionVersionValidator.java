/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.entity.validators.EntityValidatorSupport;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
@Component
@AllArgsConstructor
public class DynamicDefinitionVersionValidator extends EntityValidatorSupport<DynamicDefinitionVersion>
{
	private final DynamicDefinitionVersionRepository repository;

	@Override
	public boolean supports( Class<?> aClass ) {
		return DynamicDefinitionVersion.class.isAssignableFrom( aClass );
	}

	@Override
	protected void postValidation( DynamicDefinitionVersion entity, Errors errors, Object... validationHints ) {
		QDynamicDefinitionVersion definitionVersion = QDynamicDefinitionVersion.dynamicDefinitionVersion;

		if ( !errors.hasFieldErrors( "version" ) && !errors.hasFieldErrors( "definition" ) ) {
			BooleanExpression uniqueName = definitionVersion.version.equalsIgnoreCase( entity.getVersion() );

			DynamicDefinition definition = entity.getDefinition();
			uniqueName = uniqueName.and( definitionVersion.definition.eq( definition ) );

			if ( !entity.isNew() ) {
				uniqueName = uniqueName.and( definitionVersion.id.ne( entity.getId() ) );
			}

			if ( repository.count( uniqueName ) > 0 ) {
				errors.rejectValue( "version", "alreadyExists" );
			}
		}
	}
}
