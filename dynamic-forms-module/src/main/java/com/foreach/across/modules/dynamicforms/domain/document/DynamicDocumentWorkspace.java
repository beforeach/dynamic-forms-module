/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypes;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDocumentDefinitionService;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.*;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataLoader.Report;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.BuildDynamicDocumentFormulaEvaluationContext;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentDataValidator;
import com.foreach.across.modules.dynamicforms.utils.VersionUtils;
import com.foreach.across.modules.hibernate.jpa.config.HibernateJpaConfiguration;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.expression.spel.support.StandardTypeConverter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.MapBindingResult;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * The {@code DynamicDocumentWorkspace} is a central, stateful component for
 * working with a {@link DynamicDocument}. It manages data, allows for type conversion,
 * definition upgrading and saving the document. An instance should be created through the {@link DynamicDocumentService}.
 * <p/>
 * A {@code DynamicDocumentWorkspace} can also be used as a target for databinding.
 * If the workspace is set as a property of the {@link org.springframework.validation.DataBinder} target,
 * then it is imperative that {@link #setDataBinderTarget(String)} was called with the correct property
 * prefix or binding exceptions will not return the correct property name.
 * <p/>
 * NOTE: a {@link DynamicDocumentWorkspace} is always attached to a single document, not thread-safe
 * and not meant for caching. You can use the same workspace for multiple documents by switching document
 * using {@link #loadNewDocument()} or {@link #loadDocument(DynamicDocument)} methods. For performance, a workspace
 * caches the different definitions it has loaded.
 * <p/>
 * You can also configure a custom shared cache with {@link #setDefinitionCache(Map)}.
 * You can disable the default caching behaviour using {@link #setCacheDefinitions(boolean)}.
 *
 * @author Arne Vandamme
 * @see DynamicDocumentService
 * @since 0.0.1
 */
@NotThreadSafe
@Setter(AccessLevel.PACKAGE)
@Component
@Scope(value = "prototype")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class DynamicDocumentWorkspace
{
	private static final DynamicDocumentDataValidator INDEXED_VALIDATOR = DynamicDocumentDataValidator.forIndexedTarget( "binderMap" );
	private static final DynamicDocumentDataValidator PLAIN_VALIDATOR = DynamicDocumentDataValidator.forPlainTarget();

	private final DynamicDocumentService documentService;
	private final DynamicDocumentRepository documentRepository;
	private final DynamicDocumentVersionRepository documentVersionRepository;
	private final DynamicDocumentDefinitionService definitionService;
	private final DynamicDocumentDataMapperFactory documentDataMapperFactory;
	private final ApplicationEventPublisher applicationEventPublisher;

	/**
	 * -- SETTER --
	 * Set a (non-null) definition cache that should be used by this workspace.
	 * The key is the definition id.
	 * Any definition loaded by this workspace will be fetched from the cache,
	 * or - if missing - stored into it.
	 */
	@NonNull
	@Setter(AccessLevel.PUBLIC)
	private Map<Long, DynamicDocumentDefinition> definitionCache = new HashMap<>();
	private TransactionTemplate transactionTemplate;
	private ConversionService conversionService;
	private DynamicDocumentDefinition documentDefinitionVersion;

	/**
	 * -- GETTER --
	 * Should definitions be cached in this workspace.
	 */
	@Getter
	private boolean cacheDefinitions = true;

	/**
	 * -- GETTER --
	 * The document definition for this document.
	 */
	@Getter
	private DynamicDefinition definition;

	/**
	 * -- GETTER --
	 * The document definition version for the current document version
	 * loaded in the workspace.
	 */
	@Getter
	private DynamicDefinitionVersion definitionVersion;

	/**
	 * -- GETTER --
	 * The document that is loaded in this workspace.
	 */
	@Getter
	private DynamicDocument document;

	/**
	 * -- GETTER --
	 * The document version that is currently loaded in this workspace.
	 */
	@Getter
	private DynamicDocumentVersion documentVersion;

	/**
	 * -- GETTER --
	 * Return the fields data of the current document version loaded in the workspace.
	 */
	@Getter
	@Setter(value = AccessLevel.NONE)
	private DynamicDocumentData fields;

	/**
	 * -- GETTER --
	 * Returns {@code true} (default) if previously saved document versions can be updated.
	 * <p>
	 * -- SETTER --
	 * Set to {@code true} if you want to allow an existing (saved) version of a document
	 * to be updated. Defaults to {@code false} to avoid accidental updating of an existing
	 * version as that would be rewriting history... something you usually do not want.
	 */
	@Getter
	@Setter(value = AccessLevel.PUBLIC)
	private boolean allowVersionUpdates;

	/**
	 * -- SETTER --
	 * Change the name of this document.
	 * -- GETTER --
	 * The name of the document.
	 */
	@NotBlank
	@Length(max = 255)
	@Getter
	@Setter
	private String documentName;

	/**
	 * -- SETTER --
	 * Set the label for the current document version.
	 * -- GETTER --
	 * The (proposed) label for the current document version.
	 */
	@NotBlank
	@Length(max = 30)
	@Getter
	@Setter
	private String versionLabel;

	/**
	 * -- GETTER --
	 * The property of a {@link org.springframework.validation.DataBinder} target
	 * where this document workspace is attached.
	 */
	@Getter
	private String dataBinderTarget;

	/**
	 * -- GETTER --
	 * The report for the last version loaded in the workspace.
	 * Can be {@code null} if no version has been loaded yet.
	 */
	@Getter
	private Report report;

	private DynamicDocumentDataAsMapAdapter fieldsBinder;

	/**
	 * Should the worskspace cache definitions that have previously been loaded.
	 * Default is {@code true} meaning that switching documents or versions in the same
	 * workspace will have better performance since it will avoid parsing definitions if they
	 * have previously been loaded.
	 * <p/>
	 * If you plan to use the same workspace instance for a long time (you shouldn't though),
	 * you should put this value to {@code false}. You can also externalize the cache to use
	 * by setting it with {@link #setDefinitionCache(Map)}.
	 *
	 * @param cacheDefinitions true if definitions should be cached
	 */
	public void setCacheDefinitions( boolean cacheDefinitions ) {
		this.cacheDefinitions = cacheDefinitions;
	}

	/**
	 * @return true if calculated fields are enabled
	 */
	public boolean isCalculatedFieldsEnabled() {
		return fields.isCalculatedFieldsEnabled();
	}

	/**
	 * When enabled, calculated fields will always be re-calculated when fetched.
	 * If disabled, the last calculated value will be returned instead and you must update
	 * them manually using {@link #updateCalculatedFields()}.
	 * <p/>
	 * The workspace will always update the calculate fields before saving the document.
	 *
	 * @param enabled true if calculated fields should be enabled
	 */
	public void setCalculatedFieldsEnabled( boolean enabled ) {
		fields.setCalculatedFieldsEnabled( enabled );
	}

	/**
	 * Recalculates all calculated fields in this document and updates the cached value.
	 * The workspace will always update the calculate fields before saving the document.
	 * <p/>
	 * If you want instant calculation of fields, you should set {@link #setCalculatedFieldsEnabled(boolean)} to {@code true}.
	 */
	public void updateCalculatedFields() {
		fields.updateCalculatedFields();
	}

	/**
	 * Check if errors occurred when loading the current document version in the workspace.
	 * The detailed report can be retrieved using {@link #getReport()} though several workspace
	 * methods will immediately return the report as well.
	 *
	 * @return true if errors occured when initializing the workspace (loading a specific document version)
	 */
	public boolean hasErrors() {
		return report != null && report.hasErrors();
	}

	/**
	 * Get a {@link DynamicDocumentDataAsMapAdapter} for the current document data.
	 * The adapter is ready to be used on a {@link org.springframework.validation.DataBinder} target.
	 *
	 * @return fields binder
	 * @see #setDataBinderTarget(String)
	 */
	public Map<String, Object> getBinderMap() {
		if ( fieldsBinder == null ) {
			fieldsBinder = fields.asFlatMap();
			fieldsBinder.setBinderPrefix( StringUtils.isEmpty( dataBinderTarget ) ? "binderMap" : dataBinderTarget + ".binderMap" );
			fieldsBinder.setConversionService( conversionService );
		}
		return fieldsBinder;
	}

	/**
	 * Validate the current document fields, using the external binding result for registering the errors.
	 * This assumes that the workspace is a property on the {@link BindingResult#getTarget()} and the
	 * {@link #dataBinderTarget} has been registered with that property name.
	 * <p/>
	 * The field errors will generate a valid bean path to the actual field value.
	 * <p/>
	 * This will <strong>NOT</strong> validate the document name or version label, which is also required for a document.
	 *
	 * @param errors in which to register the errors
	 */
	public void validate( @NonNull Errors errors ) {
		if ( !StringUtils.isEmpty( dataBinderTarget ) ) {
			errors.pushNestedPath( dataBinderTarget );
			INDEXED_VALIDATOR.validate( fields, errors );
			errors.popNestedPath();
		}
		else {
			INDEXED_VALIDATOR.validate( fields, errors );
		}
	}

	/**
	 * Performs standalone validation of the currently loaded document.
	 * Returns an {@link Errors} instance holding the different errors that have been registered.
	 * Any fields will be reported as the exact path to that field.
	 * <p/>
	 * This will <strong>NOT</strong> validate the document name or version label, which is also required for a document.
	 *
	 * @return errors instance
	 */
	public Errors validate() {
		MapBindingResult bindingResult = new MapBindingResult( fields.asFlatMap(), StringUtils.defaultString( getDocumentName(), "DynamicDocumentWorkspace" ) );
		PLAIN_VALIDATOR.validate( fields, bindingResult );
		return bindingResult;
	}

	/**
	 * Set the property on a databinder target that this workspace is bound to.
	 * This will ensure the {@link #getBinderMap()} throws exceptions with the
	 * correct property names for Spring validation handling.
	 *
	 * @param dataBinderTarget property name
	 */
	public void setDataBinderTarget( String dataBinderTarget ) {
		this.dataBinderTarget = dataBinderTarget;
		this.fieldsBinder = null;
	}

	/**
	 * Load the specific version of the document into this workspace.
	 * Note that it is possible that something goes wrong when loading the version,
	 * for example if a field can no longer be retrieved.
	 * <p/>
	 * Though this should rarely happen for a known version (attached to a fixed definition
	 * that should theoretically not change), the workspace is very lenient and returns a
	 * {@link Report} with information about fields that failed.
	 * <p/>
	 * This method can only be used to load a document version of the same document currently
	 * loaded in the workspace. If you want to switch the workspace to another document
	 * altogether, use {@link #loadDocument(DynamicDocument)}.
	 *
	 * @param version that should be used
	 * @return report with errors
	 */
	public Report changeDocumentVersion( @NonNull DynamicDocumentVersion version ) {
		Assert.isTrue( document.equals( version.getDocument() ), "DocumentVersion not attached to same document." );

		val newDocumentVersion = version.toDto();
		val newDefinitionVersion = version.getDefinitionVersion();
		if ( documentDefinitionVersion == null || !newDefinitionVersion.equals( definitionVersion ) ) {
			documentDefinitionVersion = loadDocumentDefinition( newDefinitionVersion );
			definitionVersion = newDefinitionVersion;
		}

		documentVersion = newDocumentVersion;
		DynamicDocumentData documentData = createDocumentData( documentDefinitionVersion, newDefinitionVersion );
		setFields( documentData );
		val dataLoader = createDataLoader();
		documentDataMapperFactory.createMapper( DynamicDocumentDataMapper.JSON )
		                         .load( dataLoader, documentVersion.getDocumentContent() );
		documentData.initializeFields();
		versionLabel = version.getVersion();

		return attachReport( dataLoader.getReport() );
	}

	/**
	 * Set a single field value.
	 *
	 * @param field to set
	 * @param value to set
	 * @return previous value for that field
	 */
	public Object setFieldValue( String field, Object value ) {
		return fields.setValue( field, value );
	}

	/**
	 * Get the field value.
	 *
	 * @param field to get the value for
	 * @param <T>   expected value type, will perform cast
	 * @return field value
	 */
	public <T> T getFieldValue( String field ) {
		return fields.getValue( field );
	}

	/**
	 * Find the field value, {@code null} will be returned if that field
	 * does not exist, else the optional will hold the value of the field.
	 *
	 * @param field to find
	 * @param <T>   expected value type, will perform cast
	 * @return {@code null} if no such field, else an optional with the value
	 */
	public <T> Optional<T> findFieldValue( String field ) {
		return fields.findValue( field );
	}

	/**
	 * Get the field value, optionally performing type conversion using the
	 * bound {@code ConversionService}. This will throw an exception if there
	 * is no {@code ConversionService} set on the workspace.
	 *
	 * @param field      to get the value for
	 * @param returnType to which the value should be converted
	 * @param <T>        return type parameter
	 * @param <V>        specific return type, will cast after conversion
	 * @return field value in the type specified
	 */
	@SuppressWarnings("unchecked")
	public <T, V extends T> V getFieldValue( String field, @NonNull Class<T> returnType ) {
		return getFieldValue( field, TypeDescriptor.valueOf( returnType ) );
	}

	/**
	 * Get the field value, optionally performing type conversion using the
	 * bound {@code ConversionService}. This will throw an exception if there
	 * is no {@code ConversionService} set on the workspace.
	 * <p/>
	 * Will return {@code null} if there is no such field present.
	 *
	 * @param field      to get the value for
	 * @param returnType to which the value should be converted
	 * @param <T>        return type parameter
	 * @param <V>        specific return type, will cast after conversion
	 * @return field value in the type specified
	 */
	@SuppressWarnings("unchecked")
	public <T, V extends T> Optional<V> findFieldValue( String field, Class<T> returnType ) {
		return findFieldValue( field, TypeDescriptor.valueOf( returnType ) );
	}

	/**
	 * Get the field value, optionally performing type conversion using the
	 * bound {@code ConversionService}. This will throw an exception if there
	 * is no {@code ConversionService} set on the workspace.
	 *
	 * @param field      to get the value for
	 * @param returnType to which the value should be converted
	 * @param <T>        return type parameter
	 * @return field value in the type specified
	 */
	public <T> T getFieldValue( String field, TypeDescriptor returnType ) {
		Optional<T> v = findFieldValue( field, returnType );
		return v != null ? v.orElse( null ) : null;
	}

	/**
	 * Get the field value, optionally performing type conversion using the
	 * bound {@code ConversionService}. This will throw an exception if there
	 * is no {@code ConversionService} set on the workspace.
	 * <p/>
	 * Will return {@code null} if there is no such field present.
	 *
	 * @param field      to get the value for
	 * @param returnType to which the value should be converted
	 * @param <T>        return type parameter
	 * @return field value in the type specified
	 */
	@SuppressWarnings({ "unchecked", "OptionalAssignedToNull" })
	public <T> Optional<T> findFieldValue( String field, TypeDescriptor returnType ) {
		val fieldTypeHolder = fields.getFieldType( field );

		if ( fieldTypeHolder.isPresent() ) {
			return Optional.ofNullable(
					(T) fieldTypeHolder.map( sourceType -> conversionService.convert( getFieldValue( field ), sourceType, returnType ) )
					                   .orElse( null )
			);
		}

		return null;
	}

	/**
	 * Delete a field value.
	 *
	 * @param field to delete
	 * @return value that was deleted
	 */
	public Object deleteFieldValue( String field ) {
		return fields.deleteValue( field );
	}

	/**
	 * Export the document data in the format specified.
	 * An exception will be thrown if no {@link com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataMapper}
	 * is available for the format specified.
	 *
	 * @param format identifier
	 * @param <T>    expected type of the exported data object
	 * @return export result
	 */
	public <T> T exportFields( String format ) {
		return documentDataMapperFactory.<T>createMapper( format ).dump( fields );
	}

	/**
	 * Import set of fields from a map, applying type conversion where necessary.
	 *
	 * @param data to import
	 * @return report of the fields imported
	 */
	public Report importFields( @NonNull Map<String, Object> data ) {
		val dataLoader = createDataLoader();
		dataLoader.load( data );
		return dataLoader.getReport();
	}

	/**
	 * Import set of fields from another document version.
	 * The version does not need to have the same base document definition, but can
	 * be from any type of document.
	 *
	 * @param version whose fields to import
	 * @return report of the fields imported
	 */
	public Report importFieldsFromVersion( @NonNull DynamicDocumentVersion version ) {
		return importFields( version.getDocumentContent(), DynamicDocumentDataMapper.JSON );
	}

	/**
	 * Import data in an arbitrary format into the current document.
	 * A valid {@link DynamicDocumentDataMapper} needs to be registered for
	 * the specific format, in order to be able to import the data.
	 *
	 * @param data   object to import (should not be {@code null})
	 * @param format of the data object, used to fetch a {@link DynamicDocumentDataMapper}
	 * @return report of the fields imported
	 */
	public Report importFields( @NonNull Object data, @NonNull String format ) {
		val dataLoader = createDataLoader();
		documentDataMapperFactory.createMapper( format ).load( dataLoader, data );
		return dataLoader.getReport();
	}

	/**
	 * Create a new version for the current document. Depending on the parameter value,
	 * the latest definition version ({@code true}) will be used for the new version,
	 * or the same definition of the currently loaded version will be used ({@code false}).
	 * <p/>
	 * When using the latest definition, it is possible that some values could not be loaded,
	 * in that case the report will contain the details.
	 *
	 * @param useLatestDefinition true if the latest definition version should be used
	 * @return report with information about field values (never {@code null})
	 */
	public Report createNewVersion( boolean useLatestDefinition ) {
		return createNewVersion( useLatestDefinition ? definition.getLatestVersion() : definitionVersion );
	}

	/**
	 * Create a new version for the current document, using the specific definition version.
	 * Any fields of the current version that could not be set in the new definition will be
	 * present as an error in the report.
	 *
	 * @param newDefinitionVersion to use for the new document version
	 * @return report with information about field values (never {@code null})
	 */
	public Report createNewVersion( @NonNull DynamicDefinitionVersion newDefinitionVersion ) {
		Assert.isTrue( newDefinitionVersion.getDefinition().equals( definition ), "DynamicDefinitionVersion not attached to the required definition" );

		val newDocumentDefinitionVersion = loadDocumentDefinition( newDefinitionVersion );
		val newFields = createDocumentData( newDocumentDefinitionVersion, newDefinitionVersion );
		val dataLoader = createDataLoader();
		dataLoader.setDocument( newFields );
		dataLoader.load( fields.getContent() );
		newFields.initializeFields();

		versionLabel = document.getLatestVersion() != null ? VersionUtils.getNextVersion( document.getLatestVersion().getVersion() ) : "1";

		documentVersion = DynamicDocumentVersion
				.builder()
				.document( document )
				.definitionVersion( newDefinitionVersion )
				.version( versionLabel )
				.build();

		setFields( newFields );
		documentDefinitionVersion = newDocumentDefinitionVersion;
		definitionVersion = newDefinitionVersion;

		return attachReport( dataLoader.getReport() );
	}

	/**
	 * Change the currently loaded document version to use the latest definition version.
	 * The report will contain information about fields that could not be set in the
	 * upgraded document version.
	 *
	 * @return report with information about field values (never {@code null})
	 */
	public Report changeToLatestDefinitionVersion() {
		return changeDefinitionVersion( definition.getLatestVersion() );
	}

	/**
	 * Change the currently loaded document version to use the specific definition version.
	 * The report will contain information about fields that could not be set in the
	 * upgraded document version.
	 *
	 * @param newDefinitionVersion to use
	 * @return report with information about field values (never {@code null})
	 */
	public Report changeDefinitionVersion( @NonNull DynamicDefinitionVersion newDefinitionVersion ) {
		val newDocumentDefinitionVersion = loadDocumentDefinition( newDefinitionVersion );
		val newFields = createDocumentData( newDocumentDefinitionVersion, newDefinitionVersion );
		val dataLoader = createDataLoader();
		dataLoader.setDocument( newFields );
		dataLoader.load( fields.getContent() );
		newFields.initializeFields();

		definitionVersion = newDefinitionVersion;
		setFields( newFields );

		return attachReport( dataLoader.getReport() );
	}

	/**
	 * Load a new document in this workspace. This will re-initialize the workspace with a blank
	 * document for the currently loaded definition version.
	 */
	public void loadNewDocument() {
		loadNewDocument( definitionVersion );
	}

	/**
	 * Load a new document in this workspace, using the document definition specified.
	 * This will re-initialize the workspace with a blank document using the latest version of the document definition.
	 *
	 * @param documentDefinition definition whose latest version to use for the new document
	 */
	public void loadNewDocument( @NonNull DynamicDefinition documentDefinition ) {
		loadNewDocument( documentDefinition.getLatestVersion() );
	}

	/**
	 * Load a new (blank) document in this workspace, using the document definition version specified.
	 * This will re-initialize the workspace with a blank document using the definition version.
	 *
	 * @param definitionVersion definition version to use for the new document
	 */
	public void loadNewDocument( @NonNull DynamicDefinitionVersion definitionVersion ) {
		if ( !definitionVersion.equals( this.definitionVersion ) ) {
			documentDefinitionVersion = null;
		}

		this.definitionVersion = definitionVersion;
		document = null;
		documentVersion = null;
		definition = definitionVersion.getDefinition();

		initialize();
	}

	/**
	 * Load another document in this workspace. This will re-initialize the entire workspace
	 * to use the document and accompanying definition. The latest document version will be loaded.
	 *
	 * @param document to load
	 */
	public void loadDocument( @NonNull DynamicDocument document ) {
		loadDocument( document.getLatestVersion() );
	}

	/**
	 * Load another document in this workspace. This will re-initialize the entire workspace
	 * to use that document and accompanying definition. The specific document version will be loaded.
	 *
	 * @param documentVersion to load
	 */
	public void loadDocument( @NonNull DynamicDocumentVersion documentVersion ) {
		if ( !definitionVersion.equals( documentVersion.getDefinitionVersion() ) ) {
			documentDefinitionVersion = null;
		}

		this.documentVersion = documentVersion;
		document = documentVersion.getDocument();
		definitionVersion = documentVersion.getDefinitionVersion();
		definition = definitionVersion.getDefinition();

		initialize();
	}

	/**
	 * Saves the current workspace, this will save a new version of the document,
	 * and immediately switch the current workspace to a new version.
	 * Subsquent modifications of the workspace are done in isolation.
	 *
	 * @return the new version record that has been created (or the same if it was updated)
	 */
	public DynamicDocumentVersion save() {
		return transactionTemplate.execute( ( status ) -> {
			if ( document.isNew() ) {
				document.setName( documentName );
				documentRepository.save( document );
			}

			updateCalculatedFields();

			boolean documentNameChanged = !Objects.equals( documentName, document.getName() );
			boolean versionLabelChanged = !Objects.equals( versionLabel, documentVersion.getVersion() ) || documentVersion.isNew();

			String newContent = documentDataMapperFactory.<String>createMapper( DynamicDocumentDataMapper.JSON ).dump( fields );
			boolean versionContentChanged = !Objects.equals( newContent, documentVersion.getDocumentContent() );

			boolean isNewVersion = documentVersion.isNew();

			if ( !isNewVersion && !allowVersionUpdates ) {
				throw new IllegalStateException( "Not allowed to update an already saved document version" );
			}

			DynamicDocument newDocument = null;
			DynamicDocumentVersion newDocumentVersion = null;

			if ( versionLabelChanged || versionContentChanged ) {
				newDocumentVersion = documentVersion.toDto();
				newDocumentVersion.setDefinitionVersion( definitionVersion );
				newDocumentVersion.setVersion( versionLabel );

				if ( versionContentChanged ) {
					newDocumentVersion.setDocumentContent( newContent );
				}

				documentVersionRepository.save( newDocumentVersion );
			}

			if ( documentNameChanged || newDocumentVersion != null ) {
				newDocument = document.toDto();
				newDocument.setName( documentName );

				if ( isNewVersion ) {
					newDocument.setLatestVersion( newDocumentVersion );
				}

				documentRepository.save( newDocument );
			}

			document = newDocument != null ? newDocument : document;
			documentVersion = newDocumentVersion != null ? newDocumentVersion : documentVersion;

			attachReport( null );

			return documentVersion;
		} );
	}

	/**
	 * Create a {@link DynamicDocumentDataLoader} for the currently loaded document version.
	 *
	 * @return data loader
	 */
	public DynamicDocumentDataLoader createDataLoader() {
		val dataLoader = new DynamicDocumentDataLoader();
		dataLoader.setConversionService( conversionService );
		dataLoader.setIgnoreUnknownFields( true );
		dataLoader.setIgnoreIllegalFieldValues( true );
		dataLoader.setDocument( fields );
		return dataLoader;
	}

	/**
	 * Initialize the workspace after the initial properties have been set.
	 * This makes it ready for use and must be called before any interaction apart from the initial setter methods.
	 */
	void initialize() {
		Assert.notNull( transactionTemplate, "A TransactionTemplate is required to use DynamicDocumentWorkspace" );

		if ( definition == null || definitionVersion == null ) {
			throw new IllegalStateException( "A DynamicDocumentWorkspace requires at least a DynamicDefinition and DynamicDefinitionVersion for a document" );
		}

		Assert.isTrue(
				definition.getType() != null && StringUtils.equalsIgnoreCase( DynamicDefinitionTypes.DOCUMENT, definition.getType().getName() ),
				"Definition is not of type 'document'."
		);

		if ( document == null ) {
			document = DynamicDocument.builder()
			                          .type( definition )
			                          .build();
		}

		if ( documentVersion == null ) {
			documentVersion = DynamicDocumentVersion.builder()
			                                        .document( document )
			                                        .definitionVersion( definitionVersion )
			                                        .version( "1" )
			                                        .build();
		}

		documentName = document.getName();

		changeDocumentVersion( documentVersion );
	}

	private DynamicDocumentData createDocumentData( DynamicDocumentDefinition definition, DynamicDefinitionVersion definitionVersion ) {
		val data = definition.createDocumentData();
		data.setConversionService( conversionService );
		data.setEvaluationContext( createEvaluationContext( definition, definitionVersion ) );
		data.setDocumentService( documentService );
		return data;
	}

	private EvaluationContext createEvaluationContext( DynamicDocumentDefinition definition, DynamicDefinitionVersion definitionVersion ) {
		StandardEvaluationContext evaluationContext = BuildDynamicDocumentFormulaEvaluationContext.createDefaultEvaluationContext();
		evaluationContext.setTypeConverter( new StandardTypeConverter( conversionService ) );
		BuildDynamicDocumentFormulaEvaluationContext event = new BuildDynamicDocumentFormulaEvaluationContext(
				definition, definitionVersion, evaluationContext
		);
		applicationEventPublisher.publishEvent( event );
		return event.getEvaluationContext();
	}

	private DynamicDocumentDefinition loadDocumentDefinition( DynamicDefinitionVersion definitionVersion ) {
		if ( cacheDefinitions ) {
			return definitionCache.computeIfAbsent( definitionVersion.getId(), id -> definitionService.loadDocumentDefinition( definitionVersion ) );
		}
		return definitionService.loadDocumentDefinition( definitionVersion );
	}

	private void setFields( DynamicDocumentData fields ) {
		this.fields = fields;
		this.fieldsBinder = null;
	}

	private Report attachReport( Report report ) {
		this.report = report;
		return report;
	}

	@Autowired(required = false)
	void setConversionService( ConversionService conversionService ) {
		this.conversionService = conversionService;
	}

	@Autowired
	@Qualifier(HibernateJpaConfiguration.TRANSACTION_TEMPLATE)
	void setTransactionTemplate( TransactionTemplate transactionTemplate ) {
		this.transactionTemplate = transactionTemplate;
	}
}