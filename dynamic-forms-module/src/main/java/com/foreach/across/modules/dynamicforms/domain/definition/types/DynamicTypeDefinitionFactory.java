/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.core.annotations.RefreshableCollection;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition.IllegalFieldTypeException;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;

/**
 * Responsible for creating {@link GenericDynamicTypeDefinition} instances.
 * When one is created for a particular {@link DynamicDocumentFieldDefinition},
 * the {@code type} property of the field must be a known {@link GenericDynamicTypeDefinition}
 * that will be the basis for the new one created.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Service
@RequiredArgsConstructor
public class DynamicTypeDefinitionFactory
{
	private Collection<DynamicTypeDefinitionBuilder> typeDefinitionBuilders = Collections.emptyList();

	@RefreshableCollection(includeModuleInternals = true, incremental = true)
	public void setTypeDefinitionBuilders( Collection<DynamicTypeDefinitionBuilder> typeDefinitionBuilders ) {
		this.typeDefinitionBuilders = typeDefinitionBuilders;
	}

	/**
	 * Create a new type definition for a field configuration.
	 * An exception will be thrown if a type can't be resolved.
	 *
	 * @param field to get the type definition for
	 * @return type definition (never {@code null})
	 */
	public DynamicTypeDefinition createTypeDefinition( RawDocumentDefinition.AbstractField field ) {
		val builder = typeDefinitionBuilders.stream()
		                                    .filter( b -> b.accepts( field ) )
		                                    .findFirst()
		                                    .orElseThrow( () -> new IllegalFieldTypeException( field.getType() ) );

		return builder.buildTypeDefinition( this, field );
	}
}
