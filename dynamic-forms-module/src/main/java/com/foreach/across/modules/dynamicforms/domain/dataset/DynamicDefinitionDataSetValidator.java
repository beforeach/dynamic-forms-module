/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.dataset;

import com.foreach.across.modules.entity.validators.EntityValidatorSupport;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class DynamicDefinitionDataSetValidator extends EntityValidatorSupport<DynamicDefinitionDataSet>
{
	private final DynamicDefinitionDataSetRepository repository;

	@Override
	public boolean supports( Class<?> aClass ) {
		return DynamicDefinitionDataSet.class.isAssignableFrom( aClass );
	}

	@Override
	protected void postValidation( DynamicDefinitionDataSet entity, Errors errors, Object... validationHints ) {
		QDynamicDefinitionDataSet dataSet = QDynamicDefinitionDataSet.dynamicDefinitionDataSet;

		if ( !errors.hasFieldErrors( "name" ) ) {
			BooleanExpression uniqueName = dataSet.name.equalsIgnoreCase( entity.getName() );

			if ( !entity.isNew() ) {
				uniqueName = uniqueName.and( dataSet.id.ne( entity.getId() ) );
			}

			if ( repository.count( uniqueName ) > 0 ) {
				errors.rejectValue( "name", "alreadyExists" );
			}
		}

		if ( !errors.hasFieldErrors( "key" ) ) {
			BooleanExpression uniqueKey = dataSet.key.equalsIgnoreCase( entity.getKey() );

			if ( !entity.isNew() ) {
				uniqueKey = uniqueKey.and( dataSet.id.ne( entity.getId() ) );
			}

			if ( repository.count( uniqueKey ) > 0 ) {
				errors.rejectValue( "key", "alreadyExists" );
			}
		}
	}
}
