/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.web;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.adminweb.menu.AdminMenuEvent;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypes;
import com.foreach.across.modules.dynamicforms.domain.definition.QDynamicDefinition;
import com.foreach.across.modules.entity.web.links.EntityViewLinks;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Registers the admin menu items related to dynamic documents.
 * Creates a group for every dataset.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@ConditionalOnAcrossModule(AdminWebModule.NAME)
@Component
@RequiredArgsConstructor
class AdminMenuRegistrar
{
	private static final String TOP_GROUP = "/dynamicDocuments";
	private static final String ENTITY_MGMT_GROUP = "/entities/DynamicFormsModule";

	private final MessageSource messageSource;
	private final EntityViewLinks entityViewLinks;
	private final DynamicDefinitionRepository definitionRepository;

	@EventListener
	public void buildSideNavigation( AdminMenuEvent menu ) {
		// root menu + top-level items
		menu.group( "/dynamicDocuments" )
		    .title( messageSource.getMessage( "DynamicFormsModule.adminMenu.documents", new Object[0], LocaleContextHolder.getLocale() ) )
		    .order( Ordered.HIGHEST_PRECEDENCE + 1 )
		    .and()
		    .optionalItem( ENTITY_MGMT_GROUP + "/dynamicDefinitionDataSet" ).changePathTo( TOP_GROUP + "/dataSet" ).order( 1 ).and()
		    .optionalItem( ENTITY_MGMT_GROUP + "/dynamicDocumentWorkspace" ).changePathTo( TOP_GROUP + "/documents" ).order( 3 );

		// add dataset groups
		Map<DynamicDefinitionDataSet, List<DynamicDefinition>> definitionsByDataset = definitionRepository
				.findAll( QDynamicDefinition.dynamicDefinition.type.name.eq( DynamicDefinitionTypes.DOCUMENT ) )
				.stream().collect( Collectors.groupingBy( DynamicDefinition::getDataSet ) );

		definitionsByDataset.forEach( ( dataset, definitions ) -> {
			String group = TOP_GROUP + "/" + dataset.getKey();
			menu.group( group ).title( dataset.getName() );
			definitions
					.forEach( definition -> {
						String url = entityViewLinks.linkTo( definition ).association( "documents" ).listView().toString();
						menu.item( group + "/" + definition.getKey() ).title( definition.getName() ).url( url );
					} );
		} );

		// disable the entity management section
		menu.optionalItem( "/entities/DynamicFormsModule" ).disable();
	}
}
