/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.interceptors;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.hibernate.aop.EntityInterceptorAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Base interceptor that updates the latest version of a {@link DynamicDefinition} when a new {@link DynamicDefinitionVersion} has been created.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class DynamicDefinitionVersionInterceptor extends EntityInterceptorAdapter<DynamicDefinitionVersion>
{
	private final DynamicDefinitionRepository definitionRepository;

	@Override
	public boolean handles( Class<?> entityClass ) {
		return DynamicDefinitionVersion.class.isAssignableFrom( entityClass );
	}

	public void afterUpdate( DynamicDefinitionVersion entity ) {
		setLatestVersion( entity );
	}

	@Override
	public void afterCreate( DynamicDefinitionVersion entity ) {
		setLatestVersion( entity );
	}

	private void setLatestVersion( DynamicDefinitionVersion entity ) {
		// todo check if version should be set
		if ( entity.isPublished() ) {
			DynamicDefinition definition = entity.getDefinition();
			definition.setLatestVersion( entity );
			definitionRepository.save( definition );
		}
	}
}
