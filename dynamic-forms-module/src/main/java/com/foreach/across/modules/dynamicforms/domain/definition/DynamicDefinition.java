/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.Objects;

/**
 * Represents a definition of the template for a {@link DynamicDocumentDefinition}, {@link  DynamicTypeDefinition} or a form.
 * A definition holds the link to the latest {@link DynamicDefinitionVersion} for the definition it represents.
 * <p>
 * Usually a {@link DynamicDefinition} is not linked directly, but rather through a {@link DynamicDefinitionVersion} to link to a specific version of a document definition.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "dfm_definition")
@NoArgsConstructor
public class DynamicDefinition extends SettableIdAuditableEntity<DynamicDefinition> implements Comparable<DynamicDefinition>
{
	@Id
	@GeneratedValue(generator = "seq_dfm_definition_id")
	@GenericGenerator(
			name = "seq_dfm_definition_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@Parameter(name = "sequenceName", value = "seq_dfm_definition_id"),
					@Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	/**
	 * Represents the name of the data definition, must be unique within the data set.
	 */
	@Column(name = "name")
	@NotBlank
	@Length(max = 255)
	private String name;

	/**
	 * Technical key of the definition, must be unique.
	 * Key can only contain alphanumeric characters, . (dot), - (dash) or _ (underscore).
	 */
	@Column(name = "definition_key")
	@NotBlank
	@Length(max = 150)
	@Pattern(regexp = "^[\\p{Alnum}-_.]*$")
	private String key;

	/**
	 * Type of the data definition. The type defines what the definition will represents.
	 * E.g. a data definition can represent a form, one of the default data types, or a custom data type.
	 */
	@NotNull
	@ManyToOne
	@JoinColumn(name = "definition_type")
	private DynamicDefinitionType type;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "definition_data_set_id")
	private DynamicDefinitionDataSet dataSet;

	/**
	 * Represents the unique version of the data definition.
	 */
	@ManyToOne
	@JoinColumn(name = "latest_version_id")
	private DynamicDefinitionVersion latestVersion;

	/**
	 * Optional parent definition. Depending on the definition type, various interpretations can be given to the parent context.
	 * E.g. {@link DynamicDefinitionTypes#VIEW} definitions will usually have a {@link DynamicDefinitionTypes#DOCUMENT} as a parent,
	 * because the view definition type configures various views for a certain document definition.
	 */
	@ManyToOne
	@JoinColumn(name = "parent_definition_id")
	private DynamicDefinition parent;

	@Builder(toBuilder = true)
	public DynamicDefinition( Long id,
	                          String name,
	                          DynamicDefinitionType type,
	                          DynamicDefinitionDataSet dataSet,
	                          String key,
	                          DynamicDefinitionVersion latestVersion,
	                          DynamicDefinition parent,
	                          @Builder.ObtainVia(method = "getCreatedBy") String createdBy,
	                          @Builder.ObtainVia(method = "getCreatedDate") Date createdDate,
	                          @Builder.ObtainVia(method = "getLastModifiedBy") String lastModifiedBy,
	                          @Builder.ObtainVia(method = "getLastModifiedDate") Date lastModifiedDate ) {
		setId( id );
		setName( name );
		setType( type );
		setDataSet( dataSet );
		setKey( key );
		setLatestVersion( latestVersion );
		setCreatedBy( createdBy );
		setCreatedDate( createdDate );
		setLastModifiedBy( lastModifiedBy );
		setLastModifiedDate( lastModifiedDate );
		setParent( parent );
	}

	/**
	 * @return a unique identifier for this definition, based on its key, dataset and optional parent. Can be {@code null} if the definition is incomplete.
	 */
	public DynamicDefinitionId getDefinitionId() {
		return DynamicDefinitionId.create( parent, dataSet, type, key, null );
	}

	/**
	 * @return a unique string identifier for this definition, based on its key, dataset and optional parent. Can be {@code null} if the definition is incomplete.
	 */
	public String getFullKey() {
		return Objects.toString( getDefinitionId(), null );
	}

	public boolean hasParent() {
		return parent != null;
	}

	@Override
	public String toString() {
		return StringUtils.defaultString( getFullKey(), getClass().getName() + "@" + Integer.toHexString( hashCode() ) );
	}

	@Override
	public int compareTo( DynamicDefinition other ) {
		return StringUtils.compare( name, other.getName() );
	}
}
