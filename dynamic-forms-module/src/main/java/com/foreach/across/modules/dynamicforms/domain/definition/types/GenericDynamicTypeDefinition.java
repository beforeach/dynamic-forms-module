/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyRegistry;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.core.convert.TypeDescriptor;

import java.util.function.BiConsumer;

/**
 * Generic implementation of a {@link DynamicTypeDefinition}.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Builder(toBuilder = true)
public class GenericDynamicTypeDefinition implements DynamicTypeDefinition
{
	@NonNull
	@Getter
	private final String typeName;

	@NonNull
	@Getter
	private final TypeDescriptor typeDescriptor;

	private final BiConsumer<DynamicDocumentFieldDefinition, EntityPropertyDescriptorBuilder> propertyDescriptorBuilderConsumer;

	private final BiConsumer<DynamicDocumentFieldDefinition, MutableEntityPropertyRegistry> fieldPostProcessor;

	@Override
	public void customizePropertyDescriptor( DynamicDocumentFieldDefinition field, EntityPropertyDescriptorBuilder builder ) {
		if ( propertyDescriptorBuilderConsumer != null ) {
			propertyDescriptorBuilderConsumer.accept( field, builder );
		}
	}

	@Override
	public void postProcessField( DynamicDocumentFieldDefinition field, MutableEntityPropertyRegistry registry ) {
		if ( fieldPostProcessor != null ) {
			fieldPostProcessor.accept( field, registry );
		}
	}
}
