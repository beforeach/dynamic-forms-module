/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.query.EntityQuery;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.LinkedHashSet;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A {@link DynamicTypeDefinitionBuilder} for a type that refers to a specific {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument} or {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion}.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class DocumentDynamicTypeDefinitionBuilder implements DynamicTypeDefinitionBuilder
{
	private static final Pattern DOC_TYPE_REGEX = Pattern.compile( "(document(Version)?){1}(\\((.+)\\))?" );
	private static final String ATTR_OPTIONS_QUERY = EntityDynamicTypeDefinitionBuilder.ATTR_OPTIONS_QUERY;

	private final EntityRegistry entityRegistry;

	@Override
	public boolean accepts( RawDocumentDefinition.AbstractField field ) {
		return DOC_TYPE_REGEX.matcher( StringUtils.stripEnd( field.getType(), "[]" ) ).matches();
	}

	@Override
	public DynamicTypeDefinition buildTypeDefinition( DynamicTypeDefinitionFactory factory, RawDocumentDefinition.AbstractField field ) {
		Matcher matcher = DOC_TYPE_REGEX.matcher( StringUtils.stripEnd( field.getType(), "[]" ) );
		matcher.matches();
		String entityType = StringUtils.isNotBlank( matcher.group( 2 ) ) ? "dynamicDocumentVersion" : "dynamicDocument";

		val builder = GenericDynamicTypeDefinition
				.builder()
				.typeName( field.getType() );

		Class<?> entityClass = entityRegistry.getEntityConfiguration( entityType ).getEntityType();

		if ( StringUtils.endsWith( field.getType(), "[]" ) ) {
			builder.typeDescriptor( TypeDescriptor.collection( LinkedHashSet.class, TypeDescriptor.valueOf( entityClass ) ) );
		}
		else {
			builder.typeDescriptor( TypeDescriptor.valueOf( entityClass ) );
		}

		String definitionId = matcher.group( 4 );
		DynamicDefinitionId id = StringUtils.isNotBlank( definitionId ) ? DynamicDefinitionId.fromString( definitionId ) : null;
		return builder.propertyDescriptorBuilderConsumer( createDescriptorConsumer( field, id ) ).build();
	}

	private BiConsumer<DynamicDocumentFieldDefinition, EntityPropertyDescriptorBuilder> createDescriptorConsumer( RawDocumentDefinition.AbstractField field,
	                                                                                                              DynamicDefinitionId id ) {
		return ( fieldDefinition, descriptor ) -> {
			EntityQuery query = EntityQuery.and();
			if ( field.hasAttribute( ATTR_OPTIONS_QUERY ) ) {
				query = EntityQuery.parse( (String) field.getAttributes().get( ATTR_OPTIONS_QUERY ) );
			}

			if ( id != null ) {
				descriptor.attribute( DynamicDefinitionId.class, id );
				EntityQuery documentQuery = id.isForVersion()
						? EntityQuery.parse( "latestVersion.definitionVersion = '" + id + "'" )
						: EntityQuery.parse( "type = '" + id + "'" );

				if ( !CollectionUtils.isEmpty( query.getExpressions() ) || query.hasSort() ) {
					query = EntityQuery.and( documentQuery, query );
				}
				else {
					query = documentQuery;
				}
			}

			if ( !CollectionUtils.isEmpty( query.getExpressions() ) || query.hasSort() ) {
				descriptor.attribute( EntityAttributes.OPTIONS_ENTITY_QUERY, query.toString() );
			}

		};
	}
}
