/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition.IllegalFieldConfigurationException;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.bind.EntityPropertyBindingType;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.bootstrapui.elements.ViewElementFieldset;
import com.foreach.across.modules.web.ui.ViewElement;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * A {@link DynamicTypeDefinitionBuilder} for a fieldset type.
 * A fieldset represents a grouping of other fields and can have its styling.
 *
 * @author Arne Vandamme
 * @see DynamicDataFieldSet
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class FieldsetDynamicTypeDefinitionBuilder implements DynamicTypeDefinitionBuilder
{
	private final DynamicDataFieldSetTemplateRegistry fieldsetTemplateRegistry;

	@Override
	public boolean accepts( RawDocumentDefinition.AbstractField field ) {
		return StringUtils.equals( StringUtils.stripEnd( field.getType(), "[]" ), "fieldset" );
	}

	@Override
	public DynamicTypeDefinition buildTypeDefinition( DynamicTypeDefinitionFactory factory, RawDocumentDefinition.AbstractField field ) {
		val builder = GenericDynamicTypeDefinition
				.builder()
				.typeName( field.getType() );

		Object template = field.getAttributes().getOrDefault( "template", null );
		Function<ViewElementFieldset, ? extends ViewElement> resolvedTemplate = template != null ? resolveFieldSetTemplate( String.valueOf( template ) ) : null;

		if ( StringUtils.endsWith( field.getType(), "[]" ) ) {
			if ( !field.getFields().isEmpty() ) {
				throw new IllegalFieldConfigurationException( field.getId(),
				                                              "a fieldset collection may not have any direct 'fields', use the 'member' fieldset specification instead" );
			}

			if ( field.getMember() == null ) {
				throw new IllegalFieldConfigurationException( field.getId(),
				                                              "a fieldset collection must have a 'member' fieldset specification" );
			}

			builder.typeDescriptor( TypeDescriptor.collection( ArrayList.class, DynamicDataFieldSet.TYPE_DESCRIPTOR ) );
		}
		else {
			if ( field.getFields().isEmpty() ) {
				throw new IllegalFieldConfigurationException( field.getId(),
				                                              "a singular fieldset must have one or more 'fields' specified" );
			}

			builder.typeDescriptor( DynamicDataFieldSet.TYPE_DESCRIPTOR )
			       .propertyDescriptorBuilderConsumer( singleValueFieldsetDescriptor( resolvedTemplate ) );
		}

		return builder.build();
	}

	private BiConsumer<DynamicDocumentFieldDefinition, EntityPropertyDescriptorBuilder> singleValueFieldsetDescriptor( Function<ViewElementFieldset, ? extends ViewElement> templateFunction ) {
		return ( fieldDefinition, descriptorBuilder ) -> {
			String[] fieldNames = fieldDefinition.getFields()
			                                     .stream()
			                                     .filter( f -> !f.isMember() )
			                                     .map( f -> DynamicDocumentFieldDefinition.DESCRIPTOR_PREFIX + f.getCanonicalId() )
			                                     .toArray( String[]::new );

			descriptorBuilder.viewElementType( ViewElementMode.FORM_WRITE, ViewElementFieldset.ELEMENT_TYPE )
			                 .viewElementType( ViewElementMode.FORM_READ, ViewElementFieldset.ELEMENT_TYPE )
			                 .attribute( EntityPropertyBindingType.class, EntityPropertyBindingType.SINGLE_VALUE )
			                 .attribute( ViewElementFieldset.TEMPLATE, templateFunction )
			                 .attribute( EntityAttributes.FIELDSET_PROPERTY_SELECTOR, EntityPropertySelector.of( fieldNames ) )
			                 .controller( ctl -> ctl.createValueSupplier( DynamicDataFieldSet::new ) );
		};
	}

	private Function<ViewElementFieldset, ? extends ViewElement> resolveFieldSetTemplate( String templateName ) {
		Function<ViewElementFieldset, ? extends ViewElement> template = fieldsetTemplateRegistry.get( templateName );
		return template != null ? template : ViewElementFieldset.TEMPLATE_FIELDSET;
	}
}
