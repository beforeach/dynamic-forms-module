package com.foreach.across.modules.dynamicforms.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.modules.dynamicforms.DynamicFormsModule;
import com.foreach.across.modules.hibernate.business.SettableIdBasedEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Persistable;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.io.IOException;
import java.util.Locale;

import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

/**
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
@Configuration
public class DynamicDataObjectMapperConfiguration
{
	/**
	 * @return The custom JSON {@link ObjectMapper} for dynamic document data/definition conversion.
	 */
	@Bean
	@Exposed
	public ObjectMapper dynamicDataObjectMapper() {

		SimpleModule jsonModule = new SimpleModule( DynamicFormsModule.NAME );
		//entityRegistry.getEntities().forEach( e -> jsonModule.addSerializer( new CustomSerializer( entityRegistry ) ) );
		jsonModule.addSerializer( new PersistableSerializer() );
		jsonModule.addSerializer( new IdBasedEntitySerializer() );

		return Jackson2ObjectMapperBuilder.json()
		                                  .locale( Locale.US )
		                                  .indentOutput( false )
		                                  .modules( jsonModule, new JavaTimeModule() )
		                                  .failOnUnknownProperties( false )
		                                  .failOnEmptyBeans( false )
		                                  .featuresToEnable(
				                                  READ_UNKNOWN_ENUM_VALUES_AS_NULL,
				                                  READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE
		                                  )
		                                  .featuresToDisable( WRITE_DATES_AS_TIMESTAMPS )
		                                  .build();
	}

	private class PersistableSerializer extends StdSerializer<Persistable>
	{

		private PersistableSerializer() {
			super( Persistable.class );
		}

		@Override
		public void serialize( Persistable value, JsonGenerator gen, SerializerProvider serializers ) throws IOException {
			gen.writeNumber( String.valueOf( value.getId() ) );
		}
	}

	private class IdBasedEntitySerializer extends StdSerializer<SettableIdBasedEntity>
	{
		private IdBasedEntitySerializer() {
			super( SettableIdBasedEntity.class );
		}

		@Override
		public void serialize( SettableIdBasedEntity value, JsonGenerator gen, SerializerProvider serializers ) throws IOException {
			gen.writeNumber( value.getId() );
		}
	}
}
