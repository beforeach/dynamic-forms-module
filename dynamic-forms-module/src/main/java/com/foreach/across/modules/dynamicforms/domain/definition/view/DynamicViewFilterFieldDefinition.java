/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.view;

import com.foreach.across.modules.entity.query.EntityQueryOps;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Represents a single field in a view filter.
 */
@Setter(AccessLevel.PACKAGE)
@Accessors(chain = true)
public class DynamicViewFilterFieldDefinition
{
	/**
	 * -- GETTER --
	 * Id of the field.
	 */
	@Getter
	@NonNull
	private String id;

	/**
	 * -- GETTER --
	 * Descriptive label of the field.
	 */
	@Getter
	private DynamicViewDefinitionFilterSelector selector;

	/**
	 * -- GETTER --
	 * Operand to use in the filter control.
	 */
	@Getter
	private EntityQueryOps operand;

	public DynamicViewFilterFieldDefinition( String id,
	                                         DynamicViewDefinitionFilterSelector selector,
	                                         EntityQueryOps operand ) {
		this.id = id;
		this.selector = ( selector == null ? DynamicViewDefinitionFilterSelector.SINGLE : selector );
		this.operand = operand;
	}
}
