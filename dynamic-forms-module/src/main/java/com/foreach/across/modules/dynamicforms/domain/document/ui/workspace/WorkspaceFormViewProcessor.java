/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.WebDataBinder;

import static com.foreach.across.modules.bootstrapui.utils.BootstrapElementUtils.prefixControlNames;

/**
 * Applies {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace} related form configuration.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
class WorkspaceFormViewProcessor extends EntityViewProcessorAdapter
{
	@Override
	public void initializeCommandObject( EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder ) {
//		command.getProperties().setTemplateValueResolver( new EntityPropertyTemplateValueResolver()
//		{
//			@Override
//			public Object resolveTemplateValue( EntityPropertyBindingContext bindingContext, EntityPropertyDescriptor descriptor ) {
//				DynamicDocumentWorkspace document = command.getEntity( DynamicDocumentWorkspace.class );
//
//				System.out.println( command.getEntity() );
//				return null;
//			}
//		} );
	}

	@Override
	protected void postRender( EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		prefixControlNames( "entity" ).controlNamePredicate( s -> s.startsWith( "binderMap" ) ).accept( container );
	}
}
