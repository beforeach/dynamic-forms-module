/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.foreach.across.modules.bootstrapui.elements.BootstrapUiElements;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.AbstractField;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.Field;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSet;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinitionFactory;
import com.foreach.across.modules.dynamicforms.domain.definition.types.GenericDynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentFieldValue;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldExpressionParser;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldFormulaContext;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidator;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidatorFactory;
import com.foreach.across.modules.entity.registry.properties.*;
import com.foreach.across.modules.entity.util.EntityUtils;
import com.foreach.across.modules.entity.views.ViewElementMode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.domain.Sort;
import org.springframework.expression.EvaluationContext;
import org.springframework.format.Printer;
import org.springframework.util.Assert;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.regex.Pattern;

/**
 * Converts a {@link RawDocumentDefinition} into a {@link DynamicDocumentDefinition} in the context
 * of a given dataSet. This will create backing property descriptors, type and validation classes etc.
 * <p/>
 * Exceptions will be thrown if unable to find a type definition for a given type for example.
 * If a {@link DynamicDocumentDefinition} can be built, the document configuration is valid.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Slf4j
@RequiredArgsConstructor
@NotThreadSafe
public class DynamicDocumentDefinitionBuilder
{
	public static final String DOCUMENT_FIELD_ORDER = "documentFieldOrder";

	private static final Pattern FIELD_ID_PATTERN = Pattern.compile( "^[\\p{Alnum}-_]*$" );
	private static final Printer<Object> COLLECTION_COUNT_PRINTER = ( value, locale ) -> {
		Collection collection = (Collection) value;
		return collection != null ? "" + collection.size() : "0";
	};

	private final DynamicTypeDefinitionFactory typeDefinitionFactory;
	private final DynamicDocumentFieldValidatorFactory fieldValidatorFactory;
	private final DynamicDocumentFieldExpressionParser expressionParser;

	private final Map<Locale, Map<String, String>> messages = new HashMap<>();
	private final Map<String, RawDocumentDefinition.BaseField> customTypes = new HashMap<>();

	private String messageCodePrefix;

	public DynamicDocumentDefinition build( RawDocumentDefinition definition, DynamicDefinitionId dynamicDefinitionId ) {
		messages.clear();
		customTypes.clear();
		messageCodePrefix = generateMessageCodePrefix( dynamicDefinitionId );

		List<DynamicDocumentFieldDefinition> fields = new ArrayList<>();

		definition.getTypes()
		          .forEach( baseType -> customTypes.put( baseType.getId(), baseType ) );

		definition.getContent()
		          .forEach( rawField -> fields.addAll( buildDynamicFieldDefinition( rawField, null ) ) );

		DynamicDocumentDefinition documentDefinition = new DynamicDocumentDefinition( definition.getName(), dynamicDefinitionId, fields );
		documentDefinition.setRawDocumentDefinition( definition );

		registerEntityPropertyDescriptors( documentDefinition );
		registerGlobalMessages( definition );
		documentDefinition.setMessageSource( buildMessageSource() );

		return documentDefinition;
	}

	private void registerGlobalMessages( RawDocumentDefinition definition ) {
		registerMessage( "name.singular", definition.getName(), null );

		LocalizedMessage.parseAllFromMap( definition.getMessages() )
		                .forEach( msg -> registerMessage( msg.getMessageCode(), msg.getMessage(), msg.getLocale() ) );
	}

	private String generateMessageCodePrefix( DynamicDefinitionId dynamicDefinitionId ) {
		if ( dynamicDefinitionId != null ) {
			return "DynamicDocument["
					+ ( dynamicDefinitionId.isForVersion() ? dynamicDefinitionId.asDefinitionId().toString() : dynamicDefinitionId.toString() )
					+ "].";
		}
		return "";
	}

	private DynamicDocumentDefinitionMessageSource buildMessageSource() {
		DynamicDocumentDefinitionMessageSource messageSource = new DynamicDocumentDefinitionMessageSource();
		messages.forEach( ( locale, messageSet ) -> messageSource.addMessages( messageSet, locale ) );
		return messageSource;
	}

	@SuppressWarnings("unchecked")
	private List<DynamicDocumentFieldDefinition> buildDynamicFieldDefinition( Field rawField, DynamicDocumentFieldDefinition parentField ) {
		if ( !rawField.isSynthetic() ) {
			validateFieldId( rawField.getId() );
		}

		checkForCircularTypes( rawField, new HashSet<>() );

		DynamicDocumentFieldDefinition fieldDefinition;

		Collection<DynamicDocumentFieldValidator> validators = new ArrayList<>();

		Field resolvedDescriptor = resolveFieldDescriptor( rawField );

		DynamicTypeDefinition typeDefinition = typeDefinitionFactory.createTypeDefinition( resolvedDescriptor );
		fieldDefinition = new DynamicDocumentFieldDefinition( typeDefinition );
		fieldDefinition.setId( resolvedDescriptor.getId() );
		fieldDefinition.setCanonicalId( ( parentField != null ? parentField.getCanonicalId() + "." : "" ) + resolvedDescriptor.getId() );
		fieldDefinition.setCollection( isCollection( resolvedDescriptor.getType() ) );
		fieldDefinition.setMember( isCollection( resolvedDescriptor.getId() ) );
		fieldDefinition.setRawFieldDefinition( rawField );
		fieldDefinition.setLabel( StringUtils.defaultString( resolvedDescriptor.getLabel(), EntityUtils.generateDisplayName( resolvedDescriptor.getId() ) ) );
		fieldDefinition.setMessages( Collections.unmodifiableCollection( LocalizedMessage.parseAllFromMap( resolvedDescriptor.getMessages() ) ) );

		resolvedDescriptor.getValidators()
		                  .forEach( validatorSettings -> validators
				                  .add( fieldValidatorFactory.createValidator( resolvedDescriptor, fieldDefinition.getTypeDefinition(), validatorSettings ) ) );

		fieldDefinition.setValidators( validators );

		if ( rawField.isCalculatedField() ) {
			if ( expressionParser == null ) {
				throw new IllegalStateException( "Field " + rawField.getId() + " is a calculated field but no formula expression parser is available" );
			}

			fieldDefinition.setExpression( expressionParser.parseExpression( rawField.getFormula(), typeDefinition.getTypeDescriptor() ) );
		}

		fieldDefinition.setInitializerExpression( buildInitializerExpression( resolvedDescriptor, fieldDefinition ) );

		List<DynamicDocumentFieldDefinition> childFields = new ArrayList<>();
		resolvedDescriptor.getFields()
		                  .forEach( child -> childFields.addAll( buildDynamicFieldDefinition( child, fieldDefinition ) ) );
		fieldDefinition.setFields( childFields );

		List<DynamicDocumentFieldDefinition> fields = new ArrayList<>();
		fields.add( fieldDefinition );

		if ( fieldDefinition.isCollection() ) {
			Field member = resolvedDescriptor.createSyntheticMember();

			if ( member != null ) {
				member.setId( resolvedDescriptor.getId() + DynamicDocumentFieldDefinition.INDEXER );

				if ( StringUtils.isEmpty( member.getType() ) ) {
					member.setType( StringUtils.removeEnd( resolvedDescriptor.getType(), DynamicDocumentFieldDefinition.INDEXER ) );
				}

				List<DynamicDocumentFieldDefinition> collection = buildDynamicFieldDefinition( member, parentField );
				fields.addAll( collection );

				if ( !collection.isEmpty() ) {
					DynamicDocumentFieldDefinition memberFieldDefinition = collection.get( 0 );
					memberFieldDefinition.setCollectionFieldDefinition( fieldDefinition );
					fieldDefinition.setMemberFieldDefinition( memberFieldDefinition );

					if ( typeDefinition instanceof GenericDynamicTypeDefinition ) {
						TypeDescriptor fullTypeDescriptor = TypeDescriptor.collection( ArrayList.class,
						                                                               memberFieldDefinition.getTypeDefinition().getTypeDescriptor() );

						fieldDefinition.setTypeDefinition(
								( (GenericDynamicTypeDefinition) typeDefinition ).toBuilder().typeDescriptor( fullTypeDescriptor ).build()
						);
					}
				}
			}
		}

		return fields;
	}

	private BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> buildInitializerExpression( Field resolvedDescriptor,
	                                                                                                              DynamicDocumentFieldDefinition fieldDefinition ) {
		RawDocumentDefinition.DefaultValue defaultValue = resolvedDescriptor.getDefaultValue();
		boolean isSpecified = defaultValue != null && !defaultValue.equals( RawDocumentDefinition.DefaultValue.NOT_SPECIFIED );

		if ( isSpecified && defaultValue.getFormula() != null && defaultValue.getValue() != null ) {
			throw new DynamicDocumentDefinition.IllegalFieldConfigurationException( resolvedDescriptor.getId(),
			                                                                        "default value and default formula may not be combined" );
		}

		DynamicTypeDefinition typeDefinition = fieldDefinition.getTypeDefinition();

		if ( !isSpecified ) {
			return fieldDefinition.isCalculatedField() ? fieldDefinition.getExpression() : null;
		}

		if ( defaultValue.getFormula() != null ) {
			if ( expressionParser == null ) {
				throw new IllegalStateException(
						"Field " + resolvedDescriptor.getId() + " has a default formula but no formula expression parser is available" );
			}

			return expressionParser.parseExpression( defaultValue.getFormula(), typeDefinition.getTypeDescriptor() );
		}

		return ( a, b ) -> defaultValue.getValue();
	}

	private boolean isCollection( String id ) {
		return id.endsWith( DynamicDocumentFieldDefinition.INDEXER );
	}

	private void checkForCircularTypes( @NonNull AbstractField field, Set<String> knownTypes ) {
		if ( "fieldset".equals( field.getType() ) ) {
			field.getFields().forEach( f -> checkForCircularTypes( f, new HashSet<>( knownTypes ) ) );
		}

		val customType = customTypes.get( field.getType() );

		if ( customType != null ) {
			if ( knownTypes.add( customType.getId() ) ) {
				checkForCircularTypes( customType, knownTypes );
			}
			else {
				throw new DynamicDocumentDefinition.CircularFieldTypeException( field, customType );
			}
		}
	}

	private Field resolveFieldDescriptor( Field field ) {
		AbstractField template = resolveCustomType( field.getType() );

		if ( template != null ) {
			return field.asInstanceOf( template );
		}

		return field;
	}

	private AbstractField resolveCustomType( String fieldType ) {
		if ( isCollection( fieldType ) ) {
			String customMemberTypeName = StringUtils.removeEnd( fieldType, DynamicDocumentFieldDefinition.INDEXER );
			AbstractField memberTemplate = resolveCustomType( customMemberTypeName );

			if ( memberTemplate != null ) {
				val memberField = memberTemplate instanceof Field ? (Field) memberTemplate : Field.builder().build().asInstanceOf( memberTemplate );

				return Field.builder()
				            .type( memberField.getType() + DynamicDocumentFieldDefinition.INDEXER )
				            .member( memberField )
				            .build();
			}
			else {
				return null;
			}
		}
		else {
			val customType = customTypes.get( fieldType );

			if ( customType != null ) {
				val target = resolveCustomType( customType.getBaseType() );
				if ( target != null ) {
					return customType.asInstanceOf( target );
				}
			}

			return customType;
		}
	}

	private void validateFieldId( String fieldId ) {
		if ( !FIELD_ID_PATTERN.matcher( fieldId ).matches() ) {
			throw new DynamicDocumentDefinition.IllegalFieldIdException( fieldId );
		}
	}

	private void registerEntityPropertyDescriptors( DynamicDocumentDefinition definition ) {
		EntityPropertyRegistrySupport propertyRegistry = new DefaultEntityPropertyRegistry();
		definition.setEntityPropertyRegistry( propertyRegistry );

		registerEntityPropertyDescriptors( propertyRegistry, definition.getContent(), "", 0, null );
	}

	private void registerEntityPropertyDescriptors( EntityPropertyRegistrySupport propertyRegistry,
	                                                List<DynamicDocumentFieldDefinition> fields,
	                                                String namePrefix,
	                                                int startOrder,
	                                                EntityPropertyDescriptor parent ) {
		int order = startOrder;

		List<MutableEntityPropertyDescriptor> added = new ArrayList<>();

		for ( DynamicDocumentFieldDefinition field : fields ) {
			val fieldPath = namePrefix + field.getId();
			val propertyName = DynamicDocumentFieldDefinition.DESCRIPTOR_PREFIX + fieldPath;
			val fieldAsBinderTarget = "properties[" + propertyName + "]";

			val fieldAsProperty = "properties." + propertyName;

			val descriptorBuilder = EntityPropertyDescriptor
					.builder( propertyName )
					.parent( parent )
					.propertyType( field.getTypeDefinition().getTypeDescriptor() )
					.displayName( field.getLabel() )
					.hidden( field.isMember() )
					.writable( true )
					.readable( true )
					.controller( defaultController( field, fieldPath ) )
					.attribute( EntityPropertyHandlingType.class, EntityPropertyHandlingType.BINDER );

			if ( field.isMember() ) {
				String ownerPropertyName = StringUtils.removeEnd( propertyName, DynamicDocumentFieldDefinition.INDEXER );
				descriptorBuilder.parent( propertyRegistry.getProperty( ownerPropertyName ) );
			}
			else if ( parent != null ) {
				descriptorBuilder.controller( new NestedEntityPropertyController( parent, new GenericEntityPropertyController() ) );
			}

			if ( field.isCollection() ) {
				descriptorBuilder.attribute( Printer.class, COLLECTION_COUNT_PRINTER );
			}

			if ( field.isCalculatedField() ) {
				descriptorBuilder.writable( false )
				                 .viewElementType( ViewElementMode.CONTROL, BootstrapUiElements.STATIC_CONTROL );
			}
			field.getTypeDefinition()
			     .customizePropertyDescriptor( field, descriptorBuilder );

			field.getValidators()
			     .forEach( validator -> validator.customizePropertyDescriptor( field, descriptorBuilder ) );

			val descriptor = descriptorBuilder.build();
			propertyRegistry.register( descriptor );
			propertyRegistry.setPropertyOrder( descriptor.getName(), order );

			descriptor.setAttribute( DOCUMENT_FIELD_ORDER, order++ );
			added.add( descriptor );

			if ( Comparable.class.isAssignableFrom( descriptor.getPropertyType() ) ) {
				descriptor.setAttribute( Sort.Order.class, new Sort.Order( descriptor.getName() ) );
			}

			field.setEntityPropertyDescriptor( descriptor );

			field.getMessages()
			     .forEach( msg -> {
				     if ( "label".equals( msg.getMessageCode() ) ) {
					     registerMessage( fieldAsProperty, msg.getMessage(), msg.getLocale() );
				     }
				     else if ( msg.getMessageCode().startsWith( "validation." ) ) {
					     registerMessage( msg.getMessageCode() + "." + fieldAsBinderTarget, msg.getMessage(), msg.getLocale() );
				     }
				     else {
					     registerMessage( fieldAsProperty + "[" + msg.getMessageCode() + "]", msg.getMessage(), msg.getLocale() );
				     }
			     } );

			registerEntityPropertyDescriptors( propertyRegistry, field.getFields(),
			                                   fieldPath + ".",
			                                   startOrder,
			                                   descriptor );
		}
	}

	@SuppressWarnings("unchecked")
	private <U, V> Consumer<ConfigurableEntityPropertyController<U, V>> defaultController( DynamicDocumentFieldDefinition field, String fieldPath ) {
		return controller -> {
			controller
					.order( EntityPropertyController.BEFORE_ENTITY )
					.withTarget( Object.class, Object.class )
					.valueFetcher( target -> {
						if ( !field.isMember() ) {
							if ( target instanceof DynamicDocumentData ) {
								return ( (DynamicDocumentData) target ).getValue( fieldPath );
							}
							if ( target instanceof DynamicDocumentWorkspace ) {
								return ( (DynamicDocumentWorkspace) target ).getFieldValue( fieldPath );
							}
							if ( target instanceof DynamicDataFieldSet ) {
								return ( (DynamicDataFieldSet) target ).get( field.getId() );
							}
						}
						return null;
					} )
					.applyValueConsumer( ( target, value ) -> {
						Assert.notNull( target, "Cannot apply property value '" + fieldPath + "': no target set" );
						if ( !field.isMember() ) {
							if ( target instanceof DynamicDocumentData ) {
								( (DynamicDocumentData) target ).setValue( fieldPath, value.getNewValue() );
							}
							else if ( target instanceof DynamicDocumentWorkspace ) {
								( (DynamicDocumentWorkspace) target ).setFieldValue( fieldPath, value.getNewValue() );
							}
							else if ( target instanceof DynamicDataFieldSet ) {
								( (DynamicDataFieldSet) target ).put( field.getId(), value.getNewValue() );
							}
						}
					} )
					.contextualValidator( ( target, propertyValue, errors, validationHints ) -> {
						DynamicDocumentData data = null;

						if ( target instanceof DynamicDocumentData ) {
							data = (DynamicDocumentData) target;
						}
						else if ( target instanceof DynamicDocumentWorkspace ) {
							data = ( (DynamicDocumentWorkspace) target ).getFields();
						}

						DynamicDocumentFieldValue fieldValue = new DynamicDocumentFieldValue( data, fieldPath, "", field, propertyValue );

						field.getValidators().forEach( validator -> validator.validate( fieldValue, errors ) );
					} );
		};
	}

	@SuppressWarnings("all")
	private void registerMessage( String messageCode, String message, Locale locale ) {
		if ( locale == null ) {
			// Cannot be replaced with computeIfAbsent!
			Map<String, String> messageCodes = messages.get( null );
			if ( messageCodes == null ) {
				messageCodes = new HashMap<>();
				messages.put( null, messageCodes );
			}

			messageCodes.put( messageCodePrefix + messageCode, message );
		}
		else {
			messages.computeIfAbsent( locale, l -> new HashMap<>() )
			        .put( messageCodePrefix + messageCode, message );
		}
	}
}
