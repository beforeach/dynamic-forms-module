/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionType;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Order(3)
@RequiredArgsConstructor
@Installer(description = "Installs the default data definition types", phase = InstallerPhase.AfterModuleBootstrap, version = 3)
public class DynamicDataDefinitionTypeInstaller
{
	private final DynamicDefinitionTypeRepository dataDefinitionTypeRepository;

	@InstallerMethod
	public void createDefaultTypes() {
		List<DynamicDefinitionType> types = dataDefinitionTypeRepository.findAll();
		List<String> baseTypes = Arrays.asList( "type", "document", "form", "view" );
		baseTypes.forEach( typeName -> {
			Optional<DynamicDefinitionType> existingType = types.stream().filter( type -> type.getName().equals( typeName ) )
			                                                    .findFirst();
			if ( !existingType.isPresent() ) {
				dataDefinitionTypeRepository.save( DynamicDefinitionType.builder().name( typeName ).build() );
			}

		} );
	}
}
