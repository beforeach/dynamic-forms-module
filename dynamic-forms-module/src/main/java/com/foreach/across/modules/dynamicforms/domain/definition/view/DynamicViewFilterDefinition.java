/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.view;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Collections;
import java.util.List;

/**
 * Represents a single field 'filter' in a view definition.
 */
@Setter(AccessLevel.PACKAGE)
@Accessors(chain = true)
public class DynamicViewFilterDefinition
{
	@Getter
	private final Boolean basicMode;

	@Getter
	private final Boolean advancedMode;

	@Getter
	private final DynamicViewDefinitionFilterSelector defaultSelector;

	@Getter
	private final String defaultQuery;

	@Getter
	private final String basePredicate;

	@Getter
	private final List<DynamicViewFilterFieldDefinition> fields;

	public DynamicViewFilterDefinition( Boolean basicMode,
	                                    Boolean advancedMode,
	                                    DynamicViewDefinitionFilterSelector defaultSelector,
	                                    String defaultQuery,
	                                    String basePredicate,
	                                    List<DynamicViewFilterFieldDefinition> fields
	) {
		this.basicMode = fields.size() > 0 || ( basicMode == null ? false : basicMode );
		this.advancedMode = advancedMode == null ? Boolean.FALSE : advancedMode;
		this.defaultSelector = fields.stream()
		                             .anyMatch( f -> DynamicViewDefinitionFilterSelector.MULTI.equals( f.getSelector() ) )
				? DynamicViewDefinitionFilterSelector.MULTI : ( defaultSelector == null ? DynamicViewDefinitionFilterSelector.SINGLE : defaultSelector );
		this.defaultQuery = defaultQuery;
		this.basePredicate = basePredicate;
		this.fields = Collections.unmodifiableList( fields );
	}
}
