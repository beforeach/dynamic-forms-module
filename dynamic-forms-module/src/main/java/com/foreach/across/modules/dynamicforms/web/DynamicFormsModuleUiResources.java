/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.web;

import com.foreach.across.modules.web.resource.SimpleWebResourcePackage;
import com.foreach.across.modules.web.resource.WebResource;
import com.foreach.across.modules.web.resource.WebResourcePackageManager;
import org.springframework.stereotype.Component;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
public class DynamicFormsModuleUiResources extends SimpleWebResourcePackage
{
	public static final String NAME = "dfm-ui-components";

	public DynamicFormsModuleUiResources( WebResourcePackageManager adminWebResourcePackageManager ) {
		adminWebResourcePackageManager.register( NAME, this );

		setWebResources(
				// codemirror
				new WebResource( WebResource.CSS, "codemirror-css", "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.35.0/codemirror.min.css",
				                 WebResource.EXTERNAL ),
				new WebResource( WebResource.JAVASCRIPT_PAGE_END, "codemirror", "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.35.0/codemirror.min.js",
				                 WebResource.EXTERNAL ),
				new WebResource( WebResource.JAVASCRIPT_PAGE_END, "codemirror-yaml",
				                 "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.35.0/mode/yaml/yaml.min.js",
				                 WebResource.EXTERNAL ),

				new WebResource( WebResource.CSS, NAME, "/static/DynamicFormsModule/css/dfm-ui-components.css", WebResource.VIEWS ),
				new WebResource( WebResource.JAVASCRIPT_PAGE_END, NAME, "/static/DynamicFormsModule/js/dfm-ui-components.js", WebResource.VIEWS )
		);
	}
}
