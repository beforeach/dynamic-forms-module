/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionBuilder;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RequestBoundDynamicDocumentDefinitionMessageSource;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptorFactory;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistryProvider;
import com.foreach.across.modules.entity.registry.properties.MergingEntityPropertyRegistry;
import com.foreach.across.modules.entity.support.EntityMessageCodeResolver;
import com.foreach.across.modules.entity.views.context.ConfigurableEntityViewContext;
import com.foreach.across.modules.entity.views.processors.SimpleEntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.support.EntityMessages;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

/**
 * Builds the custom {@link com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry}
 * for the context of a {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace}
 * by adding the document specific fields.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class WorkspaceEntityPropertyRegistryViewProcessor extends SimpleEntityViewProcessorAdapter
{
	private final EntityPropertyRegistryProvider registryProvider;
	private final EntityPropertyDescriptorFactory descriptorFactory;
	private final MessageSource messageSource;

	@Override
	public void prepareEntityViewContext( ConfigurableEntityViewContext entityViewContext ) {
		val documentDefinition = DynamicDocumentDefinitionContextViewProcessor.currentDocumentDefinition();

		if ( documentDefinition != null ) {

			val fields = retrieveFields( documentDefinition );
			MergingEntityPropertyRegistry merged
					= new MergingEntityPropertyRegistry( entityViewContext.getPropertyRegistry(), registryProvider, descriptorFactory );

			fields.stream()
			      .map( field -> EntityPropertyDescriptor.builder( field.getName() ).original( field ).build() )
			      .forEach( merged::register );

			merged.setDefaultOrder( merged.getDefaultOrder().
					thenComparingInt( descriptor -> descriptor.getAttribute( DynamicDocumentDefinitionBuilder.DOCUMENT_FIELD_ORDER, Integer.class ) )
			);

			entityViewContext.setPropertyRegistry( merged );

			val newCodeResolver = new EntityMessageCodeResolver( entityViewContext.getMessageCodeResolver() );
			newCodeResolver.setMessageSource( documentDefinition.createMessageSource( messageSource ) );
			newCodeResolver.setPrefixes( "DynamicDocument[" + documentDefinition.getDefinitionId().asDefinitionId().toString() + "]" );
			entityViewContext.setMessageCodeResolver( newCodeResolver );
			entityViewContext.setEntityMessages( new EntityMessages( newCodeResolver ) );

			RequestBoundDynamicDocumentDefinitionMessageSource.assign( documentDefinition );
		}
	}

	private Collection<EntityPropertyDescriptor> retrieveFields( DynamicDocumentDefinition definition ) {
		if ( !definition.getContent().isEmpty() ) {
			return definition.getEntityPropertyRegistry().getRegisteredDescriptors();
		}

		return Collections.emptyList();
	}
}
