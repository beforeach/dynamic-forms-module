/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import lombok.Getter;
import lombok.NonNull;
import lombok.val;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.EvaluationContext;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Represent the document data for a specific document definition.
 * Allows reading and setting the data in a strongly typed fashion,
 * any value set will be checked against the excepted type.
 * <p/>
 * This does not apply the actual validation of the document,
 * only acceptable value conversion (expected type and allowed in the list of values).
 * <p/>
 * Supports calculated fields if {@link #setCalculatedFieldsEnabled(boolean)} is set to {@code true}.
 * By default calculated fields are disabled and only the actively stored value will be returned.
 * <p/>
 * You usually do not want to work with {@link DynamicDocumentData} directly but through a
 * {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace}.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class DynamicDocumentData implements DynamicDocumentFieldAccessorResolver
{
	/**
	 * -- GETTER --
	 * The document definition this data is for.
	 */
	@Getter
	private final DynamicDocumentDefinition documentDefinition;

	/**
	 * -- GETTER --
	 * The raw data of the document.
	 */
	@Getter
	private final Map<String, Object> content = new LinkedHashMap<>();

	private final DynamicDocumentDataAccessor accessors;

	public DynamicDocumentData( DynamicDocumentDefinition documentDefinition ) {
		this.documentDefinition = documentDefinition;

		accessors = new DynamicDocumentDataAccessor( documentDefinition, content );
	}

	/**
	 * Set the {@link ConversionService} that should be used by the {@link DynamicDocumentDataAccessor}.
	 *
	 * @param conversionService used in the data accessors
	 */
	public void setConversionService( ConversionService conversionService ) {
		accessors.setConversionService( conversionService );
	}

	/**
	 * Set a custom {@link org.springframework.expression.EvaluationContext} that should be used for the calculated fields.
	 * Usually a default context will have been set when creating the document data or the
	 * {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace}.
	 * <p/>
	 * If no evaluation context is set, field calculations will not be supported at all.
	 *
	 * @param evaluationContext to use for calculated fields
	 */
	public void setEvaluationContext( EvaluationContext evaluationContext ) {
		accessors.setEvaluationContext( evaluationContext );
	}

	/**
	 * Set the {@link DynamicDocumentService} that can be used for retrieving nested dynamic document fields.
	 * If none is set, nested document fields will not be supported (eg. for calculations).
	 *
	 * @param documentService to use
	 */
	public void setDocumentService( DynamicDocumentService documentService ) {
		accessors.setDocumentService( documentService );
	}

	/**
	 * Retrieve the value for a property path.
	 * There is no way to distinguish between the presence of the field and
	 * an empty field ({@code null} value). In both cases this will return {@code null}.
	 *
	 * @param fieldPath path to the field
	 * @param <T>       expected type of the value
	 * @return value or {@code null} if field not present (or having actual {@code null} value)
	 * @see #findValue(String)
	 */
	@SuppressWarnings("OptionalAssignedToNull")
	public <T> T getValue( String fieldPath ) {
		Optional<T> optionalValue = findValue( fieldPath );
		return optionalValue != null ? optionalValue.orElse( null ) : null;
	}

	/**
	 * Alternative for {@link #getValue(String)} which returns a <strong>nullable</strong> {@code Optional}
	 * holding the field value. If {@code null} is returned, it means the field was not at all present
	 * in the document. Else the {@code Optional} will hold the field value, which might be {@code null}.
	 *
	 * @param fieldPath path to the field
	 * @param <T>       expected type of the value
	 * @return {@code null} if field not present, else a field value holder
	 */
	@SuppressWarnings({ "unchecked", "OptionalAssignedToNull" })
	public <T> Optional<T> findValue( String fieldPath ) {
		val field = accessors.findFieldAccessor( fieldPath ).orElse( null );

		if ( field != null && field.exists() ) {
			return Optional.ofNullable( (T) field.get() );
		}

		return null;
	}

	/**
	 * Retrieve the template value for a property path. This is the same as the newly initialized value of the field,
	 * but without actually performing the field initialization. The actual field value will be ignored and not
	 * modified when requesting the template value.
	 *
	 * @param fieldPath path to the field
	 * @param <T>       expected type of the value
	 * @return value or {@code null} if field does not exist (or has an actual {@code null} value)
	 */
	@SuppressWarnings( "unchecked" )
	public <T> T getDefaultValue( @NonNull String fieldPath ) {
		val field = accessors.findFieldAccessor( fieldPath ).orElse( null );

		if ( field != null ) {
			return (T) field.getDefaultValue();
		}

		return null;
	}

	/**
	 * Set the value for a particular property path.
	 * This requires a matching {@link DynamicDocumentFieldDefinition} to be
	 * present for that property path, and the value to be of the target type expected by the field definition.
	 * <p/>
	 * This might also set intermediate property values if necessary.
	 * <p/>
	 * Exceptions will be thrown in case there is no such field or the value is of an unacceptable type.
	 *
	 * @param fieldPath to the field whose value should be set
	 * @param value     that should be set
	 */
	public Object setValue( String fieldPath, Object value ) {
		return accessors.getFieldAccessor( fieldPath ).set( value );
	}

	/**
	 * Get the {@link TypeDescriptor} for a given field.
	 * Will be empty if no field defined for that path.
	 *
	 * @param fieldPath to the field
	 * @return type descriptor
	 */
	public Optional<TypeDescriptor> getFieldType( String fieldPath ) {
		return accessors.findFieldAccessor( fieldPath ).map( DynamicDocumentFieldAccessor::getFieldType );
	}

	/**
	 * Get a field accessor for a particular property path.
	 * This requires a matching {@link DynamicDocumentFieldDefinition} to be present for that property path.
	 *
	 * @param fieldPath to the field
	 * @return field accessor
	 */
	public DynamicDocumentFieldAccessor getFieldAccessor( String fieldPath ) {
		return accessors.getFieldAccessor( fieldPath );
	}

	/**
	 * Find a field accessor for the given property path.
	 * Will return {@code empty} if accessor can't be found because no such field is registered in the definition.
	 *
	 * @param fieldPath path to the field
	 * @return optional containing the accessor, never {@code null}
	 */
	@Override
	public Optional<DynamicDocumentFieldAccessor> findFieldAccessor( String fieldPath ) {
		return accessors.findFieldAccessor( fieldPath );
	}

	/**
	 * Load the given data into the document. Can be a map with fully qualified property keys,
	 * or with nested lists and maps.
	 *
	 * @param data to load in this document
	 */
	public void load( @NonNull Map<String, Object> data ) {
		data.forEach( this::setValue );
	}

	/**
	 * Delete a field from the document.
	 * Nothing will be done if that field (or its definition) does not exist.
	 *
	 * @param fieldPath path to the field that should be deleted
	 */
	public Object deleteValue( String fieldPath ) {
		return accessors.findFieldAccessor( fieldPath )
		                .map( DynamicDocumentFieldAccessor::delete )
		                .orElse( null );
	}

	/**
	 * Reset a field value back to its default.
	 * An exception will be thrown if there is no field definition for that path.
	 *
	 * @param fieldPath path to the field that should be reset
	 */
	public void resetValue( String fieldPath ) {
		val accessor = accessors.getFieldAccessor( fieldPath );
		accessor.delete();
		accessor.getOrCreate();
	}

	/**
	 * Build a new {@link Map} accessor for this document data, as if all
	 * fields of the document have been flattened. Any operations performed
	 * on the map will result in type checking being applied.
	 * <p/>
	 * The adapter returned can be customized for databinding.
	 *
	 * @return map
	 */
	public DynamicDocumentDataAsMapAdapter asFlatMap() {
		return new DynamicDocumentDataAsMapAdapter( this );
	}

	/**
	 * When enabled, calculated fields will always be re-calculated when fetched.
	 * If disabled, the last calculated value will be returned instead and you must update
	 * them manually using {@link #updateCalculatedFields()}.
	 *
	 * @param enabled true if calculated fields should be enabled
	 */
	public void setCalculatedFieldsEnabled( boolean enabled ) {
		accessors.setCalculateFields( enabled );
	}

	/**
	 * @return true if calculated fields are enabled
	 */
	public boolean isCalculatedFieldsEnabled() {
		return accessors.isCalculateFields();
	}

	/**
	 * Recalculates all calculated fields in this document and updates the cached value.
	 * You usually want to do this before saving a document as this might add missing calculated fields.
	 * Note that only live calculation of field values ({@link #isCalculatedFieldsEnabled()} is {@code true})
	 * will be guaranteed to produce the correct result for inter-dependent calculated fields.
	 */
	public void updateCalculatedFields() {
		boolean wasEnabled = isCalculatedFieldsEnabled();
		setCalculatedFieldsEnabled( true );
		documentDefinition.getContent().stream()
		                  .filter( field -> !field.isMember() )
		                  .map( field -> getFieldAccessor( field.getId() ) )
		                  .forEach( DynamicDocumentFieldAccessor::recalculate );
		setCalculatedFieldsEnabled( wasEnabled );
	}

	/**
	 * Initialize all fields in this document.
	 * Initialization will only occur if the field has no value set and if it has an initializer expressoin.
	 */
	public void initializeFields() {
		boolean wasEnabled = accessors.isInitializeFields();
		accessors.setInitializeFields( true );
		documentDefinition.getContent().stream()
		                  .filter( field -> !field.isMember() )
		                  .map( field -> getFieldAccessor( field.getId() ) )
		                  .forEach( DynamicDocumentFieldAccessor::initialize );
		accessors.setInitializeFields( wasEnabled );
	}

	/**
	 * Create a new accessor for the same document data.
	 * Used by {@link DynamicDocumentDataLoader}.
	 *
	 * @return new accessor
	 */
	DynamicDocumentDataAccessor createDataAccessor() {
		DynamicDocumentDataAccessor accessor = new DynamicDocumentDataAccessor( documentDefinition, content );
		accessor.setEvaluationContext( accessors.getEvaluationContext() );
		accessor.setDocumentService( accessors.getDocumentService() );
		return accessor;
	}
}
