/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.query;

import com.foreach.across.modules.entity.query.EntityQueryFacade;
import com.foreach.across.modules.entity.query.EntityQueryFacadeFactory;
import com.foreach.across.modules.entity.query.EntityQueryParserFactory;
import com.foreach.across.modules.entity.query.SimpleEntityQueryFacade;
import com.foreach.across.modules.entity.registry.EntityConfiguration;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import com.foreach.across.modules.entity.views.context.EntityViewContext;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * Custom {@link com.foreach.across.modules.entity.query.EntityQueryFacadeResolver} registered on the
 * workspace entity configuration. Allows executing entity queries on document workspaces, including
 * on their internal fields.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class DynamicDocumentWorkspaceEntityQueryFacadeResolver implements EntityQueryFacadeFactory
{
	private final EntityQueryParserFactory entityQueryParserFactory;
	private final AutowireCapableBeanFactory beanFactory;

	@Override
	public EntityQueryFacade createForEntityConfiguration( EntityConfiguration entityConfiguration ) {
		WorkspaceEntityQueryExecutor executor = beanFactory.getBean( WorkspaceEntityQueryExecutor.class );
		executor.setPropertyRegistry( entityConfiguration.getPropertyRegistry() );

		return new SimpleEntityQueryFacade<>( entityQueryParserFactory.createParser( entityConfiguration.getPropertyRegistry() ), executor );
	}

	@Override
	public EntityQueryFacade createForEntityViewContext( EntityViewContext viewContext ) {
		EntityPropertyRegistry propertyRegistry = viewContext.getPropertyRegistry();

		WorkspaceEntityQueryExecutor queryExecutor = beanFactory.getBean( WorkspaceEntityQueryExecutor.class );
		queryExecutor.setPropertyRegistry( propertyRegistry );

		return new SimpleEntityQueryFacade<>( entityQueryParserFactory.createParser( propertyRegistry ), queryExecutor );
	}
}
