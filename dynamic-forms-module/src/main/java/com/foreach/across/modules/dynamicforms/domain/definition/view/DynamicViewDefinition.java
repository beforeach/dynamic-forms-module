/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.view;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Collections;
import java.util.List;

/**
 * Represents the definition of a single view.
 * <p/>
 * A {@code DynamicViewDefinition} should not be built directly but by creating a
 * {@link RawViewDefinition} and converting
 * it into a {@code DynamicViewDefinition} using a {@link DynamicViewDefinitionBuilder}.
 * The latter will perform validation of your definition, ensuring it is usable.
 *
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
@Setter(AccessLevel.PACKAGE)
@Accessors(chain = true)
public class DynamicViewDefinition
{
	@Getter
	private final List<String> properties;

	@Getter
	private final DynamicViewFilterDefinition filter;

	DynamicViewDefinition( @NonNull List<String> properties, DynamicViewFilterDefinition filter ) {
		this.properties = Collections.unmodifiableList( properties );
		this.filter = filter;
	}
}