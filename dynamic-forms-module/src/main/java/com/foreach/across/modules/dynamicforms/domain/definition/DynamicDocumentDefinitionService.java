/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinitionBuilder;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinitionFactory;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldExpressionParser;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidatorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Main service for manipulating dynamic documents related definitions.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Service
@RequiredArgsConstructor
public class DynamicDocumentDefinitionService
{
	private final DynamicTypeDefinitionFactory typeDefinitionFactory;
	private final DynamicDocumentFieldValidatorFactory fieldValidatorFactory;
	private final RawDefinitionService rawDefinitionService;
	private final DynamicDocumentFieldExpressionParser fieldExpressionParser;

	/**
	 * Load the full {@link DynamicDocumentDefinition} associated with a definition version record.
	 *
	 * @param definitionVersion definition version
	 * @return loaded document definition
	 */
	public DynamicDocumentDefinition loadDocumentDefinition( DynamicDefinitionVersion definitionVersion ) {
		return buildDocumentDefinition( definitionVersion.getDefinition(),
		                                rawDefinitionService.readRawDefinition( definitionVersion.getDefinitionContent(), RawDocumentDefinition.class ) );
	}

	/**
	 * Build a typed document definition for a raw definition object.
	 *
	 * @param definition            record
	 * @param rawDocumentDefinition deserialized definition
	 * @return loaded document definition
	 */
	private DynamicDocumentDefinition buildDocumentDefinition( DynamicDefinition definition, RawDocumentDefinition rawDocumentDefinition ) {
		return new DynamicDocumentDefinitionBuilder( typeDefinitionFactory, fieldValidatorFactory, fieldExpressionParser )
				.build( rawDocumentDefinition, definition.getDefinitionId() );
	}
}
