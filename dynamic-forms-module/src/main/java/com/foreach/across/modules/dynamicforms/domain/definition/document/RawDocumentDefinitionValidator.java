/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinitionFactory;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldExpressionParser;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidatorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validator which verifies that a {@link RawDocumentDefinition} is valid
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Component
public class RawDocumentDefinitionValidator implements Validator
{
	private final DynamicTypeDefinitionFactory dynamicTypeDefinitionFactory;
	private final DynamicDocumentFieldValidatorFactory fieldValidatorFactory;
	private final DynamicDocumentFieldExpressionParser fieldExpressionParser;

	@Override
	public boolean supports( Class<?> clazz ) {
		return RawDocumentDefinition.class.isAssignableFrom( clazz );
	}

	@Override
	public void validate( Object target, Errors errors ) {
		try {
			new DynamicDocumentDefinitionBuilder( dynamicTypeDefinitionFactory, fieldValidatorFactory, fieldExpressionParser )
					.build( (RawDocumentDefinition) target, null );
		}
		catch ( Exception e ) {
			errors.rejectValue( "definitionContent", e.getClass().getSimpleName(), new String[] { e.getMessage() }, e.getMessage() );
		}
	}
}
