/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui;

import com.foreach.across.modules.bootstrapui.elements.Grid;
import com.foreach.across.modules.bootstrapui.elements.builder.AlertViewElementBuilder;
import com.foreach.across.modules.bootstrapui.ui.factories.BootstrapViewElements;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDocumentDefinitionService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataLoader;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataMapperFactory;
import com.foreach.across.modules.entity.bind.EntityPropertiesBinder;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderService;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.web.EntityViewModel;
import com.foreach.across.modules.web.ui.DefaultViewElementBuilderContext;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilder;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilder;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.foreach.across.modules.web.ui.elements.HtmlViewElements.html;

/**
 * Helper for creating {@link com.foreach.across.modules.web.ui.ViewElement}
 * and {@link com.foreach.across.modules.web.ui.ViewElementBuilder} components
 * for document and document versions.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Service
@RequiredArgsConstructor
public class DynamicDocumentUiComponents
{
	private final EntityViewElementBuilderHelper builderHelper;
	private final EntityViewElementBuilderService viewElementBuilderService;
	private final DynamicDocumentDefinitionService definitionService;
	private final DynamicDocumentDataMapperFactory documentDataMapperFactory;
	private final DynamicDocumentService documentService;

	/**
	 * Create a readonly (form) view of all the data in a document version.
	 *
	 * @param documentVersion to view
	 * @return builder
	 */
	public ViewElementBuilder documentDataView( DynamicDocumentVersion documentVersion ) {
		return builderContext -> {
			val workspace = documentService.createDocumentWorkspace( documentVersion );
			ContainerViewElementBuilder formBuilder = html.builders.container();

			if ( workspace.hasErrors() ) {
				formBuilder.add( reportErrors( workspace.getReport() ) );
			}

			val documentData = workspace.getFields();
			documentData
					.getDocumentDefinition()
					.getContent()
					.stream()
					.filter( field -> !field.isMember() )
					.forEach(
							field ->
									formBuilder.add(
											viewElementBuilderService.createElementBuilder( field.getEntityPropertyDescriptor(), ViewElementMode.FORM_READ )
									)
					);

			val subContext = new DefaultViewElementBuilderContext( builderContext );
			subContext.setAttribute( EntityViewModel.ENTITY, documentData );
			EntityPropertiesBinder binder = new EntityPropertiesBinder( documentData.getDocumentDefinition().getEntityPropertyRegistry() );
			binder.setEntity( documentData );
			subContext.setAttribute( EntityPropertiesBinder.class, binder );

			return formBuilder.build( subContext );
		};
	}

	/**
	 * Create a fieldset with basic version information of a document version (actual document version, definition version etc).
	 *
	 * @param documentVersion to view
	 * @return fieldset builder
	 */
	public ViewElementBuilder documentVersionInformation( DynamicDocumentVersion documentVersion ) {
		return builderContext -> {
			val controlGenerator = builderHelper.createBatchForEntityType( DynamicDocumentVersion.class );
			controlGenerator.setPropertySelector( EntityPropertySelector.of( "version", "definitionVersion" ) );
			controlGenerator.setViewElementMode( ViewElementMode.FORM_READ );
			controlGenerator.setEntity( documentVersion );

			Map<String, ViewElement> versionControls = controlGenerator.build( builderContext );

			return BootstrapViewElements.bootstrap.builders.fieldset()
			                                               .legend( "#{version.info=Document version information}" )
			                                               .add(
					                                               BootstrapViewElements.bootstrap.builders.row()
					                                                                                       .add( BootstrapViewElements.bootstrap.builders
							                                                                                             .column()
							                                                                                             .layout( Grid.Device.MD.width( 4 ) )
							                                                                                             .add( versionControls
									                                                                                                   .get( "version" ) ) )
					                                                                                       .add( BootstrapViewElements.bootstrap.builders
							                                                                                             .column()
							                                                                                             .layout( Grid.Device.MD.width( 8 ) )
							                                                                                             .add( versionControls
									                                                                                                   .get( "definitionVersion" ) ) )
			                                               )
			                                               .build( builderContext );
		};
	}

	/**
	 * Create a danger alert containing the error details of a report.
	 *
	 * @param report containing the field errors
	 * @return alert
	 */
	public AlertViewElementBuilder reportErrors( DynamicDocumentDataLoader.Report report ) {
		if ( report == null || !report.hasErrors() ) {
			return null;
		}

		val alert = BootstrapViewElements.bootstrap.builders.alert().danger().add(
				html.builders.text( "#{dynamicDocumentDataLoader.errors=Some field values could not be loaded:}" ) );

		val list = html.builders.ul();
		report.getErrors()
		      .forEach( e -> list.add( html.builders.li().add( html.builders.text( e.getPath() + ": " + e.getReason() ) ) ) );

		return alert.add( list );
	}
}
