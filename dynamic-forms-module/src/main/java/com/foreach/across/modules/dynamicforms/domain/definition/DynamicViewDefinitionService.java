/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.view.DynamicViewDefinitionBuilder;
import com.foreach.across.modules.dynamicforms.domain.definition.view.RawViewDefinition;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Main service for manipulating dynamic view related definitions.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Service
@RequiredArgsConstructor
public class DynamicViewDefinitionService
{
	private final RawDefinitionService rawDefinitionService;

	/**
	 * Load the full {@link DynamicViewDefinition} associated with a definition version record.
	 *
	 * @param definitionVersion definition version
	 * @return loaded view definition
	 */
	public DynamicViewDefinition loadViewDefinition( DynamicDefinitionVersion definitionVersion ) {
		RawViewDefinition rawViewDefinition = rawDefinitionService.readRawDefinition( definitionVersion.getDefinitionContent(), RawViewDefinition.class );
		return new DynamicViewDefinitionBuilder().build( rawViewDefinition );
	}
}
