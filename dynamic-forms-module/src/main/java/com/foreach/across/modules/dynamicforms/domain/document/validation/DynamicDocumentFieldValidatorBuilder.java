/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.validation;

import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;

import java.util.Map;

/**
 * Represents a builder for a single {@link DynamicDocumentFieldValidator}.
 * A {@link DynamicDocumentFieldValidatorFactory} will iterate through the list of builders,
 * and use {@link #accepts(RawDocumentDefinition.AbstractField, DynamicTypeDefinition, Map)}
 * to check which builder applies for a given validator configuration and field.
 * The default implementation will always use the first matching builder.
 *
 * @author Arne Vandamme
 * @see DynamicDocumentFieldValidatorFactory
 * @see DynamicDocumentFieldValidator
 * @since 0.0.1
 */
public interface DynamicDocumentFieldValidatorBuilder
{
	/**
	 * Does this builder apply for the validator settings?
	 *
	 * @param field             to which the validator is aded
	 * @param typeDefinition    for the field
	 * @param validatorSettings configuration settings of the validator
	 * @return true if builder should be used
	 */
	boolean accepts( RawDocumentDefinition.AbstractField field, DynamicTypeDefinition typeDefinition, Map<String, Object> validatorSettings );

	/**
	 * Build a specific validator for a field.
	 *
	 * @param field             to which the validator is aded
	 * @param typeDefinition    for the field
	 * @param validatorSettings configuration settings of the validator
	 * @return validator (never {@code null})
	 */
	DynamicDocumentFieldValidator buildValidator( RawDocumentDefinition.AbstractField field,
	                                              DynamicTypeDefinition typeDefinition,
	                                              Map<String, Object> validatorSettings );
}
