/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataFieldWriter;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataLoader;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;

import java.util.Map;

/**
 * Data mapper that reads JSON data into a document.
 * This is the default mapper used to actually store document data on
 * a {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument} and read it back out.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
public class JsonDocumentDataMapper implements DynamicDocumentDataMapper<String>
{
	@NonNull
	private final ObjectMapper dynamicDataObjectMapper;

	private final ConversionService conversionService;

	@Override
	public void load( DynamicDocumentData document, String json ) {
		DynamicDocumentDataLoader loader = new DynamicDocumentDataLoader( document );
		loader.setConversionService( conversionService );
		load( loader, json );
	}

	@Override
	@SneakyThrows
	@SuppressWarnings("unchecked")
	public void load( @NonNull DynamicDocumentDataFieldWriter document, String json ) {
		if ( StringUtils.isNotEmpty( json ) ) {
			document.setFieldValues( dynamicDataObjectMapper.readValue( json, Map.class ) );
		}
	}

	@Override
	@SneakyThrows
	public String dump( @NonNull DynamicDocumentData document ) {
		return dynamicDataObjectMapper.writeValueAsString( document.getContent() );
	}
}
