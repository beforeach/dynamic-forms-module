/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;

/**
 * Helper object which represents a single field in a document in a readonly fashion.
 *
 * @author Arne Vandamme
 * @see DynamicDocumentFieldValidator
 * @since 0.0.1
 */
@Getter
@RequiredArgsConstructor
public class DynamicDocumentFieldValue
{
	/**
	 * -- GETTER --
	 * The document that holds this field value.
	 */
	private final DynamicDocumentData document;

	/**
	 * -- GETTER --
	 * Full path to the field in the document.
	 */
	private final String fieldPath;

	/**
	 * -- GETTER --
	 * Unique key identifying the key in a specific context.
	 * Usually this is valid bean path to the field, where
	 * the field is part of a {@link java.util.Map} on a target object.
	 * <p/>
	 * Validators should use the field key when rejecting values.
	 */
	private final String fieldKey;

	/**
	 * -- GETTER --
	 * Field definition information.
	 */
	private final DynamicDocumentFieldDefinition fieldDefinition;

	/**
	 * -- GETTER --
	 * Value of the field.
	 */
	private final Object fieldValue;

	/**
	 * @return type descriptor for the field value
	 */
	public TypeDescriptor getFieldTypeDescriptor() {
		return fieldDefinition.getTypeDefinition().getTypeDescriptor();
	}
}

