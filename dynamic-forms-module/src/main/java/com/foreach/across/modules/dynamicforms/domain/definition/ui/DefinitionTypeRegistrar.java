/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.ui;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionType;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypeConfigurationRegistry;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypes;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinitionValidator;
import com.foreach.across.modules.dynamicforms.domain.definition.view.RawViewDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.view.RawViewDefinitionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Registers the default {@link DynamicDefinitionType}s on the {@link DynamicDefinitionTypeConfigurationRegistry}.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Configuration
public class DefinitionTypeRegistrar
{
	@Autowired
	public void registerBaseTypes( DynamicDefinitionTypeConfigurationRegistry typeConfigurationRegistry,
	                               RawDocumentDefinitionValidator rawDocumentDefinitionValidator,
	                               RawViewDefinitionValidator rawViewDefinitionValidator ) {
		typeConfigurationRegistry.register( DynamicDefinitionTypes.DOCUMENT, RawDocumentDefinition.class, rawDocumentDefinitionValidator );
		typeConfigurationRegistry.register( DynamicDefinitionTypes.VIEW, RawViewDefinition.class, rawViewDefinitionValidator );
	}
}
