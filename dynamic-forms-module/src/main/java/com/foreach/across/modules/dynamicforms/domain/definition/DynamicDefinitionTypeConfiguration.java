/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.Validator;

/**
 * Configuration object that should be registered for any custom {@link DynamicDefinitionType} that is available.
 * The configuration object is used to resolve how the content for a {@link DynamicDefinition} should be parsed and validated.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Getter
@RequiredArgsConstructor
public class DynamicDefinitionTypeConfiguration
{
	/**
	 * The name representing the {@link DynamicDefinitionType}.
	 */
	private final String name;

	/**
	 * The {@link DynamicDefinitionType} instance for this configuration
	 */
	private final DynamicDefinitionType type;

	/**
	 * The class of the raw definition for the {@link DynamicDefinitionType}.
	 */
	private final Class<? extends RawDefinition> rawDefinitionClass;

	/**
	 * The validator that should be used to validate content for the {@link DynamicDefinition}
	 */
	private final Validator validator;

}
