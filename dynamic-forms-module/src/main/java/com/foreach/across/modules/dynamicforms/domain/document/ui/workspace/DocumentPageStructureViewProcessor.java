/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.adminweb.menu.AdminMenu;
import com.foreach.across.modules.adminweb.ui.PageContentStructure;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.entity.conditionals.ConditionalOnAdminWeb;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.context.EntityViewContext;
import com.foreach.across.modules.entity.views.menu.EntityAdminMenu;
import com.foreach.across.modules.entity.views.processors.SingleEntityPageStructureViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.EntityPageStructureRenderedEvent;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.views.support.EntityMessages;
import com.foreach.across.modules.web.menu.MenuFactory;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.TextViewElement;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilderSupport;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.foreach.across.modules.bootstrapui.ui.factories.BootstrapViewElements.bootstrap;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@ConditionalOnAdminWeb
@Component
@Scope("prototype")
@Accessors(chain = true)
public class DocumentPageStructureViewProcessor extends SingleEntityPageStructureViewProcessor
{
	private MenuFactory menuFactory;
	private ApplicationEventPublisher eventPublisher;

	/**
	 * Should the entity menu be generated and added to the page nav.
	 */
	@Setter
	private boolean addEntityMenu = true;

	/**
	 * Message code that should be resolved for the title of the page.
	 * Will also check if there is a <strong>.subText</strong> version to determine if sub text should be added to the title.
	 */
	@Setter
	private String titleMessageCode = EntityMessages.PAGE_TITLE_VIEW;

	@Override
	protected void render( EntityViewRequest entityViewRequest,
	                       EntityView entityView,
	                       ContainerViewElementBuilderSupport<?, ?> containerBuilder,
	                       ViewElementBuilderMap builderMap,
	                       ViewElementBuilderContext builderContext ) {
		EntityViewContext entityViewContext = resolveEntityViewContext( entityViewRequest );
		PageContentStructure page = entityViewRequest.getPageContentStructure();
		page.setRenderAsTabs( true );

		configureBreadcumb( entityViewContext );

		String codeToResolve = resolveCodeForView( entityViewRequest );

		EntityMessages entityMessages = entityViewContext.getEntityMessages();

		getPageHeading( codeToResolve, entityMessages, entityViewContext ).ifPresent( page::setPageTitle );

		getPageHeading( codeToResolve + ".subText", entityMessages, entityViewContext )
				.ifPresent( subText -> page.addToPageTitleSubText( TextViewElement.html( subText ) ) );

		if ( addEntityMenu ) {
			buildEntityMenu( entityViewContext, page, builderContext );
		}
	}

	private String resolveCodeForView( EntityViewRequest entityViewRequest ) {
		String codeToResolve;
		switch ( entityViewRequest.getViewName() ) {
			case EntityView.CREATE_VIEW_NAME:
				codeToResolve = EntityMessages.PAGE_TITLE_CREATE;
				break;
			case EntityView.DELETE_VIEW_NAME:
				codeToResolve = EntityMessages.PAGE_TITLE_DELETE;
				break;
			case EntityView.UPDATE_VIEW_NAME:
				codeToResolve = EntityMessages.PAGE_TITLE_UPDATE;
				break;
			default:
				codeToResolve = titleMessageCode;
		}
		return codeToResolve;
	}

	private Optional<String> getPageHeading( String messageCode, EntityMessages entityMessages, EntityViewContext entityViewContext ) {
		String documentName = entityViewContext.holdsEntity()
				? entityViewContext.getEntity( DynamicDocumentWorkspace.class ).getDocument().getName()
				: "";

		return Optional.ofNullable( StringUtils.defaultIfEmpty( entityMessages.withNameSingular( messageCode, documentName ), null ) );
	}

	private void configureBreadcumb( EntityViewContext entityViewContext ) {
		AdminMenu adminMenu = (AdminMenu) menuFactory.getMenuWithName( AdminMenu.NAME );
		if ( adminMenu != null ) {
			if ( entityViewContext.holdsEntity() ) {
				val document = entityViewContext.getEntity( DynamicDocumentWorkspace.class );
				adminMenu.breadcrumbLeaf( document.getDocument().getName() );
			}
			else {
				adminMenu.breadcrumbLeaf( entityViewContext.getEntityMessages().createPageTitle() );
			}
		}
	}

	@Override
	protected void postRender( EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		EntityViewContext entityViewContext = resolveEntityViewContext( entityViewRequest );

		EntityPageStructureRenderedEvent event = new EntityPageStructureRenderedEvent( false, entityViewRequest, entityView, entityViewContext,
		                                                                               builderContext );
		eventPublisher.publishEvent( event );
	}

	@SuppressWarnings("unchecked")
	private void buildEntityMenu( EntityViewContext entityViewContext, PageContentStructure page, ViewElementBuilderContext builderContext ) {
		EntityAdminMenu entityMenu = EntityAdminMenu.create( entityViewContext );
		menuFactory.buildMenu( entityMenu );

		page.addToNav(
				bootstrap.builders
						.nav()
						.menu( entityMenu )
						.tabs()
						.replaceGroupBySelectedItem()
						.build( builderContext )
		);
	}

	private EntityViewContext resolveEntityViewContext( EntityViewRequest entityViewRequest ) {
		return entityViewRequest.getEntityViewContext();
	}

	@Autowired
	void setMenuFactory( MenuFactory menuFactory ) {
		this.menuFactory = menuFactory;
	}

	@Autowired
	void setEventPublisher( ApplicationEventPublisher eventPublisher ) {
		this.eventPublisher = eventPublisher;
	}
}
