/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.validation;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentFieldValue;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;
import java.util.function.Function;

/**
 * A validator for a {@link DynamicDocumentData} instance.
 * Takes a generator for converting the path of a field
 * (eg. {@code user.name}) to a target object bean path (eg. {@code [user.name]}).
 * The latter is also considered the {@code fieldKey} and will be available as
 * {@link DynamicDocumentFieldValue#getFieldKey()}.
 * <p/>
 * Two factory methods are available as well:
 * <ul>
 * <li>{@link #forPlainTarget()} creates an instance that returns the field path as field key</li>
 * <li>{@link #forIndexedTarget(String)} creates an instance that builds an indexer string of the field path, optionally with an additional prefix</li>
 * </ul>
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Deprecated
public class DynamicDocumentDataValidator implements Validator
{
	@NonNull
	private final Function<String, String> fieldKeyGenerator;

	@Override
	public boolean supports( Class<?> clazz ) {
		return DynamicDocumentData.class.isAssignableFrom( clazz );
	}

	@Override
	public void validate( Object target, Errors errors ) {
		DynamicDocumentData document = (DynamicDocumentData) target;
		validateFields( "", document, document.getDocumentDefinition().getContent(), errors );
	}

	private void validateFields( String parentPath, DynamicDocumentData document, List<DynamicDocumentFieldDefinition> fields, Errors errors ) {
		fields.forEach(
				field -> {
					// todo: change this - for now skip indexer field validation
					if ( !field.isMember() ) {
						String fieldPath = parentPath + field.getId();
						val accessor = document.getFieldAccessor( fieldPath );
						val fieldDefinition = accessor.getFieldDefinition();
						val validators = fieldDefinition.getValidators();
						val fieldKey = fieldKeyGenerator.apply( fieldPath );

						if ( !validators.isEmpty() ) {
							DynamicDocumentFieldValue fieldInfo = new DynamicDocumentFieldValue( document, fieldPath, fieldKey, fieldDefinition,
							                                                                     accessor.get() );
							validators.forEach( v -> v.validate( fieldInfo, errors ) );
						}

						validateFields( fieldPath + ".", document, field.getFields(), errors );
					}
				}
		);
	}

	/**
	 * @return a validator that uses the field path as field key
	 */
	public static DynamicDocumentDataValidator forPlainTarget() {
		return new DynamicDocumentDataValidator( path -> path );
	}

	/**
	 * @param targetProperty optional prefix for the field key
	 * @return validator mapping the field path to an indexed key on the target property and uses that as a field key
	 */
	public static DynamicDocumentDataValidator forIndexedTarget( String targetProperty ) {
		String prefix = StringUtils.defaultString( targetProperty );
		return new DynamicDocumentDataValidator( path -> prefix + "[" + path + "]" );
	}
}
