/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data.expressions;

import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentFieldAccessor;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentFieldAccessorResolver;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * Represents a evaluation context object for a field in {@link com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData}.
 * This is always the root context object against which expressions are executed.
 *
 * @author Arne Vandamme
 * @see BuildDynamicDocumentFormulaEvaluationContext
 * @since 0.0.1
 */
@RequiredArgsConstructor
public class DynamicDocumentFieldFormulaContext
{
	private final DynamicDocumentFieldAccessorResolver rootResolver;
	private final DynamicDocumentFieldAccessorResolver relativeResolver;

	/**
	 * Return a field accessor or {@code null} if no such field.
	 * If a {@link #relativeResolver} has been set any {@code $(.name)} will be resolved by the relative resolver.
	 *
	 * @param path to the field
	 * @return accessor
	 */
	public DynamicDocumentFieldAccessor field( String path ) {
		if ( StringUtils.startsWith( path, "." ) && relativeResolver != null ) {
			return relativeResolver.findFieldAccessor( path ).orElse( null );
		}
		return rootResolver.findFieldAccessor( path ).orElse( null );
	}
}
