/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.query.EntityQueryOps;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.bootstrapui.OptionsFormElementBuilderFactory;
import com.foreach.across.modules.entity.views.bootstrapui.options.OptionIterableBuilder;
import com.foreach.across.modules.entity.views.processors.EntityQueryFilterProcessor;
import com.foreach.across.modules.entity.views.processors.query.EQLStringValueOptionEnhancer;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.format.Printer;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.BiConsumer;

/**
 * A {@link DynamicTypeDefinitionBuilder} for an enumeration type.
 * This can be pretty much any simple type, but with a limited list of possible values.
 *
 * @author Arne Vandamme
 * @see SimpleDynamicTypeDefinitionBuilder
 * @since 0.0.1
 */
@Component
@OrderInModule
@RequiredArgsConstructor
public class EnumDynamicTypeDefinitionBuilder implements DynamicTypeDefinitionBuilder
{
	private final ConversionService conversionService;
	private final SimpleDynamicTypeDefinitionBuilder simpleDynamicTypeDefinitionBuilder;

	@Override
	public boolean accepts( RawDocumentDefinition.AbstractField field ) {
		return field.hasPossibleValues() && simpleDynamicTypeDefinitionBuilder.accepts( field );
	}

	@Override
	public DynamicTypeDefinition buildTypeDefinition( DynamicTypeDefinitionFactory factory, RawDocumentDefinition.AbstractField field ) {
		val simpleType = simpleDynamicTypeDefinitionBuilder.createTypeDefinitionBuilder( field ).build();

		return simpleType.toBuilder()
		                 .propertyDescriptorBuilderConsumer( createPropertyDescriptorBuilderConsumer( field, simpleType ) )
		                 .build();
	}

	private BiConsumer<DynamicDocumentFieldDefinition, EntityPropertyDescriptorBuilder> createPropertyDescriptorBuilderConsumer( RawDocumentDefinition.AbstractField field,
	                                                                                                                             GenericDynamicTypeDefinition typeDefinition ) {
		return ( fieldDef, descriptorBuilder ) -> {
			TypeDescriptor typeDescriptor = typeDefinition.getTypeDescriptor();
			if ( typeDescriptor.isArray() || typeDescriptor.isCollection() ) {
				typeDescriptor = typeDescriptor.getElementTypeDescriptor();
			}
			EnumDynamicTypeOptions options = createOptions( field.getPossibleValues(), typeDescriptor );

			descriptorBuilder.viewElementType( ViewElementMode.CONTROL, OptionsFormElementBuilderFactory.OPTIONS )
			                 .viewElementType( ViewElementMode.FILTER_CONTROL, OptionsFormElementBuilderFactory.OPTIONS )
			                 .viewElementType( ViewElementMode.FILTER_CONTROL.forMultiple(), OptionsFormElementBuilderFactory.OPTIONS )
			                 .attribute( EntityQueryFilterProcessor.ENTITY_QUERY_OPERAND, EntityQueryOps.EQ )
			                 .attribute( EnumDynamicTypeOptions.class, options )
			                 .attribute( OptionIterableBuilder.class, options )
			                 .attribute( Printer.class, options );

			if ( String.class.equals( typeDescriptor.getObjectType() ) ) {
				descriptorBuilder.attribute( EntityAttributes.OPTIONS_ENHANCER, EQLStringValueOptionEnhancer.create() );
			}
		};
	}

	@SuppressWarnings("unchecked")
	private EnumDynamicTypeOptions createOptions( List<?> values, TypeDescriptor typeDescriptor ) {
		EnumDynamicTypeOptions options = new EnumDynamicTypeOptions( conversionService, typeDescriptor );
		values.forEach( options::addPossibleValue );
		return options;
	}
}
