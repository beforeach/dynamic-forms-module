/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.view;

import com.foreach.across.modules.entity.query.EntityQueryOps;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Converts a {@link RawViewDefinition} into a {@link DynamicViewDefinition} in the context
 * of a given dataSet. This will create backing property descriptors, type and validation classes etc.
 * <p/>
 * Exceptions will be thrown if unable to find a type definition for a given type for example.
 * If a {@link DynamicViewDefinition} can be built, the document configuration is valid.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
public class DynamicViewDefinitionBuilder
{
	public DynamicViewDefinition build( RawViewDefinition definition ) {
		DynamicViewFilterDefinition dynamicViewFilterDefinition = null;
		RawViewDefinition.Filter filter = definition.getFilter();
		if ( filter != null ) {
			List<RawViewDefinition.Field> fields = filter.getFields();
			List<DynamicViewFilterFieldDefinition> dynamicViewFilterFieldDefinitions = new ArrayList<>();
			if ( fields != null ) {
				for ( RawViewDefinition.Field field : fields ) {
					dynamicViewFilterFieldDefinitions.add(
							new DynamicViewFilterFieldDefinition( field.getId(), field.getSelector(),
							                                      field.getOperand() != null ? EntityQueryOps.forToken( field.getOperand() ) : null ) );
				}
			}
			dynamicViewFilterDefinition = new DynamicViewFilterDefinition( filter.getMode().getBasic(),
			                                                               filter.getMode().getAdvanced(),
			                                                               filter.getDefaultSelector(),
			                                                               filter.getDefaultQuery(),
			                                                               filter.getBasePredicate(),
			                                                               dynamicViewFilterFieldDefinitions
			);
		}

		return new DynamicViewDefinition( Collections.unmodifiableList( definition.getProperties() ), dynamicViewFilterDefinition );
	}
}
