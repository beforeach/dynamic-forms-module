/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.validation;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentFieldValue;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import org.springframework.validation.Errors;

/**
 * Validator interface for validating a single field in a {@link DynamicDocumentData}.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
public interface DynamicDocumentFieldValidator
{
	/**
	 * Allow this validator to apply some customization to the {@link EntityPropertyDescriptorBuilder}
	 * of a field that uses this validator.
	 *
	 * @param field   for which the descriptor is being built - partial definition
	 * @param builder to customize
	 */
	default void customizePropertyDescriptor( DynamicDocumentFieldDefinition field, EntityPropertyDescriptorBuilder builder ) {
	}

	/**
	 * Perform the actual validation. The {@link DynamicDocumentFieldValue} parameter contains the full
	 * context and value of the field being validated. Any errors are expected to
	 * be registered in the {@link Errors} object. Usually with a call like:
	 * <pre>{@code
	 * errors.rejectValue( field.getFieldKey(), "required" );
	 * }</pre>
	 *
	 * @param field  being validated
	 * @param errors containing all validation errors
	 */
	void validate( DynamicDocumentFieldValue field, Errors errors );
}
