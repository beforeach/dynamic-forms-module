/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import lombok.NonNull;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Central service for creating {@link DynamicDocumentWorkspace} objects.
 * The latter is the main component for working with {@link DynamicDocument} data.
 * <p/>
 * This is the default implementation of {@link DynamicDocumentWorkspaceFactory} where
 * a new workspace is created with its own optional definition cache. If you want to create
 * several separate workspaces in a single thread, it is often better to create a separate
 * {@link DynamicDocumentWorkspaceFactory} that uses a shared cache. You can do so
 * with the {@link #createCachingDocumentWorkspaceFactory()} methods.
 *
 * @author Arne Vandamme
 * @see DynamicDocumentWorkspace
 * @see DynamicDocumentWorkspaceFactory
 * @since 0.0.1
 */
@Service
public class DynamicDocumentService implements DynamicDocumentWorkspaceFactory
{
	private final AutowireCapableBeanFactory beanFactory;
	private final DynamicDocumentWorkspaceFactory defaultWorkspaceFactory;

	public DynamicDocumentService( AutowireCapableBeanFactory beanFactory ) {
		this.beanFactory = beanFactory;
		this.defaultWorkspaceFactory = new CachingDynamicDocumentWorkspaceFactory( beanFactory );
	}

	/**
	 * Create a workspace for a new document using the latest version of the specified document definition.
	 *
	 * @param documentDefinition definition
	 * @return document workspace - with a new blank document
	 */
	@Override
	public DynamicDocumentWorkspace createDocumentWorkspace( DynamicDefinition documentDefinition ) {
		return defaultWorkspaceFactory.createDocumentWorkspace( documentDefinition );
	}

	/**
	 * Create a workspace for a new document using the specific version of the document definition.
	 *
	 * @param documentDefinitionVersion version of the definition that should be used
	 * @return document workspace - with a new blank document
	 */
	@Override
	public DynamicDocumentWorkspace createDocumentWorkspace( DynamicDefinitionVersion documentDefinitionVersion ) {
		return defaultWorkspaceFactory.createDocumentWorkspace( documentDefinitionVersion );
	}

	/**
	 * Create a workspace for an existing document, initialized with the latest version of that document if there is one.
	 *
	 * @param document to create the workspace for
	 * @return document workspace
	 */
	@Override
	public DynamicDocumentWorkspace createDocumentWorkspace( @NonNull DynamicDocument document ) {
		return defaultWorkspaceFactory.createDocumentWorkspace( document );
	}

	/**
	 * Create a workspace for an existing document version.
	 *
	 * @param documentVersion that should be loaded in the workspace
	 * @return document workspace
	 */
	@Override
	public DynamicDocumentWorkspace createDocumentWorkspace( @NonNull DynamicDocumentVersion documentVersion ) {
		return defaultWorkspaceFactory.createDocumentWorkspace( documentVersion );
	}

	/**
	 * Create a new workspace factory with a new shared cache for parsed definitions.
	 * All workspaces created from the factory will use the same cache for retrieving the actual definitions.
	 * <p/>
	 * The default cache is an empty {@link ConcurrentHashMap} which is safe to use across multiple threads.
	 *
	 * @return workspace factory instance
	 */
	public CachingDynamicDocumentWorkspaceFactory createCachingDocumentWorkspaceFactory() {
		return createCachingDocumentWorkspaceFactory( new ConcurrentHashMap<>() );
	}

	/**
	 * Create a new workspace factory using the specified cache for storing and retrieving defintiions.
	 * All workspaces created from the factory will use the specified cache.
	 *
	 * @param definitionCache to use
	 * @return workspace factory instance
	 */
	public CachingDynamicDocumentWorkspaceFactory createCachingDocumentWorkspaceFactory( @NonNull Map<Long, DynamicDocumentDefinition> definitionCache ) {
		CachingDynamicDocumentWorkspaceFactory workspaceFactory = new CachingDynamicDocumentWorkspaceFactory( beanFactory );
		workspaceFactory.setDefinitionCache( definitionCache );
		return workspaceFactory;
	}
}
