/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.query;

import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.entity.query.*;
import com.foreach.across.modules.entity.query.collections.CollectionEntityQueryExecutor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import com.foreach.across.modules.entity.util.EntityUtils;
import lombok.val;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class WorkspaceEntityQuery
{
	private final EntityQuery original;
	private final EntityPropertyRegistry propertyRegistry;

	private Sort sort;
	private Pageable pageable;

	private EntityQuery docQuery;
	private EntityQuery wsQuery;

	WorkspaceEntityQuery( EntityPropertyRegistry propertyRegistry, EntityQuery original ) {
		this.original = original;
		this.propertyRegistry = propertyRegistry;

		splitQuery( original );
	}

	WorkspaceEntityQuery( EntityPropertyRegistry propertyRegistry, EntityQuery original, Sort sort ) {
		this( propertyRegistry, original );
		this.sort = sort;
	}

	WorkspaceEntityQuery( EntityPropertyRegistry propertyRegistry, EntityQuery original, Pageable pageable ) {
		this( propertyRegistry, original );
		this.pageable = pageable;
	}

	private void splitQuery( EntityQuery original ) {
		docQuery = EntityQuery.all();
		docQuery.setOperand( EntityQueryOps.AND );

		wsQuery = EntityQueryUtils.simplify(
				EntityQueryUtils.translateConditions(
						original, condition -> {
							docQuery.add( new EntityQueryCondition( "type", condition.getOperand(), condition.getArguments() ) );
							return null;
						}, "document.type", "definition"
				)
		);
	}

	@SuppressWarnings("unchecked")
	<T> T execute( DynamicDocumentService documentService, EntityQueryExecutor documentExecutor ) {
		List<DynamicDocument> documents = documentExecutor.findAll( docQuery );

		CollectionEntityQueryExecutor<DynamicDocumentWorkspace> workspaceExecutor = new CollectionEntityQueryExecutor<>(
				convertToWorkspaces( documentService, documents ),
				propertyRegistry
		);

		if ( pageable != null ) {
			return (T) workspaceExecutor.findAll( wsQuery, pageable );
		}

		if ( sort != null ) {
			return (T) workspaceExecutor.findAll( wsQuery, sort );
		}

		return (T) workspaceExecutor.findAll( wsQuery );
	}

	private List<DynamicDocumentWorkspace> convertToWorkspaces( DynamicDocumentService documentService, Iterable<DynamicDocument> documents ) {
		val workspaceFactory = documentService.createCachingDocumentWorkspaceFactory();
		return EntityUtils.asList( documents )
		                  .stream()
		                  .map( workspaceFactory::createDocumentWorkspace )
		                  .collect( Collectors.toList() );
	}
}
