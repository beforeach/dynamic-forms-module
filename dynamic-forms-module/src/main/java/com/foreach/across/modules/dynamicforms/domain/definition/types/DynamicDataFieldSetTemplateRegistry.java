/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.entity.views.bootstrapui.elements.ViewElementFieldset;
import com.foreach.across.modules.web.ui.ViewElement;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.function.Function;

/**
 * Registry that holds the registered templates for {@link ViewElementFieldset}s.
 * These can be specified using a {@code template} attribute on a {@code fieldset} entry
 * in a {@link com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition}.
 * <p/>
 * A number of default fieldsets, corresponding with {@link ViewElementFieldset} templates are available.
 *
 * @author Steven Gentens
 * @since 0.0.2
 */
@Service
public class DynamicDataFieldSetTemplateRegistry extends LinkedHashMap<String, Function<ViewElementFieldset, ? extends ViewElement>>
{
	public DynamicDataFieldSetTemplateRegistry() {
		put( "fields-only", ViewElementFieldset.TEMPLATE_BODY_ONLY );
		put( "panel-danger", ViewElementFieldset.TEMPLATE_PANEL_DANGER );
		put( "panel-default", ViewElementFieldset.TEMPLATE_PANEL_DEFAULT );
		put( "panel-info", ViewElementFieldset.TEMPLATE_PANEL_INFO );
		put( "panel-primary", ViewElementFieldset.TEMPLATE_PANEL_PRIMARY );
		put( "panel-success", ViewElementFieldset.TEMPLATE_PANEL_SUCCESS );
		put( "panel-warning", ViewElementFieldset.TEMPLATE_PANEL_WARNING );
		put( "section-h1", ViewElementFieldset.TEMPLATE_SECTION_H1 );
		put( "section-h2", ViewElementFieldset.TEMPLATE_SECTION_H2 );
		put( "section-h3", ViewElementFieldset.TEMPLATE_SECTION_H3 );
		put( "fieldset", ViewElementFieldset.TEMPLATE_FIELDSET );
	}
}
