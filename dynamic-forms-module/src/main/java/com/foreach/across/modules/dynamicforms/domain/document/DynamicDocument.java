/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Represents the content for a {@link DynamicDefinition}.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "dfm_document")
@NoArgsConstructor
public class DynamicDocument extends SettableIdAuditableEntity<DynamicDocument> implements Comparable<DynamicDocument>
{
	@Id
	@GeneratedValue(generator = "seq_dfm_document_id")
	@GenericGenerator(
			name = "seq_dfm_document_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@Parameter(name = "sequenceName", value = "seq_dfm_document_id"),
					@Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	/**
	 * A human readable name for this document
	 */
	@Column(name = "name")
	@NotBlank
	@Length(max = 255)
	private String name;

	/**
	 * The template for the data entry.
	 */
	@NotNull
	@ManyToOne
	@JoinColumn(name = "definition_id")
	private DynamicDefinition type;

	/**
	 * Whether the document can be versioned. If it may not be versioned, there will be a single version that is continuously updated.
	 */
	@NotNull
	@Column(name = "versionable")
	private boolean versionable;

	/**
	 * The most recent version for this document.
	 */
	@ManyToOne
	@JoinColumn(name = "latest_version_id")
	private DynamicDocumentVersion latestVersion;

	@Builder(toBuilder = true)
	public DynamicDocument( Long id,
	                        DynamicDefinition type,
	                        String name,
	                        boolean versionable,
	                        DynamicDocumentVersion latestVersion,
	                        @Builder.ObtainVia(method = "getCreatedBy") String createdBy,
	                        @Builder.ObtainVia(method = "getCreatedDate") Date createdDate,
	                        @Builder.ObtainVia(method = "getLastModifiedBy") String lastModifiedBy,
	                        @Builder.ObtainVia(method = "getLastModifiedDate") Date lastModifiedDate ) {
		setId( id );
		setType( type );
		setName( name );
		setVersionable( versionable );
		setLatestVersion( latestVersion );
		setCreatedBy( createdBy );
		setCreatedDate( createdDate );
		setLastModifiedBy( lastModifiedBy );
		setLastModifiedDate( lastModifiedDate );
	}

	@Override
	public int compareTo( DynamicDocument other ) {
		return StringUtils.compare( name, other.getName() );
	}
}
