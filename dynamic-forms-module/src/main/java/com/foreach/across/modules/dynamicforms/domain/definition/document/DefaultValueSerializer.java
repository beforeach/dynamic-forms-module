/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition.DefaultValue;

import java.io.IOException;

public class DefaultValueSerializer extends JsonSerializer<DefaultValue>
{
	@Override
	public void serialize( DefaultValue defaultValue,
	                       JsonGenerator gen,
	                       SerializerProvider serializers ) throws IOException, JsonProcessingException {

		if ( !DefaultValue.NOT_SPECIFIED.equals( defaultValue ) ) {

			gen.writeStartObject();

			if ( defaultValue != null ) {
				if ( defaultValue.getFormula() != null ) {
					gen.writeFieldName( "formula" );
					gen.writeRawValue(
							new ObjectMapper()
									.writer()
									.forType( Object.class )
									.writeValueAsString( defaultValue.getFormula() )
					);
				}
				else {
					gen.writeFieldName( "value" );
					gen.writeRawValue(
							new ObjectMapper()
									.writer()
									.forType( Object.class )
									.writeValueAsString( defaultValue.getValue() )
					);
				}
			}
			else {
				gen.writeStringField( "default", null );
			}

			gen.writeEndObject();
		}
	}
}
