/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui;

import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.modules.bootstrapui.elements.Style;
import com.foreach.across.modules.bootstrapui.elements.builder.ColumnViewElementBuilder;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.ui.ViewElementBuilder;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilderSupport;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;

import static com.foreach.across.modules.bootstrapui.ui.factories.BootstrapViewElements.bootstrap;
import static com.foreach.across.modules.web.ui.elements.HtmlViewElements.html;

/**
 * Configures the update form for a {@link com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument}.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
@Exposed
public class UpdateDynamicDocumentFormProcessor extends EntityViewProcessorAdapter
{
	private final DynamicDocumentUiComponents documentUiComponents;

	@Override
	protected void render( EntityViewRequest entityViewRequest,
	                       EntityView entityView,
	                       ContainerViewElementBuilderSupport<?, ?> containerBuilder,
	                       ViewElementBuilderMap builderMap,
	                       ViewElementBuilderContext builderContext ) {
		builderMap.get( SingleEntityFormViewProcessor.RIGHT_COLUMN, ColumnViewElementBuilder.class )
		          .add( renderCurrentDocumentVersion( entityViewRequest.getEntityViewContext().getEntity( DynamicDocument.class ) ) );
	}

	private ViewElementBuilder renderCurrentDocumentVersion( DynamicDocument document ) {
		val version = document.getLatestVersion();

		if ( version != null ) {
			return html.builders
					.container()
					.add( documentUiComponents.documentVersionInformation( version ) )
					.add( documentUiComponents.documentDataView( version ) );
		}

		return null;
	}

	@Override
	protected void postRender( EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		container.find( "buttons", ContainerViewElement.class )
		         .ifPresent( buttons -> {
			         buttons.removeFromTree( "btn-save" );
			         val currentPage = entityViewRequest.getEntityViewContext().getLinkBuilder()
			                                            .forInstance( entityViewRequest.getEntityViewContext().getEntity() )
			                                            .updateView();
			         buttons.addFirstChild(
					         bootstrap.builders.button().link()
					                           .name( "bnt-update" )
					                           .text( "#{actions.modify=Update document}" )
					                           .style( Style.PRIMARY )
					                           .url(
							                 currentPage
									                 .withViewName( "createVersion" )
									                 .withFromUrl( currentPage.toUriString() )
									                 .toUriString()
					                 )
					                           .build( builderContext )
			         );
		         } );
	}
}
