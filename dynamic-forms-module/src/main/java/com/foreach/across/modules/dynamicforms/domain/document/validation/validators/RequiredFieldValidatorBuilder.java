/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.validation.validators;

import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicTypeDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidator;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidatorBuilder;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Returns a {@link RequiredFieldValidator} with validator settings.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Order
@Component
public class RequiredFieldValidatorBuilder implements DynamicDocumentFieldValidatorBuilder
{
	public static final String ID = "required";

	/**
	 * Use this id if you want required validation but do not want to visibly indicate the control as required.
	 */
	public static final String ID_SILENT = "required-silent";

	private static final RequiredFieldValidator VALIDATOR = new RequiredFieldValidator( true );
	private static final RequiredFieldValidator VALIDATOR_SILENT = new RequiredFieldValidator( false );

	@Override
	public boolean accepts( RawDocumentDefinition.AbstractField field, DynamicTypeDefinition typeDefinition, Map<String, Object> validatorSettings ) {
		return validatorSettings.containsKey( ID ) || validatorSettings.containsKey( ID_SILENT );
	}

	@Override
	public DynamicDocumentFieldValidator buildValidator( RawDocumentDefinition.AbstractField field,
	                                                     DynamicTypeDefinition typeDefinition,
	                                                     Map<String, Object> validatorSettings ) {
		if ( validatorSettings.containsKey( ID ) ) {
			return VALIDATOR;
		}

		return VALIDATOR_SILENT;
	}
}