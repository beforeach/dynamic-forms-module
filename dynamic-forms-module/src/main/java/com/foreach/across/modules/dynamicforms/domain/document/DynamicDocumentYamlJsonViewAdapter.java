/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.YamlJsonObjectMapper;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class DynamicDocumentYamlJsonViewAdapter extends EntityViewProcessorAdapter
{
	private final YamlJsonObjectMapper yamlJsonObjectMapper;

	@Override
	protected void validateCommandObject( EntityViewRequest entityViewRequest, EntityViewCommand command, Errors errors, HttpMethod httpMethod ) {
		DynamicDocument entity = command.getEntity( DynamicDocument.class );
		DynamicDocumentVersion version = entity.getLatestVersion();
		if ( StringUtils.isNotBlank( version.getDocumentContent() ) ) {
			if ( HttpMethod.POST.equals( httpMethod ) ) {
				try {
					String output = yamlJsonObjectMapper.yamlToJson( version::getDocumentContent );
					if ( !errors.hasFieldErrors() ) {
						version.setDocumentContent( output );
					}
				}
				catch ( IOException | YAMLException e ) {
					errors.rejectValue( "entity.content", "parsingFailed", new String[] { e.getMessage() }, "Error parsing input" );
				}
			}
			else {
				version.setDocumentContent( yamlJsonObjectMapper.jsonToYaml( version::getDocumentContent ) );
			}
		}
	}
}
