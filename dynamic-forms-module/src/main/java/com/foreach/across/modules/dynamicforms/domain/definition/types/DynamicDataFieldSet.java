/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import org.springframework.core.convert.TypeDescriptor;

import java.util.LinkedHashMap;

/**
 * Specialized type that represents a fieldset type in a dynamic document.
 *
 * @author Arne Vandamme
 * @see FieldsetDynamicTypeDefinitionBuilder
 * @since 0.0.1
 */
public final class DynamicDataFieldSet extends LinkedHashMap<String, Object>
{
	public static final TypeDescriptor TYPE_DESCRIPTOR = TypeDescriptor.valueOf( DynamicDataFieldSet.class );
}
