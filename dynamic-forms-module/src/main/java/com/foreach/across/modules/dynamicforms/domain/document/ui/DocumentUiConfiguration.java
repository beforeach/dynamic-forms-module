/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionTypes;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.conditionals.ConditionalOnAdminWeb;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.registry.EntityAssociation;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

import static com.foreach.across.modules.entity.views.EntityViewCustomizers.basicSettings;
import static com.foreach.across.modules.entity.views.EntityViewCustomizers.formSettings;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 * @deprecated most of the configuration can probably be removed in the future, see workspace configuration.
 */
@Deprecated
@Configuration
@RequiredArgsConstructor
@ConditionalOnAdminWeb
class DocumentUiConfiguration implements EntityConfigurer
{
	private final DocumentVersionHistoryViewProcessor historyViewProcessor;
	private final UpdateDynamicDocumentFormProcessor updateFormProcessor;
	private final CreateDocumentVersionFormProcessor createVersionFormProcessor;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.withType( DynamicDefinition.class )
		        .association( ab -> ab.name( "dynamicDocument.type" ).parentDeleteMode( EntityAssociation.ParentDeleteMode.WARN ) );

		entities.withType( DynamicDocumentVersion.class )
		        .properties( props -> props.property( EntityPropertyRegistry.LABEL ).valueFetcher( new DynamicDocumentLabelPrinter() ) )
		        .hide();

		entities.withType( DynamicDocument.class )
		        .hide()
		        .properties( props -> props.property( EntityPropertyRegistry.LABEL ).valueFetcher( new DynamicDocumentLabelPrinter() ).and()
		                                   .property( "latestVersion" ).hidden( true ).and()
		                                   .property( "versionable" ).hidden( true ).and()
		                                   .property( "type" ).order( 1 ).and()
		                                   .property( "name" ).order( 2 )
		        )
		        .createFormView( cvf -> cvf.properties( props -> props.property( "type" )
		                                                              .attribute( EntityAttributes.OPTIONS_ENTITY_QUERY,
		                                                                          "type.name = '" + DynamicDefinitionTypes.DOCUMENT + "'" ) ) )
		        .listView( lvb -> lvb.showProperties( "name", "type", "latestVersion.version", "lastModified" ) )
		        .updateFormView(
				        fvb -> fvb.properties( props -> props.property( "type" ).writable( false ).and()
				                                             .property( "name" ).writable( false ) )
				                  .showProperties( "type", "name", "created", "lastModified" )
				                  .viewProcessor( updateFormProcessor )
		        )
		        .formView(
				        "history",
				        basicSettings()
						        .adminMenu( "/history" )
						        .accessValidator( ( view, context ) -> !context.getEntity( DynamicDocument.class ).isVersionable() )
						        .andThen( formSettings().forExtension( true ).addFormButtons( false ) )
						        .andThen( vb -> vb.viewProcessor( historyViewProcessor ) )
		        )
		        .formView(
				        "createVersion",
				        formSettings()
						        .forExtension( true )
						        .andThen( vb -> vb.viewProcessor( createVersionFormProcessor ) )
		        )
		;
	}
}
