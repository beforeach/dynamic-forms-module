/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Registry that holds all the registered {@link DynamicDefinitionTypeConfiguration}s to be used within an application.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Service
@RequiredArgsConstructor
public class DynamicDefinitionTypeConfigurationRegistry
{
	private final DynamicDefinitionTypeRepository typeRepository;
	private final Map<String, DynamicDefinitionTypeConfiguration> definitionTypeConfigurations = new HashMap<>();

	/**
	 * Retrieves a {@link DynamicDefinitionTypeConfiguration} by its {@link RawDefinition} class.
	 *
	 * @param clazz that the configuration represents
	 * @return the configuration if the type has been registered or else null.
	 */
	public DynamicDefinitionTypeConfiguration getByClass( Class clazz ) {
		return definitionTypeConfigurations.values().stream()
		                                   .filter( configuration -> Objects.equals( configuration.getRawDefinitionClass(), clazz ) )
		                                   .findFirst().orElse( null );
	}

	/**
	 * Retrieves a {@link DynamicDefinitionTypeConfiguration} by the name of the type.
	 *
	 * @param name of the type
	 * @return the configuration if the type has been registered or else null.
	 */
	public DynamicDefinitionTypeConfiguration getByName( String name ) {
		return definitionTypeConfigurations.get( name );
	}

	/**
	 * Creates a {@link DynamicDefinitionTypeConfiguration} based on the given parameters.
	 *
	 * @param name               of the {@link DynamicDefinitionType}
	 * @param rawDefinitionClass of the type
	 * @param validator          to use when validating definition content of the given type
	 * @return The registered configuration
	 */
	public DynamicDefinitionTypeConfiguration register( String name, Class<? extends RawDefinition> rawDefinitionClass, Validator validator ) {
		DynamicDefinitionTypeConfiguration configuration = new DynamicDefinitionTypeConfiguration( name, getOrCreateType( name ), rawDefinitionClass,
		                                                                                           validator );
		this.definitionTypeConfigurations.put( name, configuration );
		return configuration;
	}

	/**
	 * Retrieves a {@link DynamicDefinitionType} by name.
	 * If the type does not yet exist, it will be created.
	 *
	 * @param name of the type
	 * @return the type itself.
	 */
	private DynamicDefinitionType getOrCreateType( String name ) {
		DynamicDefinitionType type = typeRepository.findByName( name ).orElse( null );
		if ( type == null ) {
			type = DynamicDefinitionType.builder().name( name ).build();
			type = typeRepository.save( type );
		}
		return type;
	}

}
