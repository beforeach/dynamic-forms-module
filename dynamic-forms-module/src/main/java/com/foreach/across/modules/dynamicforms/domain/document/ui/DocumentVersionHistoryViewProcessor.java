/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui;

import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.modules.bootstrapui.elements.builder.ColumnViewElementBuilder;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersionRepository;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.web.links.EntityViewLinkBuilder;
import com.foreach.across.modules.web.ui.ViewElementBuilder;
import com.foreach.across.modules.web.ui.elements.HtmlViewElements;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import java.util.List;

import static com.foreach.across.modules.bootstrapui.ui.factories.BootstrapViewElements.bootstrap;
import static com.foreach.across.modules.dynamicforms.DynamicFormsModuleIcons.dynamicFormsModuleIcons;
import static com.foreach.across.modules.entity.views.util.EntityViewElementUtils.currentEntity;
import static org.springframework.web.bind.ServletRequestUtils.getLongParameter;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
@Exposed
public class DocumentVersionHistoryViewProcessor extends EntityViewProcessorAdapter
{
	private final EntityViewElementBuilderHelper builderHelper;
	private final DynamicDocumentVersionRepository documentVersionRepository;
	private final DynamicDocumentUiComponents documentUiComponents;

	@Override
	protected void createViewElementBuilders( EntityViewRequest entityViewRequest, EntityView entityView, ViewElementBuilderMap builderMap ) {

		val entity = entityViewRequest.getEntityViewContext().getEntity();
		val document = entity instanceof DynamicDocumentWorkspace ? ( (DynamicDocumentWorkspace) entity ).getDocument() : (DynamicDocument) entity;
		val versions = documentVersionRepository.findAllByDocumentOrderByLastModifiedDateDesc( document );
		val selectedVersion = selectVersionToShow( versions,
		                                           getLongParameter( entityViewRequest.getWebRequest().getNativeRequest( ServletRequest.class ),
		                                                             "versionId",
		                                                             Long.MIN_VALUE ) );

		builderMap.get( SingleEntityFormViewProcessor.LEFT_COLUMN, ColumnViewElementBuilder.class )
		          .add( createVersionsTable( entity, versions, selectedVersion, entityViewRequest.getEntityViewContext().getLinkBuilder() ) );

		builderMap.get( SingleEntityFormViewProcessor.RIGHT_COLUMN, ColumnViewElementBuilder.class )
		          .add( renderDocumentVersion( selectedVersion ) );
	}

	private DynamicDocumentVersion selectVersionToShow( List<DynamicDocumentVersion> versions, long versionId ) {
		return versions.stream().filter( version -> versionId == version.getId() ).findFirst().orElse( null );
	}

	private ViewElementBuilder createVersionsTable( Object document,
	                                                List<DynamicDocumentVersion> versions,
	                                                DynamicDocumentVersion selectedVersion,
	                                                EntityViewLinkBuilder linkBuilder ) {
		return builderHelper.createSortableTableBuilder( DynamicDocumentVersion.class )
		                    .tableOnly()
		                    .noSorting()
		                    .hideResultNumber()
		                    .properties( "version", "createdBy", "createdDate" )
		                    .items( versions )
		                    .headerRowProcessor( ( builderContext, row ) -> row.addChild( bootstrap.builders.table.headerCell().build( builderContext ) ) )
		                    .valueRowProcessor( ( builderContext, row ) -> {
			                    DynamicDocumentVersion documentVersion = currentEntity( builderContext, DynamicDocumentVersion.class );

			                    if ( documentVersion.equals( selectedVersion ) ) {
				                    row.addCssClass( "info" );
			                    }

			                    row.addChild(
					                    bootstrap.builders.table.cell()
					                                            .name( "actions" )
					                                            .add( bootstrap.builders.button()
					                                                                    .icon( dynamicFormsModuleIcons.view() )
					                                                                    .iconOnly()
					                                                                    .link( linkBuilder.forInstance( document )
					                                                                                      .updateView()
					                                                                                      .withViewName( "history" )
					                                                                                      .withQueryParam( "versionId",
					                                                                                                       documentVersion.getId() )
					                                                                                      .toUriString() )
					                                            )
					                                            .build( builderContext )
			                    );
		                    } );
	}

	private ViewElementBuilder renderDocumentVersion( DynamicDocumentVersion version ) {
		if ( version != null ) {
			return HtmlViewElements.html.builders
					.container()
					.add( documentUiComponents.documentVersionInformation( version ) )
					.add( documentUiComponents.documentDataView( version ) );
		}

		return null;
	}
}
