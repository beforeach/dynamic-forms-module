/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data.expressions;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Central parser inferface for creating an expression executor for a calculated field (field backed by a formula).
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class DynamicDocumentFieldExpressionParser
{
	private final SpelExpressionParser parser = new SpelExpressionParser();
	private final Pattern FIELD_PATTERN = Pattern.compile( "\\$\\((.*?)\\)" );

	private final ConversionService conversionService;

	/**
	 * Parse an expression that should return a value of the expected type.
	 * The return value will be a {@link Function} that will execute the
	 * expression using the evaluation context passed in.
	 *
	 * @param expression   to execute
	 * @param expectedType that should be returned
	 * @return supplier for executing the expression
	 */
	public BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> parseExpression( @NonNull String expression,
	                                                                                                  @NonNull TypeDescriptor expectedType ) {
		String expressionString = replaceShortHandFields( expression );
		LOG.debug( "Replaced field formula short-hands: \"{}\" to \"{}\"", expression, expressionString );
		val parsed = parser.parseExpression( expressionString );
		return ( evaluationContext, rootObject ) -> {
			Object value = parsed.getValue( evaluationContext, rootObject );
			return value != null ? conversionService.convert( value, TypeDescriptor.forObject( value ), expectedType ) : null;
		};
	}

	private String replaceShortHandFields( String expression ) {
		String expressionString = expression;

		Matcher matcher = FIELD_PATTERN.matcher( expression );
		Set<String> matches = new HashSet<>();
		while ( matcher.find() ) {
			matches.add( matcher.group( 1 ) );
		}

		for ( String match : matches ) {
			if ( match.contains( ":" ) ) {
				String[] fieldWithNullValue = StringUtils.split( match, ":" );
				expressionString = expressionString.replace( "$(" + match + ")",
				                                             "(field('" + fieldWithNullValue[0] + "')?.get()?:" + fieldWithNullValue[1] + ")" );
			}
			else {
				expressionString = expressionString.replace( "$(" + match + ")", "field('" + match + "').get()" );
			}
		}
		;
		return expressionString;
	}
}
