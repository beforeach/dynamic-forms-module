/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data.expressions;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import lombok.Data;
import lombok.NonNull;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.MethodResolver;
import org.springframework.expression.spel.support.ReflectiveMethodResolver;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Event published when the {@link org.springframework.expression.EvaluationContext}
 * for a specific document should be created. During execution a property {@code document}
 * will added to access the fields of the document.
 * <p/>
 * When this event is published the default evaluation context will be set. The default
 * evaluation context does not allow bean resolving but registers the formula helper functions.
 * <p/>
 * Note that when manually specifying the evaluation context to use, no {@link EvaluationContext#getRootObject()}
 * should be set as that will always be the {@link DynamicDocumentFieldFormulaContext} of the field
 * being evaluated.
 *
 * @author Arne Vandamme
 * @since 0.0.2
 */
@Data
public final class BuildDynamicDocumentFormulaEvaluationContext
{
	private final DynamicDocumentDefinition documentDefinition;
	private final DynamicDefinitionVersion definitionVersion;

	/**
	 * The SpEL {@link EvaluationContext} that should be used, must not be {@code null}.
	 */
	@NonNull
	private EvaluationContext evaluationContext;

	/**
	 * Get the {@code String} version of the {@link com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionId}.
	 * Useful for {@link org.springframework.context.event.EventListener} filter conditions.
	 *
	 * @return definition id as string
	 */
	public String getDefinitionId() {
		return documentDefinition.getDefinitionId().asDefinitionId().toString();
	}

	/**
	 * Add a variable to the evaluation context. This is the most common use case.
	 *
	 * @param name  variable name
	 * @param value to set
	 */
	public void setVariable( @NonNull String name, Object value ) {
		evaluationContext.setVariable( name, value );
	}

	/**
	 * Create a new {@link EvaluationContext} initialized with the {@link DynamicDocumentFormulaFunctions} helper functions.
	 *
	 * @return evaluation context
	 */
	public static StandardEvaluationContext createDefaultEvaluationContext() {
		StandardEvaluationContext evaluationContext = new StandardEvaluationContext();
		List<MethodResolver> methodResolvers = new ArrayList<>();
		methodResolvers.add( DynamicDocumentFormulaFunctions.asRootObjectMethodResolver() );
		methodResolvers.add( new ReflectiveMethodResolver() );
		evaluationContext.setMethodResolvers( methodResolvers );
		return evaluationContext;
	}
}
