/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDocumentDefinitionService;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.entity.views.context.ConfigurableEntityViewContext;
import com.foreach.across.modules.entity.views.context.EntityViewContext;
import com.foreach.across.modules.entity.views.processors.SimpleEntityViewProcessorAdapter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.val;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Will pre-select a {@link com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition}
 * based on the presence of a request parameter and will store the actual definition as request attribute.
 * <p/>
 * This allows subsequent processor to initialize property registries specifically for the context of that definition type
 * (allowing for field details).
 * <p/>
 * Helper methods are available to retrieve the current definition is if has been fixed.
 * <p/>
 * Inspects the request parameter {@code document.type}, if present it should contain the id of the definition to load.
 *
 * @author Arne Vandamme
 * @since 0.0.2
 */
@Component
@RequiredArgsConstructor
@Getter
@Setter
@Accessors(fluent = true)
public class DynamicDocumentDefinitionContextViewProcessor extends SimpleEntityViewProcessorAdapter
{
	public static final String REQUEST_PARAMETER = "document.type";
	public static final String USE_LATEST_DEFINITION = "DynamicDocumentDefinitionContext.useLatestDefinition";

	private static final String DEFINITION_ATTRIBUTE = "DynamicDocumentDefinitionContext.DynamicDefinition";
	private static final String DOCUMENT_DEFINITION_ATTRIBUTE = "DynamicDocumentDefinitionContext.DynamicDocumentDefinition";

	private final DynamicDefinitionRepository definitionRepository;
	private final DynamicDocumentDefinitionService documentDefinitionService;

	private boolean useLatestDefinitionVersion = false;

	@Override
	public void prepareEntityViewContext( ConfigurableEntityViewContext entityViewContext ) {
		val request = currentRequest();
		val definition = retrieveDefinition( entityViewContext, request );

		if ( definition != null ) {
			request.setAttribute( DEFINITION_ATTRIBUTE, definition );
			request.setAttribute( DOCUMENT_DEFINITION_ATTRIBUTE, getDocumentDefinition( entityViewContext, definition ) );
		}
	}

	private DynamicDefinition retrieveDefinition( EntityViewContext entityViewContext, HttpServletRequest request ) {
		if ( entityViewContext.holdsEntity() ) {
			return entityViewContext.getEntity( DynamicDocumentWorkspace.class ).getDefinition();
		}

		if ( entityViewContext.isForAssociation() ) {
			return entityViewContext.getParentContext().getEntity( DynamicDefinition.class );
		}

		long definitionId = ServletRequestUtils.getLongParameter( request, REQUEST_PARAMETER, 0 );

		if ( definitionId != 0 ) {
			return definitionRepository.findById( definitionId ).orElse( null );
		}

		return null;
	}

	private DynamicDocumentDefinition getDocumentDefinition( ConfigurableEntityViewContext entityViewContext, DynamicDefinition definition ) {
		if ( !useLatestDefinitionVersion ) {
			DynamicDocumentWorkspace entity = entityViewContext.getEntity( DynamicDocumentWorkspace.class );
			if ( entity != null ) {
				return entity.getFields().getDocumentDefinition();
			}
		}
		return documentDefinitionService.loadDocumentDefinition( definition.getLatestVersion() );
	}

	/**
	 * @return definition entity or {@code null} if none set
	 */
	public static DynamicDefinition currentDefinition() {
		return (DynamicDefinition) currentRequest().getAttribute( DEFINITION_ATTRIBUTE );
	}

	/**
	 * @return document definition or {@code null} if none set
	 */
	public static DynamicDocumentDefinition currentDocumentDefinition() {
		return (DynamicDocumentDefinition) currentRequest().getAttribute( DOCUMENT_DEFINITION_ATTRIBUTE );
	}

	private static HttpServletRequest currentRequest() {
		return ( (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes() ).getRequest();
	}
}
