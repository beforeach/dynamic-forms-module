/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import org.springframework.context.support.AbstractMessageSource;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.text.MessageFormat;
import java.util.Locale;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

/**
 * Proxy message source that delegates to the request-bound {@link DynamicDocumentDefinitionMessageSource}, if there is one.
 * There should only be a single instance registered in the Across messages hierarchy, the actual target message source
 * for a definition should be assigned using {@link #assign(DynamicDocumentDefinition)}.
 * <p/>
 * Meant for internal use only.
 *
 * @author Arne Vandamme
 * @see com.foreach.across.modules.dynamicforms.domain.document.ui.workspace.DocumentsUiConfiguration
 * @see com.foreach.across.modules.dynamicforms.domain.document.ui.workspace.WorkspaceEntityPropertyRegistryViewProcessor
 * @since 0.0.2
 */
public class RequestBoundDynamicDocumentDefinitionMessageSource extends AbstractMessageSource
{
	private static final String ATTRIBUTE_NAME = DynamicDocumentDefinitionMessageSource.class.getName();

	public static void assign( DynamicDocumentDefinition documentDefinition ) {
		RequestContextHolder.currentRequestAttributes().setAttribute( ATTRIBUTE_NAME, documentDefinition.getMessageSource(), SCOPE_REQUEST );
	}

	@Override
	protected MessageFormat resolveCode( String code, Locale locale ) {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

		if ( requestAttributes != null ) {
			DynamicDocumentDefinitionMessageSource messageSource
					= (DynamicDocumentDefinitionMessageSource) requestAttributes.getAttribute( ATTRIBUTE_NAME, SCOPE_REQUEST );

			if ( messageSource != null ) {
				return messageSource.resolveCode( code, locale );
			}
		}

		return null;
	}
}
