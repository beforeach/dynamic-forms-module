/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import lombok.SneakyThrows;

/**
 * Reporter interface for getting information from {@link com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentData}.
 *
 * @author Arne Vandamme
 * @since 0.0.2
 */
public interface Reporter
{
	@SneakyThrows
	default void unknownField( String fieldPath, Object value, Throwable exception ) {
		throw exception;
	}

	@SneakyThrows
	default void illegalValue( DynamicDocumentFieldDefinition field, String fieldPath, Object value, Throwable exception ) {
		throw exception;
	}

	default void formulaFailed( DynamicDocumentFieldDefinition field, String fieldPath, Throwable exception ) {
	}

	default void valueSet( DynamicDocumentFieldDefinition field, String fieldPath, Object oldValue, Object newValue, boolean created ) {
	}
}
