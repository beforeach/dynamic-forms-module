/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.ui;

import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.entity.views.support.ValueFetcher;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
public class DynamicDefinitionLabelPrinter implements ValueFetcher<Object>
{
	@Override
	public Object getValue( Object definitionOrVersion ) {
		if ( definitionOrVersion instanceof DynamicDefinition ) {
			return printDefinitionLabel( (DynamicDefinition) definitionOrVersion );
		}
		if ( definitionOrVersion instanceof DynamicDefinitionVersion ) {
			return printDefinitionLabel( (DynamicDefinitionVersion) definitionOrVersion );
		}
		return null;
	}

	private String printDefinitionLabel( DynamicDefinition definition ) {
		return definition.getName();
	}

	private String printDefinitionLabel( DynamicDefinitionVersion definitionVersion ) {
		return definitionVersion.getDefinition().getName() + " (v " + definitionVersion.getVersion() + ")";
	}
}
