/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.ui;

import com.foreach.across.modules.bootstrapui.elements.ColumnViewElement;
import com.foreach.across.modules.bootstrapui.elements.builder.ColumnViewElementBuilder;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersion;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionVersionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.YamlJsonObjectMapper;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderHelper;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.helpers.EntityViewElementBatch;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.builder.ContainerViewElementBuilderSupport;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Renders a read-only view for a specific {@link DynamicDefinitionVersion} of a {@link DynamicDefinition}.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Component
public class ReadDefinitionVersionViewProcessor extends EntityViewProcessorAdapter
{
	private final EntityViewElementBuilderHelper builderHelper;
	private final DynamicDefinitionVersionRepository definitionVersionRepository;
	private final YamlJsonObjectMapper yamlJsonObjectMapper;
	private final ConversionService conversionService;

	@Override
	protected void render( EntityViewRequest entityViewRequest,
	                       EntityView entityView,
	                       ContainerViewElementBuilderSupport<?, ?> containerBuilder,
	                       ViewElementBuilderMap builderMap,
	                       ViewElementBuilderContext builderContext ) {
		Long id = getRequestParameter( "id", Long.class, entityViewRequest );

		DynamicDefinitionVersion version = getDefinitionVersion( EntityViewElementUtils.currentEntity( builderContext, DynamicDefinition.class ), id );

		version.setPublished( false );
		version.setDefinitionContent( StringUtils.isNotBlank( version.getDefinitionContent() )
				                              ? yamlJsonObjectMapper.jsonToYaml( version::getDefinitionContent )
				                              : " " );

		EntityViewElementBatch<DynamicDefinitionVersion> batch = builderHelper.createBatchForEntityType( DynamicDefinitionVersion.class );
		batch.setViewElementMode( ViewElementMode.FORM_READ );
		batch.setPropertySelector( EntityPropertySelector.of( "definitionContent", "remarks", "published", "version" ) );
		batch.setEntity( version );
		batch.setParentViewElementBuilderContext( builderContext );

		Map<String, ViewElement> formGroups = batch.build();

		builderMap.get( SingleEntityFormViewProcessor.RIGHT_COLUMN, ColumnViewElementBuilder.class )
		          .addAll( formGroups.values() );
	}

	//TODO add error handling when converting the parameter.
	private <T> T getRequestParameter( String parameter, Class<T> requestedType, EntityViewRequest entityViewRequest ) {
		String value = entityViewRequest.getWebRequest().getParameter( parameter );
		return conversionService.convert( value, requestedType );
	}

	private DynamicDefinitionVersion getDefinitionVersion( DynamicDefinition definition, Long id ) {
		DynamicDefinitionVersion version;
		if ( id != null ) {
			version = definitionVersionRepository.findById( id ).orElse( null );
		}
		else {
			version = definition.getLatestVersion();
		}
		return version.toDto();
	}

	@Override
	protected void postRender( EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		ContainerViewElementUtils.find( container, SingleEntityFormViewProcessor.RIGHT_COLUMN, ColumnViewElement.class )
		                         .ifPresent( rightColumn -> ContainerViewElementUtils
				                         .find( container, SingleEntityFormViewProcessor.LEFT_COLUMN, ColumnViewElement.class )
				                         .ifPresent( leftColumn -> {
					                         List<ViewElement> children = leftColumn.getChildren();
					                         for ( int i = children.size() - 1; i >= 0; i-- ) {
						                         rightColumn.addFirstChild( children.get( i ) );
					                         }
					                         leftColumn.clearChildren();
				                         } )
		                         );
		ContainerViewElementUtils.move( container, "formGroup-definitionContent", SingleEntityFormViewProcessor.LEFT_COLUMN );
	}
}
