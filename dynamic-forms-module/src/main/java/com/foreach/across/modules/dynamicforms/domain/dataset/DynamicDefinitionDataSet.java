/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.dataset;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

/**
 * @author Marc Vanbrabant
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "dfm_definition_data_set")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class DynamicDefinitionDataSet extends SettableIdAuditableEntity<DynamicDefinitionDataSet> implements Comparable<DynamicDefinitionDataSet>
{
	@Id
	@GeneratedValue(generator = "seq_dfm_definition_data_set_id")
	@GenericGenerator(
			name = "seq_dfm_definition_data_set_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@Parameter(name = "sequenceName", value = "seq_dfm_definition_data_set_id"),
					@Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	/**
	 * Represents the name of the data set.
	 */
	@Column(name = "name")
	@NotBlank
	@Length(max = 255)
	private String name;

	/**
	 * Technical key of the data set, must be unique.
	 * Key can only contain alphanumeric characters, . (dot), - (dash) or _ (underscore).
	 */
	@Column(name = "data_set_key")
	@NotBlank
	@Length(max = 50)
	@Pattern(regexp = "^[\\p{Alnum}-_.]*$")
	private String key;

	@Override
	public int compareTo( DynamicDefinitionDataSet other ) {
		return StringUtils.compare( getName(), other.getName() );
	}
}
