/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.ui.workspace;

import com.foreach.across.modules.bootstrapui.elements.Style;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.DynamicDefinitionRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.QDynamicDefinition;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderService;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.processors.EntityQueryFilterProcessor;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.ListFormViewProcessor;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.web.resource.WebResource;
import com.foreach.across.modules.web.resource.WebResourceRegistry;
import com.foreach.across.modules.web.ui.DefaultViewElementBuilderContext;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.foreach.across.modules.bootstrapui.ui.factories.BootstrapViewElements.bootstrap;
import static com.foreach.across.modules.dynamicforms.domain.document.ui.workspace.DynamicDocumentDefinitionContextViewProcessor.REQUEST_PARAMETER;
import static com.foreach.across.modules.dynamicforms.domain.document.ui.workspace.DynamicDocumentDefinitionContextViewProcessor.currentDefinition;
import static com.foreach.across.modules.web.ui.elements.HtmlViewElements.html;
import static com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils.find;

/**
 * Configures the global documents overview. Adds the document type filter and applies it when selected.
 * If a document type is selected the create button will be modified and the list view will be customized
 * for the specific document type.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
class GlobalListViewProcessor extends EntityViewProcessorAdapter
{
	private final EntityViewElementBuilderService viewElementBuilderService;
	private final DynamicDefinitionRepository definitionRepository;

	@Override
	protected void preProcess( EntityViewRequest entityViewRequest, EntityView entityView, EntityViewCommand command ) {
		DynamicDefinition fixedDefinition = currentDefinition();

		if ( fixedDefinition != null ) {
			entityView.addAttribute( EntityQueryFilterProcessor.EQL_PREDICATE_ATTRIBUTE_NAME, "document.type = " + fixedDefinition.getId() );
		}
	}

	@Override
	protected void registerWebResources( EntityViewRequest entityViewRequest, EntityView entityView, WebResourceRegistry webResourceRegistry ) {
		webResourceRegistry.add( WebResource.JAVASCRIPT_PAGE_END, "/static/DynamicFormsModule/js/dfm-ui-documents-list-view.js", WebResource.VIEWS );
	}

	@Override
	protected void postRender( EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		DynamicDefinition fixedDefinition = currentDefinition();

		addDocumentTypeSelector( entityViewRequest, container, builderContext, fixedDefinition );
		addCreateDocumentButton( entityViewRequest, container, builderContext, fixedDefinition );
	}

	private void addDocumentTypeSelector( EntityViewRequest entityViewRequest,
	                                      ContainerViewElement container,
	                                      ViewElementBuilderContext builderContext,
	                                      DynamicDefinition fixedDefinition ) {
		val documentTypeDescriptor = entityViewRequest.getEntityViewContext().getPropertyRegistry().getProperty( "document.type" );
		val typeSelectorDescriptor = EntityPropertyDescriptor.builder( "document.type" )
		                                                     .parent( documentTypeDescriptor )
		                                                     .valueFetcher( e -> fixedDefinition )
		                                                     .build();

		val selectorBuilder = viewElementBuilderService.createElementBuilder( typeSelectorDescriptor, ViewElementMode.FILTER_CONTROL );
		val labelBuilder = html.builders.label().add( viewElementBuilderService.createElementBuilder( typeSelectorDescriptor, ViewElementMode.LABEL ) );

		val childContext = new DefaultViewElementBuilderContext( builderContext );
		EntityViewElementUtils.setCurrentEntity( childContext, "dummy" );

		val filterForm = html.builders.div()
		                              .css( "entity-query-filter-form" )
		                              .add(
				                              html.builders.div()
				                                           .css( "entity-query-filter-form-basic" )
				                                           .add( bootstrap.builders.formGroup().label( labelBuilder ).control( selectorBuilder ) )
		                              );

		Optional<ContainerViewElement> header = find( container, ListFormViewProcessor.DEFAULT_FORM_NAME + "-header", ContainerViewElement.class );
		header.ifPresent( h -> h.addFirstChild( filterForm.build( childContext ) ) );
	}

	private void addCreateDocumentButton( EntityViewRequest entityViewRequest,
	                                      ContainerViewElement container,
	                                      ViewElementBuilderContext builderContext,
	                                      DynamicDefinition fixedDefinition ) {
		val linkBuilder = entityViewRequest.getEntityViewContext().getLinkBuilder();

		container.find( "entityListForm-actions", ContainerViewElement.class )
		         .ifPresent( actions -> {
			         boolean isTypeSelected = fixedDefinition != null;

			         actions.removeFromTree( "btn-create" );

			         val groupBuilder = html.builders.div().css( "btn-group" );
			         Style buttonStyle = isTypeSelected ? Style.PRIMARY : Style.DEFAULT;

			         val createButton = bootstrap.builders.button().name( "btn-create" )
			                                              .style( buttonStyle )
			                                              .text( entityViewRequest.getEntityViewContext().getEntityMessages().createAction() );

			         if ( isTypeSelected ) {
				         // todo: ideally a split-button with the other options should be shown but this is currently not styled correctly by admin web
				         createButton.link( linkBuilder.createView().withQueryParam( REQUEST_PARAMETER, fixedDefinition.getId() ).toUriString() )
				                     .style( buttonStyle );
				         groupBuilder.add( createButton );
			         }
			         else {
				         val dropDownButton = isTypeSelected ? bootstrap.builders.button().style( buttonStyle ) : createButton.add( html.builders.text( " " ) );

				         dropDownButton.css( "dropdown-toggle" )
				                       .attribute( "data-toggle", "dropdown" )
				                       .add( html.builders.span().css( "caret" ) );

				         val dropDownOptions = html.builders.ul().css( "dropdown-menu" );

				         definitionRepository.findAll( QDynamicDefinition.dynamicDefinition.name.asc() )
				                             .forEach( def ->
						                                       dropDownOptions.add(
								                                       html.builders.li()
								                                                    .add( bootstrap.builders.link().url(
										                                                    linkBuilder.createView()
										                                                               .withQueryParam( REQUEST_PARAMETER, def.getId() )
										                                                               .toUriString() )
								                                                                            .text( def.getName() ) )
						                                       )
				                             );

				         groupBuilder.add( createButton )
				                     .add( isTypeSelected ? dropDownButton : null )
				                     .add( dropDownOptions );
			         }

			         actions.addChild( groupBuilder.build( builderContext ) );
		         } );
	}
}
