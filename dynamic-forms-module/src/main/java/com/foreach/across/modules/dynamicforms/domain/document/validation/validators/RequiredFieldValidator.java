/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.validation.validators;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentFieldValue;
import com.foreach.across.modules.dynamicforms.domain.document.validation.DynamicDocumentFieldValidator;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;

import java.lang.reflect.Array;
import java.util.Collection;

/**
 * Validator that checks a value is set for a field:
 * <ul>
 * <li>the value must not be {@code null}</li>
 * <li>a {@code String} must not be empty</li>
 * <li>an array or {@code Collection} must not be empty</li>
 * </ul>
 * <p/>
 * Also marks the {@link com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor} as required
 * if the single constructor argument was {@code true}.
 * <p/>
 * A value is rejected using the {@code required} message code.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
public class RequiredFieldValidator implements DynamicDocumentFieldValidator
{
	private final boolean indicateAsRequired;

	@Override
	public void customizePropertyDescriptor( DynamicDocumentFieldDefinition field, EntityPropertyDescriptorBuilder builder ) {
		if ( indicateAsRequired ) {
			builder.attribute( EntityAttributes.PROPERTY_REQUIRED, true );
		}
	}

	@Override
	public void validate( DynamicDocumentFieldValue field, Errors errors ) {
		if ( isNullOrEmpty( field.getFieldValue() ) ) {
			errors.rejectValue( field.getFieldKey(), "required" );
		}
	}

	private boolean isNullOrEmpty( Object value ) {
		if ( value == null ) {
			return true;
		}
		if ( value instanceof String && StringUtils.isEmpty( (String) value ) ) {
			return true;
		}
		if ( value instanceof Collection && ( (Collection) value ).isEmpty() ) {
			return true;
		}
		if ( value.getClass().isArray() && Array.getLength( value ) == 0 ) {
			return true;
		}
		return false;
	}
}
