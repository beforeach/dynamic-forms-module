/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

/**
 * Component for loading raw data into a {@link DynamicDocumentData} or converting the actual
 * data to another format (eg. JSON, YML).
 * <p/>
 * The output format will usually be a {@code String}, but this is not strictly required.
 *
 * @author Arne Vandamme
 * @see com.foreach.across.modules.dynamicforms.domain.document.data.mappers.JsonDocumentDataMapper
 * @since 0.0.1
 */
public interface DynamicDocumentDataMapper<T>
{
	/**
	 * Default data mapper types.
	 */
	String JSON = "JSON";
	String YAML = "YAML";

	/**
	 * Load the data object into the document.
	 *
	 * @param document in which to load the data
	 * @param data     object this mapper supports
	 */
	default void load( DynamicDocumentData document, T data ) {
		load( DynamicDocumentDataFieldWriter.forDocumentData( document ), data );
	}

	/**
	 * Load the data object into the document, represented by the field writer.
	 *
	 * @param document in which to load the data
	 * @param data     object this mapper supports
	 */
	void load( DynamicDocumentDataFieldWriter document, T data );

	/**
	 * Dump the document as a data object.
	 *
	 * @param document whose content to dump
	 * @return data object with the document content
	 */
	T dump( DynamicDocumentData document );
}
