/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.modules.hibernate.installers.AuditableSchemaInstaller;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Order(2)
@Installer(description = "Adds auditing columns to core tables", version = 1)
public class DynamicFormsAuditableInstaller extends AuditableSchemaInstaller {
    @Override
    protected Collection<String> getTableNames() {
	    return Arrays.asList( "dfm_definition_data_set", "dfm_definition_version", "dfm_definition", "dfm_document", "dfm_document_version" );
    }
}
