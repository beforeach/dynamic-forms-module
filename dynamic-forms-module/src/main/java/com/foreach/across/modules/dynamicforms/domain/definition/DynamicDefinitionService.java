/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Service class to find {@link DynamicDefinition} and {@link DynamicDefinitionVersion} object by {@link DynamicDefinitionId}.
 *
 * @author Marc Vanbrabant
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Service
@RequiredArgsConstructor
public class DynamicDefinitionService
{
	private final DynamicDefinitionRepository definitionRepository;
	private final DynamicDefinitionVersionRepository definitionVersionRepository;
	private final DynamicDefinitionTypeConfigurationRegistry typeConfigurationRegistry;

	/**
	 * Find the {@link DynamicDefinitionVersion} represented by the {@link DynamicDefinitionId}.
	 * If {@link DynamicDefinitionId#isForVersion()} returns {@code false}, then the latest version
	 * will be returned (if there is one).
	 *
	 * @param id for the definition version
	 * @return definition version or {@code null} if none found
	 */
	public DynamicDefinitionVersion findDefinitionVersion( @NonNull DynamicDefinitionId id ) {
		val definition = findDefinition( id );

		if ( definition != null ) {
			if ( !id.isForVersion() ) {
				return definition.getLatestVersion();
			}

			QDynamicDefinitionVersion definitionVersion = QDynamicDefinitionVersion.dynamicDefinitionVersion;
			return definitionVersionRepository.findOne( definitionVersion.definition.eq( definition ).and( definitionVersion.version.eq( id.getVersion() ) ) )
			                                  .orElse( null );
		}

		return null;
	}

	/**
	 * Find the {@link DynamicDefinition} represented by the {@link DynamicDefinitionId}.
	 *
	 * @param id for the definition
	 * @return definition or {@code null} if none found
	 */
	public DynamicDefinition findDefinition( @NonNull DynamicDefinitionId id ) {
		DynamicDefinitionTypeConfiguration typeConfiguration = typeConfigurationRegistry.getByName( id.getDefinitionType() );
		Assert.notNull( typeConfiguration, "Unknown definition type: " + id.getDefinitionType() );

		QDynamicDefinition definition = QDynamicDefinition.dynamicDefinition;
		BooleanExpression query = definition.dataSet.key.eq( id.getDataSetKey() )
		                                                .and( definition.type.eq( typeConfiguration.getType() ) )
		                                                .and( definition.key.eq( id.getDefinitionKey() ) );

		if ( id.hasParent() ) {
			val parent = findDefinition( id.getParent() );

			if ( parent == null ) {
				return null;
			}

			query = query.and( definition.parent.eq( parent ) );
		}

		return definitionRepository.findOne( query ).orElse( null );
	}

	/**
	 * Perform a full delete of a {@link DynamicDefinition}, this will delete all documents and versions
	 * linked to this definition, and finally delete the definition and its versions.
	 *
	 * @param definition to delete
	 */
	@Transactional
	public void deleteDefinition( @NonNull DynamicDefinition definition ) {

		val toDelete = definition.toDto();
		toDelete.setLatestVersion( null );

		/*
		documentRepository.save( toDelete );
		documentVersionRepository.delete( documentVersionRepository.findAllByDocumentOrderByLastModifiedDateDesc( toDelete ) );
		documentRepository.delete( toDelete );
		*/
	}
}
