/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document.data;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.types.DynamicDataFieldSet;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocument;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.dynamicforms.domain.document.Reporter;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.BuildDynamicDocumentFormulaEvaluationContext;
import com.foreach.across.modules.dynamicforms.domain.document.data.expressions.DynamicDocumentFieldFormulaContext;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.EvaluationContext;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * Helper for manipulating {@link DynamicDocumentData}.
 * Internal to the framework.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@NotThreadSafe
@RequiredArgsConstructor
@Slf4j
class DynamicDocumentDataAccessor implements DynamicDocumentFieldAccessorResolver, Reporter
{
	private final DocumentAccessor root = new DocumentAccessor();
	private final Map<String, AbstractFieldAccessor> accessorCache = new HashMap<>();

	private final DynamicDocumentDefinition definition;
	private final Map<String, Object> data;

	private final FieldCalculationStack calculationStack = new FieldCalculationStack();

	private Map<String, Object> defaultValueCache;

	/**
	 * Required for supporting nested document fields.
	 */
	@Setter
	@Getter
	private DynamicDocumentService documentService;

	/**
	 * Required for supporting calculated fields.
	 * If none specified, one will be created the first time one is required.
	 */
	@Setter
	@Getter
	private EvaluationContext evaluationContext;

	/**
	 * ConversionService that should be used to ensure field values match.
	 * If none specified, the values must be of the correct type.
	 */
	@Setter
	@Getter
	private ConversionService conversionService;

	/**
	 * Reporter callback for handling exceptions.
	 */
	@Setter
	@Getter
	private Reporter reporter = this;

	/**
	 * Set to true if fields should be calculated when their value is retrieved.
	 */
	@Getter
	@Setter
	private boolean calculateFields;

	/**
	 * Set to true if fields should be initialized when their value is retrieved on initialization.
	 */
	@Getter
	@Setter
	private boolean initializeFields;

	/**
	 * Find a field accessor for the given property path.
	 * Will return {@code empty} if accessor can't be found.
	 *
	 * @param fieldPath path to the field
	 * @return optional containing the accessor, never {@code null}
	 */
	@Override
	public Optional<DynamicDocumentFieldAccessor> findFieldAccessor( String fieldPath ) {
		try {
			return Optional.of( getFieldAccessor( fieldPath ) );
		}
		catch ( DynamicDocumentDefinition.NoSuchFieldException nsfe ) {
			return Optional.empty();
		}
	}

	/**
	 * Get the field accessor for the given property path.
	 * This method will throw an exception if no accessor can be found, and
	 * it will fail on the first segment of the entire path that can't be found.
	 *
	 * @param fieldPath path to the field
	 * @return accessor, never {@code null}
	 */
	public AbstractFieldAccessor getFieldAccessor( @NonNull String fieldPath ) {
		String pathToResolve = StringUtils.removeStart( fieldPath, DynamicDocumentFieldDefinition.DESCRIPTOR_PREFIX );

		AbstractFieldAccessor accessor = accessorCache.get( pathToResolve );

		if ( accessor == null ) {
			return root.getFieldAccessor( fieldPath );
		}

		return accessor;
	}

	int segmentSeparatorIndex( String fieldPath ) {
		int dotSeparator = fieldPath.indexOf( '.' );
		int arraySeparator = fieldPath.indexOf( '[' );

		if ( arraySeparator == 0 ) {
			arraySeparator = fieldPath.indexOf( '[', arraySeparator + 1 );
		}
		if ( arraySeparator > 0 ) {
			if ( dotSeparator != -1 ) {
				return Math.min( dotSeparator, arraySeparator );
			}
			else {
				return arraySeparator;
			}
		}

		return dotSeparator;
	}

	boolean isIndexedSegment( String segment ) {
		return segment.charAt( 0 ) == '[' && segment.charAt( segment.length() - 1 ) == ']';
	}

	int itemIndex( String segment ) {
		String indexString = segment.substring( 1, segment.length() - 1 );
		return indexString.isEmpty() ? -1 : Integer.parseInt( indexString );
	}

	String buildFieldPath( String prefix, String segment ) {
		if ( prefix.isEmpty() ) {
			return segment;
		}
		if ( segment.isEmpty() ) {
			return prefix;
		}
		if ( segment.charAt( 0 ) == '[' ) {
			return prefix + segment;
		}

		return prefix + '.' + segment;
	}

	private EvaluationContext ensureEvaluationContext() {
		if ( evaluationContext == null ) {
			// Create a default evaluation context, usually this should have been done by the DynamicDocumentWorkspace
			evaluationContext = BuildDynamicDocumentFormulaEvaluationContext.createDefaultEvaluationContext();
		}
		return evaluationContext;
	}

	/**
	 * Internal base for field accessors.
	 */
	@RequiredArgsConstructor
	abstract class AbstractFieldAccessor<T> implements DynamicDocumentFieldAccessor<T>, DynamicDocumentFieldAccessorResolver
	{
		private final Object INITIALIZATION_BUSY = new Object();

		@Getter
		private final AbstractFieldAccessor<?> parent;

		@Getter
		private final DynamicDocumentFieldDefinition fieldDefinition;

		@Getter
		private final String fieldPath;

		@Setter
		private boolean cacheable = true;

		@Override
		public TypeDescriptor getFieldType() {
			return fieldDefinition.getTypeDefinition().getTypeDescriptor();
		}

		@Override
		public final Optional<DynamicDocumentFieldAccessor> findFieldAccessor( String fieldPath ) {
			if ( StringUtils.startsWith( fieldPath, "." ) && parent != null ) {
				return parent.findFieldAccessor( StringUtils.removeStart( fieldPath, "." ) );
			}
			try {
				return Optional.of( getFieldAccessor( fieldPath ) );
			}
			catch ( DynamicDocumentDefinition.NoSuchFieldException nsfe ) {
				return Optional.empty();
			}
		}

		/**
		 * Check if the accessor can be cached and will always return the actual field.
		 * Accessors for individual collection items usually can't be cached.
		 *
		 * @return true if cacheable
		 */
		final boolean isCacheable() {
			return cacheable && ( parent == null || parent.isCacheable() );
		}

		/**
		 * @return child fields that can be resolved on this accessor
		 */
		List<DynamicDocumentFieldDefinition> getFields() {
			return getFieldDefinition().getFields();
		}

		@Override
		public Object getDefaultValue() {
			return getDefaultValue( true );
		}

		final Object getDefaultValue( boolean allowInitialization ) {
			BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> initializerExpression = getFieldDefinition().getInitializerExpression();

			if ( initializerExpression != null ) {
				boolean cacheCreated = false;

				try {
					if ( defaultValueCache == null ) {
						cacheCreated = true;
						defaultValueCache = new HashMap<>();
					}

					String fieldPath = getFieldPath();

					if ( !defaultValueCache.containsKey( fieldPath ) ) {
						if ( allowInitialization && !exists() ) {
							defaultValueCache.put( fieldPath, INITIALIZATION_BUSY );
							initialize();
							defaultValueCache.put( fieldPath, get() );
							delete();
						}
						else {
							calculationStack.execute(
									fieldPath,
									() -> {
										try {
											defaultValueCache.put(
													fieldPath,
													initializerExpression.apply( ensureEvaluationContext(),
													                             new DynamicDocumentFieldFormulaContext(
															                             DynamicDocumentDataAccessor.this, this ) )
											);
										}
										catch ( Exception e ) {
											LOG.trace( "Exception executing formula for field {} ({})", getFieldDefinition().getId(), getFieldPath(), e );
											reporter.formulaFailed( getFieldDefinition(), getFieldPath(), e );
											defaultValueCache.put( fieldPath, null );
										}
									}
							);
						}
					}

					return defaultValueCache.get( fieldPath );
				}
				finally {
					if ( cacheCreated ) {
						defaultValueCache = null;
					}
				}
			}

			return null;
		}

		@Override
		public void recalculate() {
		}

		@Override
		public void initialize() {
			BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> initializerExpression = getFieldDefinition().getInitializerExpression();

			if ( initializerExpression != null && !exists() ) {
				boolean previousInitializeStatus = initializeFields;

				try {
					initializeFields = true;

					calculationStack.execute(
							getFieldPath(),
							() -> {
								try {
									set( initializerExpression.apply( ensureEvaluationContext(),
									                                  new DynamicDocumentFieldFormulaContext(
											                                  DynamicDocumentDataAccessor.this, this ) ) );
								}
								catch ( Exception e ) {
									LOG.trace( "Exception executing formula for field {} ({})", getFieldDefinition().getId(), getFieldPath(), e );
									reporter.formulaFailed( getFieldDefinition(), getFieldPath(), e );
									set( null );
								}
							}
					);
				}
				finally {
					initializeFields = previousInitializeStatus;
				}
			}
		}

		/**
		 * Put the accessor in the cache if allowed.
		 */
		final AbstractFieldAccessor cache( AbstractFieldAccessor accessor ) {
			if ( accessor != null && accessor.isCacheable() ) {
				accessorCache.put( accessor.getFieldPath(), accessor );
			}
			return accessor;
		}

		@Override
		public AbstractFieldAccessor getFieldAccessor( @NonNull String fieldPath ) {
			int separator = segmentSeparatorIndex( fieldPath );
			boolean isLastSegment = separator < 0;
			String segmentName = isLastSegment ? fieldPath : fieldPath.substring( 0, separator );

			String segmentFieldPath = buildFieldPath( getFieldPath(), segmentName );
			AbstractFieldAccessor accessor = accessorCache.get( segmentFieldPath );

			if ( accessor == null ) {
				accessor = cache( retrieveFieldAccessorForId( segmentName, segmentFieldPath ) );
			}

			if ( !isLastSegment ) {
				String nextPath = fieldPath.charAt( separator ) == '[' ? fieldPath.substring( separator ) : fieldPath.substring( separator + 1 );
				return accessor.getFieldAccessor( nextPath );
			}

			return accessor;
		}

		AbstractFieldAccessor retrieveFieldAccessorForId( String fieldId, String fieldPath ) {
			DynamicDocumentFieldDefinition definition = retrieveFieldDefinition( fieldId );

			return createFieldAccessor( fieldPath, definition, asFieldValueContainerFor( definition.getId() ) );
		}

		abstract FieldValueContainer<Object> asFieldValueContainerFor( String fieldId );

		final AbstractFieldAccessor createFieldAccessor( String fieldPath, DynamicDocumentFieldDefinition definition, FieldValueContainer<Object> container ) {
			if ( definition.isCalculatedField() ) {
				return new CalculatedFieldValueAccessor( this, definition, fieldPath, container );
			}
			else if ( isCollection( definition ) ) {
				return new ListValueAccessor( this, definition, fieldPath, container );
			}
			else if ( isFieldSet( definition ) ) {
				return new FieldSetValueAccessor( this, definition, fieldPath, container );
			}
			else if ( isNestedDocument( definition ) ) {
				return new NestedDocumentAccessor( this, definition, fieldPath, container );
			}

			return new SingleFieldValueAccessor( this, definition, fieldPath, container );
		}

		final boolean isNestedDocument( DynamicDocumentFieldDefinition field ) {
			TypeDescriptor typeDescriptor = field.getTypeDefinition().getTypeDescriptor();
			return typeDescriptor.equals( TypeDescriptor.valueOf( DynamicDocumentVersion.class ) )
					|| typeDescriptor.equals( TypeDescriptor.valueOf( DynamicDocument.class ) );
		}

		final boolean isCollection( DynamicDocumentFieldDefinition definition ) {
			return definition.isCollection() && definition.getMemberFieldDefinition() != null;
		}

		final boolean isFieldSet( DynamicDocumentFieldDefinition definition ) {
			return TypeDescriptor.valueOf( DynamicDataFieldSet.class ).equals( definition.getTypeDefinition().getTypeDescriptor() );
		}

		final DynamicDocumentFieldDefinition retrieveFieldDefinition( String fieldId ) {
			return getFields()
					.stream()
					.filter( fld -> fieldId.equals( fld.getId() ) )
					.findFirst()
					.orElseThrow( () -> new DynamicDocumentDefinition.NoSuchFieldException( buildFieldPath( getFieldPath(), fieldId ) ) );
		}

		final boolean useDefaultValues() {
			return defaultValueCache != null && INITIALIZATION_BUSY != defaultValueCache.get( getFieldPath() );
		}
	}

	/**
	 * Field value container interface.
	 */
	@SuppressWarnings("unchecked")
	interface FieldValueContainer<T>
	{
		Object LIST_ITEM_PLACEHOLDER = new Object();

		T set( T value );

		T get();

		default T create() {
			return get();
		}

		T delete();

		boolean exists();

		static <U> FieldValueContainer<U> forAccessor( DynamicDocumentFieldAccessor<U> accessor ) {
			return new FieldValueContainer<U>()
			{
				@Override
				public U set( U value ) {
					return (U) accessor.set( value );
				}

				@Override
				public U get() {
					return accessor.get();
				}

				@Override
				public U create() {
					return accessor.getOrCreate();
				}

				@Override
				public U delete() {
					return (U) accessor.delete();
				}

				@Override
				public boolean exists() {
					return accessor.exists();
				}
			};
		}

		static FieldValueContainer<Object> forListItem( FieldValueContainer<List> container, int index ) {
			return new FieldValueContainer<Object>()
			{
				@Override
				public Object set( Object value ) {
					return container.create().set( index, value );
				}

				@Override
				public Object get() {
					return Optional.ofNullable( container.get() ).map( c -> c.get( index ) )
					               .filter( v -> v != LIST_ITEM_PLACEHOLDER )
					               .orElse( null );
				}

				@Override
				public Object delete() {
					return Optional.ofNullable( container.get() ).filter( c -> c.size() > index ).map( c -> c.remove( index ) ).orElse( null );
				}

				@Override
				public boolean exists() {
					return Optional.ofNullable( container.get() ).map( c -> c.size() > index && c.get( index ) != LIST_ITEM_PLACEHOLDER ).orElse( false );
				}

				@Override
				public Object create() {
					return Optional.ofNullable( container.get() )
					               .map( c -> {
						               Object value = c.get( index );
						               if ( value == LIST_ITEM_PLACEHOLDER ) {
							               c.set( index, null );
							               value = null;
						               }
						               return value;
					               } ).orElse( null );
				}
			};
		}

		static FieldValueContainer<Object> forMapEntry( FieldValueContainer<Map<String, Object>> container, String entryKey ) {
			return new FieldValueContainer<Object>()
			{
				@Override
				public Object set( Object value ) {
					return container.create().put( entryKey, value );
				}

				@Override
				public Object get() {
					return Optional.ofNullable( container.get() ).map( c -> c.get( entryKey ) ).orElse( null );
				}

				@Override
				public Object delete() {
					return Optional.ofNullable( container.get() ).map( c -> c.remove( entryKey ) ).orElse( null );
				}

				@Override
				public boolean exists() {
					return Optional.ofNullable( container.get() ).map( c -> c.containsKey( entryKey ) ).orElse( false );
				}
			};
		}
	}

	/**
	 * Represent a fieldset type field.
	 */
	class FieldSetValueAccessor extends AbstractFieldAccessor<Map<String, Object>>
	{
		private final FieldValueContainer<Object> container;

		FieldSetValueAccessor( AbstractFieldAccessor<?> parent,
		                       DynamicDocumentFieldDefinition fieldDefinition,
		                       String fieldPath,
		                       FieldValueContainer<Object> container ) {
			super( parent, fieldDefinition, fieldPath );
			this.container = container;
		}

		@Override
		@SuppressWarnings("unchecked")
		public Object set( Object value ) {
			try {
				Map<String, ?> data = (Map<String, ?>) value;

				if ( data == null ) {
					return container.set( null );
				}

				Object previous = container.get();

				if ( previous != null ) {
					previous = new LinkedHashMap<>( (Map) previous );
				}

				data.forEach( ( k, v ) -> {
					try {
						this.getFieldAccessor( k ).set( v );
					}
					catch ( DynamicDocumentDefinition.NoSuchFieldException nsfe ) {
						reporter.unknownField( nsfe.getFieldName(), v, nsfe );
					}
				} );

				return previous;
			}
			catch ( Exception e ) {
				reporter.illegalValue( getFieldDefinition(), getFieldPath(), value, e );
			}

			return null;
		}

		@Override
		@SuppressWarnings("unchecked")
		public Map<String, Object> get() {
			if ( useDefaultValues() && !exists() ) {
				return (Map<String, Object>) getDefaultValue();
			}

			if ( calculateFields ) {
				recalculate();
			}
			return (Map<String, Object>) container.get();
		}

		@Override
		public Map<String, Object> getOrCreate() {
			Map<String, Object> map = get();

			if ( useDefaultValues() && map == null ) {
				return new DynamicDataFieldSet();
			}
			else if ( map == null ) {
				initialize();
				map = get();
				if ( map == null ) {
					container.set( new DynamicDataFieldSet() );
					map = get();
				}
			}

			return map;
		}

		@Override
		public Object delete() {
			return container.delete();
		}

		@Override
		public boolean exists() {
			return container.exists();
		}

		@Override
		public void recalculate() {
			calculationStack.execute( getFieldPath(), () -> getFields()
					.stream()
					.filter( child -> !child.isMember() )
					.map( child -> getFieldAccessor( child.getId() ) )
					.forEach( DynamicDocumentFieldAccessor::recalculate )
			);
		}

		@Override
		public void initialize() {
			if ( getFieldDefinition().getInitializerExpression() == null && !exists() ) {
				set( new DynamicDataFieldSet() );
				initializeChildFields();
			}
			else {
				super.initialize();
				if ( !exists() || get() != null ) {
					initializeChildFields();
				}
			}
		}

		private void initializeChildFields() {
			calculationStack.execute( getFieldPath(), () -> getFields()
					.stream()
					.filter( child -> !child.isMember() )
					.map( child -> getFieldAccessor( child.getId() ) )
					.forEach( DynamicDocumentFieldAccessor::initialize )
			);
		}

		@Override
		FieldValueContainer<Object> asFieldValueContainerFor( String fieldId ) {
			return FieldValueContainer.forMapEntry( FieldValueContainer.forAccessor( this ), fieldId );
		}
	}

	/**
	 * Main accessor for the entire document.
	 */
	final class DocumentAccessor extends FieldSetValueAccessor
	{
		DocumentAccessor() {
			super( null, null, "", null );
		}

		@Override
		public TypeDescriptor getFieldType() {
			return TypeDescriptor.valueOf( LinkedHashMap.class );
		}

		@Override
		public Object set( Object value ) {
			throw new UnsupportedOperationException( "Not supported" );
		}

		@Override
		public Map<String, Object> get() {
			return data;
		}

		@Override
		public Map<String, Object> getOrCreate() {
			return data;
		}

		@Override
		public Object delete() {
			return null;
		}

		@Override
		public boolean exists() {
			return true;
		}

		@Override
		List<DynamicDocumentFieldDefinition> getFields() {
			return definition.getContent();
		}

	}

	/**
	 * Represents a simple value field (not a fieldset or collection).
	 */
	class SingleFieldValueAccessor extends AbstractFieldAccessor<Object>
	{
		private final FieldValueContainer<Object> container;

		SingleFieldValueAccessor( AbstractFieldAccessor parent,
		                          DynamicDocumentFieldDefinition fieldDefinition,
		                          String fieldPath,
		                          FieldValueContainer<Object> container ) {
			super( parent, fieldDefinition, fieldPath );
			this.container = container;
		}

		@Override
		public Object set( Object value ) {
			Object newValue = value;
			boolean existed = container.exists();
			Object currentValue = container.get();

			try {
				if ( newValue != null ) {
					if ( conversionService != null ) {
						newValue = conversionService.convert( value, TypeDescriptor.forObject( value ), getFieldType() );
					}
					else if ( !getFieldType().getObjectType().isInstance( value ) ) {
						throw new TypeAwareClassCastException( getFieldType(), value );
					}
				}

				container.set( newValue );

				reporter.valueSet( getFieldDefinition(), getFieldPath(), currentValue, newValue, !existed );
			}
			catch ( Exception e ) {
				reporter.illegalValue( getFieldDefinition(), getFieldPath(), value, e );
			}
			return currentValue;
		}

		@Override
		public Object get() {
			if ( useDefaultValues() && !exists() ) {
				return getDefaultValue();
			}

			if ( initializeFields ) {
				initialize();
			}

			return container.get();
		}

		@Override
		public Object getOrCreate() {
			if ( !useDefaultValues() && !exists() ) {
				initialize();
			}

			return get();
		}

		@Override
		public Object delete() {
			return container.delete();
		}

		@Override
		public boolean exists() {
			return container.exists();
		}

		@Override
		public AbstractFieldAccessor getFieldAccessor( String fieldPath ) {
			throw new DynamicDocumentDefinition.NoSuchFieldException( buildFieldPath( getFieldPath(), fieldPath ) );
		}

		@Override
		FieldValueContainer<Object> asFieldValueContainerFor( String fieldId ) {
			return null;
		}
	}

	/**
	 * Represents a calculated field.
	 */
	class CalculatedFieldValueAccessor extends SingleFieldValueAccessor
	{
		private final BiFunction<EvaluationContext, DynamicDocumentFieldFormulaContext, Object> expression;

		CalculatedFieldValueAccessor( AbstractFieldAccessor<?> parent,
		                              DynamicDocumentFieldDefinition fieldDefinition,
		                              String fieldPath,
		                              FieldValueContainer<Object> container ) {
			super( parent, fieldDefinition, fieldPath, container );
			expression = fieldDefinition.getExpression();
		}

		@Override
		public Object get() {
			if ( calculateFields ) {
				recalculate();
			}
			return super.get();
		}

		@Override
		public void recalculate() {
			calculationStack.execute(
					getFieldPath(),
					() -> {
						try {
							set( expression.apply( ensureEvaluationContext(),
							                       new DynamicDocumentFieldFormulaContext( DynamicDocumentDataAccessor.this, this ) ) );
						}
						catch ( Exception e ) {
							LOG.trace( "Exception executing formula for field {} ({})", getFieldDefinition().getId(), getFieldPath(), e );
							reporter.formulaFailed( getFieldDefinition(), getFieldPath(), e );
							set( null );
						}
					}
			);
		}

		@Override
		FieldValueContainer<Object> asFieldValueContainerFor( String fieldId ) {
			return null;
		}
	}

	/**
	 * Represent a collection type field.
	 */
	class ListValueAccessor extends AbstractFieldAccessor<List>
	{
		private final FieldValueContainer<Object> container;

		ListValueAccessor( AbstractFieldAccessor<?> parent,
		                   DynamicDocumentFieldDefinition fieldDefinition,
		                   String fieldPath,
		                   FieldValueContainer<Object> container ) {
			super( parent, fieldDefinition, fieldPath );
			this.container = container;
		}

		@Override
		public Object set( Object value ) {
			try {
				Iterable<?> items = (Iterable) value;

				if ( items == null ) {
					return container.set( null );
				}

				List<Object> itemsAsList = new ArrayList<>();
				items.forEach( itemsAsList::add );

				ArrayList<Object> actualItems = new ArrayList<>( itemsAsList.size() );
				Object previous = container.set( actualItems );

				for ( int i = 0; i < itemsAsList.size(); i++ ) {
					actualItems.add( FieldValueContainer.LIST_ITEM_PLACEHOLDER );
					getFieldAccessor( "[" + i + "]" ).set( itemsAsList.get( i ) );
				}

				return previous;
			}
			catch ( Exception e ) {
				reporter.illegalValue( getFieldDefinition(), getFieldPath(), value, e );
			}

			return null;
		}

		@Override
		public List get() {
			if ( calculateFields ) {
				recalculate();
			}
			return (List) container.get();
		}

		@Override
		public List getOrCreate() {
			List list = get();
			if ( list == null ) {
				initialize();
				list = get();
				if ( list == null ) {
					container.set( new ArrayList<>() );
					list = get();
				}
			}
			return list;
		}

		@Override
		public Object delete() {
			return container.delete();
		}

		@Override
		public boolean exists() {
			return container.exists();
		}

		@Override
		public void recalculate() {
			calculationStack.execute(
					getFieldPath(),
					() -> {
						List items = get();
						if ( items != null ) {
							for ( int i = 0; i < items.size(); i++ ) {
								getFieldAccessor( "[" + i + "]" ).recalculate();
							}
						}
					}
			);
		}

		@Override
		FieldValueContainer<Object> asFieldValueContainerFor( String fieldId ) {
			return null;
		}

		@Override
		AbstractFieldAccessor retrieveFieldAccessorForId( String fieldId, String fieldPath ) {
			if ( isIndexedSegment( fieldId ) ) {
				int index = itemIndex( fieldId );

				if ( index < 0 ) {
					return new ListItemFieldAccessor( this, getFieldDefinition().getMemberFieldDefinition(), fieldPath, this );
				}

				int itemCount = Optional.ofNullable( get() ).map( List::size ).orElse( 0 );
				if ( itemCount <= index ) {
					throw new DynamicDocumentDefinition.NoSuchFieldException( fieldPath );
				}

				AbstractFieldAccessor accessor = createFieldAccessor( fieldPath, getFieldDefinition().getMemberFieldDefinition(),
				                                                      FieldValueContainer.forListItem( FieldValueContainer.forAccessor( this ), index ) );
				accessor.setCacheable( false );
				return accessor;
			}

			return super.retrieveFieldAccessorForId( fieldId, fieldPath );
		}
	}

	/**
	 * Represents a collection type field that is in fact a collection of sub-fields.
	 * The default value of this field type represents the default value for a new collection member.
	 */
	class ListItemFieldAccessor extends AbstractFieldAccessor<List>
	{
		private final TypeDescriptor typeDescriptor;
		private final AbstractFieldAccessor<List> list;

		ListItemFieldAccessor( AbstractFieldAccessor<?> parent, DynamicDocumentFieldDefinition fieldDefinition, String fieldPath,
		                       AbstractFieldAccessor<List> list ) {
			super( parent, fieldDefinition, fieldPath );
			this.typeDescriptor = buildTypeDescriptor( fieldDefinition.getTypeDefinition().getTypeDescriptor() );
			this.list = list;
		}

		private TypeDescriptor buildTypeDescriptor( TypeDescriptor targetType ) {
			AbstractFieldAccessor<?> parentAccessor = getParent();

			int parentCollections = 0;

			if ( parentAccessor instanceof ListItemFieldAccessor ) {
				TypeDescriptor parentType = parentAccessor.getFieldType();

				while ( parentType.isCollection() ) {
					parentCollections++;
					parentType = parentType.getElementTypeDescriptor();
				}
			}

			TypeDescriptor typeDescriptor = TypeDescriptor.collection( ArrayList.class, targetType );

			for ( int i = 0; i < parentCollections - 1; i++ ) {
				typeDescriptor = TypeDescriptor.collection( ArrayList.class, typeDescriptor );
			}

			return typeDescriptor;
		}

		@Override
		public TypeDescriptor getFieldType() {
			return typeDescriptor;
		}

		@Override
		public Object set( Object value ) {
			try {
				List newValues = new ArrayList<>( value != null ? (Collection<?>) value : Collections.emptyList() );
				List<AbstractFieldAccessor<Object>> accessors = itemAccessors();

				if ( newValues.size() != accessors.size() ) {
					throw new IllegalArgumentException(
							"The number of new values for a collection field must match with the current number of collection items" );
				}

				List<Object> previousValues = new ArrayList<>( accessors.size() );
				for ( int i = 0; i < accessors.size(); i++ ) {
					previousValues.add( accessors.get( i ).set( newValues.get( i ) ) );
				}

				return previousValues;
			}
			catch ( Exception e ) {
				reporter.illegalValue( getFieldDefinition(), getFieldPath(), value, e );
			}

			return null;
		}

		@Override
		public List get() {
			return itemAccessors()
					.stream()
					.map( AbstractFieldAccessor::get )
					.collect( Collectors.toList() );
		}

		@Override
		public List getOrCreate() {
			return itemAccessors()
					.stream()
					.map( AbstractFieldAccessor::getOrCreate )
					.collect( Collectors.toList() );
		}

		@Override
		public Object delete() {
			return itemAccessors()
					.stream()
					.map( AbstractFieldAccessor::delete )
					.collect( Collectors.toList() );
		}

		@Override
		public Object getDefaultValue() {
			return getDefaultValue( false );
		}

		@Override
		public boolean exists() {
			return list.get() != null;
		}

		@Override
		public void recalculate() {
			itemAccessors().forEach( AbstractFieldAccessor::recalculate );
		}

		@SuppressWarnings("unchecked")
		private List<AbstractFieldAccessor<Object>> itemAccessors() {
			List items = list.get();
			if ( items != null ) {
				List<AbstractFieldAccessor<Object>> accessors = new ArrayList<>( items.size() );
				String itemFieldPath = itemFieldPath();
				for ( int i = 0; i < items.size(); i++ ) {
					accessors.add( list.getFieldAccessor( "[" + i + "]" + itemFieldPath ) );
				}
				return accessors;
			}
			return Collections.emptyList();
		}

		private String itemFieldPath() {
			String aggregatedFieldPath = getFieldPath();
			return aggregatedFieldPath.substring( StringUtils.indexOf( aggregatedFieldPath, "[]" ) + 2 );
		}

		@Override
		FieldValueContainer<Object> asFieldValueContainerFor( String fieldId ) {
			return null;
		}

		@Override
		AbstractFieldAccessor retrieveFieldAccessorForId( String fieldId, String fieldPath ) {
			DynamicDocumentFieldDefinition fieldDefinition = isIndexedSegment( fieldId ) && isCollection( getFieldDefinition() )
					? getFieldDefinition().getMemberFieldDefinition() : retrieveFieldDefinition( fieldId );

			return new ListItemFieldAccessor( this, fieldDefinition, fieldPath, list );
		}
	}

	/**
	 * Special purpose field accessor where the field value represents another dynamic document.
	 * Allows resolving nested fields of the target document, but these will be readonly.
	 */
	class NestedDocumentAccessor extends SingleFieldValueAccessor
	{
		private boolean dataLoaded;
		private DynamicDocumentData data;

		NestedDocumentAccessor( AbstractFieldAccessor parent,
		                        DynamicDocumentFieldDefinition fieldDefinition,
		                        String fieldPath,
		                        FieldValueContainer<Object> container ) {
			super( parent, fieldDefinition, fieldPath, container );
		}

		@Override
		public Object delete() {
			dataLoaded = false;
			data = null;
			return super.delete();
		}

		@Override
		public Object set( Object value ) {
			Object previous = super.set( value );

			if ( !Objects.equals( previous, get() ) ) {
				dataLoaded = false;
				data = null;
			}

			return previous;
		}

		@Override
		FieldValueContainer<Object> asFieldValueContainerFor( String fieldId ) {
			return null;
		}

		private void loadDocumentData( Object document ) {
			dataLoaded = true;
			if ( document instanceof DynamicDocument ) {
				data = documentService.createDocumentWorkspace( (DynamicDocument) document ).getFields();
			}
			else if ( document instanceof DynamicDocumentVersion ) {
				data = documentService.createDocumentWorkspace( (DynamicDocumentVersion) document ).getFields();
			}
		}

		@Override
		public AbstractFieldAccessor getFieldAccessor( String fieldPath ) {
			Object document = get();

			if ( !dataLoaded && document != null && documentService != null ) {
				loadDocumentData( document );
			}

			if ( data != null ) {
				try {
					DynamicDocumentFieldAccessor target = data.getFieldAccessor( fieldPath );
					return new ReadonlyDynamicDocumentFieldAccessor( (AbstractFieldAccessor) target );
				}
				catch ( DynamicDocumentDefinition.NoSuchFieldException nsfe ) {
					throw new DynamicDocumentDefinition.NoSuchFieldException( buildFieldPath( getFieldPath(), nsfe.getFieldName() ) );
				}
			}

			throw new DynamicDocumentDefinition.NoSuchFieldException( fieldPath );
		}
	}

	/**
	 * Field accessor wrapper that throws exceptions when attempting to set a value on a nested field.
	 */
	class ReadonlyDynamicDocumentFieldAccessor extends AbstractFieldAccessor<Object>
	{
		private final AbstractFieldAccessor target;

		ReadonlyDynamicDocumentFieldAccessor( AbstractFieldAccessor target ) {
			super( target.getParent(), target.getFieldDefinition(), target.getFieldPath() );
			this.target = target;
			setCacheable( false );
		}

		@Override
		public DynamicDocumentFieldDefinition getFieldDefinition() {
			return target.getFieldDefinition();
		}

		@Override
		public TypeDescriptor getFieldType() {
			return target.getFieldType();
		}

		@Override
		public Object set( Object value ) {
			throw new UnsupportedOperationException( "Updating a nested document field value is not supported." );
		}

		@Override
		public Object get() {
			return target.get();
		}

		@Override
		public Object getOrCreate() {
			return target.get();
		}

		@Override
		public Object delete() {
			throw new UnsupportedOperationException( "Updating a nested document field value is not supported." );
		}

		@Override
		public boolean exists() {
			return target.exists();
		}

		@Override
		public void recalculate() {
			// ignore any recalculations
		}

		@Override
		FieldValueContainer<Object> asFieldValueContainerFor( String fieldId ) {
			return null;
		}

		@Override
		public AbstractFieldAccessor getFieldAccessor( String fieldPath ) {
			return new ReadonlyDynamicDocumentFieldAccessor( target.getFieldAccessor( fieldPath ) );
		}
	}

	static class TypeAwareClassCastException extends ClassCastException
	{
		@Getter
		private final TypeDescriptor targetType;

		@Getter
		private final Object value;

		TypeAwareClassCastException( TypeDescriptor targetType, Object value ) {
			super( "Value not of type '" + targetType + "': " + value );

			this.targetType = targetType;
			this.value = value;
		}
	}
}
