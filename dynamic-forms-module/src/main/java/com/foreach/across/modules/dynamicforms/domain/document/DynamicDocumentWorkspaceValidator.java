/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.document;

import com.foreach.across.modules.entity.validators.EntityValidatorSupport;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * Default {@link org.springframework.validation.Validator} that validates a document workspace:
 * both the entity properties (document name, version label) and the fields will be validated.
 *
 * @author Arne Vandamme
 * @since 0.0.1
 */
@Component
public class DynamicDocumentWorkspaceValidator extends EntityValidatorSupport<DynamicDocumentWorkspace>
{
	@Override
	public boolean supports( Class<?> clazz ) {
		return DynamicDocumentWorkspace.class.isAssignableFrom( clazz );
	}

	@Override
	protected void postValidation( DynamicDocumentWorkspace entity, Errors errors, Object... validationHints ) {
		String dataBinderTarget = entity.getDataBinderTarget();
		entity.setDataBinderTarget( "" );
		entity.validate( errors );
		entity.setDataBinderTarget( dataBinderTarget );
	}
}
