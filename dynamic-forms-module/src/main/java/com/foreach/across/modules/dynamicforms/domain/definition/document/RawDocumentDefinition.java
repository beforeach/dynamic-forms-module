/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.document;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.foreach.across.modules.dynamicforms.domain.definition.RawDefinition;
import lombok.*;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.*;

/**
 * Stateless representation of a document definition, should be converted into
 * a {@link DynamicDocumentDefinition}
 * to be used.
 *
 * @author Arne Vandamme
 * @see DynamicDocumentDefinitionBuilder
 * @since 0.0.1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonRootName("document-definition")
@JsonPropertyOrder({ "name", "types", "content", "messages" })
public class RawDocumentDefinition implements Serializable, RawDefinition
{
	private static final long serialVersionUID = 1L;

	private String name;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonDeserialize(as = ArrayList.class, contentAs = BaseField.class)
	@Singular("type")
	private List<BaseField> types = new ArrayList<>();

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonDeserialize(as = ArrayList.class, contentAs = Field.class)
	@Singular("field")
	@NonNull
	private List<Field> content = new ArrayList<>();

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Map<String, Object> messages = new HashMap<>();

	public List<Field> getContent() {
		return content != null ? content : Collections.emptyList();
	}

	public List<BaseField> getTypes() {
		return types != null ? types : Collections.emptyList();
	}

	public Map<String, Object> getMessages() {
		return messages != null ? messages : Collections.emptyMap();
	}

	/**
	 * Create a new definition that is an exact - deep - copy of the existing definition.
	 *
	 * @param definition to create a copy of
	 * @return new instance
	 */
	public static RawDocumentDefinition copyOf( @NonNull RawDocumentDefinition definition ) {
		return SerializationUtils.clone( definition );
	}

	@Getter
	@Setter
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	@NoArgsConstructor
	public static abstract class AbstractField implements Serializable, BaseFieldType
	{
		private static final long serialVersionUID = 1L;

		/**
		 * Indicates a field was created by the framework.
		 */
		@JsonIgnore
		@Setter(AccessLevel.PRIVATE)
		private boolean synthetic;

		private String id;
		private String label;

		private String formula;

		@JsonInclude(JsonInclude.Include.NON_EMPTY)
		@JsonDeserialize(as = ArrayList.class, contentAs = Field.class)
		@Singular("field")
		private List<Field> fields = new ArrayList<>();

		private Field member;

		@JsonInclude(JsonInclude.Include.NON_EMPTY)
		@JsonProperty("possible-values")
		private List possibleValues = new ArrayList();

		@JsonInclude(JsonInclude.Include.NON_EMPTY)
		private List validators = new ArrayList();

		@JsonInclude(JsonInclude.Include.NON_EMPTY)
		private Map<String, Object> messages = new HashMap<>();

		@JsonInclude(JsonInclude.Include.NON_EMPTY)
		private Map<String, Object> attributes = new LinkedHashMap<>();

		@JsonProperty("default")
		private DefaultValue defaultValue = DefaultValue.NOT_SPECIFIED;

		public List<Field> getFields() {
			return fields != null ? fields : Collections.emptyList();
		}

		public Map<String, Object> getMessages() {
			return messages != null ? messages : Collections.emptyMap();
		}

		public Map<String, Object> getAttributes() {
			return attributes != null ? attributes : Collections.emptyMap();
		}

		public List getValidators() {
			return validators != null ? validators : Collections.emptyList();
		}

		/**
		 * Check if a specific attribute is present on this field definition.
		 *
		 * @param attributeName name of the attribute
		 * @return true if attribute key is present (value can be {@code null})
		 */
		public boolean hasAttribute( String attributeName ) {
			return attributes != null && attributes.containsKey( attributeName );
		}

		/**
		 * @return true if this field has a limited number of possible values
		 */
		@JsonIgnore
		public boolean hasPossibleValues() {
			return possibleValues != null && !possibleValues.isEmpty();
		}

		/**
		 * @return true if a formula is set
		 */
		@JsonIgnore
		public boolean isCalculatedField() {
			return !StringUtils.isEmpty( formula );
		}

		/**
		 * Create a new field definition that is an exact - deep - copy of the existing one.
		 *
		 * @param field to create a copy of
		 * @return new instance
		 */
		public static AbstractField copyOf( @NonNull AbstractField field ) {
			return SerializationUtils.clone( field );
		}

		/**
		 * Create a synthetic clone of the member property, as a standalone field that can be modified.
		 *
		 * @return synthetic clone of the {@link #getMember()}
		 */
		public Field createSyntheticMember() {
			if ( member != null ) {
				AbstractField clone = copyOf( member );
				clone.setSynthetic( true );
				return (Field) clone;
			}

			return null;
		}

		/**
		 * Create a new field definition based on the parent definition, merging selected parameters.
		 *
		 * @param template field (usually base type)
		 * @return new definition instance
		 */
		Field asInstanceOf( @NonNull AbstractField template ) {
			Field merged = new Field();
			merged.setId( id );
			merged.setLabel( label );
			if ( template.getType() != null ) {
				merged.setType( template.getType() );
			}

			if ( template.isCalculatedField() ) {
				merged.setFormula( template.getFormula() );
			}

			if ( isCalculatedField() ) {
				merged.setFormula( formula );
			}

			merged.setMessages( mergeMessages( template.getMessages(), getMessages() ) );

			Map<String, Object> attributes = new HashMap<>();
			attributes.putAll( template.getAttributes() );
			attributes.putAll( getAttributes() );
			merged.setAttributes( attributes );

			Set<Object> validators = new LinkedHashSet<>();
			validators.addAll( template.getValidators() );
			validators.addAll( getValidators() );
			merged.setValidators( new ArrayList<>( validators ) );

			if ( hasPossibleValues() ) {
				merged.setPossibleValues( getPossibleValues() );
			}
			else {
				merged.setPossibleValues( template.getPossibleValues() );
			}

			List<Field> children = new ArrayList<>();
			children.addAll( template.getFields() );
			children.addAll( getFields() );
			merged.setFields( children );

			Field templateMember = template.getMember();

			if ( templateMember != null && member == null ) {
				merged.setMember( templateMember );
			}
			else if ( templateMember == null && member != null ) {
				merged.setMember( member );
			}
			else if ( member != null ) {
				merged.setMember( member.asInstanceOf( templateMember ) );
			}

			// A default value specified on content takes precedence over a default value specified on a custom type
			if ( !DefaultValue.NOT_SPECIFIED.equals( defaultValue ) ) {
				merged.setDefaultValue( defaultValue );
			}
			else {
				merged.setDefaultValue( template.getDefaultValue() );
			}

			return merged;
		}

		private Map<String, Object> mergeMessages( Map<String, Object> template, Map<String, Object> instance ) {
			val templateMessages = LocalizedMessage.parseAllFromMap( template );
			val instanceMessage = LocalizedMessage.parseAllFromMap( instance );

			return LocalizedMessage.convertToMap( LocalizedMessage.merge( templateMessages, instanceMessage ) );
		}
	}

	/**
	 * Represents a default value specification, a default value can either be specified as a fixed 'value' or as a 'formula'.
	 */
	@Builder
	@EqualsAndHashCode
	@Data
	@JsonSerialize(using = DefaultValueSerializer.class)
	@NoArgsConstructor
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	public static class DefaultValue implements Serializable
	{
		/**
		 * Unique value specifying that no default value has been set. Is the equivalent of
		 * {@code null} for {@code DefaultValue} except that the latter implies default value
		 * has been <strong>explicitly</strong> reset to 'none'.
		 * <p/>
		 * The dummy not specified value would be illegal to actually use as it combines both
		 * value and formula.
		 */
		public static final DefaultValue NOT_SPECIFIED = new DefaultValue( "3036b75a-7256-4b1c-a78d-41befea32df0", "907f4a02-3e96-4117-af7c-f9047915e1f6" );

		private static final long serialVersionUID = 1L;

		private Object value;
		private String formula;
	}

	@EqualsAndHashCode(callSuper = false)
	@Data
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@NoArgsConstructor
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	@JsonPropertyOrder({ "type", "id", "label", "formula", "possibleValues", "validators", "attributes", "messages", "member", "fields" })
	public static class Field extends AbstractField implements Serializable
	{
		private static final long serialVersionUID = 1L;

		@NonNull
		private String type;

		@Builder
		private Field( String id,
		               String label,
		               String type,
		               String formula,
		               @Singular("field") List<Field> fields,
		               Field member,
		               List possibleValues,
		               List validators,
		               Map<String, Object> messages,
		               Map<String, Object> attributes,
		               DefaultValue defaultValue ) {
			super( false, id, label, formula, fields, member, possibleValues, validators, messages, attributes, defaultValue );
			this.type = type;

		}

		@SuppressWarnings("unused")
		public static class FieldBuilder
		{
			private DefaultValue defaultValue = DefaultValue.NOT_SPECIFIED;
		}
	}

	@EqualsAndHashCode(callSuper = false)
	@Data
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@NoArgsConstructor
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	@JsonPropertyOrder({ "baseType", "id", "label", "formula", "possibleValues", "validators", "attributes", "messages", "member", "fields" })
	public static class BaseField extends AbstractField implements Serializable
	{
		@JsonProperty("base-type")
		@NonNull
		private String baseType;

		@Builder
		private BaseField( String id,
		                   String label,
		                   String baseType,
		                   String formula,
		                   @Singular("field") List<Field> fields,
		                   Field member,
		                   List possibleValues,
		                   List validators,
		                   Map<String, Object> messages,
		                   Map<String, Object> attributes,
		                   DefaultValue defaultValue ) {
			super( false, id, label, formula, fields, member, possibleValues, validators, messages, attributes, defaultValue );
			this.baseType = baseType;
		}

		@Override
		@JsonIgnore
		public String getType() {
			return baseType;
		}

		@SuppressWarnings("unused")
		public static class BaseFieldBuilder
		{
			private DefaultValue defaultValue = DefaultValue.NOT_SPECIFIED;
		}
	}

	public interface BaseFieldType
	{
		String getType();
	}
}
