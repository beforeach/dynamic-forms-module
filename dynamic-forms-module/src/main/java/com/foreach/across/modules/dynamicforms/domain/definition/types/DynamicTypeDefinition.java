/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition.types;

import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentFieldDefinition;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyRegistry;
import org.springframework.core.convert.TypeDescriptor;

/**
 * Represents a type definition, for example:
 * <ul>
 * <li>single value string</li>
 * <li>multi value integer</li>
 * <li>selectable list of values</li>
 * </ul>
 * A type definition has a java type representation.
 *
 * @author Arne Vandamme
 * @see DynamicTypeDefinitionBuilder
 * @see DynamicTypeDefinitionFactory
 * @since 0.0.1
 */
public interface DynamicTypeDefinition
{
	/**
	 * @return name of this type
	 */
	String getTypeName();

	/**
	 * The actual java type that is represented, never {@code null}.
	 *
	 * @return type descriptor
	 */
	TypeDescriptor getTypeDescriptor();

	/**
	 * Apply the type specific customization to the {@link EntityPropertyDescriptorBuilder}.
	 * This builds for example the specific control that should be rendered for his type.
	 *
	 * @param field   for which the descriptor is being built - partial definition
	 * @param builder to customize
	 */
	void customizePropertyDescriptor( DynamicDocumentFieldDefinition field, EntityPropertyDescriptorBuilder builder );

	@Deprecated
	void postProcessField( DynamicDocumentFieldDefinition field, MutableEntityPropertyRegistry registry );
}
