/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.modules.dynamicforms.domain.definition;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.across.modules.dynamicforms.domain.definition.document.DynamicDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class DynamicDefinitionVersionYamlJsonViewAdapter extends EntityViewProcessorAdapter
{
	private final YamlJsonObjectMapper yamlJsonObjectMapper;
	private final DynamicDefinitionTypeConfigurationRegistry typeConfigurationRegistry;
	private final RawDefinitionService rawDefinitionService;
	private final ObjectMapper objectMapper = new ObjectMapper();
	private final DynamicDocumentDefinitionService definitionService;

	@Override
	protected void validateCommandObject( EntityViewRequest entityViewRequest,
	                                      EntityViewCommand command,
	                                      Errors errors,
	                                      HttpMethod httpMethod ) {
		DynamicDefinitionVersion entity = command.getEntity( DynamicDefinitionVersion.class );
		if ( HttpMethod.POST.equals( httpMethod ) ) {
			try {
				String json = yamlJsonObjectMapper.yamlToJson( entity::getDefinitionContent );
				String typeName = entity.getDefinition().getType().getName();
				DynamicDefinitionTypeConfiguration configuration = typeConfigurationRegistry.getByName( typeName );

				if ( configuration == null ) {
					throw new IllegalArgumentException(
							"The specified definition type '" + typeName + "' is not registered in the type configuration registry." );
				}

				RawDefinition rawDefinition = rawDefinitionService.readRawDefinition( json, typeName );
				Validator validator = configuration.getValidator();
				if ( validator != null ) {
					errors.pushNestedPath( "entity" );
					validator.validate( rawDefinition, errors );
					errors.popNestedPath();
				}

				if ( !errors.hasFieldErrors() ) {
					entity.setDefinitionContent( json );
				}
			}
			catch ( IOException | YAMLException e ) {
				errors.rejectValue( "entity.definitionContent", "parsingFailed", new String[] { e.getMessage() }, "Error parsing input" );
			}
		}
		else {
			if ( StringUtils.isNotBlank( entity.getDefinitionContent() ) ) {
				entity.setDefinitionContent( yamlJsonObjectMapper.jsonToYaml( entity::getDefinitionContent ) );
			}
		}
	}
}
