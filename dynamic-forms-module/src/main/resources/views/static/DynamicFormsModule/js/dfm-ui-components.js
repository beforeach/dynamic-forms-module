/*
 * Copyright 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Central registry for component profiles.
 * A profile is attached to a 'componentType' and has a unique name.
 * A profile is expected to be a JS object, usually a dictionary.
 *
 * It can optionally specify a '_parentProfile' holding the name of the parent
 * that should be merged into this profile.  Properties of the parent will be
 * overridden by the child.
 *
 * How a profile is used is entirely up to the actual component implementation,
 * this registry simply provides access to named configuration objects and the merging between parent-child.
 */

(function ( $ ) {
    var cm = CodeMirror.fromTextArea( $( '.js-content-yaml' )[0], {
        viewportMargin: Infinity,
        lineNumbers: true
    } );
    // Add a bootstrap border
    $( cm.display.wrapper ).addClass( 'card' );

    var readOnlyInstances = $( '.js-content-yaml' ).filter( '.js-readonly' );
    if ( readOnlyInstances && readOnlyInstances.length > 0 ) {
        var readOnlyInstance = readOnlyInstances[0];
        if ( readOnlyInstance !== null && readOnlyInstance !== 'undefined' ) {
            var readOnlyCm = $( readOnlyInstance ).next( '.CodeMirror' )[0].CodeMirror;
            readOnlyCm.setOption( 'readOnly', true );
        }
    }
})
( jQuery );