/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe( "Log in", function () {
    it( "Log in using the default admin user", function () {
        cy.visit( "/admin" );
        cy.contains( "Please sign in." );
        cy.get( "#username" )
                .type( "admin" );
        cy.get( "#password" )
                .type( "admin" );
        cy.get( "button[type=submit]" ).click();
        cy.get( ".admin-navbar-title" )
                .find( "a.navbar-brand" )
                .contains( "Admin Dynamic Forms" );
    } );
} );

describe( "Dynamic data set", function () {
    it( "Navigate to listview", function () {
        cy.login( "admin", "admin" );

        cy.contains( ".card-header", "Documents" )
                .parent()
                .find( "a[title='Data sets & definitions']" )
                .click();

        cy.get( "#btn-create" )
                .contains( "Create a new data set" );

        cy.get( "table" );
    } );
} );

