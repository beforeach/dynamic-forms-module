# Installing cypress

* navigate to end2-end-tests directory
* install the cypress dependency by using `yarn install`
* Optional: install npx globally by using `npm install -g npx`

# Starting Cypress

Navigate to end2end-tests/tests directory

## Open the Cypress Test Runner

* Start the DynamicFormsTestApplication
* `"../node_modules/.bin/cypress" open` or by using npx `npx cypress open`

## Running all tests

`"../node_modules/.bin/cypress" open` or by using npx `npx cypress run`


 