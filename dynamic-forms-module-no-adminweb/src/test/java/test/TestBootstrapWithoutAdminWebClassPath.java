/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test;

import com.foreach.across.modules.dynamicforms.DynamicFormsModule;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSetRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.*;
import com.foreach.across.modules.dynamicforms.domain.definition.ui.ReadDocumentDefinitionViewProcessor;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentService;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentVersion;
import com.foreach.across.modules.dynamicforms.domain.document.DynamicDocumentWorkspace;
import com.foreach.across.modules.dynamicforms.domain.document.data.DynamicDocumentDataMapperFactory;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.test.AcrossTestContext;
import org.junit.Test;

import static com.foreach.across.test.support.AcrossTestBuilders.web;
import static java.lang.Thread.currentThread;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.util.ClassUtils.isPresent;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public class TestBootstrapWithoutAdminWebClassPath
{
	@Test
	public void classesShouldNotBeOnTheClassPath() {
		assertThat( isPresent( "com.foreach.across.modules.adminweb.AdminWebModule", currentThread().getContextClassLoader() ) ).isFalse();
	}

	@Test
	public void emptyBootstrap() {
		try (AcrossTestContext context = web( true )
				.property( "acrossHibernate.hibernate.ddl-auto", "create-drop" )
				.modules( EntityModule.NAME, DynamicFormsModule.NAME )
				.build()) {
			assertThat( context.findBeanOfTypeFromModule( DynamicFormsModule.NAME, DynamicDocumentDataMapperFactory.class ) ).isNotEmpty();
			assertThat( context.findBeanOfTypeFromModule( DynamicFormsModule.NAME, ReadDocumentDefinitionViewProcessor.class ) ).isEmpty();
		}
	}

	@Test
	public void bootstrapWithData() {
		try (AcrossTestContext context = web( true )
				.property( "acrossHibernate.hibernate.ddl-auto", "create-drop" )
				.modules( EntityModule.NAME, DynamicFormsModule.NAME )
				.build()) {
			assertThat( context.findBeanOfTypeFromModule( DynamicFormsModule.NAME, DynamicDocumentDataMapperFactory.class ) ).isNotEmpty();
			assertThat( context.findBeanOfTypeFromModule( DynamicFormsModule.NAME, ReadDocumentDefinitionViewProcessor.class ) ).isEmpty();

			DynamicDefinitionDataSetRepository dataSetRepository = context.getBeanOfType( DynamicDefinitionDataSetRepository.class );
			DynamicDefinitionDataSet dataSet = DynamicDefinitionDataSet.builder()
			                                                           .name( "My dataset" )
			                                                           .key( "my-dataset" )
			                                                           .build();
			assertThat( dataSet.isNew() ).isTrue();
			dataSetRepository.save( dataSet );
			assertThat( dataSet.isNew() ).isFalse();

			DynamicDefinitionTypeRepository typeRepository = context.getBeanOfType( DynamicDefinitionTypeRepository.class );
			DynamicDefinitionRepository definitionRepository = context.getBeanOfType( DynamicDefinitionRepository.class );
			DynamicDefinitionVersionRepository definitionVersionRepository = context.getBeanOfType( DynamicDefinitionVersionRepository.class );

			DynamicDefinition definition = DynamicDefinition.builder()
			                                                .name( "My definition" )
			                                                .key( "my-definition" )
			                                                .type( typeRepository.findByName( DynamicDefinitionTypes.DOCUMENT ).orElse( null ) )
			                                                .dataSet( dataSet )
			                                                .build();
			definitionRepository.save( definition );
			assertThat( dataSet.isNew() ).isFalse();

			DynamicDefinitionVersion version = DynamicDefinitionVersion.builder()
			                                                           .definition( definition )
			                                                           .version( "1" )
			                                                           .published( true )
			                                                           .definitionContent(
					                                                           "{\"document-definition\":{\"name\":\"Customer\",\"types\":[],\"content\":[{\"type\":\"string\",\"id\":\"address\"}]}}" )
			                                                           .build();
			definitionVersionRepository.save( version );
			assertThat( version.isNew() ).isFalse();

			DynamicDocumentService documentService = context.getBeanOfType( DynamicDocumentService.class );
			DynamicDocumentWorkspace workspace = documentService.createDocumentWorkspace( version );

			workspace.setDocumentName( "John Doe" );
			assertThat( workspace.getFields().getFieldAccessor( "address" ).getFieldDefinition().getEntityPropertyDescriptor() )
					.isNotNull()
					.hasFieldOrPropertyWithValue( "propertyType", String.class );
			workspace.setFieldValue( "address", "Sesamestreet 1" );
			DynamicDocumentVersion documentVersion = workspace.save();

			assertThat( documentVersion.isNew() ).isFalse();
			assertThat( documentVersion.getDocument().getName() ).isEqualTo( "John Doe" );
			assertThat( documentVersion.getDocumentContent() ).isEqualTo( "{\"address\":\"Sesamestreet 1\"}" );
		}
	}
}
