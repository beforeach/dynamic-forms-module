# Start docker compose

## Prerequisite 
- you are logged in as root or System Administrator

## All services
    docker-compose up

## One service
    docker-compose up servicename (web, test, mock)
   
# Start cypress
## Linux
    cd ./end2end-tests
    yarn
    ./node_modules/cypress/bin/cypress open --project ./tests
## Windows
    cd end2end-tests
    yarn
    node_modules\cypress\dist\cypress\Cypress.exe open --project ..\..\..\..\..\..\..\..\tests
    
