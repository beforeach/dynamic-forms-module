package com.foreach.across.samples.dynamicforms.application.extensions;

import com.foreach.across.core.annotations.ModuleConfiguration;
import com.foreach.across.modules.spring.security.SpringSecurityModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;

/**
 * A dummy Spring Security configuration that configures a single memory user that can access
 * the Administration UI (from AdminWebModule).  Check out the UserModule if you quickly want
 * to set up a simple user domain with groups, roles and permissions.
 */
@ModuleConfiguration(SpringSecurityModule.NAME)
@EnableGlobalAuthentication
public class AdminWebUserConfiguration
{
	@Autowired
	public void configureSingleFixedAdminUser( AuthenticationManagerBuilder auth ) throws Exception {
		auth.inMemoryAuthentication()
		    .withUser( "admin" ).password( "{noop}admin" )
		    .authorities( "access administration" );
	}
}