package com.foreach.across.samples.dynamicforms;

import com.foreach.across.config.AcrossApplication;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.dynamicforms.DynamicFormsModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.filemanager.FileManagerModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.properties.PropertiesModule;
import org.springframework.boot.SpringApplication;

import java.util.Collections;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@AcrossApplication(
		modules = { AdminWebModule.NAME,
		            EntityModule.NAME,
		            DynamicFormsModule.NAME,
		            AcrossHibernateJpaModule.NAME,
		            FileManagerModule.NAME,
		            PropertiesModule.NAME
		}
)
public class DynamicFormsTestApplication
{
	public static void main( String[] args ) {
		SpringApplication springApplication = new SpringApplication( DynamicFormsTestApplication.class );
		springApplication.setDefaultProperties(
				Collections.singletonMap( "spring.config.additional-location", "${user.home}/dev-configs/dfm-test-application.yml" ) );
		springApplication.run( args );
	}
}
