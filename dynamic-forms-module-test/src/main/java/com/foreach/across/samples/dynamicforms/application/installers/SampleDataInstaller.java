/*
 * Copyright 2018 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.foreach.across.samples.dynamicforms.application.installers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.core.installers.InstallerRunCondition;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSet;
import com.foreach.across.modules.dynamicforms.domain.dataset.DynamicDefinitionDataSetRepository;
import com.foreach.across.modules.dynamicforms.domain.definition.*;
import com.foreach.across.modules.dynamicforms.domain.definition.document.RawDocumentDefinition;
import com.foreach.across.modules.dynamicforms.domain.definition.view.RawViewDefinition;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.Transactional;
import org.yaml.snakeyaml.Yaml;

/**
 * @author Arne Vandamme
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Installer(description = "Installs sample documents and definitions", phase = InstallerPhase.AfterContextBootstrap, runCondition = InstallerRunCondition.AlwaysRun)
public class SampleDataInstaller
{
	private final DynamicDefinitionDataSetRepository dataSetRepository;
	private final DynamicDefinitionRepository definitionRepository;
	private final DynamicDefinitionVersionRepository versionRepository;
	private final DynamicDefinitionTypeRepository dataDefinitionTypeRepository;
	private final RawDefinitionService rawDefinitionService;

	@Transactional
	@InstallerMethod
	public void install() {
		DynamicDefinitionType documentType = dynamicDefinitionType( "document" );

		DynamicDefinitionDataSet exampleData = createDataSet( "Example data", "example-data" );
		createDynamicDefinition( documentType, exampleData, null, "simple-document", "installers/definition-document-simple.yml",
		                         "test document for basic types" );
		createDynamicDefinition( documentType, exampleData, null, "calculated-fields-1", "installers/calculated-fields-1.yml",
		                         "Some calculated fields with nested document version" );

		DynamicDefinitionDataSet jobDataSet = createDataSet( "Job data", "job-data" );
		createDynamicDefinition( documentType, jobDataSet, null, "job", "installers/definition-document-job.yml", "A job description" );
		createDynamicDefinition( documentType, jobDataSet, null, "interview", "installers/definition-document-interview.yml", "The interview of an applicant" );
		createDynamicDefinition( documentType, jobDataSet, null, "applicant", "installers/definition-document-applicant.yml", "An applicant for a job" );

		DynamicDefinitionType viewType = dynamicDefinitionType( "view" );
		createDynamicDefinition( viewType, jobDataSet, null, "nameListView", "installers/definition-view-name.yml", "Creates a list view of names" );

		DynamicDefinitionDataSet projects = createDataSet( "Projects", "projects" );
		createDynamicDefinition( documentType, projects, null, "Customer", "installers/projects/customer.yml", "Customer" );
		createDynamicDefinition( documentType, projects, null, "Technology", "installers/projects/technology.yml", "Technology" );
		DynamicDefinition projectDefinition = createDynamicDefinition( documentType, projects, null, "Project", "installers/projects/project.yml", "Project" );
		createDynamicDefinition( viewType, projects, projectDefinition, "listView", "installers/projects/project-listView.yml", "List view" );
		createDynamicDefinition( viewType, projects, projectDefinition, "listSummaryView", "installers/projects/project-listSummaryView.yml", "List summary view" );

		DynamicDefinitionDataSet configurations = createDataSet( "Test configurations", "configurations" );
		createDynamicDefinition( documentType, configurations, null, "Simple", "installers/configurations/simple-fields.yml", "" );
		createDynamicDefinition( documentType, configurations, null, "Fieldsets", "installers/configurations/fieldsets.yml", "" );
		createDynamicDefinition( documentType, configurations, null, "Collections", "installers/configurations/fieldset-collections.yml", "" );
	}

	private DynamicDefinitionType dynamicDefinitionType( String type ) {
		DynamicDefinitionType definitionType = dataDefinitionTypeRepository.findByName( type ).orElse( null );
		if ( definitionType == null ) {
			definitionType = dataDefinitionTypeRepository.save( DynamicDefinitionType.builder().name( type ).build() );
		}
		return definitionType;
	}

	private DynamicDefinitionDataSet createDataSet( String name, String key ) {
		DynamicDefinitionDataSet dataSet = dataSetRepository.findOneByName( name ).orElse( null );

		if ( dataSet == null ) {
			dataSet = new DynamicDefinitionDataSet();
			dataSet.setName( name );
			dataSet.setKey( key );
			dataSetRepository.save( dataSet );
		}
		return dataSet;
	}

	private DynamicDefinition createDynamicDefinition( DynamicDefinitionType type,
	                                                   DynamicDefinitionDataSet dataSet,
	                                                   DynamicDefinition parent,
	                                                   String dynamicDefinitionName,
	                                                   String path,
	                                                   String remark ) {
		DynamicDefinition dynamicDefinition = definitionRepository.findOneByDataSetAndParentAndName( dataSet, parent, dynamicDefinitionName ).orElse( null );

		if ( dynamicDefinition == null ) {
			RawDefinition definition = (RawDefinition) readDefinition( path, StringUtils
					.equals( DynamicDefinitionTypes.DOCUMENT, type.getName() ) ? RawDocumentDefinition.class : RawViewDefinition.class );

			dynamicDefinition = new DynamicDefinition();
			dynamicDefinition.setName( dynamicDefinitionName );
			dynamicDefinition.setType( type );
			dynamicDefinition.setKey( StringUtils.lowerCase( dynamicDefinitionName ) );
			dynamicDefinition.setDataSet( dataSet );
			dynamicDefinition.setParent( parent );
			definitionRepository.save( dynamicDefinition );

			DynamicDefinitionVersion definitionVersion = new DynamicDefinitionVersion();
			definitionVersion.setDefinition( dynamicDefinition );
			definitionVersion.setPublished( true );
			definitionVersion.setRemarks( remark );
			definitionVersion.setVersion( "1" );
			definitionVersion.setDefinitionContent( rawDefinitionService.writeDefinition( definition ) );
			versionRepository.save( definitionVersion );

			dynamicDefinition.setLatestVersion( definitionVersion );
			return definitionRepository.save( dynamicDefinition );
		}
		return dynamicDefinition;
	}

	@SneakyThrows
	@SuppressWarnings("unchecked")
	private <T> Object readDefinition( String path, Class clazz ) {
		Yaml yaml = new Yaml();
		Object raw = yaml.load( new ClassPathResource( path ).getInputStream() );

		ObjectMapper objectMapper = new ObjectMapper();
		String convertedJson = objectMapper.writeValueAsString( raw );

		return rawDefinitionService.readRawDefinition( convertedJson, clazz );
	}
}
